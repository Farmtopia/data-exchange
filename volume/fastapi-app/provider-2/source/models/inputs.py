import time;
import datetime;
from datetime import timedelta;
from typing import Optional , List;
from enum import Enum;
from pydantic import BaseModel , Field , create_model;

class get_user_access( BaseModel ):
				id     : Optional[ int ] = Field( None , description = "" );
				limit  : Optional[ int ] = Field( None , description = "" );
				offset : Optional[ int ] = Field( None , description = "" );
				email : Optional[ str ] = Field( None , description = "" );


class user_access( BaseModel ):
				id         : Optional[ int ]             = Field( None , description = "" );
				created_on : Optional[ datetime.datetime ] = Field( None , description = "" );
				email : Optional[ str ] = Field( None , description = "" );
				last_login : Optional[ datetime.datetime ] = Field( None , description = "" );
				has_access : Optional[ int ] = Field( None , description = "" );


class put_user_access( BaseModel ):
				email : Optional[ str ] = Field( None , description = "" );
				last_login : Optional[ datetime.datetime ] = Field( None , description = "" );
				has_access : Optional[ int ] = Field( None , description = "" );


class get_map_user_to_dataset( BaseModel ):
				id     : Optional[ int ] = Field( None , description = "" );
				limit  : Optional[ int ] = Field( None , description = "" );
				offset : Optional[ int ] = Field( None , description = "" );
				user_id : Optional[ int ] = Field( None , description = "" );


class map_user_to_dataset( BaseModel ):
				id         : Optional[ int ]             = Field( None , description = "" );
				created_on : Optional[ datetime.datetime ] = Field( None , description = "" );
				user_id : Optional[ int ] = Field( None , description = "" );
				dataset_id : Optional[ int ] = Field( None , description = "" );
				is_active : Optional[ int ] = Field( None , description = "" );
				token : Optional[ str ] = Field( None , description = "" );
				expires_on : Optional[ datetime.datetime ] = Field( None , description = "" );


class put_map_user_to_dataset( BaseModel ):
				user_id : Optional[ int ] = Field( None , description = "" );
				dataset_id : Optional[ int ] = Field( None , description = "" );
				is_active : Optional[ int ] = Field( None , description = "" );
				token : Optional[ str ] = Field( None , description = "" );
				expires_on : Optional[ datetime.datetime ] = Field( None , description = "" );


class get_apikey( BaseModel ):
				id     : Optional[ int ] = Field( None , description = "" );
				limit  : Optional[ int ] = Field( None , description = "" );
				offset : Optional[ int ] = Field( None , description = "" );
				apikey : Optional[ str ] = Field( None , description = "" );


class apikey( BaseModel ):
				id         : Optional[ int ]             = Field( None , description = "" );
				created_on : Optional[ datetime.datetime ] = Field( None , description = "" );
				apikey : Optional[ str ] = Field( None , description = "" );


class put_apikey( BaseModel ):
				apikey : Optional[ str ] = Field( None , description = "" );


