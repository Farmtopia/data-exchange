# uvicorn main:app --port 8686 --host 192.168.1.108 --reload --root-path /
from   inspect import currentframe, getframeinfo;
from   typing  import Optional , List, Any , ClassVar , Annotated;
from   fastapi import Depends;
from   fastapi import FastAPI;
from   fastapi import Form;
from   fastapi import Request;
from   fastapi import HTTPException;
from   fastapi import status;
from   fastapi import Query;
from   fastapi import Path;
from   fastapi.openapi.utils import get_openapi;
from   enum     import Enum;
from   pydantic import BaseModel , Field , create_model , StringConstraints;
import time;
import datetime;
from   datetime import timedelta;
import os;
import os.path;
import array;
from decimal import *;
import base64;
import aiohttp;
import aiofiles;
import asyncio;
import json as JSON;
import sys;
from jose import JWTError, ExpiredSignatureError , jwt;
import psycopg2;
import models.models            as     ResModel;
import models.inputs            as     Inputs;
import models.SensorModel       as     SensorModel;
import models.CalendarModel     as     CalendarModel;
from   cfg.tags                 import Tags;
from   classes.amModel          import amModel;
from   classes.amTools          import amTool;
from   classes.amData           import amData;
from   classes.gaiasense_api    import convert_to_jsonld;

# security         = HTTPBasic();
amTool           = amTool();
cfg              = amTool.load( "./cfg/" , "config" );
amTool.cfg       = cfg;
myModel          = amModel( cfg );
myData           = amData( cfg );
myTags           = Tags();
endpoint_tags    = myTags.list;
DBsourceFileName = "schema";
debug            = False;

app = FastAPI( 
              title        = cfg["service"]["title"],
              description  = cfg["service"]["description"],
              version      = cfg["service"]["version"],
              openapi_tags = endpoint_tags , 
              redoc_url    = None , 
              docs_url     = "/" + cfg["service"]["docs_url"]
      );



# Administration EndPoints for initialization
@app.get( "/administration/create_db_tables", 
          tags         = [ "Administration" ] , 
          summary      = "Create basic Database Tables",
          description  = "Create the initial tables required for inserting user Data. This is an action that only needs to be performed once at the initial installation" 
        ) 
async def createDBTables():
    # Reads the SourceFile JSON and creates one Table for each entity defined in there.
    mySchema = amTool.cfg[ "database" ][ "schema" ];

    try:
        myInputsJSON = amTool.load( "" , DBsourceFileName , "json" );
        myWholeQuery = [];
        mySchema     = amTool.cfg[ "database" ][ "schema" ];

        myForeignKeys = [];
        for entity in myInputsJSON[ "inputs" ] :
            myUniques     = [];
            myComposites  = [];
            myQuery  = "DROP TABLE IF EXISTS " + str( mySchema ) + "." + str( entity ).lower() + ";";
            myQuery += "CREATE TABLE " + str( mySchema ) + "." + str( entity ).lower() + " ( ";
            myTempQuery = [];
            myTempQuery.append( "id serial NOT NULL" );
            myTempQuery.append( "created_on timestamp NOT NULL DEFAULT now()" );
            for item in myInputsJSON[ "inputs" ][ entity ]:
                defaultClause = "";
                defaultValue  = "";
                if( "default" in item and item[ "default" ] != "false" ) :
                    if( str( item[ "type" ] ) != "integer" and str( item[ "type" ] ) != "numeric" ):
                        defaultValue = "('" + str( item[ "default" ] ) + "'::" + str( item[ "type" ] ) + ")";
                    else:
                        defaultValue = "("  + str( item[ "default" ] ) + "::"  + str( item[ "type" ] ) + ")";

                    defaultClause = " NOT NULL DEFAULT (" + defaultValue + ")";    
                else:
                    defaultClause = " NULL ";

                myTempQuery.append( str( item[ "name" ] ) + " " + str( item[ "type" ] ) + str( defaultClause ) );

                if "unique" in item and item[ "unique" ] == "true" :
                    myUniques.append( "CONSTRAINT " + str( item[ "name" ] ) + "_unique UNIQUE ( " + str( item[ "name" ] ) + ")" );

                if "composite" in item and item[ "composite" ] == "true" :
                    myComposites.append( item[ "name" ] );

                if "fkey" in item :
                    myPair        = item[ "fkey" ].split( "." );
                    mySourceTable  = myPair[ 0 ];
                    mySourceColumn = myPair[ 1 ];
                    myTargetTable  = entity;
                    myTargetColumn = item[ "name" ];
                    myForeignKeys.append(
                                 "ALTER TABLE " + 
                                 "" + str( mySchema ) + "." + str( myTargetTable ) + " ADD CONSTRAINT fk_" + str( myTargetColumn ) + " " + 
                                 "FOREIGN KEY (" + str( myTargetColumn ) + ") REFERENCES " + 
                                 "" + str( mySchema ) + "." + str( mySourceTable ) + "(" + str( mySourceColumn ) + ") ON DELETE CASCADE;"
                                );

            myQuery += ",\r\n".join( myTempQuery );
            myQuery += ",PRIMARY KEY (id) ";
            if( len( myUniques ) > 0 ) : 
                myQuery += "," + ",\r\n".join( myUniques );
            myQuery += ");";
            myWholeQuery.append( myQuery );

            if( len( myComposites ) > 0 ) : 
                myWholeQuery.append( "CREATE UNIQUE INDEX " + str( entity ) + "_multi on " + str( mySchema ) + "." + str( entity ).lower() + " ( " + ",".join( myComposites ) + ")" );

        myQueryWithForeignKeys = myWholeQuery + myForeignKeys;
        for aQuery in myQueryWithForeignKeys:
            amTool.log( "------------------------" );
            amTool.log( aQuery );
            myModel.executeQuery( aQuery , None , "update" );

    except( Exception ) as error :
        print( error );

@app.get( "/administration/create_input_models", 
          tags         = [ "Administration" ] , 
          summary      = "Create our Input BaseModels",
          description  = "Create our Input BaseModels" 
        ) 
async def createInputModels():
  # Loads the SourceFile JSON and creates our BaseModels that will populatee the Inputs for each EndPoint created
    targetFileName = "models/inputs.py";
    myFile         = [];

    try:
        myInputsJSON = amTool.load( "" , DBsourceFileName , "json" );
        amTool.log( "File has been opened" );
        amTool.log( myInputsJSON );

        f = open( targetFileName, "w" );
        f.write( "import time;\r\n" );
        f.write( "import datetime;\r\n" );
        f.write( "from datetime import timedelta;\r\n" );
        f.write( "from typing import Optional , List;\r\n" );
        f.write( "from enum import Enum;\r\n" );
        f.write( "from pydantic import BaseModel , Field , create_model;\r\n" );
        f.write( "\r\n" );

        for entity in myInputsJSON[ "inputs" ] :
            myEntityClass    = [];
            myEntityClass.append( "class " + entity + "( BaseModel ):\r\n" );
            myEntityClass.append( "\t\t\t\tid         : Optional[ int ]             = Field( None , description = \"\" );\r\n" );
            myEntityClass.append( "\t\t\t\tcreated_on : Optional[ datetime.datetime ] = Field( None , description = \"\" );\r\n" );

            myEntityClassPut = [];
            myEntityClassPut.append( "class put_" + entity + "( BaseModel ):\r\n" );

            myEntityClassGet = [];
            myEntityClassGet.append( "class get_" + entity + "( BaseModel ):\r\n" );
            myEntityClassGet.append( "\t\t\t\tid     : Optional[ int ] = Field( None , description = \"\" );\r\n" );
            myEntityClassGet.append( "\t\t\t\tlimit  : Optional[ int ] = Field( None , description = \"\" );\r\n" );
            myEntityClassGet.append( "\t\t\t\toffset : Optional[ int ] = Field( None , description = \"\" );\r\n" );

            for item in myInputsJSON[ "inputs" ][ entity ] :

                myType = "str";
                if str( item[ "type" ] ) == "numeric" : 
                    myType = "float";
                elif str( item[ "type" ] ) == "varchar" : 
                    myType = "str";
                elif str( item[ "type" ] ) == "timestamp" : 
                    myType = "datetime.datetime";
                else:
                    myType = "int";

                if item[ "filter" ] == "true" and myType == "datetime.datetime" : 
                    myEntityClassGet.append( "\t\t\t\t" + str( item[ "name" ] ).lower() + "_from_date : Optional[ datetime.datetime ] = Field( None , description = \"\" );\r\n" );
                    myEntityClassGet.append( "\t\t\t\t" + str( item[ "name" ] ).lower() + "_to_date   : Optional[ datetime.datetime ] = Field( None , description = \"\" );\r\n" );
                    myEntityClass.append( "\t\t\t\t" + str( item[ "name" ] ).lower() + " : Optional[ datetime.datetime ] = Field( None , description = \"\" );\r\n" );
                else:
                    myEntityClass.append( 
                       (
                         "\t\t\t\t" + 
                             str( item[ "name" ] ).lower() + " : " + 
                             "Optional[ " + str( myType ) + " ] " + 
                             "= " + 
                             "Field( None , description = \"" + str( item[ "desc" ] )  + "\" );\r\n" 
                       )
                    );

                myEntityClassPut.append( 
                   (
                     "\t\t\t\t" + 
                         str( item[ "name" ] ).lower() + " : " + 
                         "Optional[ " + str( myType ) + " ] " + 
                         "= " + 
                         "Field( None , description = \"" + str( item[ "desc" ] )  + "\" );\r\n" 
                   )
                );

                if item[ "filter" ] == "true" :
                    myEntityClassGet.append( 
                       ( 
                         "\t\t\t\t" + 
                             str( item[ "name" ] ).lower() + " : " + 
                             "Optional[ " + str( myType ) + " ] " + 
                             "= " + 
                             "Field( None , description = \"" + str( item[ "desc" ] )  + "\" );\r\n" 
                       )
                    );

            myEntityClassGet.append( "\r\n\r\n" );
            myFile.append( "".join( myEntityClassGet ) );

            myEntityClass.append( "\r\n\r\n" );
            myFile.append( "".join( myEntityClass ) );

            myEntityClassPut.append( "\r\n\r\n" );
            myFile.append( "".join( myEntityClassPut ) );

        for row in myFile : 
            f.writelines( row );

        f.close();

    except( Exception ) as error :
        print( error );

@app.middleware("http")
async def add_process_time_header( request: Request, call_next ):
    startTime = time.time();
    print();
    print( "-------------------------------------------------------" );
    amTool.log( "[ INCOMING REQUEST : START ] : \"" + str( request.method ) + "\"" + " " + str( request.url ) );
    # amTool.log( "[ HEADERS ] " + JSON.dumps( request.headers ) );
    response  = await call_next( request );
    endTime   = time.time();
    totalTime = endTime - startTime;
    response.headers[ "X-Process-Time" ] = str( totalTime );
    amTool.log( "[ RESPONSE : ] " , response );
    amTool.log( "[ INCOMING REQUEST : END   ] : \"" + str( request.method ) + "\"" + " " + str( request.url ) + " (Code:" + str( response.status_code ) + ")" + " [Duration : " + str( round( totalTime ,3 ) ) + "]" );
    print( "-------------------------------------------------------" );
    print();
    return response;



# Helpers
def decodeJWTFromHeader( request: Request  ):

    amTool.log( "[ decodeJWTFromHeader Verification : Step 1 ] : Checking if needed Headers are available in Request" );
    if "authorization" in request.headers:
        requestJWT      = request.headers[ "authorization" ].split();
    else:
        amTool.log( "[ Verification - ERROR : Step 1 ] : Incorrect Headers. Token Missing" );
        raise HTTPException( 
            status_code = status.HTTP_400_BAD_REQUEST, 
            detail      = "Incorrect Headers. Token Missing.",
            headers     = { "WWW-Authenticate": "Bearer" }
        );

    if len( requestJWT ) != 2 : 
        amTool.log( "[ Verification - ERROR : Step 1 ] : Incorrect Headers" );
        raise HTTPException( 
            status_code = status.HTTP_400_BAD_REQUEST, 
            detail      = "Incorrect Headers",
            headers     = { "WWW-Authenticate": "Bearer" }
        );
    
    print( requestJWT[ 1 ] );

    try:
        payload = jwt.decode( 
            token      = requestJWT[ 1 ], 
            key        = None,
            options    = { "verify_signature": False }
        );

        return { 
          "payload" : payload,
          "jwt"     : requestJWT[ 1 ]
        };
    except ExpiredSignatureError :
        return { "error" : "Expired Signature" };
    except JWTError:
        return { "error" : "Invalid Signature" };

async def isDirectoryAuthorized( request: Request ):

    credentials_exception = HTTPException(
        status_code = status.HTTP_401_UNAUTHORIZED,
        detail      = "Could not validate credentials",
        headers     = { "WWW-Authenticate": "Bearer" }
    );

    amTool.log( "[ Verification : Step 1 ] : Checking if needed Headers are available in Request" );
    if "authorization" in request.headers:
        requestJWT      = request.headers[ "authorization" ].split();
    else:
        amTool.log( "[ Verification - ERROR : Step 1 ] : Incorrect Headers. Token Missing" );
        raise HTTPException( 
            status_code = status.HTTP_400_BAD_REQUEST, 
            detail      = "Incorrect Headers. Token Missing.",
            headers     = { "WWW-Authenticate": "Bearer" }
        );

    if len( requestJWT ) != 2 : 
        amTool.log( "[ Verification - ERROR : Step 1 ] : Incorrect Headers" );
        raise HTTPException( 
            status_code = status.HTTP_400_BAD_REQUEST, 
            detail      = "Incorrect Headers",
            headers     = { "WWW-Authenticate": "Bearer" }
        );

    amTool.log( "[ Verification ] : Retrieving ApiKey from Database" );
    apiKeyRecord = await myModel.getEntityAllProperties(
        params = Inputs.get_apikey(
            id     = 1 , 
            limit  = None , 
            offset = None 
        ) , 
        entity = "apikey" 
    );

    if "error" in apiKeyRecord : # return
        amTool.log( "[ ERROR - Verification ] : Api Key failed to be retrieved " , apiKeyRecord[ "error" ] );
        raise HTTPException( 
          status_code  = status.HTTP_400_BAD_REQUEST , 
          detail       = "Could not Retrieve API Key from Database" ,
          headers      = { "WWW-Authenticate": "Bearer" } 
        );

    if len( apiKeyRecord ) <= 0 : # return
        amTool.log( "[ ERROR - Verification ] : No Api Key in Database" );
        raise HTTPException( 
          status_code  = status.HTTP_400_BAD_REQUEST ,
          detail       = "No Api Key present in the Database" ,
          headers      = { "WWW-Authenticate": "Bearer" } 
        );

    serverToken  = apiKeyRecord[ 0 ][ "apikey" ];
    amTool.log( "[ Verification ] : Api Key sucesfully retrieved : \r\n'" + str( serverToken ) + "'" );
    amTool.log( "JWT : " + requestJWT[ 1 ] );

    try:
        payload = jwt.decode( 
            token      = requestJWT[ 1 ], 
            key        = serverToken, 
            algorithms = [ "HS256" ] 
        );
        username: str = payload.get( "sub" );
        # if username is None or username != "farmtopia_directory_listing" :
        if username is None :
            raise credentials_exception;

        amTool.log( "[ Verification ] : JWT Succesfully Decoded with the use of the Pre Shared Secret Key. It is properly signed from Directory Listing." );

    except JWTError as e:
        amTool.log( "[ ERROR - JWT ] : " + str( e ) );
        print( repr( e ) );
        raise credentials_exception;

    return payload;

async def getUser( userEmail ):
    # amTool.log( "[ PROVIDER : STEP 2 ] : Check if Consumer '" + str( message.get( "consumer" ) ) + "' has user record in Database" );
    userExists = await myModel.getEntityAllProperties(
        params = Inputs.get_user_access(
            email  = userEmail ,  
            limit  = None , 
            offset = None 
        ) , 
        entity = "user_access" 
    );
    amTool.log( "User Retrieved" , userExists );

    if len( userExists ) <= 0 : 
        return False;
    else: 
        amTool.log( "User Already Exists with Id : " + str( userExists[ 0 ][ "id" ] ) , debug = cfg[ "settings" ][ "debug" ] );
        return userExists[ 0 ];

async def addUser( message ):
    amTool.log( "[ PROVIDER : STEP 1 ] : Adding '" + str( message.get( "consumer" ) ) + "' in Database" );
    myResponse = await myModel.addEntity( 
                    request = [ Inputs.put_user_access(
                        email      = message.get( "consumer" ) , 
                        last_login = datetime.datetime.now().strftime( "%Y-%m-%d %H:%M:%S" ) , 
                        has_access = -1 
                    ) ] , 
                    type    = None , 
                    entity  = "user_access" 
                );
    amTool.log( "User Added" , myResponse );

    if "error" in myResponse :
        return False;
    else: 
        return await myModel.getEntityAllProperties(
            params = Inputs.get_user_access(
                id = myResponse[ "id" ]
            ) , 
            entity = "user_access" 
        );

async def _addUser( message ):
    amTool.log( "[ PROVIDER : STEP 1 ] : Adding '" + str( message.get( "consumer" ) ) + "' in Database" );
    response = await myModel.addUser( 
        email      = message.get( "consumer" ) , 
        last_login = datetime.datetime.now().strftime( "%Y-%m-%d %H:%M:%S" ) , 
        has_access = -1 
    );

    print( response );

    if "error" in myResponse :
        return False;
    else: 
        return True;

# Administrative EndPoints for the Directory
@app.put( "/directory/subscription/dataset_id/{dataset_id}", 
          tags         = [ "Administration" ] , 
          summary      = "Save a Subscription request for a user , coming from the Directory",
          description  = "Save a Subscription request for a user , coming from the Directory" 
        )
async def subscribeUser(
              request    : Request ,
              dataset_id : int , 
              message    = Depends( isDirectoryAuthorized )
          ):
    try:
        amTool.log( "Decoded Message" , message );
        amTool.log( "[ PROVIDER : STEP 1 ] : Authentication Success for '" + message.get( "sub" ) + "'" );
        amTool.log( "[ PROVIDER : STEP 1.1 ] : User '" + str( message.get( "consumer" ) ) + "' wants to Subscribe to Dataset with id : '" + str( dataset_id ) + "' " );

        # Check if User already exists in our Database.If not then simply add the user.
        userObject = await getUser( message.get( "consumer" ) );
        if userObject is False : 
            temp       = await addUser( message );

        amTool.log( "[ PROVIDER ] : Finalized returning response to Directory" );

        # Any 200 Status Code response will be treated as success. Response message is irrelevant.
        return { "success" : "ok" }

    except( Exception ) as error :
        amTool.log( "[ ERROR - subscribeUser ] : " , error );
        if isinstance( error , HTTPException ) :
            raise HTTPException(
              status_code = error.status_code, 
              detail      = error.detail
            );
        elif isinstance( error , IndexError ):
            raise HTTPException(
              status_code = 401, 
              detail      = "Wrong DataSet ID"
            );
        else:
            raise HTTPException(
              status_code = 500, 
              detail      = "Issue with Server , check Logs"
            );

@app.put( "/apikey", 
          tags         = [ "Administration" ] , 
          summary      = "Add Apikey",
          description  = "Add Apikey" 
        )
async def addApiKey(
              request : List[ Inputs.put_apikey  ]
          ):
    try:
        amTool.log( "[ PROVIDER ] : Adding ApiKey in Database " );
        myResponse = await myModel.addEntity( request , type=None , entity="apikey" );
        amTool.log( "Apikey Added" , myResponse );
        return { "res" : myResponse }

    except( Exception ) as error :
        amTool.log( "[ ERROR - subscribeUser ] : " , error );
        if isinstance( error , HTTPException ) :
            raise HTTPException(
              status_code = error.status_code, 
              detail      = error.detail
            );
        elif isinstance( error , IndexError ):
            raise HTTPException(
              status_code = 401, 
              detail      = "Wrong Resource"
            );
        else:
            raise HTTPException(
              status_code = 500, 
              detail      = "Issue with Server , check Logs"
            );

@app.post( "/apikey", 
          tags         = [ "Administration" ] , 
          summary      = "Update Apikey",
          description  = "Update Apikey" 
        )
async def updateApiKey(
              request : Inputs.apikey
          ):
    try:
        amTool.log( "[ PROVIDER ] : Adding ApiKey in Database " );
        myResponse = await myModel.updateEntity( request=request , entity="apikey" );
        amTool.log( "Apikey Updated" , myResponse );
        return { "res" : myResponse }

    except( Exception ) as error :
        amTool.log( "[ ERROR - subscribeUser ] : " , error );
        if isinstance( error , HTTPException ) :
            raise HTTPException(
              status_code = error.status_code, 
              detail      = error.detail
            );
        elif isinstance( error , IndexError ):
            raise HTTPException(
              status_code = 401, 
              detail      = "Wrong Resource"
            );
        else:
            raise HTTPException(
              status_code = 500, 
              detail      = "Issue with Server , check Logs"
            );



# Data Responses
@app.get( "/", 
          tags           = [ "JSON-LD" ] , 
          summary        = "Test for JSON-LD",
          description    = "Test for JSON-LD" 
        )
async def getData():
    try:
        EndPoint    = "http://sense-web.neuropublic.gr:8383/";
        parameters  = "locations/measurements/wsht30_temp?limit=10&fromdate=2024-08-01&todate=2024-08-02&locationid=5&radius=100&offset=0&orderby=asc";
        async with aiohttp.ClientSession() as session:
            async with session.get( 
                          url     = EndPoint + parameters , 
                          auth    = aiohttp.BasicAuth( "admin" , "123456" ) , 
                          headers = { 
                              'content-type'  : 'application/json' , 
                              'accept'        : 'application/json' 
                          }
                       ) as response:

                 myText = await response.text();

                 try:
                     myJSON = JSON.loads( myText );

                     try:
                         parsedToJSONLD = convert_to_jsonld( myText );
                         return JSON.loads( parsedToJSONLD );
                     except( Exception ) as error  :
                         print( error );
                         return { "error" : "Failed to Parse JSON to JSONLD" }

                 except ValueError as error :
                     print( error );
                     return { "error" : "There was an issue retrieving data." };

    except( Exception , HTTPException , ValidationError ) as error :
        print( error );
        return [];
