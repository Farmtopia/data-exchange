import json
from datetime import datetime, timezone

def add_observation_collection(sensor_id, property_name, unit, measurements, value_key):
    observation_collection = {
        "@id": f"{property_name}ObservationCollection_1",
        "@type": "fsm:DataPointsGroup",
        "property": property_name,
        "sourceSensor": {
            "@id": sensor_id,
            "@type": "sosa:Sensor"
        },
        "unitOfMeasure": unit,
        "contents": []
    }
    
    for i, measurement in enumerate(measurements):
        if value_key in measurement:
            timestamp = datetime.fromtimestamp(measurement['m_date'], tz=timezone.utc).isoformat()
            observation = {
                "@id": f"{property_name}Observation{i+1}",
                "@type": "fsm:Observation",
                "dateTime": timestamp,
                "value": str(measurement[value_key])
            }
            observation_collection['contents'].append(observation)
    
    return observation_collection

def convert_to_jsonld(input_json):
    print(  "reached" );
    data = json.loads(input_json)

    jsonld_output = {
        "@context": {
            "fsm": "https://gitlab.com/Farmtopia/farmtopia-semantic-model/-/raw/main/FarmtopiaSemanticModel.rdf#",
            "unit": "http://qudt.org/2.1/vocab/unit",
            "sosa": "http://www.w3.org/ns/sosa/",
            "geosparql": "http://www.opengis.net/ont/geosparql/",
            "cf": "http://purl.oclc.org/NET/ssnx/cf/cf-property#",
            "datasetSlices": {
                "@id": "fsm:datasetSlices",
                "@type": "@id",
                "@container": "@set"
            },
            "sliceContents": {
                "@id": "fsm:sliceContents",
                "@type": "@id",
                "@container": "@set"
            },
            "label": "fsm:label",
            "toponym": "fsm:toponym",
            "geometry": {
                "@id": "geosparql:hasGeometry",
                "@type": "@id"
            },
            "geoJson": {
                "@id": "geosparql:asGeoJSON",
                "@type": "@json"
            },
            "location": {
                "@id": "fsm:location",
                "@type": "@id"
            },
            "layer": {
                "@id": "fsm:layer",
                "@type": "@id"
            },
            "Atmosphere": {
                "@id": "cf:atmosphere"
            },
            "property": {
                "@id": "sosa:property",
                "@type": "@id"
            },
            "AirTemperature": {
                "@id": "fsm:air_temperature"
            },
            "sourceSensor": {
                "@id": "sosa:madeBySensor",
                "@type": "@id"
            },
            "unitOfMeasure": {
                "@id": "fsm:unitOfMeasure",
            },
            "Celsius": {
                "@id": "unit:DEG_C"
            },
            "RelativeHumidity": {
                "@id": "fsm:relative_humidity"
            },
            "Percent": {
                "@id": "unit:PERCENT"
            },
            "Precipitation": {
                "@id": "fsm:presipitation"
            },
            "Millimetre": {
                "@id": "unit:MilliM"
            },
            "WindSpeed": {
                "@id": "fsm:wind_speed"
            },
            "WindDirection": {
                "@id": "fsm:wind_direction"
            },
            "AirPressure": {
                "@id": "fsm:air_pressure"
            },
            "Evapotranspiration": {
                "@id": "fsm:evapotranspiration"
            },
            "contents": {
                "@id": "sosa:hasMember",
                "@type": "@id",
                "@container": "@set"
            },
            "dateTime": "sosa:resultTime",
            "value": "sosa:hasSimpleResult",
            "Surface": {
                "@id": "cf:surface"
            },
            "SolarIrradiance": {
                "@id": "fsm:solar_irradiance"
            },
            "UltravioletIrradiance": {
                "@id": "fsm:ultraviolet_irradiance"
            },
            "KilometerPerHour": {
                "@id": "unit:KiloM-PER-HR"
            },
            "Millibar": {
                "@id": "unit:MilliBAR"
            },
            "Volt": {
                "@id": "unit:V"
            },
            "Soil": {
                "@id": "cf:soil_layer"
            },
            "SoilTemperature": {
                "@id": "fsm:soil_temperature"
            },
            "SoilMoisture": {
                "@id": "fsm:soil_moisture"
            },
            "SoilSalinity": {
                "@id": "fsm:soil_salinity"
            },
            "Vegetation": {
                "@id": "cf:vegetation"
            },
            "LeafWetness": {
                "@id": "fsm:leaf_wetness"
            },
            "LeafTemperature": {
                "@id": "fsm:leaf_temperature"
            }
        },
        "@graph": [{
            "@id": "NP_gaiasense_serviceResource_representation_instance_1",
            "@type": "fsm:Dataset",
            "datasetSlices": []
        }]
    }

    # Add location slice
    location_slice = {
        "@id": "locationsSlice",
        "@type": "fsm:Slice",
        "sliceContents": [{
            "@id": str(data['location']['id']),
            "@type": "fsm:PointOfInterest",
            "label": data['location']['name'],
            "toponym": data['location']['toponym'],
            "geometry": {
                "@id": f"geometry{data['location']['id']}",
                "@type": "geometry",
                "geoJson": {
                    "type": "Point",
                    "coordinates": [
                        str(data['location']['long']),
                        str(data['location']['lat'])
                    ]
                }
            }
        }]
    }
    jsonld_output["@graph"][0]["datasetSlices"].append(location_slice)

    # Add measurements slice
    measurements_slice = {
        "@id": "measurementsSlice",
        "@type": "fsm:Slice",
        "sliceContents": [{
            "@id": "WeatherStation_1Observations",
            "@type": "fsm:DataPointsGroup",
            "location": {"@id": str(data['location']['id'])},
            "sliceContents": [{
                "@id": "AtmosphereObservationCollection_1",
                "@type": "fsm:DataPointsGroup",
                "layer": "Atmosphere",
                "sliceContents": []
            }]
        }]
    }

    # List of all possible measurements
    measurement_types = [
        # AIR Temperature Sensors
        ("nplwtemp1", "AirTemperature", "Celsius", "nplwtemp1"),
        ("nplwtemp2", "AirTemperature", "Celsius", "nplwtemp2"),
        ("wsht30_temp", "AirTemperature", "Celsius", "wsht30_temp"),
        # Relative Humidity Sensors
        ("nplwrh1", "RelativeHumidity", "Percent", "nplwrh1"),
        ("nplwrh2", "RelativeHumidity", "Percent", "nplwrh2"),
        ("wsht30_rh", "RelativeHumidity", "Percent", "wsht30_rh"),
        # Precipitation Sensors
        ("rain", "Precipitation", "Millimetre", "rain"),
        # Wind Speed Sensors
        ("velocity", "WindSpeed", "KilometerPerHour", "velocity"),
        # Air Pressure Sensors
        ("pressure", "AirPressure", "Millibar", "pressure"),
        # Solar Radiation Sensors
        ("ext_copernicus", "Evapotranspiration", "Millimetre", "ext_copernicus"),
        # Voltage Sensors
        ("pyranometer", "SolarIrradiance", "Volt", "pyranometer"),
        # Soil Temperature Sensors
        ("enviro_sdi12_temp_10", "SoilTemperature", "Celsius", "enviro_sdi12_temp_10"),
        ("enviro_sdi12_temp_20", "SoilTemperature", "Celsius", "enviro_sdi12_temp_20"),
        ("enviro_sdi12_temp_30", "SoilTemperature", "Celsius", "enviro_sdi12_temp_30"),
        ("enviro_sdi12_temp_40", "SoilTemperature", "Celsius", "enviro_sdi12_temp_40"),
        ("enviro_sdi12_temp_50", "SoilTemperature", "Celsius", "enviro_sdi12_temp_50"),
        ("enviro_sdi12_temp_60", "SoilTemperature", "Celsius", "enviro_sdi12_temp_60"),
        ("enviro_sdi12_temp_70", "SoilTemperature", "Celsius", "enviro_sdi12_temp_70"),
        # Soil Moisture Sensors
        ("enviro_sdi12_sm_10", "SoilMoisture", "Percent", "enviro_sdi12_sm_10"),
        ("enviro_sdi12_sm_20", "SoilMoisture", "Percent", "enviro_sdi12_sm_20"),
        ("enviro_sdi12_sm_30", "SoilMoisture", "Percent", "enviro_sdi12_sm_30"),
        ("enviro_sdi12_sm_40", "SoilMoisture", "Percent", "enviro_sdi12_sm_40"),
        ("enviro_sdi12_sm_50", "SoilMoisture", "Percent", "enviro_sdi12_sm_50"),
        ("enviro_sdi12_sm_60", "SoilMoisture", "Percent", "enviro_sdi12_sm_60"),
        ("enviro_sdi12_sm_70", "SoilMoisture", "Percent", "enviro_sdi12_sm_70"),
        # Soil Salinity Sensors
        ("enviro_sdi12_sal_10", "SoilSalinity", "Siemens", "enviro_sdi12_sal_10"),
        ("enviro_sdi12_sal_20", "SoilSalinity", "Siemens", "enviro_sdi12_sal_20"),
        ("enviro_sdi12_sal_30", "SoilSalinity", "Siemens", "enviro_sdi12_sal_30"),
        ("enviro_sdi12_sal_40", "SoilSalinity", "Siemens", "enviro_sdi12_sal_40"),
        ("enviro_sdi12_sal_50", "SoilSalinity", "Siemens", "enviro_sdi12_sal_50"),
        ("enviro_sdi12_sal_60", "SoilSalinity", "Siemens", "enviro_sdi12_sal_60"),
        ("enviro_sdi12_sal_70", "SoilSalinity", "Siemens", "enviro_sdi12_sal_70"),
        # Leaf Wetness Sensors
        ("nplwlw1", "LeafWetness", "Percent", "nplwlw1"),
        ("nplwlw2", "LeafWetness", "Percent", "nplwlw2"),
        ("vr_leafw_norm", "LeafWetness", "Percent", "vr_leafw_norm"),
        # Leaf Temperature Sensors
        ("nplwtemp1", "LeafTemperature", "Celsius", "nplwtemp1"),
        ("nplwtemp2", "LeafTemperature", "Celsius", "nplwtemp2")
    ]

    # Process different types of observations
    for sensor_id, property_name, unit, value_key in measurement_types:
        if any(value_key in measurement for measurement in data['measurements']):
            observation_collection = add_observation_collection(
                sensor_id=sensor_id,
                property_name=property_name,
                unit=unit,
                measurements=data['measurements'],
                value_key=value_key
            )
            measurements_slice["sliceContents"][0]["sliceContents"][0]["sliceContents"].append(observation_collection)

    jsonld_output["@graph"][0]["datasetSlices"].append(measurements_slice)

    return json.dumps(jsonld_output, indent=2, ensure_ascii = False)