class Tags:
    def __init__( self ):
        """Class for Tags"""

        self.list = [
            {
                "name"        : "Service Initialization",
                "description" : "Services that need to be executed when installing the Service.",
            },
            {
                "name"        : "User Management",
                "description" : "Register User , create Secret Keys , create Tokens , etc.",
            },
            {
                "name"        : "Dataset Management",
                "description" : "Actions to manage Datasources",
            },
            {
                "name"        : "Provider Subscription Management",
                "description" : "Subscription Management functionalities ( Ex: Accept / Decline Subscriptions ) that are for providers only.",
            },
            {
                "name"        : "Consumer Subscription Management",
                "description" : "Subscription Management functionalities ( Ex: Request a Subscription to a Datasource ).",
            },
            {
                "name"        : "EndPoint Validations",
                "description" : "Validations for the Datasources.",
            }
        ];