import os;
import datetime;
import sys;
import aiohttp;
import asyncio;
import psycopg2;
from psycopg2 import sql;
from psycopg2.extras import RealDictCursor
import csv;
import json as JSON;
import re;
from classes.amTools import amTool;

class amDirectory:
 def __init__( self , cfg ):
     """Class for Directory Methods"""
     self.amTool = amTool();
     self.amTool.log( "Directory Class Initiated" );
     self.cfg      = cfg;
     self.debug    = False;

 def executeQuery( self , query , customConnectionString = None , type="select"):
    connection = False;
    cursor     = False;
    try: 
    
       if( customConnectionString is not None ):
           connection = psycopg2.connect( user     = customConnectionString["user"],
                                          password = customConnectionString["password"],
                                          host     = customConnectionString["host"],
                                          port     = customConnectionString["port"],
                                          database = customConnectionString["database"] );
       else:
           connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                          password = self.cfg["database"]["connection"]["password"],
                                          host     = self.cfg["database"]["connection"]["host"],
                                          port     = self.cfg["database"]["connection"]["port"],
                                          database = self.cfg["database"]["connection"]["database"] );

       cursor = connection.cursor( cursor_factory = RealDictCursor );

       cursor.execute( query );

       if( type == "select" ):
           my_query_response = cursor.fetchall();
       elif( type == "insert" ):
           my_query_response = cursor.fetchone();
       else:
           my_query_response = { "success" : "ok"  };

       if( self.debug is True ) :
           print( cursor.query );
           print( my_query_response );

       cursor.close();
       connection.commit();

       return my_query_response;

    except( Exception, psycopg2.Error ) as error :
        self.amTool.logFile( error );
        if( self.debug is True ) :
            print( " Exception Error " , error );

        if( error.pgcode == "23505" ):
            return { "error" : "duplicate" };
        else:
            return [];

    finally:
        if( connection ):
            connection.close();
            if( self.debug is True ) :
                print( "["+__name__+"] : DB Connection Closed" );

 def evaluate_password( self , password ):
     # Rule 1: Check if the password has more than 12 characters
     if len( password ) <= 12:
         return "Password must have more than 12 characters.";

     # Rule 2: Check if the password has at least 1 uppercase character
     if not re.search( "[A-Z]", password ):
         return "Password must have at least one uppercase character.";

     # Rule 3: Check if the password has at least 1 lowercase character
     if not re.search( "[a-z]", password ):
         return "Password must have at least one lowercase character.";

     # Rule 4: Check if the password has at least one number character
     if not re.search( "[0-9]", password ):
         return "Password must have at least one number character.";

     # Rule 5: Check if the password has no characters in sequence
     for i in range( len( password ) - 2 ):
         if ord( password[ i ] ) == ord( password[ i + 1 ] ) - 1 == ord( password[ i + 2 ] ) - 2:
             return "Password must not contain characters in sequence.";

     # If all rules are passed, return True (indicating the password is valid)
     return True;

 async def addDataset( self , username , password ):
    mySchema = self.cfg[ "database" ][ "schema" ];

    usernameEvaluationResult = self.evaluate_username( username );
    if usernameEvaluationResult != True :
        return { "error" : usernameEvaluationResult };

    passwordEvaluationResult = self.evaluate_password( password );
    if passwordEvaluationResult != True :
        return { "error" : passwordEvaluationResult };

    myQuery  = ( "insert into " + 
                 "{mySchema_param}.user_access " + 
                 " ( email , password , profile_id , is_admin , locale ) " + 
                 "values " + 
                 " ( {myEmail_param} , {myPassword_param} , {myProfileId_param} , {myIsAdmin_param} , {myLocale_param} ) RETURNING user_id"
    );

    qData    = sql.SQL( myQuery ).format( 
                   mySchema_param    = sql.Identifier( mySchema ),
                   myEmail_param     = sql.Literal( username ),
                   myPassword_param  = sql.Literal( password ),
                   myProfileId_param = sql.Literal( 1 ),
                   myIsAdmin_param   = sql.Literal( -1 ),
                   myLocale_param    = sql.Literal( "el" )
               );

    myResponse = self.executeQuery( qData , None , "insert" );

    return myResponse;

 async def getDataSetToken( self , dataset_id ):
    mySchema = self.cfg[ "database" ][ "schema" ];

    myQuery  = ( "select apikey from " + 
                 "{mySchema_param}.dataset " + 
                 "where id = {myDataSetId_param} "
    );

    qData    = sql.SQL( myQuery ).format( 
                   mySchema_param    = sql.Identifier( mySchema ),
                   myDataSetId_param = sql.Literal( dataset_id )
               );

    myResponse = self.executeQuery( qData , None , "select" );

    return myResponse;

 async def addTokenInDatabase( self , consumer_id , dataset_id , expiration , token ):
    mySchema = self.cfg[ "database" ][ "schema" ];

    myQuery  = ( "insert into " + 
                 "{mySchema_param}.map_user_to_dataset " + 
                 " ( user_id , dataset_id , token , expires_on ) " + 
                 " VALUES " + 
                 " ( {myUserId_param} , {myDataSetId_param} , {myToken_param} , {myExpiration_param} ) " + 
                 " RETURNING id"
    );

    qData    = sql.SQL( myQuery ).format( 
                   mySchema_param     = sql.Identifier( mySchema ),
                   myDataSetId_param  = sql.Literal( dataset_id ),
                   myUserId_param     = sql.Literal( consumer_id ),
                   myToken_param      = sql.Literal( token ),
                   myExpiration_param = sql.Literal( expiration )
               );

    myResponse = self.executeQuery( qData , None , "insert" );
    self.amTool.log( "Insert Completed" );

    return myResponse;

 async def updateTokenInDatabase( self , consumer_id , dataset_id , expiration , token ):
    mySchema = self.cfg[ "database" ][ "schema" ];

    myQuery  = ( "update " + 
                 "{mySchema_param}.map_user_to_dataset " + 
                 " set token = {myToken_param}, expires_on = {myExpiration_param}" + 
                 " where user_id = {myUserId_param} and dataset_id = {myDataSetId_param} "
    );

    qData    = sql.SQL( myQuery ).format( 
                   mySchema_param     = sql.Identifier( mySchema ),
                   myDataSetId_param  = sql.Literal( dataset_id ),
                   myUserId_param     = sql.Literal( consumer_id ),
                   myToken_param      = sql.Literal( token ),
                   myExpiration_param = sql.Literal( expiration )
               );

    myResponse = self.executeQuery( qData , None , "update" );
    self.amTool.log( "Update Completed" );

    return myResponse;

 async def tokenRecordExists( self , user_id , dataset_id ):
    mySchema = self.cfg[ "database" ][ "schema" ];

    myQuery  = ( "select * from " + 
                 "{mySchema_param}.map_user_to_dataset " + 
                 " where user_id = {myUserId_param} and dataset_id = {myDataSetId_param} "
    );

    qData    = sql.SQL( myQuery ).format( 
                   mySchema_param     = sql.Identifier( mySchema ),
                   myDataSetId_param  = sql.Literal( dataset_id ),
                   myUserId_param     = sql.Literal( user_id )
               );

    myResponse = self.executeQuery( qData , None , "select" );
    self.amTool.log( "Select Completed" );

    return myResponse;