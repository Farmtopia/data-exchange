import os;
import datetime;
import sys;
import aiohttp;
import asyncio;
import psycopg2;
from psycopg2 import sql;
from psycopg2.extras import RealDictCursor
import csv;
import json as JSON;
import re;
import hashlib;
import hmac;
import base64;
import bcrypt;
from classes.amTools import amTool;

class amUser:
 def __init__( self ):
     """Class for User Access Levels"""
     self.amTool = amTool();
     self.amTool.log( "UserAccess Initiated" );
     self.cfg      = self.amTool.load( "./cfg/" , "config" );
     self.isAdmin  = False;
     self.access   = dict();
     self.debug    = True;

 def executeQuery( self , query , customConnectionString = None , type="select"):
    connection = False;
    cursor     = False;
    try: 
    
       if( customConnectionString is not None ):
           connection = psycopg2.connect( user     = customConnectionString["user"],
                                          password = customConnectionString["password"],
                                          host     = customConnectionString["host"],
                                          port     = customConnectionString["port"],
                                          database = customConnectionString["database"] );
       else:
           connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                          password = self.cfg["database"]["connection"]["password"],
                                          host     = self.cfg["database"]["connection"]["host"],
                                          port     = self.cfg["database"]["connection"]["port"],
                                          database = self.cfg["database"]["connection"]["database"] );

       cursor = connection.cursor( cursor_factory = RealDictCursor );

       cursor.execute( query );

       if( type == "select" ):
           my_query_response = cursor.fetchall();
       elif( type == "insert" ):
           my_query_response = cursor.fetchone();
       else:
           my_query_response = { "success" : "ok"  };

       if( self.debug is True ) :
           print( cursor.query );
           print( my_query_response );

       cursor.close();
       connection.commit();

       return my_query_response;

    except( Exception, psycopg2.Error ) as error :
        self.amTool.logFile( error );
        if( self.debug is True ) :
            print( error );

        if( error.pgcode == "23505" ):
            return { "error" : "duplicate" };
        else:
            return [];

    finally:
        if( connection ):
            connection.close();
            if( self.debug is True ) :
                print( "["+__name__+"] : DB Connection Closed" );

 def evaluate_username( self , username ):
     # Rule 1: Check if the username has more than 8 characters
     if len( username ) <= 8:
         return "Username must have more than 8 characters.";

     # Rule 2: Check if the username has a @
     if not re.search( "@", username ) or not re.search( "\.", username ) :
         return "Username must a valid email.";

     # If all rules are passed, return True (indicating the password is valid)
     return True;

 def evaluate_password( self , password ):
     # Rule 1: Check if the password has more than 12 characters
     if len( password ) <= 12:
         return "Password must have more than 12 characters.";

     # Rule 2: Check if the password has at least 1 uppercase character
     if not re.search( "[A-Z]", password ):
         return "Password must have at least one uppercase character.";

     # Rule 3: Check if the password has at least 1 lowercase character
     if not re.search( "[a-z]", password ):
         return "Password must have at least one lowercase character.";

     # Rule 4: Check if the password has at least one number character
     if not re.search( "[0-9]", password ):
         return "Password must have at least one number character.";

     # Rule 5: Check if the password has no characters in sequence
     for i in range( len( password ) - 2 ):
         if ord( password[ i ] ) == ord( password[ i + 1 ] ) - 1 == ord( password[ i + 2 ] ) - 2:
             return "Password must not contain characters in sequence.";

     # If all rules are passed, return True (indicating the password is valid)
     return True;

 def hash_password( self , password ):
     hashed_password = bcrypt.hashpw(
         password = password.encode( "utf-8" ), 
         salt     = bcrypt.gensalt()
     );
     return hashed_password.decode( "utf-8" );

 def checkHash( self , userProvidedPassword , hashedPassword ):
     hashed_password = bcrypt.checkpw( userProvidedPassword.encode( "utf-8" ), hashedPassword.encode( "utf-8" ) );

     return hashed_password;

 async def getUserAccess( self , credentials ):
    connection = False;
    try: 
       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );

       cursor     = connection.cursor( cursor_factory = RealDictCursor );

       mySchema   = self.cfg["database"]["schema"];

       myQuery = ( "select * from {mySchema_param}.user_access where email = {email_param} and password = {password_param};" );
       qData   = sql.SQL( myQuery ).format(
                     mySchema_param   = sql.Identifier( mySchema ),
                     email_param      = sql.Literal( credentials["un"] ),
                     password_param   = sql.Literal( credentials["pw"] )
                 );

       cursor.execute( qData );
       my_query_response = cursor.fetchall();
       cursor.close();
       connection.commit();

       if( len( my_query_response ) > 0 ):
           return my_query_response;
       else:
           return False;

    except( Exception, psycopg2.Error ) as error :
        return { "error" : error };

    finally:
        if( connection ):
            connection.close();
            if( self.debug is True ) :
                print( "[getUserAccess] : DB Connection Closed" );            

 async def authenticate( self , username , password ):
    connection = False;
    try: 
       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );

       cursor     = connection.cursor( cursor_factory = RealDictCursor );

       mySchema   = self.cfg["database"]["schema"];

       myQuery = ( "select * from {mySchema_param}.user_access where email = {email_param};" );
       qData1   = sql.SQL( myQuery ).format(
                     mySchema_param   = sql.Identifier( mySchema ),
                     email_param      = sql.Literal( username )
                 );

       cursor.execute( qData1 );
       my_query_response1 = cursor.fetchall();

       if( self.debug is True ) :
           print( cursor.query );
           print( my_query_response1 );

       cursor.close();
       connection.commit();

       if( len( my_query_response1 ) > 0 ):
           hashedPassword   = my_query_response1[ 0 ][ "password" ];
           myHashedPassword = self.checkHash( password , hashedPassword );
           if myHashedPassword is False : 
               return { "error" : "Hash Failed" };

           cursor     = connection.cursor( cursor_factory = RealDictCursor );

           myQuery = ( "select * from {mySchema_param}.user_access where email = {email_param} and password = {password_param};" );
           qData2   = sql.SQL( myQuery ).format(
                         mySchema_param   = sql.Identifier( mySchema ),
                         email_param      = sql.Literal( username ),
                         password_param   = sql.Literal( hashedPassword )
                     );

           cursor.execute( qData2 );
           my_query_response2 = cursor.fetchall();

           if( self.debug is True ) :
               print( cursor.query );
               print( my_query_response2 );

           cursor.close();
           connection.commit();

           if( len( my_query_response2 ) > 0 ):
               return my_query_response2[ 0 ];
           else:
               return { "error" : "No correct password" };
       else:
           return { "error" : "No user found" };

    except( Exception, psycopg2.Error ) as error :
        print( error );
        return { "error" : error };

    finally:
        if( connection ):
            connection.close();
            if( self.debug is True ) :
                print( "[authenticate] : DB Connection Closed" );

 def hasAccessToTracker( self , imei ):
     hasAccess = False;
     if( self.isAdmin > 0 ):
         return True;

     for accessDetail in self.access :
         if( accessDetail[ "imei" ] == imei ):
             hasAccess = True;
         else:
             hasAccess = False;

     return hasAccess;

 async def addUser( self , username , password ):
    mySchema = self.cfg[ "database" ][ "schema" ];

    usernameEvaluationResult = self.evaluate_username( username );
    if usernameEvaluationResult != True :
        return { "error" : usernameEvaluationResult };

    passwordEvaluationResult = self.evaluate_password( password );
    if passwordEvaluationResult != True :
        return { "error" : passwordEvaluationResult };

    hashedPassword = self.hash_password( password );

    myQuery  = ( "insert into " + 
                 "{mySchema_param}.user_access " + 
                 " ( email , password , profile_id , is_admin , locale ) " + 
                 "values " + 
                 " ( {myEmail_param} , {myPassword_param} , {myProfileId_param} , {myIsAdmin_param} , {myLocale_param} ) RETURNING user_id"
    );

    qData    = sql.SQL( myQuery ).format( 
                   mySchema_param    = sql.Identifier( mySchema ),
                   myEmail_param     = sql.Literal( username ),
                   myPassword_param  = sql.Literal( hashedPassword ),
                   myProfileId_param = sql.Literal( 1 ),
                   myIsAdmin_param   = sql.Literal( -1 ),
                   myLocale_param    = sql.Literal( "el" )
               );

    myResponse = self.executeQuery( qData , None , "insert" );

    return myResponse;

 async def getUserById( self , user_id ):
    mySchema = self.cfg[ "database" ][ "schema" ];

    myQuery  = ( "select * from {mySchema_param}.user_access " + 
                 " where user_id = {myUserId_param} "
    );

    qData    = sql.SQL( myQuery ).format( 
                   mySchema_param    = sql.Identifier( mySchema ),
                   myUserId_param    = sql.Literal( user_id )
               );

    myResponse = self.executeQuery( qData , None , "select" );

    return myResponse;

 async def updateUserToken( self , user_id , apikey ):
    mySchema = self.cfg[ "database" ][ "schema" ];

    myQuery  = ( "update {mySchema_param}.user_access set apikey = {myApiKey_param} " + 
                 " where user_id = {myUserId_param} "
    );

    qData    = sql.SQL( myQuery ).format( 
                   mySchema_param    = sql.Identifier( mySchema ),
                   myUserId_param    = sql.Literal( user_id ),
                   myApiKey_param    = sql.Literal( apikey )
               );

    myResponse = self.executeQuery( qData , None , "update" );

    return myResponse;

 async def checkUserToken( self , user_id ):
    mySchema = self.cfg[ "database" ][ "schema" ];

    myQuery  = ( "select user_id , apikey from {mySchema_param}.user_access " + 
                 " where user_id = {myUserId_param} "
    );

    qData    = sql.SQL( myQuery ).format( 
                   mySchema_param    = sql.Identifier( mySchema ),
                   myUserId_param    = sql.Literal( user_id )
               );

    myResponse = self.executeQuery( qData , None , "select" );

    return myResponse;

 async def getUserByEmail( self , email ):
    mySchema = self.cfg[ "database" ][ "schema" ];

    myQuery  = ( "select * from {mySchema_param}.user_access " + 
                 " where email = {myUserEmail_param} "
    );

    qData    = sql.SQL( myQuery ).format( 
                   mySchema_param    = sql.Identifier( mySchema ),
                   myUserEmail_param = sql.Literal( email )
               );

    myResponse = self.executeQuery( qData , None , "select" );

    return myResponse;