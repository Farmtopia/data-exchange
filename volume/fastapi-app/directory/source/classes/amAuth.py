from typing  import Union;
from jose import JWTError, jwt;
import time;
import datetime;
from   datetime import timedelta , timezone;
import hashlib;
import hmac;
import base64;
import bcrypt;
from classes.amTools import amTool;
from classes.amUser  import amUser;

class amAuth:
 def __init__( self , cfg ):
     """Class for Authentication"""
     self.amTool = amTool();
     self.amTool.log( "amAuth Initiated" );
     self.cfg      = cfg;
     self.error    = False;
     self.debug    = False;
     self.myUser   = amUser();

 def verify_password( self , plain_password, hashed_password ):
     password_byte_enc = plain_password.encode( "utf-8" );
     # print( password_byte_enc );
     # print( bytes( hashed_password , "utf-8" )  );
     try :
         return bcrypt.checkpw( 
             password        = password_byte_enc , 
             hashed_password = bytes( hashed_password , "utf-8" ) 
         );
     except( Exception ) as error :
         return False;

 async def authenticate_user( self , username: str, password: str ):
     user = await self.myUser.getUserByEmail( email = username );
     if len( user ) > 0 :
         if not self.verify_password( password, user[ 0 ][ "password" ] ):
             return False;
     else:
         return False;

     return user[ 0 ];

 def create_access_token( self , data: dict, expires_delta: Union[ int, None ] = None , secret_key = None ):
     expires_delta = timedelta( minutes = expires_delta );
     to_encode = data.copy();
     if expires_delta:
         expire = datetime.datetime.now( timezone.utc ) + expires_delta;
     else:
         expire = datetime.datetime.now( timezone.utc ) + timedelta( minutes = 15 );

     to_encode.update( { "exp" : expire } );
     encoded_jwt = jwt.encode( 
         to_encode, 
         key       = ( self.cfg[ "auth" ][ "secret_key" ] if secret_key == None else secret_key ) , 
         algorithm = self.cfg[ "auth" ][ "algorithm" ] 
     );

     return encoded_jwt;

 def hash_password( self , password ):
     hashed_password = bcrypt.hashpw(
         password = password.encode( "utf-8" ), 
         salt     = bcrypt.gensalt()
     );
     return hashed_password.decode( "utf-8" );
