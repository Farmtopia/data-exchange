from typing  import Union;
from jose import JWTError, jwt;
import time;
import datetime;
from   datetime import timedelta , timezone;
import hashlib;
import hmac;
import base64;
import bcrypt;
from rdflib import Graph;
from classes.amTools import amTool;

class amEndpointEvaluator:
 def __init__( self , cfg ):
     """Class for EndPoint Evaluation"""
     self.amTool = amTool();
     self.amTool.log( "amEndPointEvaluation Initiated" );
     self.cfg      = cfg;
     self.error    = False;
     self.debug    = False;

 def setSchema( self , schema ):
     self.schemaClass = schema;

 def handleUnexpectedError( self , index , error ) :
     return { "error" : index + ": Exception of type '{0}'. Arguments: {1!r}".format( type(error).__name__ , error.args ) };

 def checkProperty( self , sourceObject , targetObject , schemaProperty ):
     problematicFields = [];

     try:
         if schemaProperty not in targetObject : 
             problematicFields.append( {
                 "property" : schemaProperty , 
                 "issue"    : "Missing Schema property '" + schemaProperty + "'" 
             } );
         else:
             for parameter in sourceObject[ schemaProperty ] :
                 if parameter == "anyOf" :
                     myFieldEligibleTypes = [];
                     for anyOfType in sourceObject[ schemaProperty ][ parameter ] :
                         myFieldEligibleTypes.append( anyOfType[ "type" ] );

                     if parameter not in targetObject[ schemaProperty ] : 
                         problematicFields.append( { 
                             "property" : schemaProperty , 
                             "issue"    : "Missing Schema parameter '" + parameter + ":" + str( sourceObject[ schemaProperty ][ parameter ] ) + "'" ,
                             "type" : "|".join( myFieldEligibleTypes ) 
                         } );
                 else:
                     if parameter not in targetObject[ schemaProperty ] : 
                         problematicFields.append( {
                             "property" : schemaProperty , 
                             "issue"    : "Missing Schema parameter '" + parameter + ":" + str( sourceObject[ schemaProperty ][ parameter ] ) + "'" 
                         } );
                     else:
                         if sourceObject[ schemaProperty ][ parameter ] != targetObject[ schemaProperty ][ parameter ] : 
                             problematicFields.append( { 
                                 "property" : schemaProperty , 
                                 "issue"    : "Wrong value for parameter '" + parameter + "'" , 
                                 "expected" : sourceObject[ schemaProperty ][ parameter ] ,
                                 "received" : targetObject[ schemaProperty ][ parameter ]
                             } );

         return problematicFields;
     except Exception as error :
         return [{ "error" : "01: 'checkProperty' Exception of type '{0}'. Arguments: {1!r}".format( type(error).__name__ , error.args ) }];

 def compareJSONSchemaToModel( self , schemaModel , ModelName , clientOpenApiComponentSchemas ):

     if "properties" not in schemaModel : 
         return { "error" : "02: Model should have a 'properties' Object under components->schemas->" + ModelName };

     if "type" not in schemaModel : 
         return { "error" : "02: Model should have a 'type' property under components->schemas->" + ModelName };

     myMissingProperties = [];
     for schemaProperty in self.schemaClass[ "properties" ]:
         if schemaProperty not in schemaModel[ "properties" ] :
             myMissingProperties.append( schemaProperty );

     if len( myMissingProperties ) > 0 :
         return { "error" : "02: Schema Model should have the following properties under components->schemas->" + ModelName + "->properties : " + ", ".join( myMissingProperties ) };

     missingFields     = [];
     problematicFields = [];
     for field in self.schemaClass[ "properties" ] : 
         if field not in schemaModel[ "properties" ] : 
             missingFields.append( field );
         else:
             resCheck = self.checkProperty( 
                 sourceObject    = self.schemaClass[ "properties" ] , 
                 targetObject    = schemaModel[ "properties" ] , 
                 schemaProperty  = field 
             );

             if( len( resCheck ) > 0 ) :
                 problematicFields.extend( resCheck );

     if len( problematicFields ) > 0 :
         return { "model" : str( ModelName ) , "error" : "02: Issues on comparison" , "detail" : problematicFields };

     if len( missingFields ) > 0 :
         return { "error" : "02: Model should have the following fields under components->schemas->sensor->properties : " + ", ".join( missingFields ) };

     return True;

 def recurseNestedJSON( self , dictionary, pathList ):
     currentDictionaryLevel = dictionary;
     path    = [];
     for pathItem in pathList:
         if pathItem in currentDictionaryLevel:
             currentDictionaryLevel = currentDictionaryLevel[ pathItem ];
             path.append( pathItem );
         else:
             return { "error" : ( "The last property in the following chain " + 
                                  "'[" + ']->['.join( path + [ pathItem ] ) + "]' does not exist in the " +
                                  "OpenAPI JSON Schema provided") };
     return { "res" : "Success" };

 def validateEndPointPath( self , endPoint , clientOpenApi , cfg ):

     try:
         paths = [
           "paths" , 
             endPoint , 
              cfg[ "method" ] , 
               "responses" , 
                "200" , 
                 "content" , 
                  cfg[ "responses" ][ "200" ][ "content" ] ,
                   "schema" 
         ];
         myCheck = self.recurseNestedJSON( clientOpenApi, paths );
         if "error" in myCheck : 
             return { "error" : myCheck[ "error" ] };
     except Exception as error :
         return self.handleUnexpectedError( 1 , error );

     try:
         myClientSchema = clientOpenApi[ "paths" ][ endPoint ][ cfg[ "method" ] ][ "responses" ][ "200" ][ "content" ][ cfg[ "responses" ][ "200" ][ "content" ] ][ "schema" ];

         if cfg[ "responses" ][ "200" ][ "type" ] == "array" : 
             if "items" not in myClientSchema : 
                 return { "error" : "8: Path '" + endPoint + "' should return a List of '" + str( cfg[ "responses" ][ "200" ][ "schema" ] ) + "' Models." }

             if "$ref" not in myClientSchema[ "items" ] :  
                 return { "error" : "8: Path '" + endPoint + "' missing 'Reference Schema' in schema->items." }

             if "type" not in myClientSchema : 
                 return { "error" : "8: Path '" + endPoint + "' missing 'Type' in schema." }

             if myClientSchema[ "type" ] != cfg[ "responses" ][ "200" ][ "type" ] : 
                 return { "error" : ( "8: Path '" + endPoint + "' should respond with an '" + cfg[ "responses" ][ "200" ][ "type" ] + "' " + 
                                     "and not '" + myClientSchema[ "type" ] + "'" ) };
         else:
             if "$ref" not in myClientSchema : 
                 return { "error" : "8: Path '" + endPoint + "' missing 'Reference Schema' [item]." }

             if cfg[ "responses" ][ "200" ][ "schema" ] != myClientSchema[ "$ref" ].replace( "#/components/schemas/" , "" ) : 
                 return { "error" : "8: Path '" + endPoint + "' incorect Schema assigned." }
     except Exception as error :
         return self.handleUnexpectedError( 8 , error );

     return { "res" : "Everything is ok" };

 def validateEndPointResponse( self , clientOpenApi , cfg ):

     try:
         myCheck = self.recurseNestedJSON( clientOpenApi, [
           "components" , 
           "schemas"
         ] );
         if "error" in myCheck : 
             return { "error" : myCheck[ "error" ] };
     except Exception as error :
         return self.handleUnexpectedError( 1 , error );

     schemaName  = cfg[ "responses" ][ "200" ][ "schema" ];

     if schemaName in clientOpenApi[ "components" ][ "schemas" ] : 
         schemaModel = clientOpenApi[ "components" ][ "schemas" ][ schemaName ];
     else:
         return { "error" : "Missing schema '" + str( schemaName ) + "' from 'components'->'schemas'." };

     try:
         myComparisonResult = self.compareJSONSchemaToModel( schemaModel , schemaName , clientOpenApi[ "components" ][ "schemas" ] );
     except Exception as error :
         return self.handleUnexpectedError( 10 , error );

     if myComparisonResult is True :
         return { "res" : "Everything is ok" };
     else:
         return myComparisonResult;

 def validate( self , cfg , endPoint , clientOpenApi ):

     try:
         endPointSchemaValidationResult = self.validateEndPointPath( 
             endPoint      = endPoint , 
             clientOpenApi = clientOpenApi , 
             cfg           = cfg
         );

         if "error" in endPointSchemaValidationResult :
             return { "error" : endPointSchemaValidationResult };

     except Exception as error :
         return self.handleUnexpectedError( 11 , error );

     try:
         endPointResponseValidationResult = self.validateEndPointResponse(
             clientOpenApi = clientOpenApi , 
             cfg           = cfg
         );

         if "error" in endPointResponseValidationResult : 
             return { "error" : endPointResponseValidationResult };

     except Exception as error :
         return self.handleUnexpectedError( 12 , error );

     return { "res" : "Success" };

 def validateJSONLD( self , endPoint ):
     g = Graph(); 
     try:
         myParse = g.parse( endPoint , format="json-ld");
         print( myParse );
         myRes = g.serialize(format="json-ld");
         print( myRes );
         # for subj, pred, obj in g:
             # if (subj, pred, obj) not in g:
                # raise Exception("It better be!");
         # print(f"Graph g has {len(g)} statements.");
         # print(g.serialize(format="json-ld"));
         return True;
     except ( Exception ) as error :
         print( error );
         # print(g.serialize(format="json-ld"));
         return False;
 
 