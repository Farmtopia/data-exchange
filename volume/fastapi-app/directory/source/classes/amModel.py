import os;
import datetime;
import time;
import sys;
import aiohttp;
import asyncio;
import psycopg2;
import base64;
import hashlib;
from psycopg2 import sql;
from psycopg2.extras import RealDictCursor;
import csv;
import json as JSON;
from classes.amTools import amTool;

class amModel:
 def __init__( self , cfg ):
  """Class for Queryinjg Database"""
  self.amTool = amTool();
  self.amTool.log( "Model Initiated" );
  self.cfg    = cfg;
  self.debug  = False;

 def executeQuery( self , query , customConnectionString = None , type="select"):
    connection = False;
    cursor     = False;
    try: 
    
       if( customConnectionString is not None ):
           connection = psycopg2.connect( user     = customConnectionString["user"],
                                          password = customConnectionString["password"],
                                          host     = customConnectionString["host"],
                                          port     = customConnectionString["port"],
                                          database = customConnectionString["database"] );
       else:
           connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                          password = self.cfg["database"]["connection"]["password"],
                                          host     = self.cfg["database"]["connection"]["host"],
                                          port     = self.cfg["database"]["connection"]["port"],
                                          database = self.cfg["database"]["connection"]["database"] );

       cursor = connection.cursor( cursor_factory = RealDictCursor );

       cursor.execute( query );

       if( type == "select" ):
           my_query_response = cursor.fetchall();
       elif( type == "insert" ):
           my_query_response = cursor.fetchone();
       else:
           my_query_response = { "success" : "ok"  };

       if( self.debug is True ) :
           print( cursor.query );
           print( my_query_response );

       cursor.close();
       connection.commit();

       return my_query_response;

    except( Exception, psycopg2.Error ) as error :
        self.amTool.logFile( error );
        if( self.debug is True ) :
            print( error , error.pgcode );

        if( error.pgcode == "23505" ):
            return { "error" : "duplicate" };
        elif( error.pgcode == "23503" ):
            return { "error" : "Foreign key violation" };
        else:
            return [];

    finally:
        if( connection ):
            connection.close();
            if( self.debug is True ) :
                print( "["+__name__+"] : DB Connection Closed" );

 def parseStringIntoArrayWithIntegers( self , string ):
    myListArrayResponse = list();
    print( string );
    if( string == "" or string is None ):
        return [];

    if( string is None ):
        return None;

    if( string.find(",") > 0 ):
        myList = string.split( "," );
        for item in myList : 
            if( amTool.isInt( item ) ):
                myListArrayResponse.append( item );
            else:
                print( "This is not a number" );
        return myListArrayResponse;
    else:
        if( amTool.isInt( string ) ):
            return [ string ];
        else:
            print( "This is not a number" );
            return myListArrayResponse;

 async def getEntity( self , entity , id=[] , limit=None , offset=None ):
 
    mySchema = self.cfg["database"]["schema"];
    myLimit   = "" if ( limit is None )  else ( "limit " + str( limit ) );
    myOffset  = "" if ( offset is None ) else ( "offset " + str( offset ) );
    
    print( id );
    myWhereClause = "";
    if( len( id ) > 0 ):
        id            = "','".join( id );
        myWhereClause = "where id in ('" + id + "') ";

    myQuery  = ( "select " + 
                 " * " + 
                 "from {mySchema_param}.{myEntityParam} " + 
                 myWhereClause + 
                 "order by id DESC " + 
                 str( myLimit ) + " " + str( myOffset )
    );

    qData    = sql.SQL( myQuery ).format( 
                   mySchema_param = sql.Identifier( mySchema ),
                   myEntityParam  = sql.Identifier( entity )
               );

    myResponse = self.executeQuery( qData , None , "select" );

    return myResponse;

 async def getEntityAllProperties( self , params , entity ):

    mySchema  = self.cfg["database"]["schema"];
    myLimit   = "" if ( "limit" not in params or params.limit is None )  else ( "limit " + str( params.limit ) );
    myOffset  = "" if ( "offset" not in params or params.offset is None ) else ( "offset " + str( params.offset ) );

    myWhereClauseArray  = [];
    myWhereClauseString = "";
    myPeriodString      = "";

    for item in params : 
        if( item[ 0 ] not in [ "limit" , "offset" ] ):
            if item[ 1 ] is not None:
                if item[ 0 ].find( "from_date" ) >= 0 : 
                    myParentItem    = item[ 0 ].replace( "_from_date" , "" );
                    myPeriodString += ( str( myParentItem ) + " between '" + str( item[ 1 ] ) + "' " );
                elif item[ 0 ].find( "to_date" ) >= 0 : 
                    myPeriodString += " and '" + str( item[ 1 ] ) + "' ";
                    myWhereClauseArray.append( myPeriodString );
                    myPeriodString = "";
                else:
                    if type( item[ 1 ] ) == int or type( item[ 1 ] ) == float:
                        myWhereClauseArray.append( str( item[ 0 ] ) + " = " + str( item[ 1 ] ) + "" );
                    else:
                        myWhereClauseArray.append( str( item[ 0 ] ) + " = '" + str( item[ 1 ] ) + "'" );

    if( len( myWhereClauseArray ) > 0 ):
        myWhereClauseString = "where " + " and ".join( myWhereClauseArray ) + " " ;

    myQuery  = ( "select " + 
                 " * " + 
                 "from {mySchema_param}.{myEntityParam} " + 
                 myWhereClauseString + 
                 "order by id ASC " + 
                 str( myLimit ) + " " + str( myOffset )
    );

    qData    = sql.SQL( myQuery ).format( 
                   mySchema_param = sql.Identifier( mySchema ),
                   myEntityParam  = sql.Identifier( entity )
               );

    myResponse = self.executeQuery( qData , None , "select" );

    return myResponse;

 async def addEntity( self , request , type , entity , returning = "ID" ):
    mySchema = self.cfg[ "database" ][ "schema" ];

    myObject = {};
    # print( request );
    # return [];

    for record in request:
        self.amTool.log( "[ REQUEST DETAILS ]" );
        for item in record:
            self.amTool.log( item );
            myString = [];
            for value in item:
                myString.append( str( value ) );
            myObject[ myString[ 0 ] ] = myString[ 1 ];
        self.amTool.log( "" );
        if type is None : 
            myColumns = [];
            myValues  = [];
        else : 
            myColumns = [ "\"type\"" ];
            myValues  = [ "'" + type + "'" ];

        for prop in myObject :
            myColumns.append( "\"" + prop.lower() + "\"" );
            myValues.append( myObject[ prop ] );
            # myValue = "";
            # try:
                # JSON.loads( myObject[ prop ] );
                # myValue = JSON.dumps( myObject[ prop ] );
                # self.amTool.log( "This is a JSON " + str( prop ) );
            # except ValueError as e:
                # myValue = myObject[ prop ];
                # self.amTool.log( "This is String " + str( prop ) );
            # myValues.append( myValue );

        myQuery = "INSERT INTO \"" + mySchema + "\".\"" + entity + "\" ";
        myQuery += "(" + " , ".join( myColumns ) + ")";
        myQuery += " VALUES ";
        myQuery += "('" + "' , '".join( myValues ) + "') RETURNING " + str( returning ) + ";";
        result = self.executeQuery( myQuery , None , "insert" );
        """
        myQuery = "INSERT INTO {mySchema_param}.\"" + entity + "\" ";
        myQuery += "(" + " , ".join( myColumns ) + ")";
        myQuery += " VALUES ";
        myQuery += "('" + "' , '".join( myValues ) + "') RETURNING " + str( returning ) + ";";
        
        print( myQuery );

        qData    = sql.SQL( myQuery ).format( 
                       mySchema_param = sql.Identifier( mySchema )
                   );
        
        print( qData );

        result = self.executeQuery( qData , None , "insert" );
        """
        if( "error" in result ):
            return { "error" : result[ "error" ] };

    return result;

 async def updateEntity( self , request , entity ):
    mySchema = self.cfg[ "database" ][ "schema" ];

    myResponse = {};
    myEntityData = await self.getEntity( id=[str(request.id)] , entity=entity , limit=1 , offset=0 );

    if( len( myEntityData ) <= 0 ):
        return { "error" : "Nothing to update" };

    myValues = [];
    myFields = [];

    for item in request:
        if item[ 0 ] == "id" :
            continue;

        myProperty = item[ 0 ];
        myValue    = item[ 1 ];

        if myProperty in myEntityData[ 0 ] : 
            myFields.append( myProperty );
            myValues.append( myValue );

    myUpdates = [];
    for index, field in enumerate( myFields ) : 
       if( myValues[ index ] is not None ) :
           myUpdates.append( str( field ) + " = '" + str( myValues[ index ] ) + "'" );

    myQuery = ( "update {mySchema_param}.{myEntity_param} " + 
                "set " + 
                " , ".join( myUpdates ) + 
                "where id = {myId_param};"
    );

    qData   = sql.SQL( myQuery ).format( 
                  mySchema_param  = sql.Identifier( mySchema ),
                  myEntity_param  = sql.Identifier( entity ),
                  myId_param      = sql.Literal( request.id )
            );

    myResponse = self.executeQuery( qData , None , "update" );

    return myResponse;

 async def deleteEntity( self , entity , id=[] ):

    mySchema      = self.cfg["database"]["schema"];
    myWhereClause = "";
    if( len( id ) > 0 ):
        id            = "','".join( id );
        myWhereClause = "where id in ('" + id + "') ";

    myQuery  = ( "delete  " + 
                 "from {mySchema_param}.{myEntityParam} " + 
                 myWhereClause 
    );

    qData    = sql.SQL( myQuery ).format( 
                   mySchema_param = sql.Identifier( mySchema ),
                   myEntityParam  = sql.Identifier( entity )
               );

    myResponse = self.executeQuery( qData , None , "delete" );

    return myResponse;

 async def createInMemoryObject( self ):
    mySchema = self.cfg[ "database" ][ "schema" ];

    myResults = {};
    for entity in [ "contextentity" , "priceentity" , "volumeentity" , "timeentity" , "n2o_emissions_direct" , "n2o_emissions_indirect" , "co2_emissions_fuel" ]:
        myQuery = "select * from {mySchema_param}.{myEntityParam};";
        qData   = sql.SQL( myQuery ).format( 
                      mySchema_param = sql.Identifier( mySchema ),
                      myEntityParam  = sql.Identifier( entity )
                  );
        result = self.executeQuery( qData , None , "select" );
        myResults[ entity ] = result;

        if( "error" in result ):
            myResults.append( { "error" : result[ "error" ] } );

    myMemoryObject = {};
    for entity in myResults: 
        for row in myResults[ entity ] : 
            type = row[ "type" ] + "_" if ( row[ "type" ] != "default" and row[ "type" ] != "" ) else "";
            for property in row:
                if property == "id" : 
                    continue;
                myMemoryObject[ type + property ] = row[ property ];

    

    return myMemoryObject;

 async def getValues( self , arrayOfProperties ):
    mySchema = self.cfg[ "database" ][ "schema" ];

    myObject = {};
    for item in request:
        myString = [];
        for value in item:
            myString.append( str( value ) );
        myObject[ myString[ 0 ] ] = myString[ 1 ];

    myColumns = [];
    myValues  = [];
    for prop in myObject :
        myColumns.append( "\"" + prop + "\"" );
        myValues.append( myObject[ prop ] );

    myQuery = "INSERT INTO {mySchema_param}.\"" + entity + "\" ";
    myQuery += "(" + " , ".join( myColumns ) + ")";
    myQuery += " VALUES ";
    myQuery += "(" + " , ".join( myValues ) + ") RETURNING ID;";

    qData    = sql.SQL( myQuery ).format( 
                   mySchema_param = sql.Identifier( mySchema )
               );

    result = self.executeQuery( qData , None , "insert" );
    if( "error" in result ):
        return { "error" : result[ "error" ] };

    return result;

 async def getTableData( self , tableName ):
    mySchema = self.cfg[ "database" ][ "schema" ];

    myQuery  = "select * from {mySchema_param}.{myTableName_param} order by id asc;";

    qData    = sql.SQL( myQuery ).format( 
                   mySchema_param    = sql.Identifier( mySchema ),
                   myTableName_param = sql.Identifier( tableName )
               );

    result = self.executeQuery( qData , None , "select" );
    if( "error" in result ):
        return { "error" : result[ "error" ] };

    return result;
 
 async def request( self , options ):

    async with aiohttp.ClientSession() as session:

        async with session.post( 
                      options[ "endPoint" ] , 
                      json    = options[ "myData"] , 
                      headers = {'content-type': 'application/json' , 'accept' : 'application/json' } 
                   ) as response:

             myText = await response.text();

             try:
                 myJSON = JSON.loads( myText );
                 return myJSON;
             except ValueError as error :
                 if( self.debug == True ) : 
                     self.amTool.logFile( myCurl );

                 return { "error" : "There was an issues retrieving data." };
 
 async def clearSensorData( self ):
    mySchema = self.cfg["database"]["schema"];
    myQuery  = ( "TRUNCATE TABLE {mySchema_param}.sensordata RESTART IDENTITY RESTRICT;" );

    qData    = sql.SQL( myQuery ).format( 
                   mySchema_param = sql.Identifier( mySchema )
               );

    myResponse = self.executeQuery( qData , None , "delete" );

    return myResponse;

 async def getApikeyOfUser( self , email ):

    mySchema  = self.cfg["database"]["schema"];

    myQuery  = ( "select " + 
                 " apikey " + 
                 "from {mySchema_param}.user_access " + 
                 "where email = {myEmailParam}"
    );

    qData    = sql.SQL( myQuery ).format( 
                   mySchema_param = sql.Identifier( mySchema ),
                   myEmailParam   = sql.Literal( email )
               );

    myResponse = self.executeQuery( qData , None , "select" );

    return myResponse;

 async def setDatasetSubscriptionStatusForUser( self , status_id , dataset_id , user_id ):
    mySchema  = self.cfg["database"]["schema"];
    
    self.amTool.log( "status_id : " + str( status_id ) + " | dataset_id : " + str( dataset_id ) + " | user_id : " + str( user_id ) );

    myQuery  = ( "update " + 
                 "{mySchema_param}.map_user_to_dataset " + 
                 " set status = {myStatusIdParam} " + 
                 "where dataset_id = {myDatasetIdParam} " + 
                 "and user_id = {myUserIdParam} "
    );

    qData    = sql.SQL( myQuery ).format( 
                   mySchema_param    = sql.Identifier( mySchema ),
                   myUserIdParam     = sql.Literal( user_id ),
                   myDatasetIdParam  = sql.Literal( dataset_id ),
                   myStatusIdParam   = sql.Literal( status_id )
               );

    myResponse = self.executeQuery( qData , None , "update" );

    return myResponse;

 async def getDatasetSubscriptionStatusForUser( self , dataset_id , user_id ):
    mySchema  = self.cfg["database"]["schema"];

    myQuery  = ( "select * " + 
                 "from {mySchema_param}.map_user_to_dataset " + 
                 "where dataset_id = {myDatasetIdParam} " + 
                 "and user_id = {myUserIdParam} "
    );

    qData    = sql.SQL( myQuery ).format( 
                   mySchema_param    = sql.Identifier( mySchema ),
                   myUserIdParam     = sql.Literal( user_id ),
                   myDatasetIdParam  = sql.Literal( dataset_id )
               );

    myResponse = self.executeQuery( qData , None , "select" );
    
    if( len( myResponse) > 0 ):
        return myResponse[ 0 ];
    else:
        return [];

 async def getDatasetSubscriptionStatus( self , dataset_id ):
    mySchema  = self.cfg["database"]["schema"];

    myQuery  = ( "select * " + 
                 "from {mySchema_param}.map_user_to_dataset " + 
                 "where dataset_id = {myDatasetIdParam} "
    );

    qData    = sql.SQL( myQuery ).format( 
                   mySchema_param    = sql.Identifier( mySchema ),
                   myDatasetIdParam  = sql.Literal( dataset_id )
               );

    myResponse = self.executeQuery( qData , None , "select" );
    
    if( len( myResponse) > 0 ):
        return myResponse;
    else:
        return [];

 async def clearAllData( self ):

    self.executeQuery( "TRUNCATE TABLE farmtopia_directory.dataset RESTART IDENTITY CASCADE;" , None , "update" );
    self.executeQuery( "TRUNCATE TABLE farmtopia_directory.map_user_to_dataset RESTART IDENTITY CASCADE;" , None , "update" );
    self.executeQuery( "TRUNCATE TABLE farmtopia_directory.user_access RESTART IDENTITY CASCADE;" , None , "update" );
    self.executeQuery( "TRUNCATE TABLE farmtopia_provider.apikey RESTART IDENTITY CASCADE;" , None , "update" );
    self.executeQuery( "TRUNCATE TABLE farmtopia_provider.map_user_to_dataset RESTART IDENTITY CASCADE;" , None , "update" );
    self.executeQuery( "TRUNCATE TABLE farmtopia_provider.user_access RESTART IDENTITY CASCADE;" , None , "update" );
    self.executeQuery( "TRUNCATE TABLE farmtopia_provider_2.apikey RESTART IDENTITY CASCADE;" , None , "update" );
    self.executeQuery( "TRUNCATE TABLE farmtopia_provider_2.map_user_to_dataset RESTART IDENTITY CASCADE;" , None , "update" );
    self.executeQuery( "TRUNCATE TABLE farmtopia_provider_2.user_access RESTART IDENTITY CASCADE;" , None , "update" );

    return [];