# uvicorn main:app --port 8686 --host 192.168.1.108 --reload --root-path /
from   inspect import currentframe, getframeinfo;
from   typing  import Optional , List, Any , ClassVar , Union , Dict , Annotated;
from   fastapi import Depends;
from   fastapi import FastAPI;
from   fastapi import File;
from   fastapi import UploadFile;
from   fastapi import Form;
from   fastapi import Request;
from   fastapi import HTTPException;
from   fastapi import status;
from   fastapi import Query;
from   fastapi import Body;
from   fastapi import Path;
from   fastapi.staticfiles   import StaticFiles;
from   fastapi.responses     import HTMLResponse;
from   fastapi.security      import HTTPBasic, HTTPBasicCredentials;
from   fastapi.openapi.utils import get_openapi;
from   enum     import Enum;
from   pydantic import BaseModel , Field , create_model , StringConstraints , EmailStr;
import time;
import datetime;
from   datetime import timedelta , timezone;
import os;
import os.path;
import re;
import array;
from   os import path;
from   decimal import *;
import aiohttp;
import uuid;
import aiofiles;
import asyncio;
import json as JSON;
import sys;
import random;
import csv;
import hashlib;
import hmac;
import base64;
import psycopg2;
import models.models               as     ResModel;
import models.parametrics          as     Parametric;
import models.inputs               as     Inputs;
from   cfg.tags                    import Tags;
from   classes.amModel             import amModel;
from   classes.amDirectory         import amDirectory;
from   classes.amTools             import amTool;
from   classes.amAccess            import amAccess;
from   classes.amUser              import amUser;
from   classes.amAuth              import amAuth;
from   classes.amEndpointEvaluator import amEndpointEvaluator;

# Local hosted Swagger 
from   fastapi.openapi.docs import (
       get_redoc_html,
       get_swagger_ui_html,
       get_swagger_ui_oauth2_redirect_html,
);

from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm;
from jose import JWTError, jwt;
from passlib.context import CryptContext;
pwd_context      = CryptContext( schemes = [ "bcrypt" ], deprecated = "auto" );
oauth2_scheme    = OAuth2PasswordBearer( tokenUrl = "token" );

security         = HTTPBasic();
amTool           = amTool();
cfg              = amTool.load( "./cfg/" , "config" );
amTool.cfg       = cfg;
amAccess         = amAccess( cfg );
myModel          = amModel( cfg );
myDirectory      = amDirectory( cfg );
myTags           = Tags();
myUser           = amUser();
myAuth           = amAuth( cfg );
myEval           = amEndpointEvaluator( cfg );

fromDate_default = datetime.date.today() - timedelta( days = 1);
toDate_default   = datetime.date.today();
endpoint_tags    = myTags.list;

DBsourceFileName = "schema";
debug            = False;

myIP             = os.getenv( 'localip' );

serverSecretKey = "DqBflEflm8JFGMaGF4iL4uU7zYcCPw7bCKRXXCqymvnsSXhXF35LduRuXdDfi6hAn7Ki";

app = FastAPI( 
              title        = cfg["service"]["title"],
              description  = cfg["service"]["description"],
              version      = cfg["service"]["version"],
              openapi_tags = endpoint_tags , 
              redoc_url    = None , 
              # openapi_url  = "/cfg/openapi.json" , 
              docs_url     = "/" + cfg["service"]["docs_url"]
      );

# Local hosted Swagger 
app.mount( "/static", StaticFiles( directory = "static" ), name = "static" );

app.mount(
           "/"+cfg["uploads"]["images"]["service_name"] , 
           StaticFiles( directory = cfg["uploads"]["images"]["path"] ), 
           name=cfg["uploads"]["images"]["service_name"] 
         );


# Generic Helpers

def getCredentials( credentials ):
    return { "username" : credentials.username, "password" : credentials.password }

def validateDate( date_string ):
    try:
        datetime.datetime.strptime( date_string , '%Y-%m-%d %H:%M:%S' );
        return True;
    except ValueError:
        return False;

def parseStringIntoArrayWithIntegers( string ):
   myListArrayResponse = list();
   print( string );
   if( string == "" or string is None ):
       return [];

   if( string is None ):
       return None;

   if( string.find(",") > 0 ):
       myList = string.split( "," );
       for item in myList : 
           if( amTool.isInt( item ) ):
               myListArrayResponse.append( item );
           else:
               print( "This is not a number" );
       return myListArrayResponse;
   else:
       if( amTool.isInt( string ) ):
           return [ string ];
       else:
           print( "This is not a number" );
           return myListArrayResponse;

def parseModelsStringIntoArray( models ):
   myListArrayResponse = list();

   if( models.find(",") > 0 ):
       myList = models.split( "," );
       for item in myList : 
           myListArrayResponse.append( item );

       return myListArrayResponse;
   else:
       return [ models ];

def logRequest( request ):
    amTool.log( request.url );

def decodeJWTFromHeader( request: Request  ):

    amTool.log( "[ decodeJWTFromHeader Verification : Step 1 ] : Checking if needed Headers are available in Request" );
    if "authorization" in request.headers:
        requestJWT      = request.headers[ "authorization" ].split();
    else:
        amTool.log( "[ Verification - ERROR : Step 1 ] : Incorrect Headers. Token Missing" );
        raise HTTPException( 
            status_code = status.HTTP_400_BAD_REQUEST, 
            detail      = "Incorrect Headers. Token Missing.",
            headers     = { "WWW-Authenticate": "Bearer" }
        );

    if len( requestJWT ) != 2 : 
        amTool.log( "[ Verification - ERROR : Step 1 ] : Incorrect Headers" );
        raise HTTPException( 
            status_code = status.HTTP_400_BAD_REQUEST, 
            detail      = "Incorrect Headers",
            headers     = { "WWW-Authenticate": "Bearer" }
        );
    
    print( requestJWT[ 1 ] );

    try:
        payload = jwt.decode( 
            token      = requestJWT[ 1 ], 
            key        = None,
            options    = { "verify_signature": False }
        );

        return { 
          "payload" : payload,
          "jwt"     : requestJWT[ 1 ]
        };
    except ExpiredSignatureError :
        return { "error" : "Expired Signature" };
    except JWTError:
        return { "error" : "Invalid Signature" };

async def get_current_user( token: str = Depends( oauth2_scheme ) ):
    credentials_exception = HTTPException(
        status_code = status.HTTP_401_UNAUTHORIZED,
        detail      = "Could not validate credentials",
        headers     = { "WWW-Authenticate": "Bearer" },
    );

    try:
        payload = jwt.decode(
            token      = token, 
            key        = amTool.cfg[ "auth" ][ "secret_key" ], 
            algorithms = [ amTool.cfg[ "auth" ][ "algorithm" ] ] 
        );

        amTool.log( "[ PAYLOAD ] : " + JSON.dumps( payload ) );
        username: str = payload.get( "sub" );
        if username is None:
            raise credentials_exception;

    except JWTError:
        raise credentials_exception;

    user = await myUser.getUserByEmail( email = username );

    if len( user ) <= 0 :
        raise credentials_exception;

    return user;

async def validateRequest( request , typeOfValidation ):
    response = {
     "isvalid" : False , 
     "error"   : ""
    };

    myMainsearchCriteria = "";

    for type in typeOfValidation : 
        if( type == "coords" ) : 
            if ( "longitude" in request.query_params and "latitude" in request.query_params ):
                myMainsearchCriteria = "coords";
        if( type == "locationid" ) :
            if ( "locationid" in request.query_params ) : 
                myMainsearchCriteria = "locationid";
        if( type == "parcelid" ) :
            if ( "parcelid" in request.query_params ) : 
                myMainsearchCriteria = "parcelid";

    if( myMainsearchCriteria == "" ):
        raise HTTPException( status_code = 400, detail = "Missing query parameters. At least one entity of parameters from ( "+" , ".join( typeOfValidation )+" ) must be provided." );

async def isValidApp( request: Request , credentials  : HTTPBasicCredentials = Depends( security ) ):
    appCredentials  = getCredentials( credentials );
    myOptions       = dict();
    myOptions[ "un" ] = appCredentials[ "username" ];
    myOptions[ "pw" ] = appCredentials[ "password" ];

    hasAppAccess = await amAccess.getAppAccess( myOptions );

    if( hasAppAccess is False ):
        raise HTTPException( status_code = 401 );
    else:
        # await amAccess.logSession( hasAppAccess[ 0 ][ "user_id" ] , request );
        return True;

async def isAuthorized( request: Request ):

    amTool.log( "[ Verification : Step 1 ] : Checking if needed Headers are available in Request" );
    if "authorization" in request.headers:
        requestJWT      = request.headers[ "authorization" ].split();
    else:
        amTool.log( "[ Verification - ERROR : Step 1 ] : Incorrect Headers. Token Missing" );
        raise HTTPException( 
            status_code = status.HTTP_400_BAD_REQUEST, 
            detail      = "Incorrect Headers. Token Missing.",
            headers     = { "WWW-Authenticate": "Bearer" }
        );

    if len( requestJWT ) != 2 : 
        amTool.log( "[ Verification - ERROR : Step 1 ] : Incorrect Headers" );
        raise HTTPException( 
            status_code = status.HTTP_400_BAD_REQUEST, 
            detail      = "Incorrect Headers",
            headers     = { "WWW-Authenticate": "Bearer" }
        );

    myDecodedJWT = decodeJWTFromHeader( request );

    amTool.log( "[ Verification ] : Retrieving ApiKey from Database" );

    apiKeyRecord = await myModel.getApikeyOfUser( myDecodedJWT[ "payload" ][ "sub" ] );

    if "error" in apiKeyRecord : # return
        amTool.log( "[ ERROR - Verification ] : Api Key failed to be retrieved " , apiKeyRecord[ "error" ] );
        raise HTTPException( 
          status_code  = status.HTTP_400_BAD_REQUEST , 
          detail       = "Could not Retrieve API Key from Database" ,
          headers      = { "WWW-Authenticate": "Bearer" } 
        );

    if len( apiKeyRecord ) <= 0 : # return
        amTool.log( "[ ERROR - Verification ] : No Api Key in Database" );
        raise HTTPException( 
          status_code  = status.HTTP_400_BAD_REQUEST ,
          detail       = "No Api Key present in the Database" ,
          headers      = { "WWW-Authenticate": "Bearer" } 
        );

    serverToken  = apiKeyRecord[ 0 ][ "apikey" ];
    amTool.log( "[ Verification ] : Api Key sucesfully retrieved : \r\n'" + str( serverToken ) + "'" );
    amTool.log( "JWT : " + requestJWT[ 1 ] );

    try:
        payload = jwt.decode( 
            token      = requestJWT[ 1 ], 
            key        = serverToken, 
            algorithms = [ "HS256" ] 
        );
        amTool.log( "[ PAYLOAD ]" , payload );
        username: str = payload.get( "sub" );
        if username is None:
            raise HTTPException(
                status_code = status.HTTP_401_UNAUTHORIZED,
                detail      = "Could not validate credentials",
                headers     = { "WWW-Authenticate": "Bearer" }
            );

        amTool.log( "[ Verification ] : JWT Succesfully Decoded with the use of the Pre Shared Secret Key. It is properly signed from Directory Listing." );

    except JWTError as e:
        amTool.log( "[ ERROR - JWT ] : " + str( e ) );
        print( repr( e ) );
        raise HTTPException(
            status_code = status.HTTP_401_UNAUTHORIZED,
            detail      = "Could not validate credentials",
            headers     = { "WWW-Authenticate": "Bearer" }
        );

    return payload;

@app.middleware("http")
async def add_process_time_header( request: Request, call_next ):
    startTime = time.time();
    print();
    print( "-------------------------------------------------------" );
    amTool.log( "[ INCOMING REQUEST : START ] : \"" + str( request.method ) + "\"" + " " + str( request.url ) );
    # amTool.log( "[ HEADERS ] " + JSON.dumps( request.headers ) );
    response  = await call_next( request );
    endTime   = time.time();
    totalTime = endTime - startTime;
    response.headers[ "X-Process-Time" ] = str( totalTime );
    amTool.log( "[ RESPONSE : ] " , response );
    amTool.log( "[ INCOMING REQUEST : END   ] : \"" + str( request.method ) + "\"" + " " + str( request.url ) + " (Code:" + str( response.status_code ) + ")" + " [Duration : " + str( round( totalTime ,3 ) ) + "]" );
    print( "-------------------------------------------------------" );
    print();
    return response;


# Initialization EndPoints

@app.get( "/administration/create_db_tables", 
          tags         = [ "Service Initialization" ] , 
          summary      = "Create basic Database Tables",
          description  = "Create the initial tables required for inserting user Data. This is an action that only needs to be performed once at the initial installation" 
        ) 
async def createDBTables(
              token : str
          ):

    if token != serverSecretKey:
         return [];

    # Reads the SourceFile JSON and creates one Table for each entity defined in there.
    mySchema = amTool.cfg[ "database" ][ "schema" ];

    try:
        myInputsJSON = amTool.load( "" , DBsourceFileName , "json" );
        myWholeQuery = [];
        mySchema     = amTool.cfg[ "database" ][ "schema" ];

        myForeignKeys = [];
        for entity in myInputsJSON[ "inputs" ] :
            myUniques     = [];
            myComposites  = [];
            myQuery  = "DROP TABLE IF EXISTS " + str( mySchema ) + "." + str( entity ).lower() + ";";
            myQuery += "CREATE TABLE " + str( mySchema ) + "." + str( entity ).lower() + " ( ";
            myTempQuery = [];
            myTempQuery.append( "id serial NOT NULL" );
            myTempQuery.append( "created_on timestamp NOT NULL DEFAULT now()" );
            for item in myInputsJSON[ "inputs" ][ entity ]:
                defaultClause = "";
                defaultValue  = "";
                if( "default" in item and item[ "default" ] != "false" ) :
                    if( str( item[ "type" ] ) != "integer" and str( item[ "type" ] ) != "numeric" ):
                        defaultValue = "('" + str( item[ "default" ] ) + "'::" + str( item[ "type" ] ) + ")";
                    else:
                        defaultValue = "("  + str( item[ "default" ] ) + "::"  + str( item[ "type" ] ) + ")";

                    defaultClause = " NOT NULL DEFAULT (" + defaultValue + ")";    
                else:
                    defaultClause = " NULL ";

                myTempQuery.append( str( item[ "name" ] ) + " " + str( item[ "type" ] ) + str( defaultClause ) );

                if "unique" in item and item[ "unique" ] == "true" :
                    myUniques.append( "CONSTRAINT " + str( item[ "name" ] ) + "_unique UNIQUE ( " + str( item[ "name" ] ) + ")" );

                if "composite" in item and item[ "composite" ] == "true" :
                    myComposites.append( item[ "name" ] );

                if "fkey" in item :
                    myPair         = item[ "fkey" ].split( "." );
                    mySourceTable  = myPair[ 0 ];
                    mySourceColumn = myPair[ 1 ];
                    myTargetTable  = entity;
                    myTargetColumn = item[ "name" ];
                    myForeignKeys.append(
                                 "ALTER TABLE " + 
                                 "" + str( mySchema ) + "." + str( myTargetTable ) + " ADD CONSTRAINT fk_" + str( myTargetColumn ) + " " + 
                                 "FOREIGN KEY (" + str( myTargetColumn ) + ") REFERENCES " + 
                                 "" + str( mySchema ) + "." + str( mySourceTable ) + "(" + str( mySourceColumn ) + ") ON DELETE CASCADE;"
                                );

            myQuery += ",\r\n".join( myTempQuery );
            myQuery += ",PRIMARY KEY (id) ";
            if( len( myUniques ) > 0 ) : 
                myQuery += "," + ",\r\n".join( myUniques );
            myQuery += ");";
            myWholeQuery.append( myQuery );

            if( len( myComposites ) > 0 ) : 
                myWholeQuery.append( "CREATE UNIQUE INDEX " + str( entity ) + "_multi on " + str( mySchema ) + "." + str( entity ).lower() + " ( " + ",".join( myComposites ) + ")" );

        myQueryWithForeignKeys = myWholeQuery + myForeignKeys;
        for aQuery in myQueryWithForeignKeys:
            amTool.log( "------------------------" );
            amTool.log( aQuery );
            myModel.executeQuery( aQuery , None , "update" );
        
        if "defaultdata" in myInputsJSON : 
            for tableName in myInputsJSON[ "defaultdata" ] :
                myTableColumns = [];
                for columnName in myInputsJSON[ "inputs" ][ tableName ] : 
                    myTableColumns.append( columnName[ "name" ] );

                for dataRecordToInsert in myInputsJSON[ "defaultdata" ][ tableName ] : 
                    myValues = [];
                    for aProperty in myTableColumns : 
                        myValues.append( dataRecordToInsert[ aProperty ] );
                        
                    myInsertQuery = (
                        "insert into  " + str( mySchema ) + "." + str( tableName ) + 
                        " ( " + " , ".join( myTableColumns ) + " ) " + 
                        " VALUES " + 
                        " ( '" + "' , '".join( myValues ) + "' ) RETURNING ID"
                    );
                    
                    myModel.executeQuery( myInsertQuery , None , "insert" );
                    
                    print( myInsertQuery );

    except( Exception ) as error :
        print( error );

@app.get( "/administration/create_input_models", 
          tags         = [ "Service Initialization" ] , 
          summary      = "Create our Input BaseModels",
          description  = "Create our Input BaseModels" 
        ) 
async def createInputModels(
              token : str
          ):

    if token != serverSecretKey:
         return [];
  # Loads the SourceFile JSON and creates our BaseModels that will populate the Inputs for each EndPoint created
    targetFileName = "models/inputs.py";
    myFile         = [];

    try:
        myInputsJSON = amTool.load( "" , DBsourceFileName , "json" );
        amTool.log( "File has been opened" );
        amTool.log( myInputsJSON );

        f = open( targetFileName, "w" );
        f.write( "import time;\r\n" );
        f.write( "import datetime;\r\n" );
        f.write( "from datetime import timedelta;\r\n" );
        f.write( "from typing import Optional , List;\r\n" );
        f.write( "from enum import Enum;\r\n" );
        f.write( "from pydantic import BaseModel , Field , create_model , ConfigDict;\r\n" );
        f.write( "\r\n" );

        for entity in myInputsJSON[ "inputs" ] :
            myEntityClass    = [];
            myEntityClass.append( "class " + entity + "( BaseModel ):\r\n" );
            myEntityClass.append( "\t\t\t\tid         : Optional[ int ]             = Field( None , description = \"\" );\r\n" );
            myEntityClass.append( "\t\t\t\tcreated_on : Optional[ datetime.datetime ] = Field( None , description = \"\" );\r\n" );

            myEntityClassPut = [];
            myEntityClassPut.append( "class put_" + entity + "( BaseModel ):\r\n" );

            myEntityClassGet = [];
            myEntityClassGet.append( "class get_" + entity + "( BaseModel ):\r\n" );
            myEntityClassGet.append( "\t\t\t\tid     : Optional[ int ] = Field( None , description = \"\" );\r\n" );
            myEntityClassGet.append( "\t\t\t\tlimit  : Optional[ int ] = Field( None , description = \"\" );\r\n" );
            myEntityClassGet.append( "\t\t\t\toffset : Optional[ int ] = Field( None , description = \"\" );\r\n" );

            for item in myInputsJSON[ "inputs" ][ entity ] :

                myType = "str";
                if str( item[ "type" ] ) == "numeric" : 
                    myType = "float";
                elif str( item[ "type" ] ) == "varchar" : 
                    myType = "str";
                elif str( item[ "type" ] ) == "timestamp" : 
                    myType = "datetime.datetime";
                else:
                    myType = "int";

                if item[ "filter" ] == "true" and myType == "datetime.datetime" : 
                    myEntityClassGet.append( "\t\t\t\t" + str( item[ "name" ] ).lower() + "_from_date : Optional[ datetime.datetime ] = Field( None , description = \"\" );\r\n" );
                    myEntityClassGet.append( "\t\t\t\t" + str( item[ "name" ] ).lower() + "_to_date   : Optional[ datetime.datetime ] = Field( None , description = \"\" );\r\n" );
                    myEntityClass.append( "\t\t\t\t" + str( item[ "name" ] ).lower() + " : Optional[ datetime.datetime ] = Field( None , description = \"\" );\r\n" );
                else:
                    myEntityClass.append( 
                       (
                         "\t\t\t\t" + 
                             str( item[ "name" ] ).lower() + " : " + 
                             "Optional[ " + str( myType ) + " ] " + 
                             "= " + 
                             "Field( None , description = \"" + str( item[ "desc" ] )  + "\" );\r\n" 
                       )
                    );

                myEntityClassPut.append( 
                   (
                     "\t\t\t\t" + 
                         str( item[ "name" ] ).lower() + " : " + 
                         "Optional[ " + str( myType ) + " ] " + 
                         "= " + 
                         "Field( None , description = \"" + str( item[ "desc" ] )  + "\" );\r\n" 
                   )
                );

                if item[ "filter" ] == "true" :
                    myEntityClassGet.append( 
                       ( 
                         "\t\t\t\t" + 
                             str( item[ "name" ] ).lower() + " : " + 
                             "Optional[ " + str( myType ) + " ] " + 
                             "= " + 
                             "Field( None , description = \"" + str( item[ "desc" ] )  + "\" );\r\n" 
                       )
                    );

            myEntityClassGet.append( "\t\t\t\t" + "model_config = ConfigDict( extra = 'allow' );\r\n" );
            myEntityClassGet.append( "\r\n\r\n" );
            myFile.append( "".join( myEntityClassGet ) );

            myEntityClass.append( "\t\t\t\t" + "model_config = ConfigDict( extra = 'allow' );\r\n" );
            myEntityClass.append( "\r\n\r\n" );
            myFile.append( "".join( myEntityClass ) );

            myEntityClassPut.append( "\t\t\t\t" + "model_config = ConfigDict( extra = 'allow' );\r\n" );
            myEntityClassPut.append( "\r\n\r\n" );
            myFile.append( "".join( myEntityClassPut ) );

        for row in myFile : 
            f.writelines( row );

        f.close();

    except( Exception ) as error :
        print( error );

@app.get( "/administration/clear_everything", 
          tags         = [ "Service Initialization" ] , 
          summary      = "Clear all Data so that procedures can be run again. This is for Dev purposes.",
          description  = "Clear all Data so that procedures can be run again. This is for Dev purposes."
        ) 
async def clearAllData(
              token : str
          ):

    if token != serverSecretKey:
         return [];
  # Loads the SourceFile JSON and creates our BaseModels that will populate the Inputs for each EndPoint created
    targetFileName = "models/inputs.py";
    myFile         = [];

    try:
        result = await myModel.clearAllData();
        return { "res" : "success" };
    except( Exception ) as error :
        print( error );
 

# Dynamicaly created EndPoints (Based on DBsourceFileName)

myJSONSchema  = amTool.load( "" , DBsourceFileName , "json" );
myIterator    = iter( myJSONSchema[ "inputs" ] );
# loopCompleted = False if debug is False else True;
loopCompleted = True;

while not loopCompleted : 
    try:
        item = next( myIterator );
        def preventLastBind( item = item ):
            try:
                eval( "Inputs." + item );
                @app.put( "/" + item, 
                          tags           = [ item ] , 
                          summary        = "Add Item",
                          description    = "Add Item"
                        ) 
                async def addContext( 
                    request : List[ eval( "Inputs.put_" + item ) ] , 
                    current_user = Depends( get_current_user ) 
                ):
                    myResponse = [];

                    if( cfg[ "settings" ][ "debug" ] is True ):
                        print( "" , flush=True );

                    request.user_id = current_user[ 0 ][ "user_id" ];
                    amTool.log( "--- Adding Record to Database ---" , cfg[ "settings" ][ "debug" ] );
                    myResponse = await myModel.addEntity( request , type=None , entity=item.lower() );

                    return myResponse;

                @app.get( "/" + item, 
                          tags         = [ item ] , 
                          response_model = List[ eval( "Inputs." + item ) ] ,
                          summary      = "Retrieve Item",
                          description  = "Retrieve Item"
                        )
                async def getContext(
                  params : eval( "Inputs.get_" + item ) = Depends() ,
                  current_user = Depends( get_current_user ) 
                ):
                    myResponse = [];

                    if( cfg[ "settings" ][ "debug" ] is True ):
                        print( "" , flush=True );

                    # params.user_id = current_user[ 0 ][ "user_id" ];
                    amTool.log( "--- Getting Record from Database ---" , cfg[ "settings" ][ "debug" ] );
                    myResponse = await myModel.getEntityAllProperties( params , entity=item.lower() );

                    return myResponse;

                @app.delete( "/" + item, 
                          tags         = [ item ] , 
                          summary      = "Delete Item",
                          description  = "Delete Item"
                        ) 
                async def deleteContext( 
                  id           : str ,
                  current_user = Depends( get_current_user ) 
                ):
                    myResponse = [];

                    if( cfg[ "settings" ][ "debug" ] is True ):
                        print( "" , flush=True );

                    amTool.log( "--- Deleting Record from Database ---" , cfg[ "settings" ][ "debug" ] );
                    myResponse = await myModel.deleteEntity( id=parseStringIntoArrayWithIntegers( id ) , entity=item.lower() );

                    return myResponse;

                @app.post( "/" + item, 
                          tags         = [ item ] , 
                          summary      = "Update Item",
                          description  = "Update Item"
                        ) 
                async def updateContext( 
                    request : eval( "Inputs." + item ) ,
                    current_user = Depends( get_current_user ) 
                ):
                    myResponse = [];

                    if( cfg[ "settings" ][ "debug" ] is True ):
                        print( "" , flush=True );

                    amTool.log( "--- Updating Record in Database ---" , cfg[ "settings" ][ "debug" ] );
                    myResponse = await myModel.updateEntity( request=request , entity=item.lower() );

                    return myResponse;
            except( Exception ) as error :
                print( "Nothing Found : " + str( item ) );

        preventLastBind();

    except StopIteration:
        loopCompleted = True;


# Helpers

def createSubscriptionToken( expirationDate ):
    myHash     = "^8z3eARc4X3VL29Bjo8ebbwShbLaYK2AdSvd$B$TApDXW&Sh#3i%4ZmXm$&H28@d!GzqMTqHXo6KBFN$vm$Ynm2qVue3G2&bTA4g444vt2CREnKKGTAa7@XuAgfu6y$c";
    myUUID     = str( uuid.uuid4() );
    myPassword = str( expirationDate ) + str( myUUID );
    myString   = ( str( myHash ) + str( myPassword ) ).encode('UTF-8');
    myToken    = hashlib.sha256( myString ).hexdigest();

    return myToken;

async def sendInformationToProvider( apikey , endPoint , consumerEmail , consumerToken = None , datasetId = None , expiration = None , method = "post" ):
    async with aiohttp.ClientSession() as session:

        amTool.log( "Sending Data to Provider" );
        myPost = {
            "sub"        : "farmtopia_directory_listing",
            "token"      : consumerToken ,
            "consumer"   : consumerEmail ,
            "dataset"    : datasetId ,
            "expiration" : expiration 
        }

        myJWT = jwt.encode( myPost, apikey, algorithm = 'HS256' );
        amTool.log( "JWT Created for Directory to Provider Authentication using the ApiKey of the Provider" , myJWT );

        amTool.log( "Sending Request with attached JWT : " + endPoint  , myPost );
        if( method == "post" ):
            async with session.post( 
                          endPoint , 
                          json    = myPost , 
                          # params  = myPost , 
                          headers = { 
                              'content-type'  : 'application/json' , 
                              'accept'        : 'application/json' , 
                              'Authorization' : 'Bearer ' + myJWT 
                          }
                       ) as response:

                 # amTool.log( "Retrieved Response from " + endPoint );
                 myText = await response.text();

                 try:
                     myJSON = JSON.loads( myText );
                     return {
                       "status_code" : response.status,
                       "message"     : myJSON
                     };
                     # return myJSON;
                     # myJSON = JSON.loads( myText );
                     # return myJSON;
                 except ValueError as error :
                     print( error );
                     return { "error" : "There was an issue retrieving data." };
        if( method == "put" ):
            async with session.put( 
                          endPoint , 
                          json    = myPost , 
                          # params  = myPost , 
                          headers = { 
                              'content-type'  : 'application/json' , 
                              'accept'        : 'application/json' , 
                              'Authorization' : 'Bearer ' + myJWT 
                          }
                       ) as response:

                 # amTool.log( "Retrieved Response from " + endPoint );
                 myText = await response.text();

                 try:
                     # myJSON = JSON.loads( myText );
                     # return myJSON;
                     myJSON = JSON.loads( myText );
                     return {
                       "status_code" : response.status,
                       "message"     : myJSON
                     };
                     # return myJSON;
                 except ValueError as error :
                     print( error );
                     return { "error" : "There was an issue retrieving data." };      
        elif( method == "get" ): 
            async with session.get( 
                          endPoint , 
                          json    = myPost , 
                          # params  = myPost , 
                          headers = { 
                              'content-type'  : 'application/json' , 
                              'accept'        : 'application/json' , 
                              'Authorization' : 'Bearer ' + myJWT 
                          }
                       ) as response:

                 # amTool.log( "Retrieved Response from " + endPoint );
                 myText = await response.text();
                 # print( response.version );
                 # print( response.status );
                 # print( response.reason );
                 # print( response.ok );
                 # print( response.method );
                 # print( response.url );
                 # print( response.real_url );
                 # print( response.connection );
                 # print( response.content );
                 # print( response.headers );
                 # print( response.raw_headers );
                 # print( response.content_type );
                 # print( response.request_info );

                 try:
                     myJSON = JSON.loads( myText );
                     return {
                       "status_code" : response.status,
                       "message"     : myJSON
                     };
                     # return myJSON;
                 except ValueError as error :
                     print( error );
                     return { "error" : "There was an issue retrieving data." };
        elif( method == "delete" ): 
            async with session.delete( 
                          endPoint , 
                          json    = myPost , 
                          # params  = myPost , 
                          headers = { 
                              'content-type'  : 'application/json' , 
                              'accept'        : 'application/json' , 
                              'Authorization' : 'Bearer ' + myJWT 
                          }
                       ) as response:

                 # amTool.log( "Retrieved Response from " + endPoint );
                 myText = await response.text();

                 try:
                     myJSON = JSON.loads( myText );
                     return {
                       "status_code" : response.status,
                       "message"     : myJSON
                     };
                     # return myJSON;
                     # myJSON = JSON.loads( myText );
                     # return myJSON;
                 except ValueError as error :
                     print( error );
                     return { "error" : "There was an issue retrieving data." };

async def retrieveOpenApiJsonFromEndPoint( EndPoint ):
    async with aiohttp.ClientSession() as session:

        amTool.log( "Retrieving OpenAPI json" );
        async with session.get( 
                      EndPoint , 
                      headers = { 
                          'content-type'  : 'application/json' , 
                          'accept'        : 'application/json' 
                      }
                   ) as response:

             myText = await response.text();

             try:
                 myJSON = JSON.loads( myText );
                 return myJSON;
             except ValueError as error :
                 print( error );
                 return { "error" : "There was an issue retrieving data." };

async def getProviderTokenFromDatasetId( dataset_id ):
    amTool.log( "" );
    amTool.log( "[ DIRECTORY ] : Retrieve the Dataset from Dataset with ID [" + str( dataset_id ) + "] for Token" );
    datasetObject = await myModel.getEntityAllProperties(
        params = Inputs.get_dataset(
            id     = dataset_id , 
            limit  = None , 
            offset = None 
        ) , 
        entity = "dataset" 
    );

    providerUserId       = datasetObject[ 0 ][ "user_id" ];
    amTool.log( "[ DIRECTORY ] : Retrieved the following Properties : " );
    amTool.log( "[ DIRECTORY ] : >>> providerUserId : " + str( providerUserId ) );
    
    providerToken = await myUser.checkUserToken( providerUserId );
    return providerToken[ 0 ][ "apikey" ];

async def createTokenForConsumerToProviderCommunication( consumer_email , dataset_id ):
    try:
        amTool.log( "" );
        amTool.log( "[ DIRECTORY ] : Create Communication JWT for peers" );
        consumerToken = myAuth.create_access_token(
            data          = { 
                "sub"        : consumer_email ,
                "dataset_id" : dataset_id
            }, 
            expires_delta = amTool.cfg[ "auth" ][ "expire_minutes" ],
            secret_key    = await getProviderTokenFromDatasetId( dataset_id )
        );
        amTool.log( "Created Token for Consumer <> Provider communication" , consumerToken );
        time.sleep( 1 ); # This is just for logging purposes , can be safely removed
        return consumerToken;
    except( Exception ) as error :
        return "";

async def userHasSubscriptionToDataset( user_id , dataset_id ):
    userHasMapping = await myModel.getEntityAllProperties(
        params = Inputs.get_map_user_to_dataset(
            user_id    = user_id , 
            dataset_id = dataset_id , 
            limit      = None , 
            offset     = None 
        ) , 
        entity = "map_user_to_dataset" 
    );

    if "error" in userHasMapping or len( userHasMapping ) <= 0 :
        raise HTTPException(
            status_code = 401, 
            detail      = "User has not requested any subscription to specific dataset"
        );
    else:
        amTool.log( "User [" + str( user_id ) + "] has Mapping to subscriptions for specific Dataset [" + str( dataset_id ) + "]" );
        return userHasMapping;

def handleException( error ):
    amTool.log( "[ ERROR ] : " , error );
    if isinstance( error , HTTPException ) :
        raise HTTPException(
          status_code = error.status_code, 
          detail      = error.detail
        );
    elif isinstance( error , IndexError ):
        raise HTTPException(
          status_code = 401, 
          detail      = "Wrong DataSet ID"
        );
    else:
        raise HTTPException(
          status_code = 500, 
          detail      = "Issue with Server , check Logs"
        );

# Local hosted Swagger 

@app.get( "/docs", include_in_schema = False )
async def custom_swagger_ui_html():
    return get_swagger_ui_html(
        openapi_url         = app.openapi_url,
        title               = app.title + " - Swagger UI",
        oauth2_redirect_url = app.swagger_ui_oauth2_redirect_url,
        swagger_js_url      = "/static/swagger-ui-bundle.js",
        swagger_css_url     = "/static/swagger-ui.css"
    );

@app.get( app.swagger_ui_oauth2_redirect_url, include_in_schema = False )
async def swagger_ui_redirect():
    return get_swagger_ui_oauth2_redirect_html();

@app.get( "/redoc", include_in_schema = False )
async def redoc_html():
    return get_redoc_html(
        openapi_url  = app.openapi_url,
        title        = app.title + " - ReDoc",
        redoc_js_url = "/static/redoc.standalone.js",
    );


# Administration Usage 

@app.put( "/administration/user", 
          tags         = [ "User Management" ] , 
          summary      = "",
          description  = "" 
        )
async def addUser(
             username : str , 
             password : str ,
             token    : str
          ):

    if token != serverSecretKey:
         return [];
    try:
        myResponse = await myUser.addUser( username , password );

        return myResponse;
    except( Exception ) as error :
        print( "There was an error " , error );
        return { "error" : "failed to create user" };

@app.get( "/administration/user" , 
          tags           = [ "User Management" ] , 
          summary        = "Get User ID from JWT",
          description    = "Get User ID from JWT"
        ) 
async def getUserByJWT(
              current_user = Depends( get_current_user ) 
          ):
    myResponse = [];

    if( cfg[ "settings" ][ "debug" ] is True ):
        print( "" , flush=True );

    return { "user_id" : current_user[ 0 ][ "user_id" ] };

@app.get( "/administration/userdata" , 
          tags           = [ "User Management" ] , 
          summary        = "Get User from Credentials",
          description    = "Get User from Credentials"
        )
async def getUserByCredentials(
             username : str , 
             password : str 
          ):
    try:
        myResponse = await myUser.authenticate( username , password );
        return myResponse;

    except( Exception ) as error :
        print( "There was an error " , error );
        return { "error" : "failed to authenticate user" };

@app.get( "/administration/shared_secret_key", 
          tags         = [ "User Management" ] , 
          summary      = "",
          description  = ( 
                           "Create a Shared Secret Key for Identifying requests originating from Directory. " + 
                           "Save it into your Database and compare it with any incoming Request from the Directory to verify the origin" + 
                           "This Shared Secret Key Token does not have an expiration date" + 
                           "It can be though generated at any time as many times as needed"
                          )
        )
async def createSharedSecretKeyForUser(
              current_user = Depends( get_current_user ) 
          ):
    try:
        user_id        = current_user[ 0 ][ "user_id" ];
        email          = current_user[ 0 ][ "email" ];
        expirationDate = datetime.date.today() + timedelta( days = 7 );
        apikey         = createSubscriptionToken( expirationDate.strftime( "%Y-%m-%d 00:00:00" ) + str( email ) + str( user_id ) );
        updateToken    = await myUser.updateUserToken( user_id , apikey );
        return { "apikey" : apikey }

    except( Exception ) as error :
        print( error );

@app.get( "/administration/has_secret_key", 
          tags         = [ "User Management" ] , 
          summary      = "",
          description  = ( "check if user has secret key" )
        )
async def checkSecretKeyExists(
              current_user = Depends( get_current_user ) 
          ):
    try:
        user_id    = current_user[ 0 ][ "user_id" ];
        checkToken = await myUser.checkUserToken( user_id );

        if len( checkToken ) <= 0 : 
            return { "res" : "false" };
        else:
            if checkToken[ 0 ][ "apikey" ] != "" and checkToken[ 0 ][ "apikey" ] is not None : 
                return { "res" : "true" };
            else:
                return { "res" : "false" };

    except( Exception ) as error :
        print( error );
        return { "error" : error };

@app.post( "/token" , 
           tags         = [ "User Management" ] , 
           summary      = "",
           description  = ( "Authenticate with the Directory and receive an appropriate JWT" )
         )
async def get_access_token( form_data: OAuth2PasswordRequestForm = Depends() ) -> ResModel.Token:
    user = await myAuth.authenticate_user( form_data.username, form_data.password );
    if not user:
        raise HTTPException(
            status_code = status.HTTP_401_UNAUTHORIZED,
            detail      = "Incorrect username or password",
            headers     = { "WWW-Authenticate": "Bearer" },
        );

    access_token = myAuth.create_access_token(
        data          = { "sub" : user[ "email" ] }, 
        expires_delta = amTool.cfg[ "auth" ][ "expire_minutes" ]
    );

    return ResModel.Token(
        access_token = access_token, 
        token_type   = "bearer"
    );


# Datasets

@app.post( "/administration/dataset" , 
          tags           = [ "Dataset Management" ] , 
          summary        = "Update a Dataset that the user is owner of.",
          description    = "Update a Dataset that the user is owner of."
        ) 
async def updateDataset(
              request    : eval( "Inputs.dataset" ) ,
              current_user = Depends( get_current_user )
          ):
    myResponse = [];
    dataset    = await myModel.getEntityAllProperties(
        params = Inputs.get_dataset(
            id      = request.dataset_id , 
            limit   = None , 
            offset  = None , 
            user_id = current_user[ 0 ][ "user_id" ]
        ) , 
        entity = "dataset" 
    );

    request.id = dataset[ 0 ][ "id" ];
    myResponse = await myModel.updateEntity( request=request , entity="dataset" );

    return myResponse;

@app.put( "/administration/dataset" , 
          tags           = [ "Dataset Management" ] , 
          summary        = "Add a Dataset to the Directory.",
          description    = "Add a Dataset to the Directory."
        ) 
async def addDataset(
              request      : List[ eval( "Inputs.put_dataset" ) ] , 
              current_user = Depends( get_current_user ) 
          ):
    myResponse = [];

    if( cfg[ "settings" ][ "debug" ] is True ):
        print( "" , flush=True );

    request[ 0 ].user_id = current_user[ 0 ][ "user_id" ];

    myResponse = await myModel.addEntity( request , type=None , entity="dataset" );

    return myResponse;

@app.get( "/administration/dataset", 
          tags           = [ "Dataset Management" ] , 
          summary      = "Retrieve Datasets that a user is owner of.",
          description  = "Retrieve Datasets that a user is owner of." 
        )
async def getDataset(
             request      : Request ,
             name         : Optional[ str ] = None ,
             current_user = Depends( get_current_user ) 
          ):
    try:
        dataset = await myModel.getEntityAllProperties(
            params = Inputs.get_dataset(
                name    = name , 
                limit   = None , 
                offset  = None , 
                user_id = current_user[ 0 ][ "user_id" ]
            ) , 
            entity = "dataset" 
        );

        return dataset;

    except( Exception ) as error :
        print( error );

@app.delete( "/administration/dataset" , 
          tags           = [ "Dataset Management" ] , 
          summary        = "Delete a Dataset that the user is owner of.",
          description    = "Delete a Dataset that the user is owner of."
        ) 
async def deleteDataset(
              dataset_id   : int ,
              current_user = Depends( get_current_user )
          ):
    myResponse = [];
    dataset    = await myModel.getEntityAllProperties(
        params = Inputs.get_dataset(
            id      = dataset_id , 
            limit   = None , 
            offset  = None , 
            user_id = current_user[ 0 ][ "user_id" ]
        ) , 
        entity = "dataset" 
    );

    if "error" not in dataset and len( dataset ) > 0 : 
        myResponse = await myModel.deleteEntity( entity="dataset" , id = [ str( dataset_id ) ] );
        return { "dataset_id" : dataset_id };
    else:
        return { "error" : "Invalid Dataset ID" };

@app.get( "/administration/datasets", 
          tags           = [ "Datasets" ] , 
          summary      = "Retrieve all Datasets in Directory.",
          description  = "Retrieve all Datasets in Directory." 
        )
async def getDatasets(
             current_user = Depends( get_current_user ) 
          ):
    try:
        dataset = await myModel.getEntityAllProperties(
            params = Inputs.get_dataset(
                name    = None , 
                limit   = None , 
                offset  = None 
            ) , 
            entity = "dataset" 
        );

        return dataset;

    except( Exception ) as error :
        print( error );


# Subscriptions [Provider EndPoints]

@app.post( "/administration/subscription/dataset_id/{dataset_id}", 
          tags         = [ "Provider Subscription Management" ] , 
          summary      = "Set the status of a Dataset subscription for a user",
          description  = ( "<b>Status Ids</b>" + 
                            "<ul>" + 
                                "<li>1 &nbsp;:&nbsp; Pending</li>" + 
                                "<li>2 &nbsp;:&nbsp; Declined</li>" + 
                                "<li>3 &nbsp;:&nbsp; Accepted</li>" + 
                                "<li>4 &nbsp;:&nbsp; Unsubscribed</li>" + 
                            "</ul>" 
                         )
        )
async def setSubscriptionStatusForUser(
             dataset_id    : int ,
             consumerEmail : EmailStr ,
             status_id     : int = Query( 1, ge = 1, le = 4 ) , 
             current_user  = Depends( get_current_user ) 
          ):
    try:
        # Check if User Requesting the change is owner of the Dataset
        # Retrieve the Dataset Object that the Consumer wants to change status
        amTool.log( "" );
        amTool.log( "[ DIRECTORY : STEP 1 ] : Retrieve the Dataset that the user wants to Update status." );
        datasetObject = await myModel.getEntityAllProperties(
            params = Inputs.get_dataset(
                id     = dataset_id , 
                limit  = None , 
                offset = None 
            ) , 
            entity = "dataset" 
        );

        if( len( datasetObject ) <= 0 ) :
            raise HTTPException(
                status_code = 404, 
                detail      = "Dataset not Found"
            );
        else:
            amTool.log( "Dataset with id " + str( dataset_id ) + " succesfully retrieved from directory." );

        # Check if Requesting User is the woner of the Dataset
        if( current_user[ 0 ][ "user_id" ] != datasetObject[ 0 ][ "user_id" ] ) : 
            raise HTTPException(
                status_code = 401, 
                detail      = "No Access to Dataset"
            );
        else:
            amTool.log( "User is owner of the Dataset" );

        # Try retrieving the Consumer Object from the provided email
        try:
            myConsumerObject = await myUser.getUserByEmail( consumerEmail );
        except( Exception ) as error :
            raise HTTPException(
                status_code = 400, 
                detail      = "User Does not exist"
            );
        
        amTool.log( "[ DIRECTORY : STEP 1.1 ] : User Retrieved." , myConsumerObject );

        # Check if User has requested a Subscription to specific Dataset
        userHasMapping = await myModel.getEntityAllProperties(
            params = Inputs.get_map_user_to_dataset(
                user_id    = myConsumerObject[ 0 ][ "user_id" ] , 
                dataset_id = dataset_id , 
                limit      = None , 
                offset     = None 
            ) , 
            entity = "map_user_to_dataset" 
        );

        if "error" in userHasMapping or len( userHasMapping ) <= 0 :
            raise HTTPException(
                status_code = 401, 
                detail      = "User has not requested any subscription to specific dataset"
            );
        else:
            amTool.log( "User [" + str( myConsumerObject[ 0 ][ "user_id" ] ) + "] has Mapping to subscriptions for specific Dataset [" + str( dataset_id ) + "]" );

        # If the Provider is activating the Subscription then we need to 
        # generate a random JWT that will be sent to the Provider and also be made available to the Consumer
        # That will be the JWT used by Consumer in order to authenticate with the Provider
        consumerToken = "";
        if status_id == 3 :
            try:
                amTool.log( "" );
                amTool.log( "[ DIRECTORY ] : Create Communication JWT for peers" );
                consumerToken = myAuth.create_access_token(
                    data          = { 
                        "sub"        : myConsumerObject[ 0 ][ "email" ] ,
                        "dataset_id" : dataset_id
                    }, 
                    expires_delta = amTool.cfg[ "auth" ][ "expire_minutes" ],
                    secret_key    = await getProviderTokenFromDatasetId( dataset_id )
                );

                amTool.log( "Created Token for Consumer <> Provider communication" , consumerToken );
                time.sleep( 1 ); # This is just for logging purposes , can be safely removed
            except( Exception ) as error :
                print( error );
                raise HTTPException(
                    status_code = 500, 
                    detail      = "Failed to generate JWT for consumer subscription."
                );

        # Update our Mapping with the correct data
        myResponse = await myModel.updateEntity( 
          request = Inputs.get_map_user_to_dataset(
                    id         = userHasMapping[ 0 ][ "id" ] ,
                    status     = status_id , 
                    token      = consumerToken
          ) , 
          entity = "map_user_to_dataset"
        );
        print( myResponse );

        if "error" in myResponse : 
            raise HTTPException(
                status_code = 500, 
                detail      = "Something went Wrong"
            );
        else:
            amTool.log( "Subscription status to Dataset with id [" + str( dataset_id ) + "] for user [" + str( myConsumerObject[ 0 ][ "user_id" ] ) + "] changed to [" + str( status_id ) + "]" );

        # return { "token" : consumerToken }
        return { "success" : "ok" }

    except( Exception ) as error :
        amTool.log( "[ ERROR - subscribeUser ] : " , error );
        if isinstance( error , HTTPException ) :
            raise HTTPException(
              status_code = error.status_code, 
              detail      = error.detail
            );
        elif isinstance( error , IndexError ):
            raise HTTPException(
              status_code = 401, 
              detail      = "Wrong DataSet ID"
            );
        else:
            raise HTTPException(
              status_code = 500, 
              detail      = "Issue with Server , check Logs"
            );

@app.get( "/administration/subscription/dataset_id/{dataset_id}", 
          tags         = [ "Provider Subscription Management" ] , 
          summary      = "Get the status of a Dataset subscription for a user",
          description  = ( "<b>Status Ids</b>" + 
                            "<ul>" + 
                                "<li>1 &nbsp;:&nbsp; Pending</li>" + 
                                "<li>2 &nbsp;:&nbsp; Declined</li>" + 
                                "<li>3 &nbsp;:&nbsp; Accepted</li>" + 
                            "</ul>" 
                         )
        )
async def getSubscriptionStatusForUser(
             dataset_id    : int ,
             consumerEmail : Optional[ EmailStr ] = None ,
             current_user  = Depends( get_current_user ) 
          ):
    try:
        # Check if user is the owner of the dataset requsting details
        amTool.log( "" );
        amTool.log( "[ DIRECTORY : STEP 1 ] : Retrieve the Dataset that the user wants." );
        datasetObject = await myModel.getEntityAllProperties(
            params = Inputs.get_dataset(
                id     = dataset_id , 
                limit  = None , 
                offset = None 
            ) , 
            entity = "dataset" 
        );

        if( current_user[ 0 ][ "user_id" ] != datasetObject[ 0 ][ "user_id" ] ) : 
            raise HTTPException(
                status_code = 401, 
                detail      = "No Access to Dataset"
            );
        else:
            amTool.log( "User is owner of the Dataset" );

        if consumerEmail is not None : 
            amTool.log( "[ DIRECTORY : STEP 2 ] : Specific user has been provided." );
            # Try retrieving the Consumer Object from the provided email
            try:
                myConsumerObject = await myUser.getUserByEmail( consumerEmail );
            except( Exception ) as error :
                raise HTTPException(
                    status_code = 400, 
                    detail      = "User Does not exist"
                );

            # Get the subscription status of specific user
            try:
                mySubscriptionStatus = await myModel.getDatasetSubscriptionStatusForUser( 
                    dataset_id = dataset_id , 
                    user_id    = myConsumerObject[ 0 ][ "user_id" ] 
                );
                amTool.log( "Status Retrieval : " , mySubscriptionStatus );
                return mySubscriptionStatus;
            except( Exception ) as error :
                raise HTTPException(
                    status_code = 500, 
                    detail      = "Something went wrong"
                );
        else:
            amTool.log( "[ DIRECTORY : STEP 2 ] : No user has been provided." );
            # Get the subscription status of specific user
            try:
                mySubscriptionStatus = await myModel.getDatasetSubscriptionStatus( 
                    dataset_id = dataset_id 
                );
                amTool.log( "Status Retrieval : " , mySubscriptionStatus );
                return mySubscriptionStatus;
            except( Exception ) as error :
                raise HTTPException(
                    status_code = 500, 
                    detail      = "Something went wrong"
                );        

    except( Exception ) as error :
        amTool.log( "[ ERROR - subscribeUser ] : " , error );
        if isinstance( error , HTTPException ) :
            raise HTTPException(
                status_code = error.status_code, 
                detail      = error.detail
            );
        elif isinstance( error , IndexError ):
            raise HTTPException(
                status_code = 401, 
                detail      = "Wrong DataSet ID"
            );
        else:
            raise HTTPException(
                status_code = 500, 
                detail      = "Issue with Server , check Logs"
            );


# Subscriptions [Consumer EndPoints]

@app.get( "/administration/user/subscriptions", 
          tags         = [ "Consumer Subscription Management" ] , 
          summary      = "Retrieve the Subscriptions of an authenticated user",
          description  = "Retrieve the Subscriptions of an authenticated user" 
        )
async def getAllSubscriptionsOfAuthenticatedUser(
             current_user = Depends( get_current_user ) 
          ):
    try:
        amTool.log( "" );
        amTool.log( "[ DIRECTORY ] : Retrieved subscriptions of User" );
        datasetObject = await myModel.getEntityAllProperties(
            params = Inputs.map_user_to_dataset(
                user_id = current_user[ 0 ][ "user_id" ] 
            ) , 
            entity = "map_user_to_dataset" 
        );

        return datasetObject;

    except( Exception ) as error :
        amTool.log( "[ ERROR - getSubscriptions ] : " , error );
        if isinstance( error , HTTPException ) :
            raise HTTPException(
                status_code = error.status_code, 
                detail      = error.detail
            );
        elif isinstance( error , IndexError ):
            raise HTTPException(
                status_code = 401, 
                detail      = "Resource not Found"
            );
        else:
            raise HTTPException(
                status_code = 500, 
                detail      = "Issue with Server , check Logs"
            );

@app.get( "/administration/user/subscription/dataset_id/{dataset_id}", 
          tags         = [ "Consumer Subscription Management" ] , 
          summary      = "Retrieve a specific Subscription status of an authenticated user",
          description  = "Retrieve a specific Subscription status of an authenticated user"
        )
async def getSubscriptionStatusOfAuthenticatedUser(
             dataset_id    : int ,
             current_user  = Depends( get_current_user ) 
          ):
    try:
        userHasMapping = await myModel.getEntityAllProperties(
            params = Inputs.get_map_user_to_dataset(
                user_id    = current_user[ 0 ][ "user_id" ] , 
                dataset_id = dataset_id , 
                limit      = None , 
                offset     = None 
            ) , 
            entity = "map_user_to_dataset" 
        );

        if "error" in userHasMapping:
            raise HTTPException(
                status_code = 401, 
                detail      = "User has not requested any subscription to specific dataset"
            );

        # Update the map_user_to_dataset table and set the status of the subscription to the provided one
        try:
            mySubscriptionStatus = await myModel.getDatasetSubscriptionStatusForUser( 
                dataset_id = dataset_id , 
                user_id    = current_user[ 0 ][ "user_id" ] 
            );
            amTool.log( "Status Retrieval : " , mySubscriptionStatus );
            return mySubscriptionStatus;
        except( Exception ) as error :
            raise HTTPException(
                status_code = 500, 
                detail      = "Something went wrong"
            );

    except( Exception ) as error :
        amTool.log( "[ ERROR - subscribeUser ] : " , error );
        if isinstance( error , HTTPException ) :
            raise HTTPException(
                status_code = error.status_code, 
                detail      = error.detail
            );
        elif isinstance( error , IndexError ):
            raise HTTPException(
                status_code = 401, 
                detail      = "Wrong DataSet ID"
            );
        else:
            raise HTTPException(
                status_code = 500, 
                detail      = "Issue with Server , check Logs"
            );

@app.post( "/administration/user/subscription/dataset_id/{dataset_id}", 
          tags         = [ "Consumer Subscription Management" ] , 
          summary      = "Subscribe a User to a Dataset",
          description  = "Subscribe a User to a Dataset" 
        )
async def subscribeUser(
             dataset_id   : int , 
             current_user = Depends( get_current_user ) 
          ):
    try:
        # Retrieve the Dataset Object that the Consumer wants to Subscribe to
        try:
            amTool.log( "" );
            amTool.log( "[ DIRECTORY : STEP 1 ] : Retrieve the Dataset that the user wants to subscribe to from Database" );
            datasetObject = await myModel.getEntityAllProperties(
                params = Inputs.get_dataset(
                    id     = dataset_id , 
                    limit  = None , 
                    offset = None 
                ) , 
                entity = "dataset" 
            );

            amTool.log( datasetObject );

            if( len( datasetObject ) <= 0 ) :
                raise HTTPException(
                    status_code = 401, 
                    detail      = "No Dataset with the specific Dataset ID"
                );

            providerUserId       = datasetObject[ 0 ][ "user_id" ];
            providerAuthEndpoint = datasetObject[ 0 ][ "subscription" ];
            amTool.log( "[ DIRECTORY : STEP 1 ] : Retrieved the following Properties : " );
            amTool.log( "[ DIRECTORY : STEP 1 ] : >>> providerUserId : " + str( providerUserId ) );
            amTool.log( "[ DIRECTORY : STEP 1 ] : >>> providerAuthEndpoint : " + str( providerAuthEndpoint ) );
        except( Exception ) as error :
            raise HTTPException(
                status_code = 500, 
                detail      = "Failed to retrieve Dataset Object"
            );

        # Retrieve the Provider Object to get the SECRET_KEY in order to sign the Request
        try:
            amTool.log( "" );
            amTool.log( "[ DIRECTORY : STEP 2 ] : Retrieve User Objects from Database" );
            myProviderUserObject = await myUser.getUserById( providerUserId );
            if( len( datasetObject ) <= 0 ) :
                raise HTTPException(
                    status_code = 401, 
                    detail      = "No Dataset with the specific Dataset ID"
                );
            amTool.log( "Retrieved User Object [ Provider : id = " + str( providerUserId ) + " ] "  , myProviderUserObject );
            if myProviderUserObject[ 0 ][ "apikey" ] is None : 
                raise HTTPException(
                    status_code = 502, 
                    detail      = "Provider has not created an Api Key"
                );
        except( Exception ) as error :
            raise HTTPException(
                status_code = 500, 
                detail      = "Failed to extract provider from Dataset record"
            );

        # Make the appropriate Request to the Provider so that the Consumer is added into the Providers database 
        try:
            amTool.log( "" );
            amTool.log( "[ DIRECTORY : STEP 3 ] : Send information to Provider" );
            myProviderResponse = await sendInformationToProvider(
                apikey        = myProviderUserObject[ 0 ][ "apikey" ] ,  # This is the Secret Key with which the requests are signed for communciation between Directory and Provider
                endPoint      = providerAuthEndpoint + "/dataset_id/" + str( dataset_id ) , # This is the EndPoint of the Provider that the Directory will send the request in order to subscribe the Consumer
                consumerToken = None , 
                consumerEmail = current_user[ 0 ][ "email" ] ,
                datasetId     = dataset_id , 
                expiration    = datetime.date.today().strftime( "%Y-%m-%d 00:00:00") , 
                method        = "put"
            );
        except( Exception ) as error :
            print( error );
            raise HTTPException(
                status_code = 500, 
                detail      = "Failed to send request to provider for subscription."
            );

        if "error" in myProviderResponse : 
            raise HTTPException(
              status_code = 502, 
              detail      = myProviderResponse[ "error" ]
            );
        elif "status_code" not in myProviderResponse : 
            raise HTTPException(
              status_code = 502, 
              detail      = "Failed Subscription try again later" 
            );
        else:
            amTool.log( "" );
            amTool.log( "[ DIRECTORY : STEP 5 ] : Received Response from Provider" );
            amTool.log( "Populating Directory Database with mapping and Token" );
            try :
                # check if a Record already exists so that we update it
                amTool.log( "" );
                amTool.log( "[ DIRECTORY : STEP 6 ] : check if we need Update or Insert" );
                hasToken = await myDirectory.tokenRecordExists( current_user[ 0 ][ "user_id" ] , dataset_id );
                expirationDate = datetime.date.today();

                if hasToken is not False and len( hasToken ) > 0 : 
                    amTool.log( "" );
                    amTool.log( "[ DIRECTORY : STEP 6 ] : Token already exists , UPDATING." );
                    addToDatabase = await myDirectory.updateTokenInDatabase(
                        consumer_id = current_user[ 0 ][ "user_id" ],
                        dataset_id  = dataset_id,
                        expiration  = expirationDate.strftime( "%Y-%m-%d 00:00:00"),
                        token       = ""
                    );
                else:
                    amTool.log( "" );
                    amTool.log( "[ DIRECTORY : STEP 6 ] : Token missing , INSERTING." );
                    addToDatabase = await myDirectory.addTokenInDatabase(
                        consumer_id = current_user[ 0 ][ "user_id" ],
                        dataset_id  = dataset_id,
                        expiration  = expirationDate.strftime( "%Y-%m-%d 00:00:00"),
                        token       = ""
                    );

                return { 
                  "res"   : "Request has been submitted for evaluation. Provider will send email when evaluation has been completed." 
                }

            except( Exception ) as error :
                amTool.log( "[ ERROR - myDirectory: addTokenInDatabase || updateTokenInDatabase ] : " , error );
                raise HTTPException(
                    status_code = 502, 
                    detail      = "Failed to add token to Database"
                );

    except( Exception ) as error :
        amTool.log( "[ ERROR - subscribeUser ] : " , error );
        if isinstance( error , HTTPException ) :
            raise HTTPException(
                status_code = error.status_code, 
                detail      = error.detail
            );
        elif isinstance( error , IndexError ):
            raise HTTPException(
                status_code = 401, 
                detail      = "Wrong DataSet ID"
            );
        else:
            raise HTTPException(
                status_code = 500, 
                detail      = "Issue with Server , check Logs"
            );

@app.post( "/administration/user/unsubscribe/dataset_id/{dataset_id}", 
          tags         = [ "Consumer Subscription Management" ] , 
          summary      = "Remove a user from a Subscription",
          description  = "Remove a user from a Subscription" 
        )
async def unsubscribeUser(
             dataset_id   : int , 
             current_user = Depends( get_current_user ) 
          ):
    try:

        # Check if the Dataset that the Consumer wants to Subscribe to exists
        try:
            amTool.log( "" );
            amTool.log( "[ DIRECTORY : STEP 1 ] : Retrieve the Dataset that the user wants to unsubscribe from" );
            datasetObject = await myModel.getEntityAllProperties(
                params = Inputs.get_dataset(
                    id     = dataset_id , 
                    limit  = None , 
                    offset = None 
                ) , 
                entity = "dataset" 
            );

            if( len( datasetObject ) <= 0 ) :
                raise HTTPException(
                    status_code = 401, 
                    detail      = "No Dataset with the specific Dataset ID"
                );
        except( Exception ) as error :
            raise HTTPException(
                status_code = 500, 
                detail      = "Failed to find Dataset from dataset id"
            );

        # Check if User has a subscription mapping for specific dataset
        try:
            amTool.log( "[ DIRECTORY : STEP 2 ] : Check if User has Subscription for Dataset with id [" + str( dataset_id ) + "]" );
            userHasMapping = await myModel.getEntityAllProperties(
                params = Inputs.get_map_user_to_dataset(
                    user_id    = current_user[ 0 ][ "user_id" ] , 
                    dataset_id = dataset_id , 
                    limit      = None , 
                    offset     = None 
                ) , 
                entity = "map_user_to_dataset" 
            );

            if "error" in userHasMapping:
                raise HTTPException(
                    status_code = 401, 
                    detail      = "User has not requested any subscription to specific dataset"
                );
        except( Exception ) as error :
            raise HTTPException(
                status_code = 500, 
                detail      = "Failed to extract subscription for user"
            );

        # Update the Subscription to the new status 
        try:
            amTool.log( "[ DIRECTORY : STEP 3 ] : Updating subscription to Unsubscribed for Dataset with id [" + str( dataset_id ) + "]" );
            myResponse = await myModel.updateEntity( 
              request = Inputs.get_map_user_to_dataset(
                        id         = userHasMapping[ 0 ][ "id" ] ,
                        status     = 4 , 
                        token      = ""
              ) , 
              entity = "map_user_to_dataset"
            );

            if "error" in myResponse : 
                raise HTTPException(
                    status_code = 500, 
                    detail      = "Failed to unsubscribe user"
                );
            else:
                return { "success" : "ok" };
        except( Exception ) as error :
            raise HTTPException(
                status_code = 500, 
                detail      = "Something went wrong updating entity"
            );

    except( Exception ) as error :
        amTool.log( "[ ERROR - subscribeUser ] : " , error );
        if isinstance( error , HTTPException ) :
            raise HTTPException(
                status_code = error.status_code, 
                detail      = error.detail
            );
        elif isinstance( error , IndexError ):
            raise HTTPException(
                status_code = 401, 
                detail      = "Wrong DataSet ID"
            );
        else:
            raise HTTPException(
                status_code = 500, 
                detail      = "Issue with Server , check Logs"
            );

@app.post( "/administration/user/subscription/dataset_id/{dataset_id}/update", 
          tags         = [ "Consumer Subscription Management" ] , 
          summary      = "Retrieve a new Updated Subscription Token for use with the Provider.",
          description  = "Retrieve a new Updated Subscription Token for use with the Provider."
        )
async def updateSubscriptionToken(
             dataset_id   : int , 
             current_user = Depends( get_current_user ) 
          ):
    try:

        # User must have already a Subscription in the Directory to specific Dataset to be able to update the Token
        userHasMapping = await userHasSubscriptionToDataset( 
            user_id    = current_user[ 0 ][ "user_id" ] , 
            dataset_id = dataset_id
        );

        # This is the JWT that the Consumer will use to contact and authenticate against the provider
        consumerToken = await createTokenForConsumerToProviderCommunication( 
            consumer_email = current_user[ 0 ][ "email" ] , 
            dataset_id     = dataset_id
        );

        try:
            # Update our Mapping with the newly created Token
            myResponse = await myModel.updateEntity( 
              request = Inputs.get_map_user_to_dataset(
                        id         = userHasMapping[ 0 ][ "id" ] ,
                        token      = consumerToken
              ) , 
              entity = "map_user_to_dataset"
            );

            if "error" in myResponse : 
                raise HTTPException(
                    status_code = 500, 
                    detail      = "Something went Wrong"
                );
            else:
                amTool.log( "Subscription token updated to Dataset with id [" + str( dataset_id ) + "] for user [" + str( current_user[ 0 ][ "user_id" ] ) + "]" );

            return { "token" : consumerToken }
        except( Exception ) as error :
            print( error );
            raise HTTPException(
                status_code = 500, 
                detail      = "Something went Wrong."
            );

    except( Exception ) as error :
        handleException( error );


# EndPoint Validations

@app.get( "/administration/validate", 
          tags         = [ "EndPoint Validations" ] , 
          summary      = "",
          description  = "" 
        )
async def validateOpenAPI(
              datasetid : int , 
             # request      : Request ,
             # name         : Optional[ str ] = None ,
             # current_user = Depends( get_current_user ) 
             token    : str
          ):

    if token != serverSecretKey:
         return [];
    try:
        openAPI = [ "http://" + str( myIP ) + ":8686/openapi.json" , "http://192.168.1.9:8686/openapi.json" , "https://farmtopia-fb-api.dih-agrifood.com/swagger/v1/swagger.json" ];
        index   = 0;

        try:
            dataset = await myModel.getEntityAllProperties(
                params = Inputs.get_dataset(
                    id     = datasetid , 
                    limit  = None , 
                    offset = None 
                ) , 
                entity = "dataset" 
            );

        except( Exception ) as error :
            print( error );
            return { "error" : "Failed to retrieve Dataset" };

        # clientOpenApi = await retrieveOpenApiJsonFromEndPoint( openAPI[ index ] );
        clientOpenApi = await retrieveOpenApiJsonFromEndPoint( dataset[ 0 ][ "openapi" ] );
        try:
            JSON.dumps( clientOpenApi );
        except JSON.JSONDecodeError as error : 
            return { "error" : error };
        
        # print( clientOpenApi );

        # Do not change the properties apart from the starting path.
        # These properties are hard coded and represent a minimal acceptable 
        # OpenAPI JSON schema. Changing them will reuslt in Access Error on list keys and Objects.
        # if index == 2 :
        if dataset[ 0 ][ "id" ] == 2 : 
            cfg = {
              "/farms" : {
                "method" : "get" , 
                "responses" : {
                  "200" : {
                    "content" : "application/json" , 
                    "schema"  : "Farm" , 
                    "type"    : "array"
                  }
                }
              }
            }
            myEval.setSchema({
                "properties": {
                    "uuid"     : { "type" : "string", "nullable" : True    , "format"   : "uuid" },
                    # "uuid"     : { "anyOf": [ { "type": "string" }, { "type": "null" } ] },
                    "latitude" : { "type" : "number", "format"   : "double", "nullable" : True },
                    "longitude": { "type" : "number", "format"   : "double", "nullable" : True },
                    "ecoFarm"  : { "type" : "boolean" }
                },
                "title": "Farm"
            });
        else:
            cfg = {
              "/calendar" : {
                "method" : "get" , 
                "responses" : {
                  "200" : {
                    "content" : "application/json" , 
                    "schema"  : "Field" , 
                    "type"    : "array"
                  }
                }
              }
            }
            myEval.setSchema({
                "properties": {
                    "name": {
                        "anyOf": [ { "type": "string" }, { "type": "null" } ],
                        "title": "Name",
                        "description": ""
                    },
                    "crops": {
                        "items"       : "CropGeneric",
                        "title"       : "Crops",
                        "description" : "",
                        "type"        : "array"
                    }
                 } , 
                 "title": "Field"
            });
            """
            cfg = {
              "/sensors" : {
                "method" : "get" , 
                "responses" : {
                  "200" : {
                    "content" : "application/json" , 
                    "schema"  : "Sensor" , 
                    "type"    : "array"
                  }
                }
              }
            }
            myEval.setSchema({
                "properties": {
                    "name": {
                        "anyOf": [ { "type": "string" }, { "type": "null" } ],
                        "title": "Name",
                        "description": ""
                    },
                    "deployment": { 
                        "anyOf": [ { "type": "string" }, { "type": "null" } ],
                        "title": "Deployment",
                        "description": ""
                    },
                    "longitude": {
                        "anyOf": [ { "type": "number" }, { "type": "null" } ],
                        "title": "Longitude",
                        "description": ""
                    },
                    "latitude": {
                        "anyOf": [ { "type": "number" }, { "type": "null" } ],
                        "title": "Latitude",
                        "description": ""
                    },
                    "sensor_name": {
                        "anyOf": [ { "type": "string" }, { "type": "null" } ],
                        "title": "Sensor Name",
                        "description": ""
                    },
                    "sensor_quantitykind": {
                        "anyOf": [ { "type": "string" }, { "type": "null" } ],
                        "title": "Sensor Quantitykind",
                        "description": ""
                    },
                    "sensor_unit": {
                        "anyOf": [ { "type": "string" }, { "type": "null" } ],
                        "title": "Sensor Unit",
                        "description": ""
                    },
                    "sensor_metadata": {
                        "anyOf": [ { "type": "string" }, { "type": "null" } ],
                        "title": "Sensor Metadata",
                        "description": ""
                    },
                    "description": {
                        "anyOf": [ { "type": "string" }, { "type": "null" } ],
                        "title": "Description",
                        "description": ""
                    },
                    "state": {
                        "anyOf": [ { "type": "string" }, { "type": "null" } ],
                        "title": "State",
                        "description": ""
                    }
                },
                "title": "Sensor"
            });
            """

        for endPoint in cfg:
            myEvaluationResult = myEval.validate(
                cfg           = cfg[ endPoint ] , 
                endPoint      = endPoint , 
                clientOpenApi = clientOpenApi 
            );

            if "error" in myEvaluationResult:
                return myEvaluationResult;

        return { "res" : "Everything is ok" };

    except Exception as error :
        return { "error" : "11: Exception of type '{0}'. Arguments: {1!r}".format( type(error).__name__ , error.args ) };

@app.get( "/administration/jsonldparser", 
          tags         = [ "EndPoint Validations" ] , 
          summary      = "",
          description  = "" 
        )
async def parseJSONLD(
             # current_user = Depends( get_current_user ) 
             token    : str
          ):

    if token != serverSecretKey:
         return [];
    try:
        JSONLDEvalResult = myEval.validateJSONLD( "http://" + str( myIP ) + ":8686/jsonld" );
        
        print( JSONLDEvalResult );

    except( Exception ) as error :
        amTool.log( "[ ERROR - parseJSONLD ] : " , error );
        if isinstance( error , HTTPException ) :
            raise HTTPException(
                status_code = error.status_code, 
                detail      = error.detail
            );
        elif isinstance( error , IndexError ):
            raise HTTPException(
                status_code = 401, 
                detail      = "Resource not Found"
            );
        else:
            raise HTTPException(
                status_code = 500, 
                detail      = "Issue with Server , check Logs"
            );

