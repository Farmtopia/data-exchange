import time;
import datetime;
from datetime import timedelta;
from typing import Optional , List;
from enum import Enum;
from pydantic import BaseModel , Field , create_model , ConfigDict;

class get_dataset( BaseModel ):
				id     : Optional[ int ] = Field( None , description = "" );
				limit  : Optional[ int ] = Field( None , description = "" );
				offset : Optional[ int ] = Field( None , description = "" );
				name : Optional[ str ] = Field( None , description = "" );
				user_id : Optional[ int ] = Field( None , description = "" );
				model_config = ConfigDict( extra = 'allow' );


class dataset( BaseModel ):
				id         : Optional[ int ]             = Field( None , description = "" );
				created_on : Optional[ datetime.datetime ] = Field( None , description = "" );
				name : Optional[ str ] = Field( None , description = "" );
				description : Optional[ str ] = Field( None , description = "" );
				service : Optional[ str ] = Field( None , description = "" );
				swagger : Optional[ str ] = Field( None , description = "" );
				openapi : Optional[ str ] = Field( None , description = "" );
				data : Optional[ str ] = Field( None , description = "" );
				metadata : Optional[ str ] = Field( None , description = "" );
				subscription : Optional[ str ] = Field( None , description = "" );
				user_id : Optional[ int ] = Field( None , description = "" );
				model_config = ConfigDict( extra = 'allow' );


class put_dataset( BaseModel ):
				name : Optional[ str ] = Field( None , description = "" );
				description : Optional[ str ] = Field( None , description = "" );
				service : Optional[ str ] = Field( None , description = "" );
				swagger : Optional[ str ] = Field( None , description = "" );
				openapi : Optional[ str ] = Field( None , description = "" );
				data : Optional[ str ] = Field( None , description = "" );
				metadata : Optional[ str ] = Field( None , description = "" );
				subscription : Optional[ str ] = Field( None , description = "" );
				user_id : Optional[ int ] = Field( None , description = "" );
				model_config = ConfigDict( extra = 'allow' );


class get_map_user_to_dataset( BaseModel ):
				id     : Optional[ int ] = Field( None , description = "" );
				limit  : Optional[ int ] = Field( None , description = "" );
				offset : Optional[ int ] = Field( None , description = "" );
				model_config = ConfigDict( extra = 'allow' );


class map_user_to_dataset( BaseModel ):
				id         : Optional[ int ]             = Field( None , description = "" );
				created_on : Optional[ datetime.datetime ] = Field( None , description = "" );
				user_id : Optional[ int ] = Field( None , description = "" );
				dataset_id : Optional[ int ] = Field( None , description = "" );
				token : Optional[ str ] = Field( None , description = "" );
				expires_on : Optional[ datetime.datetime ] = Field( None , description = "" );
				status : Optional[ int ] = Field( None , description = "" );
				model_config = ConfigDict( extra = 'allow' );


class put_map_user_to_dataset( BaseModel ):
				user_id : Optional[ int ] = Field( None , description = "" );
				dataset_id : Optional[ int ] = Field( None , description = "" );
				token : Optional[ str ] = Field( None , description = "" );
				expires_on : Optional[ datetime.datetime ] = Field( None , description = "" );
				status : Optional[ int ] = Field( None , description = "" );
				model_config = ConfigDict( extra = 'allow' );


class get_lst_of_val( BaseModel ):
				id     : Optional[ int ] = Field( None , description = "" );
				limit  : Optional[ int ] = Field( None , description = "" );
				offset : Optional[ int ] = Field( None , description = "" );
				model_config = ConfigDict( extra = 'allow' );


class lst_of_val( BaseModel ):
				id         : Optional[ int ]             = Field( None , description = "" );
				created_on : Optional[ datetime.datetime ] = Field( None , description = "" );
				type : Optional[ str ] = Field( None , description = "" );
				value : Optional[ str ] = Field( None , description = "" );
				model_config = ConfigDict( extra = 'allow' );


class put_lst_of_val( BaseModel ):
				type : Optional[ str ] = Field( None , description = "" );
				value : Optional[ str ] = Field( None , description = "" );
				model_config = ConfigDict( extra = 'allow' );


