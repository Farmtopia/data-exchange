import re;
import csv;
import json as JSON;
from classes.amTools import amTool;

class amData:
    def __init__( self , cfg ):
        """Class for Providing Data"""
        self.amTool = amTool();
        self.amTool.log( "Data Initiated" );
        self.cfg    = cfg;
        self.debug  = False;
        
        self.datasetToReturn = 2;

        self.dataset = [
            { 
                "name" : "dataset1" , 
                "measurements" : [
                    [ 1715160000 , 10 ] , [ 1715161000 , 11 ] , [ 1715162000 , 12 ] , [ 1715163000 , 13 ] 
                ]
            },
            { 
                "name" : "dataset2" , 
                "measurements" : [
                    [ 1715160000 , 20 ] , [ 1715161000 , 11 ]  , [ 1715162000 , 0 ]   , [ 1715163000 , 55 ] ,
                    [ 1715164000 , 30 ] , [ 1715165000 , -5 ]  , [ 1715166000 , 15 ]  , [ 1715167000 , 150 ] ,
                    [ 1715168000 , 40 ] , [ 1715169000 , 21 ]  , [ 1715170000 , -22 ] , [ 1715171000 , 15 ] ,
                    [ 1715172000 , 50 ] , [ 1715173000 , 150 ] , [ 1715174000 , 2 ]   , [ 1715175000 , 5 ] ,
                    [ 1715176000 , 60 ] , [ 1715177000 , 120 ] , [ 1715178000 , 220 ] , [ 1715179000 , -23 ] 
                ]
            }
        ]
        
        self.datasetJSONLD = {
                                  "@context": {
                                      "fsm": "https://gitlab.com/Farmtopia/farmtopia-semantic-model/-/raw/main/FarmtopiaSemanticModel.rdf#",
                                      "unit": "http://qudt.org/2.1/vocab/unit",
                                      "sosa": "http://www.w3.org/ns/sosa/",
                                      "geosparql": "http://www.opengis.net/ont/geosparql/",
                                      "cf": "http://purl.oclc.org/NET/ssnx/cf/cf-property#",
                                      "datasetSlices": {
                                          "@id": "fsm:datasetSlices",
                                          "@type": "@id",
                                          "@container": "@set"
                                      },
                                      "sliceContents": {
                                          "@id": "fsm:sliceContents",
                                          "@type": "@id",
                                          "@container": "@set"
                                      },
                                      "label": "fsm:label",
                                      "toponym": "fsm:toponym",
                                      "geometry": {
                                          "@id": "geosparql:hasGeometry",
                                          "@type": "@id"
                                      },
                                      "geoJson": {
                                          "@id": "geosparql:asGeoJSON",
                                          "@type": "@json"
                                      },
                                      "location": {
                                          "@id": "fsm:location",
                                          "@type": "@id"
                                      },
                                      "layer": {
                                          "@id": "fsm:layer",
                                          "@type": "@id"
                                      },
                                      "Atmosphere": {
                                          "@id": "cf:atmosphere"
                                      },
                                      "property": {
                                          "@id": "sosa:property",
                                          "@type": "@id"
                                      },
                                      "AirTemperature": {
                                          "@id": "fsm:air_temperature"
                                      },
                                      "sourceSensor": {
                                          "@id": "sosa:madeBySensor",
                                          "@type": "@id"
                                      },
                                      "unitOfMeasure": {
                                          "@id": "fsm:unitOfMeasure",
                                      },
                                      "Celsius": {
                                          "@id": "unit:DEG_C"
                                      },
                                      "RelativeHumidity": {
                                          "@id": "fsm:relative_humidity"
                                      },
                                      "Percent": {
                                      "@id": "unit:PERCENT"
                                      },
                                      "Precipitation": {
                                          "@id": "fsm:presipitation"
                                      },
                                      "Millimetre": {
                                      "@id": "unit:MilliM"
                                      },
                                      "WindSpeed": {
                                          "@id": "fsm:wind_speed"
                                      },
                                      "WindDirection": {
                                          "@id": "fsm:wind_direction"
                                      },
                                      "AirPressure": {
                                          "@id": "fsm:air_pressure"
                                      },
                                      "Evapotranspiration": {
                                          "@id": "fsm:evapotranspiration"
                                      },
                                      "contents": {
                                          "@id": "sosa:hasMember",
                                          "@type": "@id",
                                          "@container": "@set"
                                      },
                                      "dateTime": "sosa:resultTime",
                                      "value": "sosa:hasSimpleResult",
                                      "Surface": {
                                          "@id": "cf:surface"
                                      },
                                      "SolarIrradiance": {
                                          "@id": "fsm:solar_irradiance"
                                      },
                                      "UltravioletIrradiance": {
                                      "@id": "fsm:ultraviolet_irradiance"
                                      },
                                      "KilometerPerHour": {
                                          "@id": "unit:KiloM-PER-HR"
                                      },
                                      "Millibar": {
                                          "@id": "unit:MilliBAR"
                                      },
                                      "Volt": {
                                          "@id": "unit:V"
                                      },
                                      "Soil": {
                                          "@id": "cf:soil_layer"
                                      },
                                      "SoilTemperature": {
                                          "@id": "fsm:soil_temperature"
                                      },
                                      "SoilMoisture": {
                                      "@id": "fsm:soil_moisture"
                                      },
                                      "SoilSalinity": {
                                      "@id": "fsm:soil_salinity"
                                      },
                                      "Vegetation": {
                                          "@id": "cf:vegetation"
                                      },
                                      "LeafWetness": {
                                          "@id": "fsm:leaf_wetness"
                                      },
                                      "LeafTemperature": {
                                          "@id": "fsm:leaf_temperature"
                                      }
                                  },
                                  "@graph": [
                                      {
                                          "@id": "NP_gaiasense_serviceResource_representation_instance_1",
                                          "@type": "fsm:Dataset",
                                          "datasetSlices": [
                                              {
                                                  "@id": "locationsSlice",
                                                  "@type": "fsm:Slice",
                                                  "sliceContents": [
                                                      {
                                                          "@id": "901",
                                                          "@type": "fsm:PointOfInterest",
                                                          "label": "Χανιά 19",
                                                          "toponym": "Δημητρόπουλο",
                                                          "geometry": {
                                                              "@id": "geometry4",
                                                              "@type": "geometry",
                                                              "geoJson": {
                                                                  "type": "Point",
                                                                  "coordinates": [
                                                                      "35.503023",
                                                                      "23.906684"
                                                                  ]
                                                              }
                                                          }
                                                      }
                                                  ]
                                              },
                                              {
                                                  "@id": "measurementsSlice",
                                                  "@type": "fsm:Slice",
                                                  "sliceContents": [
                                                      {
                                                          "@id": "WeatherStation_1Observations",
                                                          "@type": "fsm:DataPointsGroup",
                                                          "location": {
                                                              "@id": "901"
                                                          },
                                                          "sliceContents": [
                                                              {
                                                                  "@id": "AtmosphereObservationCollection_1",
                                                                  "@type": "fsm:DataPointsGroup",
                                                                  "layer": "Atmosphere",
                                                                  "sliceContents": [
                                                                      {
                                                                          "@id": "AirTemperatureObservationCollection_1",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "AirTemperature",
                                                                          "sourceSensor": {
                                                                              "@id": "wsht30_temp",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Celsius",
                                                                          "contents": [
                                                                           {
                                                                            "@id": "TemperatureAirObservation1",
                                                                            "@type": "fsm:Observation",
                                                                            "dateTime": "2024-08-01T00:00:00+03:00",
                                                                            "value": "12.93"
                                                                           },
                                                                           {
                                                                            "@id": "TemperatureAirObservation1",
                                                                            "@type": "fsm:Observation",
                                                                            "dateTime": "2024-08-01T01:00:00+03:00",
                                                                            "value": "14.67"
                                                                           },
                                                                           {
                                                                            "@id": "TemperatureAirObservation1",
                                                                            "@type": "fsm:Observation",
                                                                            "dateTime": "2024-08-01T02:00:00+03:00",
                                                                            "value": "16.22"
                                                                           },
                                                                           {
                                                                            "@id": "TemperatureAirObservation1",
                                                                            "@type": "fsm:Observation",
                                                                            "dateTime": "2024-08-01T03:00:00+03:00",
                                                                            "value": "15.13"
                                                                           },
                                                                           {
                                                                            "@id": "TemperatureAirObservation1",
                                                                            "@type": "fsm:Observation",
                                                                            "dateTime": "2024-08-01T04:00:00+03:00",
                                                                            "value": "14.88"
                                                                           },
                                                                           {
                                                                            "@id": "TemperatureAirObservation1",
                                                                            "@type": "fsm:Observation",
                                                                            "dateTime": "2024-08-01T05:00:00+03:00",
                                                                            "value": "13.45"
                                                                           },
                                                                           {
                                                                            "@id": "TemperatureAirObservation1",
                                                                            "@type": "fsm:Observation",
                                                                            "dateTime": "2024-08-01T06:00:00+03:00",
                                                                            "value": "13.01"
                                                                           },
                                                                           {
                                                                            "@id": "TemperatureAirObservation1",
                                                                            "@type": "fsm:Observation",
                                                                            "dateTime": "2024-08-01T07:00:00+03:00",
                                                                            "value": "12.10"
                                                                           },
                                                                           {
                                                                            "@id": "TemperatureAirObservation1",
                                                                            "@type": "fsm:Observation",
                                                                            "dateTime": "2024-08-01T08:00:00+03:00",
                                                                            "value": "13.56"
                                                                           },
                                                                           {
                                                                            "@id": "TemperatureAirObservation1",
                                                                            "@type": "fsm:Observation",
                                                                            "dateTime": "2024-08-01T09:00:00+03:00",
                                                                            "value": "15.24"
                                                                           }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "RelativeHumidityObservationCollection_1",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "RelativeHumidity",
                                                                          "sourceSensor": {
                                                                              "@id": "wsht30_rh",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Percent",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "RelativeHumidityObservation1",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "94.17"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "RelativeHumidityObservationCollection_2",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "RelativeHumidity",
                                                                          "sourceSensor": {
                                                                              "@id": "nplwrh2",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "RelativeHumidityObservation2",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "97"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "PrecipitationObservationCollection_1",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "Precipitation",
                                                                          "sourceSensor": {
                                                                              "@id": "rain",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Millimetre",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "PrecipitationObservation1",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "0"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "WindSpeedObservationCollection_1",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "WindSpeed",
                                                                          "sourceSensor": {
                                                                              "@id": "velocity",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "KilometerPerHour",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "WindSpeedObservation1",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "0.05"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "WindDirectionObservationCollection_1",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "WindDirection",
                                                                          "sourceSensor": {
                                                                              "@id": "vr_winddir",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "WindDirectionObservation1",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "9"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "AtmosphericPressureObservationCollection_1",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "AirPressure",
                                                                          "sourceSensor": {
                                                                              "@id": "pressure",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Millibar",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "AtmosphericPressureObservation1",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "11007.82"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "EvapotranspirationObservationCollection_1",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "Evapotranspiration",
                                                                          "sourceSensor": {
                                                                              "@id": "ext_copernicus",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Millimetre",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "AtmosphericPressureObservation1",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "0"
                                                                              }
                                                                          ]
                                                                      }
                                                                  ]
                                                              },
                                                              {
                                                                  "@id": "SurfaceObservationCollection_1",
                                                                  "@type": "fsm:DataPointsGroup",
                                                                  "layer": "Surface",
                                                                  "sliceContents": [
                                                                      {
                                                                          "@id": "SolarIrradianceObservationCollection_1",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SolarIrradiance",
                                                                          "sourceSensor": {
                                                                              "@id": "pyranometer",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Volt",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SolarIrradianceObservation1",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "96"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "UltravioletIrradianceObservationCollection_1",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "UltravioletIrradiance",
                                                                          "sourceSensor": {
                                                                              "@id": "nplwlw1",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Volt",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "UltravioletIrradianceObservation1",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "16"
                                                                              }
                                                                          ]
                                                                      }
                                                                  ]
                                                              },
                                                              {
                                                                  "@id": "SoilObservationCollection_1",
                                                                  "@type": "fsm:DataPointsGroup",
                                                                  "layer":"Soil",
                                                                  "sliceContents": [
                                                                      {
                                                                          "@id": "SoilTemperatureObservationCollection_1",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilTemperature",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_temp_10",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Celsius",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilTemperatureObservation1",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "12.76"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilTemperatureObservationCollection_2",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilTemperature",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_temp_20",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Celsius",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilTemperatureObservation2",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "13.12"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilTemperatureObservationCollection_1",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilTemperature",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_temp_10",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Celsius",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilTemperatureObservation1",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "12.76"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilTemperatureObservationCollection_3",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilTemperature",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_temp_30",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Celsius",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilTemperatureObservation3",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "13.21"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilTemperatureObservationCollection_4",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilTemperature",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_temp_40",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Celsius",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilTemperatureObservation4",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "13.22"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilTemperatureObservationCollection_5",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilTemperature",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_temp_50",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Celsius",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilTemperatureObservation5",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "13.04"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilTemperatureObservationCollection_6",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilTemperature",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_temp_60",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Celsius",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilTemperatureObservation6",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "13.59"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilTemperatureObservationCollection_7",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilTemperature",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_temp_70",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Celsius",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilTemperatureObservation7",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "14"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilTemperatureObservationCollection_8",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilTemperature",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_temp_80",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Celsius",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilTemperatureObservation8",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "14.24"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilTemperatureObservationCollection_9",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilTemperature",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_temp_90",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Celsius",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilTemperatureObservation9",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "14.24"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilMoistureObservationCollection_1",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilMoisture",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_sm_10",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Percent",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilMoistureObservation1",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "35.3"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilMoistureObservationCollection_2",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilMoisture",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_sm_20",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Percent",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilMoistureObservation2",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "41.06"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilMoistureObservationCollection_3",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilMoisture",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_sm_30",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Percent",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilMoistureObservation3",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "41"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilMoistureObservationCollection_4",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilMoisture",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_sm_40",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Percent",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilMoistureObservation4",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "38.38"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilMoistureObservationCollection_5",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilMoisture",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_sm_50",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Percent",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilMoistureObservation5",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "36.28"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilMoistureObservationCollection_6",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilMoisture",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_sm_60",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Percent",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilMoistureObservation6",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "30.42"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilMoistureObservationCollection_7",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilMoisture",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_sm_70",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Percent",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilMoistureObservation7",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "33.33"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilMoistureObservationCollection_8",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilMoisture",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_sm_80",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Percent",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilMoistureObservation8",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "36.42"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilMoistureObservationCollection_9",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilMoisture",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_sm_90",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "unitOfMeasure": "Percent",
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilMoistureObservation9",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "28.9"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilSalinityObservationCollection_1",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilSalinity",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_sal_10",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilSalinityObservation1",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "1484.93"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilSalinityObservationCollection_2",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilSalinity",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_sal_20",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilSalinityObservation2",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "1541"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilSalinityObservationCollection_3",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilSalinity",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_sal_30",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilSalinityObservation3",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "1547.25"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilSalinityObservationCollection_4",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilSalinity",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_sal_40",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilSalinityObservation4",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "1488.46"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilSalinityObservationCollection_5",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilSalinity",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_sal_50",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilSalinityObservation5",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "1481.56"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilSalinityObservationCollection_6",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilSalinity",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_sal_60",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilSalinityObservation6",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "1446.56"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilSalinityObservationCollection_7",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilSalinity",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_sal_70",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilSalinityObservation7",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "1490.51"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilSalinityObservationCollection_8",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilSalinity",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_sal_80",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilSalinityObservation8",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "1587.99"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "SoilSalinityObservationCollection_9",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "SoilSalinity",
                                                                          "sourceSensor": {
                                                                              "@id": "dnd_90_sdi12_sal_90",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "SoilSalinityObservation9",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "1339.44"
                                                                              }
                                                                          ]
                                                                      }

                                                                  ]
                                                              },
                                                              {
                                                                  "@id": "CanopyObservationCollection_1",
                                                                  "@type": "fsm:DataPointsGroup",
                                                                  "layer":"Vegetation",
                                                                  "sliceContents": [
                                                                      {
                                                                          "@id": "LeafWetnessObservationCollection1",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "LeafWetness",
                                                                          "sourceSensor": {
                                                                              "@id": "nplwlw1",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "LeafWetnessObservation1",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "1"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "LeafWetnessObservationCollection2",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "LeafWetness",
                                                                          "sourceSensor": {
                                                                              "@id": "nplwlw2",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "LeafWetnessObservation2",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "1"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "LeafWetnessObservationCollection3",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "LeafWetness",
                                                                          "sourceSensor": {
                                                                              "@id": "nplwrh1",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "LeafWetnessObservation3",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "96.67"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "LeafWetnessObservationTemperature1",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "LeafTemperature",
                                                                          "sourceSensor": {
                                                                              "@id": "nplwtemp1",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "LeafTemperatureObservation1",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "12.62"
                                                                              }
                                                                          ]
                                                                      },
                                                                      {
                                                                          "@id": "LeafWetnessObservationTemperature2",
                                                                          "@type": "fsm:DataPointsGroup",
                                                                          "property": "LeafTemperature",
                                                                          "sourceSensor": {
                                                                              "@id": "nplwtemp2",
                                                                              "@type": "sosa:Sensor"
                                                                          },
                                                                          "contents": [
                                                                              {
                                                                                  "@id": "LeafTemperatureObservation2",
                                                                                  "@type": "fsm:Observation",
                                                                                  "dateTime": "2024-02-26T16:00:00+03:00",
                                                                                  "value": "12.68"
                                                                              }
                                                                          ]
                                                                      }
                                                                  ]
                                                              }
                                                          ]
                                                      }
                                                  ]
                                              }
                                          ]
                                      }
                                  ]
                              }


    def get( self , email ):
        if self.datasetToReturn == 1 :
            return self.dataset[ 1 ];
        else:
            return self.datasetJSONLD;
        # if email == "amanos.dev@gmail.com" : 
            # return self.dataset[ 0 ];
        # else:
            # return self.dataset[ 1 ];






