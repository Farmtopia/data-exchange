import time;
import datetime;
from datetime import timedelta;
from typing   import Optional , List, Any , ClassVar , Annotated;
from enum     import Enum;
from pydantic import BaseModel , Field , create_model , StringConstraints;

class Sensor( BaseModel ):
				name                : Annotated[ str   | None , Field( description = "" ) ];
				deployment          : Annotated[ str   | None , Field( description = "" ) ];
				longitude           : Annotated[ float | None , Field( description = "" ) ];
				latitude            : Annotated[ float | None , Field( description = "" ) ];
				sensor_name         : Annotated[ str   | None , Field( description = "" ) ];
				sensor_quantitykind : Annotated[ str   | None , Field( description = "" ) ];
				sensor_unit         : Annotated[ str   | None , Field( description = "" ) ];
				sensor_metadata     : Annotated[ str   | None , Field( description = "" ) ];
				description         : Annotated[ str   | None , Field( description = "" ) ];
				state               : Annotated[ str   | None , Field( description = "" ) ];