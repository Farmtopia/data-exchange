import time;
import datetime;
from datetime import timedelta;
from typing import Optional , List;
from enum import Enum;
from pydantic import BaseModel , Field , create_model;

class EventTypes( str , Enum ):
				HARVEST = "harvest";
				LAND_MANAGEMENT = "land_management";
				SPRAY = "spray";
				IRRIGATION = "irrigation";
				PHAENOLOGICAL_STAGE = "phaenological_stage";
				INSECT = "insect";
				FERTILIZATION = "fertilization";
				PLOW = "plow";

class Unit( str , Enum ):
				TON = "ton";
				KG = "kg";
				GR = "gr";
				KL = "kl";
				LT = "lt";
				ML = "ml";

class UnitReference( str , Enum ):
				STREMMA = "stremma";
				HECTAR = "hectar";

class FuelType( str , Enum ):
				DIESEL = "diesel";
				GAS = "gas";
				ELECTRICITY = "electricity";
				WINDTURBINE = "windturbine";
				HYDRO = "hydro";

