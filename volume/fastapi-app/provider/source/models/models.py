import time;
import datetime;
from datetime import timedelta;
from typing import Optional , List;
from enum import Enum;
from pydantic import BaseModel , Field , create_model;
import models.parametrics as Parametric;

class OrderBy( str , Enum ):
    asc  = "asc";
    desc = "desc";

class EvaluationScale( str , Enum ):
    very_poorly = "Very Poorly";
    poorly      = "Poorly";
    neutral     = "Neutral";
    well        = "Well";
    very_well   = "Very Well";