import time;
import datetime;
from datetime import timedelta;
from typing   import Optional , List, Any , ClassVar , Annotated;
from enum     import Enum;
from pydantic import BaseModel , Field , create_model , StringConstraints;

class Farm( BaseModel ):
    name      : Annotated[ str | None , Field( description = "" ) ];
    uuid      : Annotated[ str | None , Field( description = "" ) ];
    latitude  : Annotated[ float | None , Field( description = "" ) ];
    longitude : Annotated[ float | None , Field( description = "" ) ];
    ecoFarm   : Annotated[ bool | None , Field( description = "" ) ];

class Crop( BaseModel ):
    uuid   : Annotated[ str | None , Field( description = "" ) ];
    name   : Annotated[ str | None , Field( description = "" ) ];
    nameEn : Annotated[ str | None , Field( description = "" ) ];

class GeoJSON( BaseModel ):
    type        : Annotated[ str , Field( default="Polygon" , description = "" ) ];
    coordinates : List[ List[ List[ float ] ] ];

class FieldType( BaseModel ):
    id     : Annotated[ int | None , Field( description = "" ) ];
    name   : Annotated[ str | None , Field( description = "" ) ];
    nameEn : Annotated[ str | None , Field( description = "" ) ];

class CropGeneric( BaseModel ):
    year        : Annotated[ int | None , Field( description = "2024" ) ];
    cropType    : Annotated[ int | None , Field( description = "" ) ];
    sowingDate  : Annotated[ datetime.date | None , Field( description = "" ) ];
    harvestDate : Annotated[ datetime.date | None , Field( description = "" ) ];
    crop        : Annotated[ Crop | None , Field( description = "" ) ];

class Field( BaseModel ):
    uuid        : Annotated[ str | None , Field( description = "" ) ];
    identifier  : Annotated[ int | None , Field( description = "" ) ];
    name        : Annotated[ str | None , Field( description = "" ) ];
    description : Annotated[ str | None , Field( description = "" ) ];
    areaM2      : Annotated[ float | None , Field( description = "" ) ];
    fieldType   : FieldType;
    crops       : List[ CropGeneric ];
    geoJSON     : GeoJSON;

class Wrapper( BaseModel ):
    FSM : dict;
    ITC : Field;
