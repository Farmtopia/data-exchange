# uvicorn main:app --port 8686 --host 192.168.1.108 --reload --root-path /
from   inspect import currentframe, getframeinfo;
from   typing  import Optional , List, Any , ClassVar , Annotated;
from   fastapi import Depends;
from   fastapi import FastAPI;
from   fastapi import Form;
from   fastapi import Request;
from   fastapi import HTTPException;
from   fastapi import status;
from   fastapi import Query;
from   fastapi import Path;
from   fastapi.openapi.utils import get_openapi;
from   enum     import Enum;
from   pydantic import BaseModel , Field , create_model , StringConstraints;
import time;
import datetime;
from   datetime import timedelta;
import os;
import os.path;
import array;
from decimal import *;
import jsonschema;
from jsonschema import validate;
from jsonschema import ValidationError;
from pyld import jsonld;
from rdflib import Graph;
import aiohttp;
import aiofiles;
import asyncio;
import json as JSON;
import sys;
from jose import JWTError, ExpiredSignatureError , jwt;
import psycopg2;
import models.models            as     ResModel;
import models.inputs            as     Inputs;
import models.SensorModel       as     SensorModel;
import models.CalendarModel     as     CalendarModel;
from   cfg.tags                 import Tags;
from   classes.amModel          import amModel;
from   classes.amTools          import amTool;
from   classes.amData           import amData;

# security         = HTTPBasic();
amTool           = amTool();
cfg              = amTool.load( "./cfg/" , "config" );
amTool.cfg       = cfg;
myModel          = amModel( cfg );
myData           = amData( cfg );
myTags           = Tags();
endpoint_tags    = myTags.list;
DBsourceFileName = "schema";
debug            = False;

app = FastAPI( 
              title        = cfg["service"]["title"],
              description  = cfg["service"]["description"],
              version      = cfg["service"]["version"],
              openapi_tags = endpoint_tags , 
              redoc_url    = None , 
              docs_url     = "/" + cfg["service"]["docs_url"]
      );

def parseStringIntoArrayWithIntegers( string ):
   myListArrayResponse = list();

   if( string == "" or string is None ):
       return [];

   if( string.find(",") > 0 ):
       myList = string.split( "," );
       for item in myList : 
           if( amTool.isInt( item ) ):
               myListArrayResponse.append( item );
           else:
               print( "This is not a number" );
       return myListArrayResponse;
   else:
       if( amTool.isInt( string ) ):
           return [ string ];
       else:
           print( "This is not a number" );
           return myListArrayResponse;


# Administration EndPoints for initialization
@app.get( "/administration/create_db_tables", 
          tags         = [ "Administration" ] , 
          summary      = "Create basic Database Tables",
          description  = "Create the initial tables required for inserting user Data. This is an action that only needs to be performed once at the initial installation" 
        ) 
async def createDBTables():
    # Reads the SourceFile JSON and creates one Table for each entity defined in there.
    mySchema = amTool.cfg[ "database" ][ "schema" ];

    try:
        myInputsJSON = amTool.load( "" , DBsourceFileName , "json" );
        myWholeQuery = [];
        mySchema     = amTool.cfg[ "database" ][ "schema" ];

        myForeignKeys = [];
        for entity in myInputsJSON[ "inputs" ] :
            myUniques     = [];
            myComposites  = [];
            myQuery  = "DROP TABLE IF EXISTS " + str( mySchema ) + "." + str( entity ).lower() + ";";
            myQuery += "CREATE TABLE " + str( mySchema ) + "." + str( entity ).lower() + " ( ";
            myTempQuery = [];
            myTempQuery.append( "id serial NOT NULL" );
            myTempQuery.append( "created_on timestamp NOT NULL DEFAULT now()" );
            for item in myInputsJSON[ "inputs" ][ entity ]:
                defaultClause = "";
                defaultValue  = "";
                if( "default" in item and item[ "default" ] != "false" ) :
                    if( str( item[ "type" ] ) != "integer" and str( item[ "type" ] ) != "numeric" ):
                        defaultValue = "('" + str( item[ "default" ] ) + "'::" + str( item[ "type" ] ) + ")";
                    else:
                        defaultValue = "("  + str( item[ "default" ] ) + "::"  + str( item[ "type" ] ) + ")";

                    defaultClause = " NOT NULL DEFAULT (" + defaultValue + ")";    
                else:
                    defaultClause = " NULL ";

                myTempQuery.append( str( item[ "name" ] ) + " " + str( item[ "type" ] ) + str( defaultClause ) );

                if "unique" in item and item[ "unique" ] == "true" :
                    myUniques.append( "CONSTRAINT " + str( item[ "name" ] ) + "_unique UNIQUE ( " + str( item[ "name" ] ) + ")" );

                if "composite" in item and item[ "composite" ] == "true" :
                    myComposites.append( item[ "name" ] );

                if "fkey" in item :
                    myPair        = item[ "fkey" ].split( "." );
                    mySourceTable  = myPair[ 0 ];
                    mySourceColumn = myPair[ 1 ];
                    myTargetTable  = entity;
                    myTargetColumn = item[ "name" ];
                    myForeignKeys.append(
                                 "ALTER TABLE " + 
                                 "" + str( mySchema ) + "." + str( myTargetTable ) + " ADD CONSTRAINT fk_" + str( myTargetColumn ) + " " + 
                                 "FOREIGN KEY (" + str( myTargetColumn ) + ") REFERENCES " + 
                                 "" + str( mySchema ) + "." + str( mySourceTable ) + "(" + str( mySourceColumn ) + ") ON DELETE CASCADE;"
                                );

            myQuery += ",\r\n".join( myTempQuery );
            myQuery += ",PRIMARY KEY (id) ";
            if( len( myUniques ) > 0 ) : 
                myQuery += "," + ",\r\n".join( myUniques );
            myQuery += ");";
            myWholeQuery.append( myQuery );

            if( len( myComposites ) > 0 ) : 
                myWholeQuery.append( "CREATE UNIQUE INDEX " + str( entity ) + "_multi on " + str( mySchema ) + "." + str( entity ).lower() + " ( " + ",".join( myComposites ) + ")" );

        myQueryWithForeignKeys = myWholeQuery + myForeignKeys;
        for aQuery in myQueryWithForeignKeys:
            amTool.log( "------------------------" );
            amTool.log( aQuery );
            myModel.executeQuery( aQuery , None , "update" );

    except( Exception ) as error :
        print( error );

@app.get( "/administration/create_input_models", 
          tags         = [ "Administration" ] , 
          summary      = "Create our Input BaseModels",
          description  = "Create our Input BaseModels" 
        ) 
async def createInputModels():
  # Loads the SourceFile JSON and creates our BaseModels that will populatee the Inputs for each EndPoint created
    targetFileName = "models/inputs.py";
    myFile         = [];

    try:
        myInputsJSON = amTool.load( "" , DBsourceFileName , "json" );
        amTool.log( "File has been opened" );
        amTool.log( myInputsJSON );

        f = open( targetFileName, "w" );
        f.write( "import time;\r\n" );
        f.write( "import datetime;\r\n" );
        f.write( "from datetime import timedelta;\r\n" );
        f.write( "from typing import Optional , List;\r\n" );
        f.write( "from enum import Enum;\r\n" );
        f.write( "from pydantic import BaseModel , Field , create_model;\r\n" );
        f.write( "\r\n" );

        for entity in myInputsJSON[ "inputs" ] :
            myEntityClass    = [];
            myEntityClass.append( "class " + entity + "( BaseModel ):\r\n" );
            myEntityClass.append( "\t\t\t\tid         : Optional[ int ]             = Field( None , description = \"\" );\r\n" );
            myEntityClass.append( "\t\t\t\tcreated_on : Optional[ datetime.datetime ] = Field( None , description = \"\" );\r\n" );

            myEntityClassPut = [];
            myEntityClassPut.append( "class put_" + entity + "( BaseModel ):\r\n" );

            myEntityClassGet = [];
            myEntityClassGet.append( "class get_" + entity + "( BaseModel ):\r\n" );
            myEntityClassGet.append( "\t\t\t\tid     : Optional[ int ] = Field( None , description = \"\" );\r\n" );
            myEntityClassGet.append( "\t\t\t\tlimit  : Optional[ int ] = Field( None , description = \"\" );\r\n" );
            myEntityClassGet.append( "\t\t\t\toffset : Optional[ int ] = Field( None , description = \"\" );\r\n" );

            for item in myInputsJSON[ "inputs" ][ entity ] :

                myType = "str";
                if str( item[ "type" ] ) == "numeric" : 
                    myType = "float";
                elif str( item[ "type" ] ) == "varchar" : 
                    myType = "str";
                elif str( item[ "type" ] ) == "timestamp" : 
                    myType = "datetime.datetime";
                else:
                    myType = "int";

                if item[ "filter" ] == "true" and myType == "datetime.datetime" : 
                    myEntityClassGet.append( "\t\t\t\t" + str( item[ "name" ] ).lower() + "_from_date : Optional[ datetime.datetime ] = Field( None , description = \"\" );\r\n" );
                    myEntityClassGet.append( "\t\t\t\t" + str( item[ "name" ] ).lower() + "_to_date   : Optional[ datetime.datetime ] = Field( None , description = \"\" );\r\n" );
                    myEntityClass.append( "\t\t\t\t" + str( item[ "name" ] ).lower() + " : Optional[ datetime.datetime ] = Field( None , description = \"\" );\r\n" );
                else:
                    myEntityClass.append( 
                       (
                         "\t\t\t\t" + 
                             str( item[ "name" ] ).lower() + " : " + 
                             "Optional[ " + str( myType ) + " ] " + 
                             "= " + 
                             "Field( None , description = \"" + str( item[ "desc" ] )  + "\" );\r\n" 
                       )
                    );

                myEntityClassPut.append( 
                   (
                     "\t\t\t\t" + 
                         str( item[ "name" ] ).lower() + " : " + 
                         "Optional[ " + str( myType ) + " ] " + 
                         "= " + 
                         "Field( None , description = \"" + str( item[ "desc" ] )  + "\" );\r\n" 
                   )
                );

                if item[ "filter" ] == "true" :
                    myEntityClassGet.append( 
                       ( 
                         "\t\t\t\t" + 
                             str( item[ "name" ] ).lower() + " : " + 
                             "Optional[ " + str( myType ) + " ] " + 
                             "= " + 
                             "Field( None , description = \"" + str( item[ "desc" ] )  + "\" );\r\n" 
                       )
                    );

            myEntityClassGet.append( "\r\n\r\n" );
            myFile.append( "".join( myEntityClassGet ) );

            myEntityClass.append( "\r\n\r\n" );
            myFile.append( "".join( myEntityClass ) );

            myEntityClassPut.append( "\r\n\r\n" );
            myFile.append( "".join( myEntityClassPut ) );

        for row in myFile : 
            f.writelines( row );

        f.close();

    except( Exception ) as error :
        print( error );

@app.middleware("http")
async def add_process_time_header( request: Request, call_next ):
    startTime = time.time();
    print();
    print( "-------------------------------------------------------" );
    amTool.log( "[ INCOMING REQUEST : START ] : \"" + str( request.method ) + "\"" + " " + str( request.url ) );
    # amTool.log( "[ HEADERS ] " + JSON.dumps( request.headers ) );
    response  = await call_next( request );
    endTime   = time.time();
    totalTime = endTime - startTime;
    response.headers[ "X-Process-Time" ] = str( totalTime );
    amTool.log( "[ RESPONSE : ] " , response );
    amTool.log( "[ INCOMING REQUEST : END   ] : \"" + str( request.method ) + "\"" + " " + str( request.url ) + " (Code:" + str( response.status_code ) + ")" + " [Duration : " + str( round( totalTime ,3 ) ) + "]" );
    print( "-------------------------------------------------------" );
    print();
    return response;


# Dynamically creating EndPoints 
myJSONSchema  = amTool.load( "" , DBsourceFileName , "json" );
myIterator    = iter( myJSONSchema[ "inputs" ] );
loopCompleted = False if debug is False else True;

while not loopCompleted : 
    try:
        item = next( myIterator );
        def preventLastBind( item = item ):
            try:
                eval( "Inputs." + item );
                @app.put( "/" + item, 
                          tags           = [ item ] , 
                          summary        = "Add Item",
                          description    = "Add Item"
                        ) 
                async def addContext( request : List[ eval( "Inputs.put_" + item ) ] ):
                    myResponse = [];

                    if( cfg[ "settings" ][ "debug" ] is True ):
                        print( "" , flush=True );

                    amTool.log( "--- Adding Record to Database ---" , debug = cfg[ "settings" ][ "debug" ] );
                    myResponse = await myModel.addEntity( request , type=None , entity=item.lower() );

                    return myResponse;

                @app.get( "/" + item, 
                          tags         = [ item ] , 
                          response_model = List[ eval( "Inputs." + item ) ] ,
                          summary      = "Retrieve Item",
                          description  = "Retrieve Item"
                        )
                async def getContext(
                  params : eval( "Inputs.get_" + item ) = Depends()
                ):
                    myResponse = [];

                    if( cfg[ "settings" ][ "debug" ] is True ):
                        print( "" , flush=True );

                    amTool.log( "--- Getting Record from Database ---" , debug = cfg[ "settings" ][ "debug" ] );
                    myResponse = await myModel.getEntityAllProperties( params , entity=item.lower() );

                    return myResponse;

                @app.delete( "/" + item, 
                          tags         = [ item ] , 
                          summary      = "Delete Item",
                          description  = "Delete Item"
                        ) 
                async def deleteContext( id : Optional[ str ] ):
                    myResponse = [];

                    if( cfg[ "settings" ][ "debug" ] is True ):
                        print( "" , flush=True );

                    amTool.log( "--- Deleting Record from Database ---" , debug = cfg[ "settings" ][ "debug" ] );
                    myResponse = await myModel.deleteEntity( id=parseStringIntoArrayWithIntegers( id ) , entity=item.lower() );

                    return myResponse;

                @app.post( "/" + item, 
                          tags         = [ item ] , 
                          summary      = "Update Item",
                          description  = "Update Item"
                        ) 
                async def updateContext( request : eval( "Inputs." + item ) ):
                    myResponse = [];

                    if( cfg[ "settings" ][ "debug" ] is True ):
                        print( "" , flush=True );

                    amTool.log( "--- Updating Record in Database ---" , debug = cfg[ "settings" ][ "debug" ] );
                    myResponse = await myModel.updateEntity( request=request , entity=item.lower() );

                    return myResponse;
            except( Exception ) as error :
                print( "Nothing Found : " + str( item ) );

        preventLastBind();

    except StopIteration:
        loopCompleted = True;



# Helpers
def decodeJWTFromHeader( request: Request  ):

    amTool.log( "[ decodeJWTFromHeader Verification : Step 1 ] : Checking if needed Headers are available in Request" );
    if "authorization" in request.headers:
        requestJWT      = request.headers[ "authorization" ].split();
    else:
        amTool.log( "[ Verification - ERROR : Step 1 ] : Incorrect Headers. Token Missing" );
        raise HTTPException( 
            status_code = status.HTTP_400_BAD_REQUEST, 
            detail      = "Incorrect Headers. Token Missing.",
            headers     = { "WWW-Authenticate": "Bearer" }
        );

    if len( requestJWT ) != 2 : 
        amTool.log( "[ Verification - ERROR : Step 1 ] : Incorrect Headers" );
        raise HTTPException( 
            status_code = status.HTTP_400_BAD_REQUEST, 
            detail      = "Incorrect Headers",
            headers     = { "WWW-Authenticate": "Bearer" }
        );
    
    print( requestJWT[ 1 ] );

    try:
        payload = jwt.decode( 
            token      = requestJWT[ 1 ], 
            key        = None,
            options    = { "verify_signature": False }
        );

        return { 
          "payload" : payload,
          "jwt"     : requestJWT[ 1 ]
        };
    except ExpiredSignatureError :
        return { "error" : "Expired Signature" };
    except JWTError:
        return { "error" : "Invalid Signature" };

async def isDirectoryAuthorized( request: Request ):

    credentials_exception = HTTPException(
        status_code = status.HTTP_401_UNAUTHORIZED,
        detail      = "Could not validate credentials",
        headers     = { "WWW-Authenticate": "Bearer" }
    );

    amTool.log( "[ Verification : Step 1 ] : Checking if needed Headers are available in Request" );
    if "authorization" in request.headers:
        requestJWT      = request.headers[ "authorization" ].split();
    else:
        amTool.log( "[ Verification - ERROR : Step 1 ] : Incorrect Headers. Token Missing" );
        raise HTTPException( 
            status_code = status.HTTP_400_BAD_REQUEST, 
            detail      = "Incorrect Headers. Token Missing.",
            headers     = { "WWW-Authenticate": "Bearer" }
        );

    if len( requestJWT ) != 2 : 
        amTool.log( "[ Verification - ERROR : Step 1 ] : Incorrect Headers" );
        raise HTTPException( 
            status_code = status.HTTP_400_BAD_REQUEST, 
            detail      = "Incorrect Headers",
            headers     = { "WWW-Authenticate": "Bearer" }
        );

    amTool.log( "[ Verification ] : Retrieving ApiKey from Database" );
    apiKeyRecord = await myModel.getEntityAllProperties(
        params = Inputs.get_apikey(
            id     = 1 , 
            limit  = None , 
            offset = None 
        ) , 
        entity = "apikey" 
    );

    if "error" in apiKeyRecord : # return
        amTool.log( "[ ERROR - Verification ] : Api Key failed to be retrieved " , apiKeyRecord[ "error" ] );
        raise HTTPException( 
          status_code  = status.HTTP_400_BAD_REQUEST , 
          detail       = "Could not Retrieve API Key from Database" ,
          headers      = { "WWW-Authenticate": "Bearer" } 
        );

    if len( apiKeyRecord ) <= 0 : # return
        amTool.log( "[ ERROR - Verification ] : No Api Key in Database" );
        raise HTTPException( 
          status_code  = status.HTTP_400_BAD_REQUEST ,
          detail       = "No Api Key present in the Database" ,
          headers      = { "WWW-Authenticate": "Bearer" } 
        );

    serverToken  = apiKeyRecord[ 0 ][ "apikey" ];
    amTool.log( "[ Verification ] : Api Key sucesfully retrieved : \r\n'" + str( serverToken ) + "'" );
    amTool.log( "JWT : " + requestJWT[ 1 ] );

    try:
        payload = jwt.decode( 
            token      = requestJWT[ 1 ], 
            key        = serverToken, 
            algorithms = [ "HS256" ] 
        );
        username: str = payload.get( "sub" );
        # if username is None or username != "farmtopia_directory_listing" :
        if username is None :
            raise credentials_exception;

        amTool.log( "[ Verification ] : JWT Succesfully Decoded with the use of the Pre Shared Secret Key. It is properly signed from Directory Listing." );

    except JWTError as e:
        amTool.log( "[ ERROR - JWT ] : " + str( e ) );
        print( repr( e ) );
        raise credentials_exception;

    return payload;

async def getUser( userEmail ):
    # amTool.log( "[ PROVIDER : STEP 2 ] : Check if Consumer '" + str( message.get( "consumer" ) ) + "' has user record in Database" );
    userExists = await myModel.getEntityAllProperties(
        params = Inputs.get_user_access(
            # email  = str( message.get( "consumer" ) ) , 
            email  = userEmail ,  
            limit  = None , 
            offset = None 
        ) , 
        entity = "user_access" 
    );
    amTool.log( "User Retrieved" , userExists );

    if len( userExists ) <= 0 : 
        return False;
    else: 
        amTool.log( "User Already Exists with Id : " + str( userExists[ 0 ][ "id" ] ) , debug = cfg[ "settings" ][ "debug" ] );
        return userExists[ 0 ];

async def addUser( message ):
    amTool.log( "[ PROVIDER : STEP 1 ] : Adding '" + str( message.get( "consumer" ) ) + "' in Database" );
    myResponse = await myModel.addEntity( 
                    request = [ Inputs.put_user_access(
                        email = message.get( "consumer" ) , 
                        last_login = datetime.datetime.now().strftime( "%Y-%m-%d %H:%M:%S" ) , 
                        has_access = -1 
                    ) ] , 
                    type    = None , 
                    entity  = "user_access" 
                );
    amTool.log( "User Added" , myResponse );

    if "error" in myResponse :
        return False;
    else: 
        return await myModel.getEntityAllProperties(
            params = Inputs.get_user_access(
                id = myResponse[ "id" ]
            ) , 
            entity = "user_access" 
        );



# Administrative EndPoints for the Directory
@app.put( "/directory/subscription/dataset_id/{dataset_id}", 
          tags         = [ "Administration" ] , 
          summary      = "Save a Subscription request for a user , coming from the Directory",
          description  = "Save a Subscription request for a user , coming from the Directory" 
        )
async def subscribeUser(
              request    : Request ,
              dataset_id : int , 
              message    = Depends( isDirectoryAuthorized )
          ):
    try:
        amTool.log( "Decoded Message" , message );
        amTool.log( "[ PROVIDER : STEP 1 ] : Authentication Success for '" + message.get( "sub" ) + "'" );
        amTool.log( "[ PROVIDER : STEP 1.1 ] : User '" + str( message.get( "consumer" ) ) + "' wants to Subscribe to Dataset with id : '" + str( dataset_id ) + "' " );

        # Check if User already exists in our Database.If not then simply add the user.
        userObject = await getUser( message.get( "consumer" ) );
        if userObject is False : 
            temp       = await addUser( message );

        amTool.log( "[ PROVIDER ] : Finalized returning response to Directory" );

        # Any 200 Status Code response will be treated as success. Response message is irrelevant.
        return { "success" : "ok" }

    except( Exception ) as error :
        amTool.log( "[ ERROR - subscribeUser ] : " , error );
        if isinstance( error , HTTPException ) :
            raise HTTPException(
              status_code = error.status_code, 
              detail      = error.detail
            );
        elif isinstance( error , IndexError ):
            raise HTTPException(
              status_code = 401, 
              detail      = "Wrong DataSet ID"
            );
        else:
            raise HTTPException(
              status_code = 500, 
              detail      = "Issue with Server , check Logs"
            );



# Data Responses
@app.get( "/", 
          tags         = [ "Data" ] , 
          summary      = "",
          description  = "" 
        )
async def getData(
              request : Request ,
              message = Depends( isDirectoryAuthorized )
          ):

    try:
        return myData.get( None );
    except( Exception ) as error :
        amTool.log( "[ ERROR - getData ] : " , error );
        if isinstance( error , HTTPException ) :
            raise HTTPException(
              status_code = error.status_code, 
              detail      = error.detail
            );
        elif isinstance( error , IndexError ):
            raise HTTPException(
              status_code = 401, 
              detail      = "Wrong DataSet ID"
            );
        else:
            raise HTTPException(
              status_code = 500, 
              detail      = "Issue with Server , check Logs"
            );

@app.get( "/calendar", 
          tags           = [ "JSON-LD" ] , 
          summary        = "Test for JSON-LD",
          description    = "Test for JSON-LD" 
        )
async def getCalendar():
    try:
        mySchema = {
          "@context": {
            "fsm": "https://gitlab.com/Farmtopia/farmtopia-semantic-model/-/raw/main/FarmtopiaSemanticModel.rdf#",
            "pcsm": "https://www.tno.nl/agrifood/ontology/ploutos/pcsm#",
            "operatedOn": {
              "@id": "pcsm:isOperatedOn",
              "@type": "@id"
            },
            "label": "fsm:label",
            "startDatetime": "pcsm:hasStartDatetime",
            "endDatetime": "pcsm:hasEndDatetime",
            "operationNotes": "fsm:operationNotes",
            "sliceContents": {
              "@id": "fsm:sliceContents",
              "@type": "@json",
              "@container": "@set"
            },
            "datasetSlices": {
              "@id": "fsm:datasetSlices",
              "@type": "@json",
              "@container": "@set"
            }
          },
          "@graph": [
            {
              "@id": "ITC_tasks_serviceResource_representation_instance_43421",
              "@type": "fsm:Dataset",
              "datasetSlices": [
                {
                  "@id": "TasksSlice_1",
                  "@type": "fsm:Slice",
                  "@sliceContents": [
                    {
                      "@id": "b9ba775f-6558-4386-9fb4-bdb42b8452c4",
                      "@type": "pcsm:CropProtectionOperation",
                      "label": "Removal of invasive plants",
                      "startDatetime": "2024-04-24T00:00:00",
                      "endDatetime": "2024-04-24T00:00:00",
                      "operationNotes": null,
                      "operatedOn": {
                        "@id": "f258b3a1-ae7c-4ff5-acaa-fd5d51b845bd"
                      }
                    }
                  ]
                }
              ]
            }
          ]
        }


        myValidator = {
            "type": "object",
            "properties": {
                "@context": { 
                  "type": "object"
                 },
                "@graph": {
                  "type": "array"
                },
            },
            "required": [ "@context" , "@graph" ],
            "additionalProperties": False
        };

        # compacted = jsonld.compact( mySchema[ "@graph" ] , mySchema[ "@context" ] );
        # print(JSON.dumps(compacted, indent=2))

        # myRes = validate( mySchema , myValidator );
        # print( myRes );
        return mySchema;

    except( Exception , HTTPException , ValidationError ) as error :
        # print( error.message );
        print( error );
        
        return [];




"""
# EndPoints (Obsolete) 
# These have been removed , and are not used by any code , so should not be used nor implemented.
# I will leave them here for historical reasons , till the next version when they will be deleted.

@app.get( "/_obsolete/directory/subscription", 
          tags         = [ "Administration" ] , 
          summary      = "",
          description  = "" 
        )
async def getUserSubscriptions(
              request : Request ,
              message = Depends( isDirectoryAuthorized ) 
          ):
    try:
        amTool.log( "Decoded Message" , message );
        amTool.log( "[ PROVIDER : STEP 1 ] : Authentication Success for '" + message.get( "sub" ) + "'" );
        amTool.log( "[ PROVIDER : STEP 1.1 ] : User '" + str( message.get( "consumer" ) ) + "' wants all Subscribed Datasets." );
        userObject     = await getUser( message.get( "consumer" ) );
        userHasMapping = await getUserMapping( userObject , dataset_id = "" );

        myResponse         = [];
        eligibleProperties = [ "dataset_id" , "is_active" ];
        if( len( userHasMapping ) > 0 ) : 
            for record in userHasMapping : 
                myTempObject = {};
                for property in record :
                    if property in eligibleProperties : 
                        myTempObject[ property ] = record[ property ];
                myResponse.append( myTempObject );
            return myResponse;
        else:
            return [];

    except( Exception ) as error :
        amTool.log( "[ ERROR - getUserSubscriptions ] : " , error );
        if isinstance( error , HTTPException ) :
            raise HTTPException(
              status_code = error.status_code, 
              detail      = error.detail
            );
        elif isinstance( error , IndexError ):
            raise HTTPException(
              status_code = 401, 
              detail      = "Wrong DataSet ID"
            );
        else:
            raise HTTPException(
              status_code = 500, 
              detail      = "Issue with Server , check Logs"
            );

@app.get( "/_obsolete/directory/subscription/dataset_id/{dataset_id}", 
          tags         = [ "Administration" ] , 
          summary      = "",
          description  = "" 
        )
async def checkSubscription(
              request    : Request ,
              dataset_id : int , 
              message    = Depends( isDirectoryAuthorized ) 
          ):
    try:
        amTool.log( "Decoded Message" , message );
        amTool.log( "[ PROVIDER : STEP 1   ] : Authentication Success for '" + message.get( "sub" ) + "'" );
        amTool.log( "[ PROVIDER : STEP 1.1 ] : User '" + str( message.get( "consumer" ) ) + "' wants details for Dataset with ID : '" + str( dataset_id ) + "' " );
        userObject     = await getUser( message.get( "consumer" ) );
        userHasMapping = await getUserMapping( userObject , dataset_id );

        if len( userHasMapping ) > 0 : 
            amTool.log( "User has records in Mapping" );
            return userHasMapping[ 0 ];
        else: 
            amTool.log( "User has NO records in Mapping" );
            raise HTTPException(
                status_code = 401, 
                detail      = { "error" : "user '" + str( message.get( "consumer" ) ) + "' has no mapping to dataset with id '" + str( dataset_id ) + "'" }
            );

        amTool.log( "[ PROVIDER : STEP 4 ] : Finalized returning response to Directory" );

    except( Exception ) as error :
        amTool.log( "[ ERROR - checkSubscription ] : " , error );
        if isinstance( error , HTTPException ) :
            raise HTTPException(
              status_code = error.status_code, 
              detail      = error.detail
            );
        elif isinstance( error , IndexError ):
            raise HTTPException(
              status_code = 401, 
              detail      = "Wrong DataSet ID"
            );
        else:
            raise HTTPException(
              status_code = 500, 
              detail      = "Issue with Server , check Logs"
            );

@app.post( "/_obsolete/directory/subscription/dataset_id/{dataset_id}/accept", 
          tags         = [ "Administration" ] , 
          summary      = "",
          description  = "" 
        )
async def acceptSubscription(
              user_email : str , 
              dataset_id : int 
          ):
    try:

        myUserObject = await myModel.getEntityAllProperties(
            params = Inputs.get_user_access(
                email   = user_email , 
                limit   = None , 
                offset  = None 
            ) , 
            entity = "user_access" 
        );
        
        if "error" in myUserObject:
            raise HTTPException(
                status_code = 401, 
                detail      = { "error" : "User not found" }
            );
        
        amTool.log( "User Access retrieved" );
        amTool.log( myUserObject );

        myMappingObject = await myModel.getEntityAllProperties(
            params = Inputs.map_user_to_dataset(
                user_id    = myUserObject[ 0 ][ "id" ] , 
                dataset_id = dataset_id 
            ) , 
            entity = "map_user_to_dataset" 
        );
        
        if "error" in myMappingObject:
            raise HTTPException(
                status_code = 401, 
                detail      = { "error" : "Subscription not found" }
            );
        
        amTool.log( "Mapping Object Received" );
        amTool.log( myMappingObject );

        amTool.log( "[ Verification ] : Retrieving ApiKey from Database" );
        apiKeyRecord = await myModel.getEntityAllProperties(
            params = Inputs.get_apikey(
                id     = 1 , 
                limit  = None , 
                offset = None 
            ) , 
            entity = "apikey" 
        );

        if "error" in apiKeyRecord : # return
            amTool.log( "[ ERROR - Verification ] : Api Key failed to be retrieved " , apiKeyRecord[ "error" ] );
            raise HTTPException( 
              status_code  = status.HTTP_400_BAD_REQUEST , 
              detail       = "Could not Retrieve API Key from Database" ,
              headers      = { "WWW-Authenticate": "Bearer" } 
            );

        if len( apiKeyRecord ) <= 0 : # return
            amTool.log( "[ ERROR - Verification ] : No Api Key in Database" );
            raise HTTPException( 
              status_code  = status.HTTP_400_BAD_REQUEST ,
              detail       = "No Api Key present in the Database" ,
              headers      = { "WWW-Authenticate": "Bearer" } 
            );

        amTool.log( "Apikey retrieved" );
        amTool.log( apiKeyRecord );

        reqToDirectory = await sendActionResultToDirectory( "accept_subscription" , apiKeyRecord[ 0 ][ "apikey" ] , dataset_id , user_email );

        if "error" in reqToDirectory : 
            raise HTTPException(
                status_code = 500, 
                detail      = { "error" : "Failed to connect to directory." }
            );
        else:
            amTool.log( "--- Updating Record in Database ---" , debug = cfg[ "settings" ][ "debug" ] );
            myResponse = await myModel.updateEntity( 
                request = Inputs.map_user_to_dataset(
                    id         = myMappingObject[ 0 ][ "id" ],
                    is_active  = 1
                ) , 
                entity  = "map_user_to_dataset" 
            );

            if "error" in myResponse : 
                raise HTTPException(
                    status_code = 500, 
                    detail      = { "error" : "Failed to update Subscription. Please try again later." }
                );
            else:
                return { "res" : "success" }

    except( Exception ) as error :
        amTool.log( error );
        raise HTTPException(
            status_code = 500, 
            detail      = { "error" : "Exception." }
        );

@app.delete( "/_obsolete/directory/subscription/dataset_id/{dataset_id}", 
          tags         = [ "Administration" ] , 
          summary      = "",
          description  = "" 
        )
async def unSubscribeUser(
              request    : Request ,
              dataset_id : int , 
              message    = Depends( isDirectoryAuthorized ) 
          ):
    try:
        amTool.log( "Decoded Message" , message );
        amTool.log( "[ PROVIDER : STEP 1 ] : Authentication Success for '" + message.get( "sub" ) + "'" );
        amTool.log( "[ PROVIDER : STEP 1.1 ] : User '" + str( message.get( "consumer" ) ) + "' wants to UnSubscribe from Dataset with id : '" + str( dataset_id ) + "' " );
        userObject     = await getUser( message.get( "consumer" ) );
        userHasMapping = await getUserMapping( userObject , dataset_id );

        if len( userHasMapping ) > 0 : # If user has record in Mapping , Update the API Key provider by Directory
            amTool.log( "User has records in Mapping" , debug = cfg[ "settings" ][ "debug" ] );
            myDeleteMappingResponse = await myModel.deleteEntity( 
                entity = "map_user_to_dataset" , 
                id     = [ str( userHasMapping[ 0 ][ "id" ] ) ]
            );

        myDeleteUserResponse = await myModel.deleteEntity( 
            entity = "user_access" , 
            id     = [ str( userObject[ "id" ] ) ]
        );

        amTool.log( "[ PROVIDER : STEP 4 ] : Finalized returning response to Directory" );            
        return { "status" : "success" };

    except( Exception ) as error :
        amTool.log( "[ ERROR - unSubscribeUser ] : " , error );
        if isinstance( error , HTTPException ) :
            raise HTTPException(
              status_code = error.status_code, 
              detail      = error.detail
            );
        elif isinstance( error , IndexError ):
            raise HTTPException(
              status_code = 401, 
              detail      = "Wrong DataSet ID"
            );
        else:
            raise HTTPException(
              status_code = 500, 
              detail      = "Issue with Server , check Logs"
            );

@app.put( "/_obsolete/directory/subscription/dataset_id/{dataset_id}", 
          tags         = [ "Administration" ] , 
          summary      = "Save a Subscription request for a user , coming from the Directory",
          description  = "Save a Subscription request for a user , coming from the Directory" 
        )
async def subscribeUser(
              request    : Request ,
              dataset_id : int , 
              message    = Depends( isDirectoryAuthorized )
          ):
    try:
        amTool.log( "Decoded Message" , message );
        amTool.log( "[ PROVIDER : STEP 1 ] : Authentication Success for '" + message.get( "sub" ) + "'" );
        amTool.log( "[ PROVIDER : STEP 1.1 ] : User '" + str( message.get( "consumer" ) ) + "' wants to Subscribe to Dataset with id : '" + str( dataset_id ) + "' " );
        
        # Check if User already exists in our Database
        # If not then create the user
        userObject = await getUser( message.get( "consumer" ) );
        if userObject is False : 
            temp       = await addUser( message );
            userObject = temp[ 0 ];

        # Check if the User has already requested subscription on the specific dataset
        userHasMapping = await getUserMapping( userObject , dataset_id );

        # If the user has a subscription already then : 
        # If it is declined , then reset it to pending. All other statuses leave it as is
        if len( userHasMapping ) > 0 : 
            amTool.log( "User has records in Mapping" , debug = cfg[ "settings" ][ "debug" ] );
            amTool.log( "Updating User API Key for row in map '" + str( userHasMapping[ 0 ][ "dataset_id" ] ) + "' " , debug = cfg[ "settings" ][ "debug" ] );
            myResponse = await myModel.updateEntity(
                request = Inputs.map_user_to_dataset(
                    id         = userHasMapping[ 0 ][ "id" ],
                    user_id    = userObject[ "id" ] , 
                    dataset_id = dataset_id ,
                    # token      = message.get( "token" ) ,
                    expires_on = message.get( "expiration" ) , 
                    is_active  = userHasMapping[ 0 ][ "is_active" ]
                ) , 
                entity  = "map_user_to_dataset" 
            );
            return { "status" : "success" };

        else: # If User Does not have a record in mapping add the one provided by directory
            amTool.log( "User has NO records in Mapping" , debug = cfg[ "settings" ][ "debug" ] );
            amTool.log( "Adding User to Mapping" , debug = cfg[ "settings" ][ "debug" ] );
            myResponse = await myModel.addEntity(
                request = [ Inputs.put_map_user_to_dataset(
                    user_id    = userObject[ "id" ] , 
                    dataset_id = dataset_id , 
                    # token      = message.get( "token" ) , 
                    expires_on = message.get( "expiration" ) ,
                    is_active  = -1
                ) ] , 
                type    = None , 
                entity  = "map_user_to_dataset" 
            );
            return { "status" : "pending" };

        amTool.log( "[ PROVIDER : STEP 4 ] : Finalized returning response to Directory" );

    except( Exception ) as error :
        amTool.log( "[ ERROR - subscribeUser ] : " , error );
        if isinstance( error , HTTPException ) :
            raise HTTPException(
              status_code = error.status_code, 
              detail      = error.detail
            );
        elif isinstance( error , IndexError ):
            raise HTTPException(
              status_code = 401, 
              detail      = "Wrong DataSet ID"
            );
        else:
            raise HTTPException(
              status_code = 500, 
              detail      = "Issue with Server , check Logs"
            );

@app.post( "/_obsolete/directory/subscription/dataset_id/{dataset_id}/update_token", 
          tags         = [ "Administration" ] , 
          summary      = "",
          description  = "" 
        )
async def updateSubscriptionToken(
              request       : Request ,
              dataset_id    : int , 
              consumerEmail : str , 
              token         : str 
          ):
    try:
        amTool.log( "[ updateSubscriptionToken ] : For dataset with id [ " + str( dataset_id ) + " ] and email [ " + str( consumerEmail ) + " ]" );

        userObject = await getUser( consumerEmail );
        if userObject is False : 
            raise HTTPException(
              status_code = 401, 
              detail      = "User is not in the database"
            );

        userHasMapping = await getUserMapping( userObject , dataset_id );

        if len( userHasMapping ) > 0 : 
            amTool.log( "User has records in Mapping" , debug = cfg[ "settings" ][ "debug" ] );
            amTool.log( "Updating User API Key for row in map '" + str( userHasMapping[ 0 ][ "dataset_id" ] ) + "' " , debug = cfg[ "settings" ][ "debug" ] );
            myResponse = await myModel.updateEntity(
                request = Inputs.map_user_to_dataset(
                    id         = userHasMapping[ 0 ][ "id" ],
                    token      = token 
                    # user_id    = userObject[ "id" ] , 
                    # dataset_id = dataset_id ,
                    # expires_on = message.get( "expiration" ) , 
                    # is_active  = userHasMapping[ 0 ][ "is_active" ]
                ) , 
                entity  = "map_user_to_dataset" 
            );
            return { "status" : "success" };

        else: # If User Does not have a record in mapping add the one provided by directory
            raise HTTPException(
              status_code = 401, 
              detail      = "User has no subscription for dataset"
            );

        amTool.log( "[ PROVIDER : STEP 4 ] : Finalized returning response to Directory" );

    except( Exception ) as error :
        amTool.log( "[ ERROR - subscribeUser ] : " , error );
        if isinstance( error , HTTPException ) :
            raise HTTPException(
              status_code = error.status_code, 
              detail      = error.detail
            );
        elif isinstance( error , IndexError ):
            raise HTTPException(
              status_code = 401, 
              detail      = "Wrong DataSet ID"
            );
        else:
            raise HTTPException(
              status_code = 500, 
              detail      = "Issue with Server , check Logs"
            );


# Helpers (Obsolete) 
# These have been removed , and are not used by any code , so should not be used nor implemented.
# I will leave them here for historical reasons , till the next version when they will be deleted.
def __getJWT( api_secret , message ):
    # jwt = hmac.new(
        # bytes( api_secret , 'latin-1' ),
        # msg       = bytes( message, 'latin-1' ),
        # digestmod = hashlib.sha256
    # ).hexdigest().upper();

    # Encode payload
    encoded_payload = base64.urlsafe_b64encode( 
                            JSON.dumps( message )
                                .encode( 'utf-8' )
                        ).decode( 'utf-8' );

    # Create a header
    header = {"alg": "HS256", "typ": "JWT"}
    encoded_header = base64.urlsafe_b64encode(
                             JSON.dumps( header )
                                 .encode( 'utf-8' )
                         ).decode( 'utf-8' );

    # Create a signature
    signature = hmac.new( 
        api_secret.encode( 'utf-8' ) , 
        f"{encoded_header}.{encoded_payload}".encode('utf-8') , 
        hashlib.sha256
    );
    encoded_signature = base64.urlsafe_b64encode(
        signature.digest()
    ).decode( 'utf-8' );

    # Construct JWT
    jwt = f"{encoded_header}.{encoded_payload}.{encoded_signature}";

    return jwt;

async def __sendActionResultToDirectory( type , apikey , dataset_id , user_email ):
    async with aiohttp.ClientSession() as session:
        myPost = {
            "sub" : "provider@provider.com" 
        }

        myJWT = jwt.encode( myPost, apikey, algorithm = 'HS256' );

        async with session.post( 
                      "http://" + str( myIP ) + ":8585/administration/subscription/activate?" + 
                      "dataset_id=" + str( dataset_id ) + 
                      "&user_email=" + str( user_email ) , 
                      # json    = myPost , 
                      headers = {
                        'content-type': 'application/json' , 
                        'accept' : 'application/json' , 
                        'Authorization' : 'Bearer ' + myJWT 
                      } 
                   ) as response:

             myText = await response.text();

             try:
                 myJSON = JSON.loads( myText );
                 return myJSON;
             except ValueError as error :
                 print( error );
                 return { "error" : "There was an issue retrieving data." };

async def __verifyRequest( myProvidedEmail , jwt ):

    amTool.log( "[ PROVIDER - JWT Verification ] : Incoming Request" );
    amTool.log( "[ Verification : Step 2 ] : Retrieving User '" + str( myProvidedEmail ) + "' from DB" );
    myUserConsumerObject = await myModel.getEntityAllProperties(
        params = Inputs.get_user_access(
            email   = myProvidedEmail , 
            limit   = None , 
            offset  = None 
        ) , 
        entity = "user_access" 
    );

    if "error" in myUserConsumerObject : 
        amTool.log( "[ Verification - ERROR : Step 2 ] : Issue retrieving user" , myUserConsumerObject[ "error" ] );
        # return { "status" : 500 , "error" : "Could not Retrieve User from the Database" }
        raise HTTPException( 
            status_code = 401, 
            detail      = "Invalid User",
            headers     = { "WWW-Authenticate": "Bearer" }
        );

    if len( myUserConsumerObject ) <= 0 :
        amTool.log( "[ Verification - ERROR : Step 2 ] : No User in the Database" );
        # return { "status" : 401 , "error" : "No User present in the Database" }
        raise HTTPException( 
            status_code = 401, 
            detail      = "No User present in the Database",
            headers     = { "WWW-Authenticate": "Bearer" }
        );

    amTool.log( "[ Verification : Step 3 ] : Retrieving Peer-Key of '" + str( myProvidedEmail ) + "' from DB" );
    apiKeyRecord = await myModel.getEntityAllProperties(
        params = Inputs.get_map_user_to_dataset(
            user_id    = myUserConsumerObject[ 0 ][ "id" ] , 
            limit      = None , 
            offset     = None 
        ) , 
        entity = "map_user_to_dataset" 
    );

    if "error" in apiKeyRecord : 
        amTool.log( "[ Verification - ERROR : Step 3 ] : Could not Retrieve Peer-Key from Database" , apiKeyRecord[ "error" ] );
        raise HTTPException( 
            status_code = 500, 
            detail      = "Could not Retrieve Peer-Key from Database",
            headers     = { "WWW-Authenticate": "Bearer" }
        );

    if len( apiKeyRecord ) <= 0 :
        amTool.log( "[ Verification - ERROR : Step 3 ] : No Peer-Key present in the Database" );
        # return { "status" : 401 , "error" : "No Peer-Key present in the Database" }
        raise HTTPException( 
            status_code = 401, 
            detail      = "No Peer-Key present in the Database",
            headers     = { "WWW-Authenticate": "Bearer" }
        );

    amTool.log( "[ Verification : Step 3 ] : Peer-Key successfully retrieved '" + str( apiKeyRecord[ 0 ][ "token" ] ) + "'" );

    # Checking expiration Date in order to discard it
    amTool.log( "[ Verification : Step 5 ] : Comparing recreated JWT with the JWT provided." );
    if apiKeyRecord[ 0 ][ "token" ] == jwt : 
        amTool.log( "[ Verification - SUCCESS : Step 5 ] : Request and Server JWT match" );
    else:
        amTool.log( "[ Verification - ERROR : Step 5 ] : Request and Server JWT DO NOT match" );
        raise HTTPException( 
            status_code = 401, 
            detail      = "Unknown Requester",
            headers     = { "WWW-Authenticate": "Bearer" }
        );

    return True;

async def __getUserMapping( userObject , dataset_id ):
    amTool.log( "[ PROVIDER : STEP 3 ] : Check if User has already been Mapped with a Dataset in the Database" );
    amTool.log( "Checking Mapping of User with Id : " + str( userObject[ "id" ] ) + " and DatasetID : " + str( dataset_id ) );
    userHasMapping = await myModel.getEntityAllProperties(
        params = Inputs.map_user_to_dataset(
            user_id    = userObject[ "id" ] , 
            dataset_id = dataset_id,
            limit      = None , 
            offset     = None 
        ) , 
        entity = "map_user_to_dataset" 
    );

    amTool.log( "userHasMapping " , userHasMapping );

    return userHasMapping;

"""