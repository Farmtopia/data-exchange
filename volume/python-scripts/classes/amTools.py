import os;
import datetime;
import sys;
import aiohttp;
import asyncio;
import json as JSON;
import hashlib;
from time import time;

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[91m'
    FAIL = '\033[93m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class amTool:

 def __init__( self ):
     """Class for Misc Tools"""
     # self.log( "Tools Initiated" );
     # self.cfg = self.load( "./cfg/" , "config" );

 def log( self , myString ):
     myCurrentTime = datetime.datetime.now();
     myMessage = bcolors.OKBLUE + "[" + datetime.datetime.strftime( myCurrentTime , "%Y-%m-%d %H:%M:%S" ) + "] : " + bcolors.ENDC + str( myString );
     print( bcolors.OKGREEN + " [ INFO ] " + bcolors.ENDC + myMessage  , flush=True);

 def load( self , filePath , fileName , type = "json" ):
     """
      Loads Configuration from Disk
     """
     myCFG = dict();
     try:
         fileName = filePath + fileName + "." + type;
         if( os.path.exists( fileName ) == False ):
            self.log("[ amModel Error ] : File [" + fileName + "] Not Found");
            return False;

         f = open( fileName, "r" );
         if f.mode == "r":
             myContents = f.read();
             try:
                 if( type == "json" ):
                     myCFG = dict( JSON.loads( myContents ) );
                     self.log( "File ["+fileName+"] loaded." );
                 else:
                     myCFG = myContents;
             except:
                 self.log( "File ["+fileName+"] could not be loaded. Reason : ["+ str(sys.exc_info()[1]) +"]" );
         f.close();
         return myCFG;
     except:
         self.log( "Unexpected error upon loading Storage ["+fileName+"]:" + str( sys.exc_info()[1] ) );
         return False;

 def logFile( self , myString , cfg ) : 
     myCurrentTime = datetime.datetime.now();
     myPrefix = "[" + datetime.datetime.strftime( myCurrentTime , "%Y-%m-%d %H:%M:%S" ) + "] : ";

     fd = open( os.path.abspath( cfg["logs"]["path"]+"log.log" ) , "a+");
     fd.write( myPrefix + str( myString ) + "\n");
     fd.close();

 def log_curl( self , method , postData , endpoint , file=False , auth=False ):
     myMethod      = "";
     myAuth        = "";
     myContentType = "Content-type: application/json";
     myEndpoint    = endpoint;

     if( method     == "post" ):   myMethod = "-XPOST";
     elif( method   == "get" ):    myMethod = "-XGET";
     elif( method   == "delete" ): myMethod = "-XDELETE";
     elif( method   == "put" ):    myMethod = "-XPUT";
     else: myMethod =  "XGET";

     if( auth is not False ): myAuth = auth;

     if( file == False ):
         myPostData  = JSON.dumps( postData ).replace( "'", "\\\"" ).replace( "\"", "\\\"" );
         myLogString = f"curl {myMethod} -u {myAuth} -H \"{myContentType}\" -d \"{myPostData}\" \"{myEndpoint}\" ";
     else:
         myFileName  = file["name"];
         myFilePath  = file["path"];
         myLogString = f"curl -u {myAuth} -F name=\"{myFileName}\" -F filedata=@{myFilePath}\" \"{myEndpoint}\" ";

     print();
     print( " [ CURL ] " )
     print( myLogString );
     print();
     return myLogString;

 def getUID( self , hash ):
     currentDate     = datetime.datetime.now();
     currentTS       = round( currentDate.timestamp() );
     myStringHash    = str( currentTS ) + str( hash );
     
     return myStringHash;

 def isValidJSON( self , json ):
     try:
         myJSON = JSON.loads( json );
     except ValueError as error :
         return False;
     return True;

 def isInt( self , value ):
     try:
         isValueInt = int( value );
         return True;
     except( Exception ) as error:
         return False;

 def createFile( self , fileName , myString , cfg ) : 
     myCurrentTime = datetime.datetime.now();
     myPrefix = "[" + datetime.datetime.strftime( myCurrentTime , "%Y-%m-%d %H:%M:%S" ) + "] : ";

     fd = open( os.path.abspath( cfg["logs"]["path"] + datetime.datetime.strftime( myCurrentTime , "%Y_%m_%d_%H_%M_%S" ) + "_" + fileName + ".log" ) , "a+");
     fd.write( myPrefix + str( myString ) + "\n");
     fd.close();
 
 def isJSON( self , string ):
     try:
         JSON.loads( string );
     except ValueError as e :
         return False;
     
     return True;
 
 async def request( self , options ):
     timeout = aiohttp.ClientTimeout( total=2 );
     async with aiohttp.ClientSession( timeout=timeout ) as session:
         try:
             if options[ "method" ] == "GET" : 
                 async with session.get( 
                               url     = options[ "endPoint" ] , 
                               headers = options[ "headers" ]
                            ) as response:

                     myText = await response.text();
                     # print( myText );

                     try:
                         myJSON = JSON.loads( myText );
                         return myJSON;
                     except ValueError as error :
                         return { "error" : error };
             elif options[ "method" ] == "POST" : 
                 async with session.post( 
                               url     = options[ "endPoint" ] ,
                               json    = options[ "myData"], 
                               headers = options[ "headers" ]
                            ) as response:

                     myText = await response.text();
                     # print( myText );

                     try:
                         myJSON = JSON.loads( myText );
                         return myJSON;
                     except ValueError as error :
                         return { "error" : error };
             elif options[ "method" ] == "PUT" : 
                 async with session.put( 
                               url     = options[ "endPoint" ] ,
                               json    = options[ "myData"], 
                               headers = options[ "headers" ]
                            ) as response:

                     myText = await response.text();
                     # print( myText );

                     try:
                         myJSON = JSON.loads( myText );
                         return myJSON;
                     except ValueError as error :
                         template = "0) An exception of type {0} occurred. Arguments:\n{1!r}"
                         message = template.format(type(error).__name__, error.args)
                         print( message );
                         return { "error" : error };
         except aiohttp.ClientConnectorError as connerror :
             return { "error" : "[aiohttp] An exception of type {0} occurred.".format( type( connerror ).__name__ ) };
             # print( connerror );
             # return { "error" : connerror };
         except Exception as e :
             return { "error" : "[generic] An exception of type {0} occurred.".format( type( e ).__name__ ) };

 def writeFile( self , path , string ) : 
     fd = open( path , "w" );
     fd.write( string );
     fd.close();
