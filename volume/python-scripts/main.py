"""
Go to the Database of your Docker and execute the following in order to truncate all tables and start from scratch
TRUNCATE TABLE farmtopia_directory.dataset RESTART IDENTITY CASCADE;
TRUNCATE TABLE farmtopia_directory.map_user_to_dataset RESTART IDENTITY CASCADE;
TRUNCATE TABLE farmtopia_directory.user_access RESTART IDENTITY CASCADE;
TRUNCATE TABLE farmtopia_provider.apikey RESTART IDENTITY CASCADE;
TRUNCATE TABLE farmtopia_provider.map_user_to_dataset RESTART IDENTITY CASCADE;
TRUNCATE TABLE farmtopia_provider.user_access RESTART IDENTITY CASCADE;
TRUNCATE TABLE farmtopia_provider_2.apikey RESTART IDENTITY CASCADE;
TRUNCATE TABLE farmtopia_provider_2.map_user_to_dataset RESTART IDENTITY CASCADE;
TRUNCATE TABLE farmtopia_provider_2.user_access RESTART IDENTITY CASCADE;
"""

from   enum     import Enum;
import time;
import datetime;
from   datetime import timedelta;
import os;
import os.path
from   os import path
import aiohttp;
import aiofiles;
import asyncio;
import json as JSON;
import sys;
import random;
import hashlib;
import psycopg2;
import re;
from   jose            import JWTError, ExpiredSignatureError , jwt;
from   classes.amTools import amTool;
from   classes.amTools import bcolors;

os.system('color');
amTool          = amTool();
bcolors         = bcolors();
cfg             = amTool.load( "./cfg/" , "config" );
myLocalStorage  = globals();
stopIndex       = 26;

myIP = os.getenv( 'localip' );
serverSecretKey = "DqBflEflm8JFGMaGF4iL4uU7zYcCPw7bCKRXXCqymvnsSXhXF35LduRuXdDfi6hAn7Ki";

def replaceValuesRecursive( obj, myLocalStorage ):
    def replaceString( myString ):
        def replacer( match ):
            myVariableName = match.group( 1 );
            if myVariableName in myLocalStorage:
                return str( myLocalStorage[ myVariableName ] );
            return match.group( 0 )  # return the match unchanged if not found in myLocalStorage
        return re.sub( r"@(\w+)", replacer, myString );

    if isinstance( obj, dict ):
        for key, value in obj.items():
            if isinstance( value, str ):
                newValue = replaceString( value );
                if newValue != value:
                    amTool.log( "[ REPLACING ] : " + str( key ) + " | " + str( newValue ) );
                obj[ key ] = newValue;
            else:
                replaceValuesRecursive( value, myLocalStorage );
    elif isinstance( obj, list ):
        for index, item in enumerate( obj ):
            if isinstance( item, str ):
                newValue = replaceString( item );
                if newValue != item:
                    amTool.log( "[ REPLACING ] : List Item " + str( index ) + " | " + str( newValue ) );
                obj[index] = newValue;
            else:
                replaceValuesRecursive( item, myLocalStorage );

def getJWT( params , apikey ):
    return jwt.encode( params, apikey, algorithm = 'HS256' );

"""
Accept Subscription Request

curl -XPOST "http://np-mos.centraldc.gr:8885/token" ^
-H "accept: application/json" ^
-H "Content-Type: application/x-www-form-urlencoded" ^
-d "grant_type=&username=provider%40provider.com&password=FarmTopia11334&scope=&client_id=&client_secret="

curl -XPOST "http://np-mos.centraldc.gr:8885/administration/subscription/dataset_id/2?consumerEmail=consumer%40consumer.com&status_id=3" ^
-H "accept: application/json" ^
-H "Content-Type: application/json" ^
-H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJwcm92aWRlckBwcm92aWRlci5jb20iLCJleHAiOjE3MjYwNTUxMzR9.YrN19TziWZPscDrcmO6mGUUKbPkRFInDOrKtpX7U4aE"

curl -XPOST "http://np-mos.centraldc.gr:8885/token" ^
-H "accept: application/json" ^
-H "Content-Type: application/x-www-form-urlencoded" ^
-d "grant_type=&username=provider_2%40provider_2.com&password=FarmTopia11334&scope=&client_id=&client_secret="

curl -XPOST "http://np-mos.centraldc.gr:8885/administration/subscription/dataset_id/2?consumerEmail=consumer%40consumer.com&status_id=3" ^
-H "accept: application/json" ^
-H "Content-Type: application/json" ^
-H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJwcm92aWRlcl8yQHByb3ZpZGVyXzIuY29tIiwiZXhwIjoxNzI2MDYzNDA0fQ.xwC6HuzqIcjsKwv4u32V4yEMJLhnpUTd5R39RPff5t0"

"""

async def main():
    myHeaders = {
        'accept' : 'application/json' 
    }

    amTool.log( "Starting Script Execution" );

    requestList = [
      { "title"    : "Reset all previous changes and make clear database for the integration test." , 
        "method"   : "GET" , 
        "endpoint" : "http://" + myIP + ":8885/administration/clear_everything", 
        "dataType" : "url" , 
        "myData"   : {
          "token"    : serverSecretKey
        },
        "headers"  : {
          "accept" : "application/json" 
        }
      },
      { "title"    : "(Provider 1) Create User in Directory" , 
        "method"   : "PUT" , 
        "endpoint" : "http://" + myIP + ":8885/administration/user", 
        "dataType" : "url" , 
        "myData"   : {
          "username" : "provider@provider.com" , 
          "password" : "FarmTopia11334",
          "token"    : serverSecretKey
        },
        "headers"  : {
          "accept" : "application/json" 
        },
        "responseType"   : "object" , 
        "responseToKeep" : { 
          "propertySource" : "user_id" , 
          "propertyTarget" : "user_id"
        }
      },
      { "title"    : "(Provider 1) Receive JWT for User created" , 
        "method"   : "POST" , 
        "endpoint" : "http://" + myIP + ":8885/token", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/x-www-form-urlencoded" , 
        },
        "dataType" : "url" , 
        "myData" : {
          "grant_type" : "" ,
          "username" : "provider@provider.com" , 
          "password" : "FarmTopia11334" ,
          "scope" : "",
          "client_id" : "",
          "client_secret" : ""
        },
        "responseType"   : "object" , 
        "responseToKeep" : { 
          "propertySource" : "access_token" , 
          "propertyTarget" : "provider_access_token"
        }
      },
      { "title"    : "(Provider 1) Get UserID with the use of JWT of user" , 
        "method"   : "GET" , 
        "endpoint" : "http://" + myIP + ":8885/administration/user", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/json" , 
          "Authorization" : "Bearer @provider_access_token"
        }
      },
      { "title"    : "(Provider 1) Get UserData Object with the credentials of user" , 
        "method"   : "GET" , 
        "endpoint" : "http://" + myIP + ":8885/administration/userdata?username=provider@provider.com&password=FarmTopia11334", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/json" , 
          "Authorization" : "Bearer @provider_access_token"
        }
      },
      { "title"    : "(Provider 1) Create Dataset for Provider 1" , 
        "method"   : "PUT" , 
        "endpoint" : "http://" + myIP + ":8885/administration/dataset", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/json" , 
          "Authorization" : "Bearer @provider_access_token"
        },
        "dataType" : "json" , 
        "myData" : [
          {
            "name"            : "Provider 1 (Source)",
            "description"     : "Some description goes here",
            "service"         : "http://" + myIP + ":8886",
            "data"            : "http://" + myIP + ":8886/data",
            "metadata"        : "{ \"asdasd\" : \"asdasdasd\" }",
            "swagger"         : "http://" + myIP + ":8886/documentation",
            "openapi"         : "http://" + myIP + ":8886/openapi.json",
            "subscription"    : "http://" + myIP + ":8886/directory/subscription"
          }
        ],
        "responseType"   : "object" , 
        "responseToKeep" : { 
          "propertySource" : "id" , 
          "propertyTarget" : "dataset_id"
        }
      },
      { "title"    : "(Provider 1) Update Dataset for Provider 1 (Step 6)" , 
        "method"   : "POST" , 
        "endpoint" : "http://" + myIP + ":8885/administration/dataset", 
        "headers"  : {
          "accept"        : "application/json" ,
          "Content-Type"  : "application/json" , 
          "Authorization" : "Bearer @provider_access_token"
        },
        "dataType" : "json" , 
        "myData" : {
          "dataset_id"  : "@dataset_id",
          "description" : "this is something new"
        }
      },
      { "title"    : "(Provider 1) Create Shared Secret Key" , 
        "method"   : "GET" , 
        "endpoint" : "http://" + myIP + ":8885/administration/shared_secret_key", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/json" , 
          "Authorization" : "Bearer @provider_access_token"
        },
        "responseType"   : "object" , 
        "responseToKeep" : { 
          "propertySource" : "apikey" , 
          "propertyTarget" : "provider_secret_key"
        }
      },
      { "title"    : "(Provider 1) Check if user has Secret key" , 
        "method"   : "GET" , 
        "endpoint" : "http://" + myIP + ":8885/administration/has_secret_key", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/json" , 
          "Authorization" : "Bearer @provider_access_token"
        }
      },
      { "title"    : "(Provider 1) Check if Provider has already a record for Secret Key in the Database (Provider EndPoint for Testing this is not needed it is just for Dev purposes)" , 
        "method"   : "GET" , 
        "endpoint" : "http://" + myIP + ":8886/apikey", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/json" 
        },
        "dataType" : "url" , 
        "myData" : {
          "id" : "1"
        }
      },
      { "title"    : "(Provider 1) Save Secret Key to Provider Database (Provider EndPoint for Testing this is not needed it is just for Dev purposes)" , 
        "method"   : "PUT" , 
        "endpoint" : "http://" + myIP + ":8886/apikey", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/json" 
        },
        "dataType" : "json" , 
        "myData" : [{
          "apikey" : "@provider_secret_key"
        }]
      },
      { "title"    : "(Provider 1) Update Secret Key with the new one in the Database (Provider EndPoint for Testing this is not needed it is just for Dev purposes) (step 12)" , 
        "method"   : "POST" , 
        "endpoint" : "http://" + myIP + ":8886/apikey", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/json" 
        },
        "dataType" : "json" , 
        "myData" : {
          "id"     : "1",
          "apikey" : "@provider_secret_key"
        }
      },
      { "title"    : "(Provider 2) Create User in Directory" , 
        "method"   : "PUT" , 
        "endpoint" : "http://" + myIP + ":8885/administration/user", 
        "dataType" : "url" , 
        "myData"   : {
          "username" : "provider_2@provider_2.com" , 
          "password" : "FarmTopia11334",
          "token"    : serverSecretKey
        },
        "headers"  : {
          "accept" : "application/json" 
        },
        "responseType"   : "object" , 
        "responseToKeep" : { 
          "propertySource" : "user_id" , 
          "propertyTarget" : "user_id"
        }
      },
      { "title"    : "(Provider 2) Receive JWT for User created" , 
        "method"   : "POST" , 
        "endpoint" : "http://" + myIP + ":8885/token", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/x-www-form-urlencoded" , 
        },
        "dataType" : "url" , 
        "myData" : {
          "grant_type" : "" ,
          "username" : "provider_2@provider_2.com" , 
          "password" : "FarmTopia11334" ,
          "scope" : "",
          "client_id" : "",
          "client_secret" : ""
        },
        "responseType"   : "object" , 
        "responseToKeep" : { 
          "propertySource" : "access_token" , 
          "propertyTarget" : "provider_2_access_token"
        }
      },
      { "title"    : "(Provider 2) Create Dataset for Provider 2" , 
        "method"   : "PUT" , 
        "endpoint" : "http://" + myIP + ":8885/administration/dataset", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/json" , 
          "Authorization" : "Bearer @provider_2_access_token"
        },
        "dataType" : "json" , 
        "myData" : [
          {
            "name"            : "Provider 2 (Translator)",
            "description"     : "Some description goes here",
            "service"         : "http://" + myIP + ":8887",
            "data"            : "http://" + myIP + ":8887/data",
            "metadata"        : "{ \"provider2\" : \"provider2\" }",
            "swagger"         : "http://" + myIP + ":8887/documentation",
            "openapi"         : "http://" + myIP + ":8887/openapi.json",
            "subscription"    : "http://" + myIP + ":8887/directory/subscription"
          }
        ],
        "responseType"   : "object" , 
        "responseToKeep" : { 
          "propertySource" : "id" , 
          "propertyTarget" : "dataset_id_provider_2"
        }
      },
      { "title"    : "(Provider 2) Create Shared Secret Key" , 
        "method"   : "GET" , 
        "endpoint" : "http://" + myIP + ":8885/administration/shared_secret_key", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/json" , 
          "Authorization" : "Bearer @provider_2_access_token"
        },
        "responseType"   : "object" , 
        "responseToKeep" : { 
          "propertySource" : "apikey" , 
          "propertyTarget" : "provider_2_secret_key"
        }
      },
      { "title"    : "(Provider 2) Save Secret Key to Provider Database (Provider EndPoint for Testing this is not needed it is just for Dev purposes) (step 17)" , 
        "method"   : "PUT" , 
        "endpoint" : "http://" + myIP + ":8887/apikey", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/json" 
        },
        "dataType" : "json" , 
        "myData" : [{
          "apikey" : "@provider_2_secret_key"
        }]
      },
      { "title"    : "(Consumer) Create User in Directory" , 
        "method"   : "PUT" , 
        "endpoint" : "http://" + myIP + ":8885/administration/user", 
        "dataType" : "url" , 
        "myData"   : {
          "username" : "consumer@consumer.com" , 
          "password" : "FarmTopia11334",
          "token"    : serverSecretKey
        },
        "headers"  : {
          "accept" : "application/json" 
        },
        "responseType"   : "object" , 
        "responseToKeep" : { 
          "propertySource" : "user_id" , 
          "propertyTarget" : "user_id"
        }
      },
      { "title"    : "(Consumer) Receive JWT for User created" , 
        "method"   : "POST" , 
        "endpoint" : "http://" + myIP + ":8885/token", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/x-www-form-urlencoded" , 
        },
        "dataType" : "url" , 
        "myData" : {
          "grant_type" : "" ,
          "username" : "consumer@consumer.com" , 
          "password" : "FarmTopia11334" ,
          "scope" : "",
          "client_id" : "",
          "client_secret" : ""
        },
        "responseType"   : "object" , 
        "responseToKeep" : { 
          "propertySource" : "access_token" , 
          "propertyTarget" : "consumer_access_token"
        }
      },
      { "title"    : "(Consumer) Retrieve all Datasets registered in Directory (Authenticated User)" , 
        "method"   : "GET" , 
        "endpoint" : "http://" + myIP + ":8885/administration/datasets", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/json" , 
          "Authorization" : "Bearer @consumer_access_token"
        }
      },
      { "title"    : "(Consumer) Subscribe to a Dataset (Authenticated User) (Step 21)" , 
        "method"   : "POST" , 
        "endpoint" : "http://" + myIP + ":8885/administration/user/subscription/dataset_id/@dataset_id_provider_2", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/json" , 
          "Authorization" : "Bearer @consumer_access_token"
        }
      },
      { "title"    : "(Consumer) Get Subscription Status (Authenticated User)" , 
        "method"   : "GET" , 
        "endpoint" : "http://" + myIP + ":8885/administration/user/subscription/dataset_id/@dataset_id_provider_2", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/json" , 
          "Authorization" : "Bearer @consumer_access_token"
        }
      },
      { "title"    : "(Provider 1) Decline Subscription (Step 23)" , 
        "method"   : "POST" , 
        "endpoint" : "http://" + myIP + ":8885/administration/subscription/dataset_id/@dataset_id_provider_2", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/json" , 
          "Authorization" : "Bearer @provider_2_access_token"
        },
        "dataType" : "url" , 
        "myData" : {
          "consumerEmail" : "consumer@consumer.com" ,
          "status_id"     : "2"
        }
      },
      { "title"    : "(Provider 1) Get Subscription Status for specific user" , 
        "method"   : "GET" , 
        "endpoint" : "http://" + myIP + ":8885/administration/subscription/dataset_id/@dataset_id_provider_2", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/json" , 
          "Authorization" : "Bearer @provider_2_access_token"
        },
        "dataType" : "url" , 
        "myData" : {
          "consumerEmail" : "consumer@consumer.com" 
        }
      },
      { "title"    : "(Provider 1) Accept Subscription (Step 25)" , 
        "method"   : "POST" , 
        "endpoint" : "http://" + myIP + ":8885/administration/subscription/dataset_id/@dataset_id_provider_2", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/json" , 
          "Authorization" : "Bearer @provider_2_access_token"
        },
        "dataType" : "url" , 
        "myData" : {
          "consumerEmail" : "consumer@consumer.com" ,
          "status_id"     : "3"
        },
      },
      { "title"    : "(Consumer) Get Subscription Status" , 
        "method"   : "GET" , 
        "endpoint" : "http://" + myIP + ":8885/administration/user/subscription/dataset_id/@dataset_id_provider_2", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/json" , 
          "Authorization" : "Bearer @consumer_access_token"
        },
        "dataType" : "url" , 
        "myData" : {
          "consumerEmail" : "consumer@consumer.com" 
        },
        "responseType"   : "object" , 
        "responseToKeep" : { 
          "propertySource" : "token" , 
          "propertyTarget" : "consumer_access_token_for_provider_2"
        }
      },
      { "title"    : "(Consumer) Get Data from Provider" , 
        "method"   : "GET" , 
        "endpoint" : "http://" + myIP + ":8887", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/json" , 
          "Authorization" : "Bearer @consumer_access_token_for_provider_2"
        }
      },
      { "title"    : "(Consumer) Update Subscription Token" , 
        "method"   : "POST" , 
        "endpoint" : "http://" + myIP + ":8885/administration/user/subscription/dataset_id/@dataset_id_provider_2/update", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/json" , 
          "Authorization" : "Bearer @consumer_access_token"
        },
        "responseType"   : "object" , 
        "responseToKeep" : { 
          "propertySource" : "token" , 
          "propertyTarget" : "consumer_access_token_for_provider_2"
        }
      },
      { "title"    : "(Consumer) Get Data from Provider" , 
        "method"   : "GET" , 
        "endpoint" : "http://" + myIP + ":8887", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/json" , 
          "Authorization" : "Bearer @consumer_access_token_for_provider_2"
        }
      },
      { "title"    : "(Consumer) Get all User Subscriptions" , 
        "method"   : "GET" , 
        "endpoint" : "http://" + myIP + ":8885/administration/user/subscriptions", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/json" , 
          "Authorization" : "Bearer @consumer_access_token"
        }
      },
      { "title"    : "(Consumer) Get Data from Provider" , 
        "method"   : "GET" , 
        "endpoint" : "http://" + myIP + ":8887", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/json" , 
          "Authorization" : "Bearer @consumer_access_token_for_provider_2"
        }
      },
      { "title"    : "(Consumer) UnSubscribe from Dataset" , 
        "method"   : "POST" , 
        "endpoint" : "http://" + myIP + ":8885/administration/user/unsubscribe/dataset_id/@dataset_id_provider_2", 
        "headers"  : {
          "accept" : "application/json" ,
          "Content-Type" : "application/json" , 
          "Authorization" : "Bearer @consumer_access_token"
        }
      }
    ];

    amTool.log( bcolors.FAIL + "-------------------------------------------------------------------" + bcolors.ENDC );
    amTool.log( bcolors.FAIL + "---------------                                      --------------" + bcolors.ENDC );
    amTool.log( bcolors.FAIL + "---------------          Starting Requests           --------------" + bcolors.ENDC );
    amTool.log( bcolors.FAIL + "---------------                                      --------------" + bcolors.ENDC );
    amTool.log( bcolors.FAIL + "-------------------------------------------------------------------" + bcolors.ENDC );
    amTool.log( "" );

    for index , req in enumerate( requestList ):
        if (index + 1) > stopIndex :
            break;

        amTool.log( bcolors.HEADER + " --------------------" + bcolors.FAIL + " " + str( index + 1 ) + " / " + str( len( requestList ) ) + " " + bcolors.HEADER + "------------------------------ " + bcolors.ENDC );
        amTool.log( "[ TITLE ] : "     + bcolors.FAIL + "'" + str( req[ "title" ] ) + "'" + bcolors.ENDC );
        amTool.log( "[ ENDPOINT ] : '" + str( req[ "endpoint" ] ) + "'" );
        amTool.log( "[ METHOD ] : '"   + str( req[ "method" ] )   + "'" );
        
        replaceValuesRecursive( req , myLocalStorage );
        myDataFixed = "";

        myUrlParameters = [];
        if "dataType" in req and req[ "dataType" ] == "url" : 
            amTool.log( "[ DATATYPE ] : Request has '" + str( req[ "dataType" ] ) + "' parameters" );

            for formInput in req[ "myData" ]:
                myUrlParameters.append( str( formInput ) + "=" + str( req[ "myData" ][ formInput ] ) );

            myTempData = "&".join( myUrlParameters );
            myDataFixed = "?" + myTempData;
            req[ "endpoint" ] = req[ "endpoint" ] + myDataFixed;

            amTool.log( "[ URL DATA ] : '" + str( myDataFixed ) + "'" );

        elif "dataType" in req and req[ "dataType" ] == "json" : 
            amTool.log( "[ DATATYPE ] : Request has '" + str( req[ "dataType" ] ) + "' parameters" );
            myDataFixed = req[ "myData" ];
            amTool.log( "[ JSON ] : " + str( JSON.dumps( myDataFixed ) ) );

        amTool.log( "[ HEADERS ] : " + JSON.dumps( req[ "headers" ] ) );

        try:
            myResponse = await amTool.request({
                  "endPoint" : req[ "endpoint" ],
                  "method"   : req[ "method" ],
                  "headers"  : req[ "headers" ],
                  "myData"   : myDataFixed
            });

            if myResponse is not None and "error" not in myResponse: 
                if "detail" in myResponse : 
                    amTool.log( "[ " + bcolors.FAIL + "ERROR" + bcolors.ENDC + " ] : " + JSON.dumps( myResponse ) );
                    break;
                else:
                    amTool.log( "[ RESPONSE ] : " + JSON.dumps( myResponse ) );
            else:
                if "TimeoutError" in JSON.dumps( myResponse ):
                    amTool.log( "[ " + bcolors.FAIL + "ERROR" + bcolors.ENDC + " ] : " + JSON.dumps( myResponse ) );
                    amTool.log( "[ " + bcolors.FAIL + "ERROR INFO" + bcolors.ENDC + " ] : Check that you have the correct IPs and that the services are running." );
                    break;
                    return;
                elif "duplicate" in JSON.dumps( myResponse ):
                    amTool.log( "[ " + bcolors.WARNING + "WARNING" + bcolors.ENDC + " ] : " + JSON.dumps( myResponse ) );
                    continue;
                else:
                    amTool.log( "[ " + bcolors.FAIL + "ERROR" + bcolors.ENDC + " ] : " + JSON.dumps( myResponse ) );
                    break;
                    return;
        except Exception as error :
            if error is not None or error != "" :
                amTool.log( "[ " + bcolors.FAIL + "ERROR" + bcolors.ENDC + " ] : " + str( error ) );
                break;
            else:
                amTool.log( "[ " + bcolors.FAIL + "ERROR" + bcolors.ENDC + " ] : Failed to Receive response from server. Make sure Services are running and the IPs are correct." );
                break;
            return;

        if "responseType" in req and req[ "responseType" ] == "array" : 
            if req[ "responseToKeep" ][ "propertySource" ] in myResponse[ 0 ]:
                myLocalStorage[ req[ "responseToKeep" ][ "propertyTarget" ] ] = myResponse[ 0 ][ req[ "responseToKeep" ][ "propertySource" ] ];
                amTool.log( 
                  "[ GLOBALS ] : Saving to Globals : " + 
                    str( req[ "responseToKeep" ][ "propertyTarget" ] ) + 
                    " | " + 
                    str( myResponse[ 0 ][ req[ "responseToKeep" ][ "propertySource" ] ] ) 
                );
        elif "responseType" in req and req[ "responseType" ] == "object" : 
            if req[ "responseToKeep" ][ "propertySource" ] in myResponse:
                myLocalStorage[ req[ "responseToKeep" ][ "propertyTarget" ] ] = myResponse[ req[ "responseToKeep" ][ "propertySource" ] ];
                amTool.log( 
                  "[ GLOBALS ] : Saving '" + 
                  str( req[ "responseToKeep" ][ "propertyTarget" ] ) + 
                  "' to Globals : " + 
                  str( myResponse[ req[ "responseToKeep" ][ "propertySource" ] ] ) 
                );

        amTool.log( "" );

    print( "Complete" );

if __name__ ==  '__main__':
    loop = asyncio.new_event_loop();
    asyncio.set_event_loop( loop );
    loop.run_until_complete( main() );
