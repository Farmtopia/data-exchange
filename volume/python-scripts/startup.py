"""
Go to the Database of your Docker and execute the following in order to truncate all tables and start from scratch
TRUNCATE TABLE farmtopia_directory.dataset RESTART IDENTITY CASCADE;
TRUNCATE TABLE farmtopia_directory.map_user_to_dataset RESTART IDENTITY CASCADE;
TRUNCATE TABLE farmtopia_directory.user_access RESTART IDENTITY CASCADE;
TRUNCATE TABLE farmtopia_provider.apikey RESTART IDENTITY CASCADE;
TRUNCATE TABLE farmtopia_provider.map_user_to_dataset RESTART IDENTITY CASCADE;
TRUNCATE TABLE farmtopia_provider.user_access RESTART IDENTITY CASCADE;

TRUNCATE TABLE farmtopia_provider_2.apikey RESTART IDENTITY CASCADE;
TRUNCATE TABLE farmtopia_provider_2.map_user_to_dataset RESTART IDENTITY CASCADE;
TRUNCATE TABLE farmtopia_provider_2.user_access RESTART IDENTITY CASCADE;
"""

from   enum     import Enum;
import time;
import datetime;
from   datetime import timedelta;
import os;
import os.path
from   os import path
import aiohttp;
import aiofiles;
import asyncio;
import json as JSON;
import sys;
import random;
import hashlib;
import psycopg2;
import re;
from   classes.amTools         import amTool;
# from   classes.amTools         import bcolors;

# os.system('color');
amTool          = amTool();
# bcolors         = bcolors();
cfg             = amTool.load( "./cfg/" , "config" );
myLocalStorage  = globals();

serverSecretKey = "DqBflEflm8JFGMaGF4iL4uU7zYcCPw7bCKRXXCqymvnsSXhXF35LduRuXdDfi6hAn7Ki";

myIP = os.getenv( 'localip' );

async def main():
    myHeaders = {
        'accept' : 'application/json' 
    }

    amTool.log( "Starting Script Execution" );

    myRequests = [
      {
        "endpoint" : "http://" + myIP + ":8885/administration/create_db_tables?token=" + serverSecretKey, 
        "method"   : "GET" , 
        "headers"  : myHeaders , 
        "dataType" : "url" , 
        "data"     : {}
      },
      {
        "endpoint" : "http://" + myIP + ":8885/administration/create_input_models?token=" + serverSecretKey, 
        "method"   : "GET" , 
        "headers"  : myHeaders , 
        "dataType" : "url" , 
        "data"     : {}
      },
      {
        "endpoint" : "http://" + myIP + ":8886/administration/create_db_tables?token=" + serverSecretKey, 
        "method"   : "GET" , 
        "headers"  : myHeaders , 
        "dataType" : "url" , 
        "data"     : {}
      },
      {
        "endpoint" : "http://" + myIP + ":8886/administration/create_input_models?token=" + serverSecretKey, 
        "method"   : "GET" , 
        "headers"  : myHeaders , 
        "dataType" : "url" , 
        "data"     : {}
      },
      {
        "endpoint" : "http://" + myIP + ":8887/administration/create_db_tables?token=" + serverSecretKey, 
        "method"   : "GET" , 
        "headers"  : myHeaders , 
        "dataType" : "url" , 
        "data"     : {}
      },
      {
        "endpoint" : "http://" + myIP + ":8887/administration/create_input_models?token=" + serverSecretKey, 
        "method"   : "GET" , 
        "headers"  : myHeaders , 
        "dataType" : "url" , 
        "data"     : {}
      }
    ];

    for req in myRequests : 
        try:
            myResponse = await amTool.request({
                  "endPoint" : req[ "endpoint" ],
                  "method"   : req[ "method" ],
                  "headers"  : req[ "headers" ],
                  "myData"   : req[ "data" ]
            });
            amTool.log( "Request '" + str( req[ "endpoint" ] ) + "' completed." );
            time.sleep( 2 );
        except Exception as error :
            print( error );

    print( "Complete" );

if __name__ ==  '__main__':
    loop = asyncio.new_event_loop();
    asyncio.set_event_loop( loop );
    loop.run_until_complete( main() );
