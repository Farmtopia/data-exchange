<?php 

echo "<HR>";
echo "<b>SERVER PROPERTIES</b><br><br>";
foreach( $_SERVER as $item => $value ){
   echo "<b>" . $item . "</b> : " . json_encode( $value ) . "<BR>";
}
echo "<HR>";

class Config{

  public function __construct(){
      $this->DB_TYPE     = 'pgsql';
      $this->DB_NAME     = 'postgres';
      $this->DB_SCHEMA   = 'public';
      $this->DB_HOST     = getenv( "localip" );   // This is the Container Name of Postgresql , you can also use the IP
      $this->DB_PORT     = 5555;           // This is the port on Postgresql. Not the one exposed but the internal one.
      $this->DB_USERNAME = 'postgres';
      $this->DB_PASSWORD = 'mysecretpassword';
  }

}

$Settings = new Config();

if( $Settings->DB_TYPE == "mysql" ){
 $dsn = $Settings->DB_TYPE.":host={$Settings->DB_HOST};dbname={$Settings->DB_SCHEMA};charset=utf8";
 $options = [
  PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
  PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
  PDO::ATTR_EMULATE_PREPARES   => false,
 ];
 try {
  $pdo = new PDO($dsn, $Settings->DB_USERNAME, $Settings->DB_PASSWORD, $options);
  echo "Succesfull connection to {$Settings->DB_TYPE} ";
 } catch (\PDOException $e) {
  echo $e->getMessage();
 }

}

if( $Settings->DB_TYPE == "pgsql" ){
 $dsn = $Settings->DB_TYPE.":host={$Settings->DB_HOST};port={$Settings->DB_PORT};dbname={$Settings->DB_NAME};user={$Settings->DB_USERNAME};password={$Settings->DB_PASSWORD};";
 $options = [
  PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
  PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
  PDO::ATTR_EMULATE_PREPARES   => false,
 ];
 try {
  $pdo = new PDO($dsn, $Settings->DB_USERNAME, $Settings->DB_PASSWORD, $options);
  echo "Succesfull connection to {$Settings->DB_TYPE} ";
 } catch (\PDOException $e) {
   echo $e->getMessage();
 }

}

?>