class amWidget{
 constructor(){
  console.log("Widget loaded");
  
  this.series;
  this.text;
  this.title;
  this.style = "";

  return this;
 }
 
 setType( type ){
  this.type = type;
  return this;
 }
 
 setContainer( container ){
  this.container = document.getElementById( container );
 }
 
 getWrapperHTML( ){
  // let title = ( this.title ) ? '<h5 class="card-title">'+this.title+'</h5>' : '';
  let text  = ( this.text )  ? '<p class="card-text">'+this.text+'</p>'     : '';
  return '<div class="card ctx-container" style="'+this.style+'">' + 
    '<canvas class="amwidget" id="widget-'+this.widgetID+'" ></canvas>' + 
    '<div class="card-body">' + 
      // title +
      text + 
    '</div>' + 
  '</div>';
  // return '<div class="ctx-container"> <canvas class="amwidget" id="widget-'+this.widgetID+'" ></canvas> </div>';
 }

 setWrapperToDOM(){
  this.container.insertAdjacentHTML( "afterBegin" , this.getWrapperHTML() );
 }

 setText( text ){
  this.text = text;
  return this;
 }

 setTitle( title ){
  this.title = title;
  return this;
 }

 setLabels( labels ){
  this.labels = labels;
  return this;
 }

 setData( data ){
  this.data = data;
  return this;
 }

 setStyle( styleSheet ){
  let myStyleArray = new Array();
  Object.keys( styleSheet ).forEach( ( type ) => {
   myStyleArray.push( type + ":" + styleSheet[type] );
  });
  this.style = myStyleArray.join(";");
  return this;
 }

 createSeries(){
  this.series = {
    labels: this.labels,
    datasets: [{
     label: this.title,
     data: this.data,
     backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
     hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"],
     borderWidth: 2
    }]
  }

  return this;
 }

 _getSeries(){
  if( this.type == 'bar'){
   return {
     labels: ["Red", "Blue"],
     datasets: [{
      label: 'Dataset1',
      data: [12, 19],
      backgroundColor: ['rgba(255, 99, 132, 0.2)','rgba(54, 162, 235, 0.2)'],
      borderColor: ['rgba(255,99,132,1)','rgba(54, 162, 235, 1)'],
      borderWidth: 1
     }]
   }
  }
  else if( this.type == 'line'){
   return {
     labels: ["January", "February", "March", "April", "May", "June", "July"],
     datasets: [{
      label: 'Dataset2',
      data: [65, 59, 80, 81, 56, 55, 40],
      backgroundColor: ['rgba(255, 99, 132, 0.2)'],
      borderColor: ['rgba(200, 99, 132, .7)'],
      borderWidth: 2
     }]
   }
  }
  else if( this.type == 'horizontalBar'){
   return {
     labels: ["Red", "Orange", "Yellow", "Green", "Blue", "Purple", "Grey"],
     datasets: [{
      label: 'Dataset3',
      data: [22, 33, 55, 12, 86, 23, 14],
      backgroundColor: [
       "rgba(255, 99, 132, 0.2)", 
       "rgba(255, 159, 64, 0.2)",
       "rgba(255, 205, 86, 0.2)", 
       "rgba(75, 192, 192, 0.2)", 
       "rgba(54, 162, 235, 0.2)",
       "rgba(153, 102, 255, 0.2)", 
       "rgba(201, 203, 207, 0.2)"
      ],
      borderColor: ["rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)","rgb(75, 192, 192)", "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)"],
      borderWidth: 2
     }]
   }
  }
  else if( this.type == 'doughnut'){
   return {
     labels: ["Red", "Green", "Yellow", "Grey", "Dark Grey"],
     datasets: [{
      label: 'Dataset2',
      data:  [300, 50, 100, 40, 120],
      backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
      hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
     }]
   }
  }
  else if( this.type == 'pie'){
   return {
     labels: ["Red", "Green", "Yellow", "Grey", "Dark Grey"],
     datasets: [{
      label: 'Dataset2',
      data:  [300, 50, 100, 40, 120],
      backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
      hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
     }]
   }
  }
  else if( this.type == 'polarArea'){
   return {
     labels: ["Red", "Green", "Yellow", "Grey", "Dark Grey"],
     datasets: [{
      label: 'Dataset2',
      data:  [300, 50, 100, 40, 120],
      backgroundColor: ["rgba(219, 0, 0, 0.1)", "rgba(0, 165, 2, 0.1)", "rgba(255, 195, 15, 0.2)","rgba(55, 59, 66, 0.1)", "rgba(0, 0, 0, 0.3)"],
      hoverBackgroundColor: ["rgba(219, 0, 0, 0.2)", "rgba(0, 165, 2, 0.2)","rgba(255, 195, 15, 0.3)", "rgba(55, 59, 66, 0.1)", "rgba(0, 0, 0, 0.4)"]
     }]
   }
  }
 }

 render( container , options ){
  this.widgetID = options.widgetID;
  this.setContainer( container );
  this.setWrapperToDOM();
  this.createSeries();
  this.myGraph = document.getElementById( 'widget-'+this.widgetID );
  var myChart = new Chart( this.myGraph.getContext('2d'), {
   type: this.type,
   data: this.series , 
   options: {
    maintainAspectRatio: false,
    responsive: true,
    scales: {
     yAxes: [{
      ticks: {
       beginAtZero: true
      }
     }]
    }
   }
  });  
 }
}


