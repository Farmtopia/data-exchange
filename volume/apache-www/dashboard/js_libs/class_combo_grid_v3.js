class comboGrid{

 constructor(options){
  console.log( " [ Class comboGrid : options ] " , options );
  this.options               = options;
  this.modalID               = options.myModalID;
  this.box                   = document.getElementById( options.boxid );
  this.mainid                = options.mainid;
  this.server                = options.server;
  this.selectedItems         = new Array();
  this.removedItems          = new Array();
  this.parentid              = options.parentid;
  this.parententity          = options.parententity;
  this.uniqueID              = options.options.hiddenProperty;
  this.activateActionButtons = options.activateActionButtons === false ? false : true;
  
  this.getGrid();
  this.resetDisplayBox();
  this.populateDisplay();
 }

 getIcon(type){
  var myIcon = "";
  switch(type){
   case "calendar" : 
    myIcon = '<svg class="bi bi-calendar" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
              '<path fill-rule="evenodd" d="M14 0H2a2 2 0 00-2 2v12a2 2 0 002 2h12a2 2 0 002-2V2a2 2 0 00-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z" clip-rule="evenodd"/>' + 
              '<path fill-rule="evenodd" d="M6.5 7a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd"/>' + 
             '</svg>';
   break;
   case "text" : 
    myIcon = '<svg class="bi bi-card-text" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
              '<path fill-rule="evenodd" d="M14.5 3h-13a.5.5 0 00-.5.5v9a.5.5 0 00.5.5h13a.5.5 0 00.5-.5v-9a.5.5 0 00-.5-.5zm-13-1A1.5 1.5 0 000 3.5v9A1.5 1.5 0 001.5 14h13a1.5 1.5 0 001.5-1.5v-9A1.5 1.5 0 0014.5 2h-13z" clip-rule="evenodd"/>' + 
              '<path fill-rule="evenodd" d="M3 5.5a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9a.5.5 0 01-.5-.5zM3 8a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9A.5.5 0 013 8zm0 2.5a.5.5 0 01.5-.5h6a.5.5 0 010 1h-6a.5.5 0 01-.5-.5z" clip-rule="evenodd"/>' + 
             '</svg>';
   break;
   case "email" : 
    myIcon = '<svg class="bi bi-at" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
              '<path fill-rule="evenodd" d="M13.106 7.222c0-2.967-2.249-5.032-5.482-5.032-3.35 0-5.646 2.318-5.646 5.702 0 3.493 2.235 5.708 5.762 5.708.862 0 1.689-.123 2.304-.335v-.862c-.43.199-1.354.328-2.29.328-2.926 0-4.813-1.88-4.813-4.798 0-2.844 1.921-4.881 4.594-4.881 2.735 0 4.608 1.688 4.608 4.156 0 1.682-.554 2.769-1.416 2.769-.492 0-.772-.28-.772-.76V5.206H8.923v.834h-.11c-.266-.595-.881-.964-1.6-.964-1.4 0-2.378 1.162-2.378 2.823 0 1.737.957 2.906 2.379 2.906.8 0 1.415-.39 1.709-1.087h.11c.081.67.703 1.148 1.503 1.148 1.572 0 2.57-1.415 2.57-3.643zm-7.177.704c0-1.197.54-1.907 1.456-1.907.93 0 1.524.738 1.524 1.907S8.308 9.84 7.371 9.84c-.895 0-1.442-.725-1.442-1.914z" clip-rule="evenodd"/>' + 
             '</svg>';
   break;
   case "password" : 
    myIcon = '<svg class="bi bi-eye-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
               '<path d="M10.5 8a2.5 2.5 0 11-5 0 2.5 2.5 0 015 0z"/>' + 
               '<path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 100-7 3.5 3.5 0 000 7z" clip-rule="evenodd"/>' + 
             '</svg>';
   break;
   case "geo" : 
    myIcon = '<svg class="bi bi-geo-alt" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
               '<path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 002 6c0 4.314 6 10 6 10zm0-7a3 3 0 100-6 3 3 0 000 6z" clip-rule="evenodd"/>' + 
             '</svg>';
   break;
   case "contact" : 
    myIcon = '<svg class="bi bi-people-circle" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
               '<path d="M13.468 12.37C12.758 11.226 11.195 10 8 10s-4.757 1.225-5.468 2.37A6.987 6.987 0 008 15a6.987 6.987 0 005.468-2.63z"/>' + 
               '<path fill-rule="evenodd" d="M8 9a3 3 0 100-6 3 3 0 000 6z" clip-rule="evenodd"/>' + 
               '<path fill-rule="evenodd" d="M8 1a7 7 0 100 14A7 7 0 008 1zM0 8a8 8 0 1116 0A8 8 0 010 8z" clip-rule="evenodd"/>' + 
             '</svg>';
   break;
   case "search" : 
    myIcon = '<svg class="bi bi-search" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' +
               '<path fill-rule="evenodd" d="M10.442 10.442a1 1 0 011.415 0l3.85 3.85a1 1 0 01-1.414 1.415l-3.85-3.85a1 1 0 010-1.415z" clip-rule="evenodd"/>' +
               '<path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 100-11 5.5 5.5 0 000 11zM13 6.5a6.5 6.5 0 11-13 0 6.5 6.5 0 0113 0z" clip-rule="evenodd"/>' +
             '</svg>';
   break;
   case "number" : 
    myIcon = '<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-calculator-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
                '<path fill-rule="evenodd" d="M12 1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM4 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H4z"/>' + 
                '<path d="M4 2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-2zm0 4a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm3-6a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm3-6a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-4z"/>' + 
               '</svg>';
   break;
   case "add" : 
    myIcon = '<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
               '<path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>' + 
               '<path fill-rule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>' + 
             '</svg>';
   break;
   case "edit" : 
    myIcon = '<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
               '<path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>' + 
               '<path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>' + 
             '</svg>';
   break;
   case "save" : 
    myIcon = '<i class="far fa-save"></i>';
   break;
   case "cancel" : 
    myIcon = '<i class="fas fa-ban"></i>';
   break;
   default : 
    myIcon = '<svg class="bi bi-card-text" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
              '<path fill-rule="evenodd" d="M14.5 3h-13a.5.5 0 00-.5.5v9a.5.5 0 00.5.5h13a.5.5 0 00.5-.5v-9a.5.5 0 00-.5-.5zm-13-1A1.5 1.5 0 000 3.5v9A1.5 1.5 0 001.5 14h13a1.5 1.5 0 001.5-1.5v-9A1.5 1.5 0 0014.5 2h-13z" clip-rule="evenodd"/>' + 
              '<path fill-rule="evenodd" d="M3 5.5a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9a.5.5 0 01-.5-.5zM3 8a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9A.5.5 0 013 8zm0 2.5a.5.5 0 01.5-.5h6a.5.5 0 010 1h-6a.5.5 0 01-.5-.5z" clip-rule="evenodd"/>' + 
             '</svg>';    
   break;
  }

  return myIcon;
 }

 getInputHTML(options){
  var myInputHTML = "";
  var inputRequired = options.required ? "required" : "";
  var myValidation = "";

  if( options.validation ){
   myValidation = options.validation;
  }

  switch(options.type){
   case "text" : 
    myInputHTML +=  '<div class="input-group-prepend">' + 
                     '<span class="input-group-text" id="'+options.name+'-icon">' + this.getIcon(options.icon) + '</span>' + 
                    '</div>' + 
                    '<input type="text" class="form-control" ' + 
                    myValidation + 
                    'name="'+options.name+'"' + 
                    'id="'+options.id+'" ' + 
                    'placeholder="'+options.placeholder+'" ' + 
                    'aria-label="'+options.placeholder+'" ' + 
                    'aria-describedby="'+options.placeholder+'" '+inputRequired+' value="'+options.default+'">';
   break;
   case "autotext" : 
    myInputHTML +=  '<div class="input-group-prepend">' + 
                     '<span class="input-group-text" id="'+options.name+'-icon">' + this.getIcon(options.icon) + '</span>' + 
                    '</div>' + 
                    '<input type="text" class="form-control amAutoComplete" ' + 
                    myValidation + 
                    'name="'+options.name+'"' + 
                    'id="'+options.id+'" ' + 
                    'placeholder="'+options.placeholder+'" ' + 
                    'aria-label="'+options.placeholder+'" ' + 
                    'aria-describedby="'+options.placeholder+'" '+inputRequired+' value="'+options.default+'">';
   break;
   case "date" : 
    myInputHTML +=  '<div class="input-group-prepend">' + 
                      '<span class="input-group-text" id="'+options.name+'-icon">' + this.getIcon(options.icon) + '</span>' + 
                     '</div>' + 
                     '<input type="text" class="form-control datetimepicker-input" ' + 
                     myValidation + 
                     'name="'+options.name+'"' + 
                     'id="'+options.id+'" ' + 
                     'data-toggle="datetimepicker" ' +
                     'data-target="#'+options.id+'" ' +
                     'placeholder="'+options.placeholder+'" ' + 
                     'aria-label="'+options.placeholder+'" ' + 
                     'aria-describedby="'+options.placeholder+'" '+inputRequired+' value="'+options.default+'">';
    break;
   case "datetime" : 
    myInputHTML +=  '<div class="input-group-prepend">' + 
                      '<span class="input-group-text" id="'+options.name+'-icon">' + this.getIcon(options.icon) + '</span>' + 
                     '</div>' + 
                     '<input type="text" class="form-control datetimepicker-input" ' + 
                     myValidation + 
                     'name="'+options.name+'"' + 
                     'id="'+options.id+'" ' + 
                     'data-toggle="datetimepicker" ' +
                     'data-target="#'+options.id+'" ' +
                     'placeholder="'+options.placeholder+'" ' + 
                     'aria-label="'+options.placeholder+'" ' + 
                     'aria-describedby="'+options.placeholder+'" '+inputRequired+' value="'+options.default+'">';
    break;
   case "time" : 
    myInputHTML +=  '<div class="input-group-prepend">' + 
                      '<span class="input-group-text" id="'+options.name+'-icon">' + this.getIcon(options.icon) + '</span>' + 
                     '</div>' + 
                     '<input type="text" class="form-control datetimepicker-input" ' + 
                     myValidation + 
                     'name="'+options.name+'"' + 
                     'id="'+options.id+'" ' + 
                     'data-toggle="datetimepicker" ' +
                     'data-target="#'+options.id+'" ' +
                     'placeholder="'+options.placeholder+'" ' + 
                     'aria-label="'+options.placeholder+'" ' + 
                     'aria-describedby="'+options.placeholder+'" '+inputRequired+' value="'+options.default+'">';
   break;
   case "number" : 
    myInputHTML +=  '<div class="input-group-prepend">' + 
                     '<span class="input-group-text" id="'+options.name+'-icon">' + this.getIcon(options.icon) + '</span>' + 
                    '</div>' + 
                    '<input type="number" class="form-control" ' + 
                    myValidation + 
                    'name="'+options.name+'"' + 
                    'id="'+options.id+'" ' + 
                    'placeholder="'+options.placeholder+'" ' + 
                    'aria-label="'+options.placeholder+'" ' + 
                    'aria-describedby="'+options.placeholder+'" '+inputRequired+' value="'+options.default+'">';
   break;
   case "email" : 
    myInputHTML +=  '<div class="input-group-prepend">' + 
                     '<span class="input-group-text" id="'+options.name+'-icon">' + this.getIcon(options.icon) + '</span>' + 
                    '</div>' + 
                    '<input type="email" class="form-control" ' + 
                    myValidation + 
                    'name="'+options.name+'"' + 
                    'id="'+options.id+'" ' + 
                    'placeholder="'+options.placeholder+'" ' + 
                    'aria-label="'+options.placeholder+'" ' + 
                    'aria-describedby="'+options.placeholder+'" '+inputRequired+' value="'+options.default+'">';
   break;
   case "combo" : 
    myInputHTML +=  '<div class="input-group-prepend">' + 
                     '<span class="input-group-text" id="'+options.name+'-icon">' + this.getIcon(options.icon) + '</span>' + 
                    '</div>' + 
                    '<input type="text" class="form-control" ' + 
                    myValidation + 
                    'name="'+options.name+'"' + 
                    'id="'+options.id+'" ' + 
                    'placeholder="'+options.placeholder+'" ' + 
                    'aria-label="'+options.placeholder+'" ' + 
                    'aria-describedby="'+options.placeholder+'" '+inputRequired+'>';
   break;
   case "combo-multi" : 
    myInputHTML +=  '<button class="btn btn-block btn-primary btn-sm" id="'+options.id+'-frmbtn" '+inputRequired+'><i class="fas fa-plus-circle"> ' + options.label+'</i></button>';
   break;
   case "combo-multi-form" : 
    myInputHTML +=  '<button class="btn btn-block btn-primary btn-sm" id="'+options.id+'-frmbtn" '+inputRequired+'><i class="fas fa-plus-circle"> ' + options.label+'</i></button>';
   break;
   case "textarea" : 
    myInputHTML += '<div class="input-group-prepend">' + 
                     '<span class="input-group-text" id="'+options.name+'-icon">' + this.getIcon(options.icon) + '</span>' + 
                    '</div>' + 
                    '<textarea class="form-control" ' + 
                    myValidation + 
                    'name="'+options.name+'"' + 
                    'id="'+options.id+'" ' + 
                    'aria-label="'+options.placeholder+'" ' + 
                    'aria-describedby="'+options.placeholder+'" rows="3" '+inputRequired+'>'+options.default+'</textarea>';
   break;
   case "select" : 
    myInputHTML += '<div class="input-group-prepend">' + 
                     '<span class="input-group-text" id="'+options.name+'-icon">' + this.getIcon(options.icon) + '</span>' + 
                    '</div>' + 
                    '<select class="form-control" ' + 
                    'name="'+options.name+'"' + 
                    'id="'+options.id+'" ' + 
                    'aria-label="'+options.placeholder+'" ' + 
                    'aria-describedby="'+options.placeholder+'" '+inputRequired+'>';

    myInputHTML += '<option value=""></option>';

    Object.keys(options.options).forEach((item)=>{
     if(item == options.default)
      myInputHTML += '<option value="'+item+'" selected>'+options.options[item]+'</option>';
     else
      myInputHTML += '<option value="'+item+'">'+options.options[item]+'</option>';
    });
    myInputHTML += '</select>';
   break;
   case "checkbox" : 
    var isChecked = options.default == "checked" ? "checked" : "";
    myInputHTML += '<div class="form-check">' + 
                    '<input class="form-check-input" ' + 
                     'type="checkbox" ' + 
                     'name="'+options.name+'"' + 
                     'id="'+options.id+'" '+isChecked+' '+inputRequired+'>' + 
                     '<label class="form-check-label" for="'+options.id+'">' + options.sign + '</label>' + 
                   '</div>';
   break;
   default : 
    myInputHTML +=  '<div class="input-group-prepend">' + 
                     '<span class="input-group-text" id="'+options.name+'-icon">' + this.getIcon(options.icon) + '</span>' + 
                    '</div>' + 
                    '<input type="text" class="form-control" ' + 
                    myValidation + 
                    'name="'+options.name+'"' + 
                    'id="'+options.id+'" ' + 
                    'placeholder="'+options.placeholder+'" ' + 
                    'aria-label="'+options.placeholder+'" ' + 
                    'aria-describedby="'+options.placeholder+'" '+inputRequired+' value="'+options.default+'">';
   break;
  }

  return myInputHTML;

 }

 resetDisplayBox(){
  this.box.innerHTML = '';
 }

 getServerDisplayItems(){
  return new Promise( (resolutionFunc,rejectionFunc) => {
    // AJAX(this.server.data.dispatcherPath , { ...this.server.data.request },
    AJAX(this.server.data.dispatcherPath , this.server.data.request ,
     ( response )=>{
      if( response.error ){
       rejectionFunc( response.error );
      }
      else{
       resolutionFunc( response );
      }
     }
    );
  });  
 }

 populateDisplay(){
  this.getServerDisplayItems()
  .then( ( response ) => {
   if( response.res ){
    response.res.forEach( ( item ) => {
     this.addItem( item );
    });
   }
  });
 }

 _getDestinationPopupContent(){
   let items = [
    { "label" : "From" , "type" : "date" , "icon"  : "calendar", "id"    : "from" },
    { "label" : "To",    "type" : "date" , "icon"  : "calendar", "id"    : "to" }
   ];

   let popupTitle = 'Select Destination';
   let popupBody  = '<div class="form-row" style="display: block;">';

   items.forEach( ( item ) => {
    popupBody += '<div class="col">' + 
                  '<label for="check_in">'+item.label+'</label>' + 
                  '<div class="form-group">' + 
                   '<div class="input-group mb-3">' + 
                    '<div class="input-group-prepend">' + 
                     '<span class="input-group-text">' + this.getIcon( item.icon ) + '</span>' + 
                    '</div>';
         if ( item.type == "date" || item.type == "datetime" || item.type == "time" ){
          popupBody += '<input type="text" class="form-control datetimepicker-input" ';
         }
         else{
          popupBody += '<input type="text" class="form-control" ';
         }
         popupBody += 'name="'+item.id+'" ' + 
                      'id="'+item.id+'" ' + 
                      'placeholder="'+item.label+'" ';
         if ( item.type == "calendar" ){
          popupBody += 'data-toggle="datetimepicker" data-target="#'+item.id+'" ';
         }
          popupBody += 'aria-label="'+item.label+'" ' + 
                      'aria-describedby="'+item.label+'" value="">' + 
                   '</div>' + 
                  '</div>' + 
                 '</div>';
   });

   popupBody += "</div>";
   
   popupBody += '<script>';
   items.forEach( ( item ) => {

    if ( item.type == "date" ){
     console.log( "is date" );
     popupBody += 'jQuery("#'+item.id+'").datetimepicker({ "type":"L" , "format": "YYYY-MM-DD HH:mm" , "sideBySide" : false , "showToday" : true });';
    }

    if ( item.type == "datetime" ){
     console.log( "is datetime" );
     popupBody += 'jQuery("#'+item.id+'").datetimepicker({ "format": "YYYY-MM-DD HH:mm" , "sideBySide" : false , "showToday" : true });';
    }

    if ( item.type == "time" ){
     console.log( "is time" );
       popupBody += 'jQuery("#'+item.id+'").datetimepicker({ "type":"LT" , "format": "YYYY-MM-DD HH:mm" , "sideBySide" : false , "showToday" : true });';
    }

   });
   popupBody += '</script>';

   return {
    "title" : popupTitle,
    "body"  : popupBody
   }
 }

 _getTransportationPopupContent(){
   let items = [
    { "label" : "From Date" , "icon"  : "calendar", "id"    : "fromDate" },
    { "label" : "To Date",    "icon"  : "calendar", "id"    : "toDate" },
    { "label" : "From Destination",    "icon"  : "geo", "id"    : "fromDestination" },
    { "label" : "To Destination",    "icon"  : "geo", "id"    : "toDestination" },
    { "label" : "Price",    "icon"  : "number", "id"    : "price" },
    { "label" : "Duration",    "icon"  : "number", "id"    : "duration" },
    { "label" : "Type",    "icon"  : "text", "id"    : "type" },
    { "label" : "Route",    "icon"  : "geo", "id"    : "route" }
   ];

   let popupTitle = 'Select Transportation Details';
   let popupBody  = '<div class="form-row" style="display: block;">';

   items.forEach( ( item ) => {
    popupBody += '<div class="col">' + 
                  '<label for="check_in">'+item.label+'</label>' + 
                  '<div class="form-group">' + 
                   '<div class="input-group mb-3">' + 
                    '<div class="input-group-prepend">' + 
                     '<span class="input-group-text">' + this.getIcon( item.icon ) + '</span>' + 
                    '</div>' + 
                    '<input type="text" class="form-control datetimepicker-input" ' + 
                      'name="'+item.id+'" ' + 
                      'id="'+item.id+'" ' + 
                      'placeholder="'+item.label+'" ';
                       if ( item.icon == "calendar" ){
                        popupBody += 'data-toggle="datetimepicker" data-target="#'+item.id+'" ';
                       }
          popupBody += 'aria-label="'+item.label+'" ' + 
                      'aria-describedby="'+item.label+'" value="">' + 
                   '</div>' + 
                  '</div>' + 
                 '</div>';
   });

   popupBody += "</div>";
   
   popupBody += '<script>';
   items.forEach( ( item ) => {
    if ( item.icon == "calendar" ){
       popupBody += 'jQuery("#'+item.id+'").datetimepicker({ "format": "YYYY-MM-DD HH:mm" , "sideBySide" : false , "showToday" : true });';
    }
   });
   popupBody += '</script>';

   return {
    "title" : popupTitle,
    "body"  : popupBody
   }
 }

 getTravelerPopupContent(){
   let popupTitle   = 'Select Traveler';
   let popupBody    = '<div class="col">' + 
                       '<div class="form-group">' + 
                        '<div class="input-group mb-3">' + 
                         '<div class="input-group-prepend">' + 
                          '<span class="input-group-text">' + this.getIcon("contact") + '</span>' + 
                         '</div>' + 
                         '<input type="text" class="form-control" ' + 
                           'name="traveler_first_name" ' + 
                           'id="traveler_first_name" ' + 
                           'placeholder="traveler_first_name" ' + 
                           'aria-label="traveler_first_name" ' + 
                           'aria-describedby="traveler_first_name" value="">' + 
                        '</div>' + 
                       '</div>' + 
                      '</div>' + 
                      '<div class="col">' + 
                       '<div class="form-group">' + 
                        '<div class="input-group mb-3">' + 
                         '<div class="input-group-prepend">' + 
                          '<span class="input-group-text">' + this.getIcon("contact") + '</span>' + 
                         '</div>' + 
                         '<input type="text" class="form-control" ' + 
                           'name="traveler_last_name" ' + 
                           'id="traveler_last_name" ' + 
                           'placeholder="traveler_last_name" ' + 
                           'aria-label="traveler_last_name" ' + 
                           'aria-describedby="traveler_last_name" value="">' + 
                        '</div>' +                          
                       '</div>' + 
                      '</div>' + 
                      '<div class="col">' + 
                       '<div class="form-group">' + 
                        '<div class="input-group mb-3">' + 
                         '<div class="input-group-prepend">' + 
                          '<span class="input-group-text">' + this.getIcon("email") + '</span>' + 
                         '</div>' + 
                         '<input type="email" class="form-control" ' + 
                           'name="traveler_email" ' + 
                           'id="traveler_email" ' + 
                           'placeholder="traveler_email" ' + 
                           'aria-label="traveler_email" ' + 
                           'aria-describedby="traveler_email" value="">' + 
                        '</div>' +                          
                       '</div>' + 
                      '</div>' +  
                      '<div class="col">' + 
                       '<div class="form-group">' + 
                        '<div class="input-group mb-3">' + 
                         '<div class="input-group-prepend">' + 
                          '<span class="input-group-text">' + this.getIcon("number") + '</span>' + 
                         '</div>' + 
                         '<input type="number" class="form-control" ' + 
                           'name="traveler_age" ' + 
                           'id="traveler_age" ' + 
                           'placeholder="traveler_age" ' + 
                           'aria-label="traveler_age" ' + 
                           'aria-describedby="traveler_age" value="">' + 
                        '</div>' +                          
                       '</div>' + 
                      '</div>' +  
                      '<div class="col">' + 
                       '<div class="form-group">' + 
                        '<div class="input-group mb-3">' + 
                         '<div class="input-group-prepend">' + 
                          '<span class="input-group-text">' + this.getIcon("phone") + '</span>' + 
                         '</div>' + 
                         '<input type="number" class="form-control" ' + 
                           'name="traveler_phone" ' + 
                           'id="traveler_phone" ' + 
                           'placeholder="traveler_phone" ' + 
                           'aria-label="traveler_phone" ' + 
                           'aria-describedby="traveler_phone" value="">' + 
                        '</div>' +                          
                       '</div>' + 
                      '</div>';
   return {
    "title" : popupTitle,
    "body"  : popupBody
   }
 }

 getEntityPopupContent( items , title ){
   let popupBody  = '<div class="form-row" style="display: block;">';

   items.forEach( ( item ) => {
    popupBody += '<div class="col">' + 
                  item.label + 
                  '<div class="form-group">' + 
                   '<div class="input-group mb-3">' + 
                    this.getInputHTML( item ) + 
                   '</div>' + 
                   item.help + 
                  '</div>' + 
                 '</div>';
   });

   popupBody += "</div>";

   popupBody += '<script>';
   items.forEach( ( item ) => {
    if ( item.type == "date" )
     popupBody += 'jQuery("#'+item.id+'").datetimepicker({ "format":"YYYY-MM-DD" , "sideBySide" : false , "showToday" : true });';

    if ( item.type == "datetime" )
     popupBody += 'jQuery("#'+item.id+'").datetimepicker({ "format": "YYYY-MM-DD HH:mm" , "sideBySide" : false , "showToday" : true });';

    if ( item.type == "time" )
     popupBody += 'jQuery("#'+item.id+'").datetimepicker({ "format":"HH:mm" , "sideBySide" : false , "showToday" : true });';

   });
   popupBody += '</script>';

   return {
    "title" : title,
    "body"  : popupBody
   }
 }

 rowEvent( event ){
  let myRowData = event.data;
  
  console.log( this.activateActionButtons );
  
  if( !this.activateActionButtons ) {
   jQuery.prompt("Action available only on EDIT mode");
   return;
  }

  if( !this.ifExists( 'card-'+event.data[this.uniqueID] ) ){
   
   if( this.options.options.displayType == "hotel" ){
    let items = [
     { "label" : "Check In" , "type"  : "date",  "icon" : "calendar" , "id" : "check_in"  , "default" : "" , "name" : "check_in"  , "help" : "" , "placeholder" : "Check In" },
     { "label" : "Check Out", "type"  : "date",  "icon" : "calendar" , "id" : "check_out" , "default" : "" , "name" : "check_out" , "help" : "" , "placeholder" : "Check Out" },
     { "label" : "Food",      "type"  : "text",  "icon" : "text" ,     "id" : "food"      , "default" : "" , "name" : "food"      , "help" : "" , "placeholder" : "Food" },
     { "label" : "Type",      "type"  : "text",  "icon" : "text" ,     "id" : "type"      , "default" : "" , "name" : "type"      , "help" : "" , "placeholder" : "Type" },
     { "label" : "Amenities", "type"  : "text",  "icon" : "text" ,     "id" : "amenities" , "default" : "" , "name" : "amenities" , "help" : "" , "placeholder" : "Amenities" }
    ];

    let popupTitle = 'Additional Info';
    let myPopupDetails = this.getEntityPopupContent( items , popupTitle );

    this.getPopup( myPopupDetails.title , myPopupDetails.body )
    .then( response => {
     // this.addItem( { ...event.data , ...response } );
     this.addItem( Object.assign( event.data , response ) );
    });
   }
   else if( this.options.options.displayType == "destination" ){
    let items = [
     { "label" : "From Date" ,  "type"  : "date",     "icon" : "calendar" , "id" : "from"  ,  "default" : "" , "name" : "from"  ,  "help" : "" , "placeholder" : "From Date" },
     { "label" : "To Date",     "type"  : "date",     "icon" : "calendar" , "id" : "to" ,     "default" : "" , "name" : "to" ,     "help" : "" , "placeholder" : "To Date" }
    ];

    let popupTitle = 'Additional Info';
    let myPopupDetails = this.getEntityPopupContent( items , popupTitle );

    this.getPopup( myPopupDetails.title , myPopupDetails.body )
    .then( response => {
     // this.addItem( { ...event.data , ...response } );
     this.addItem( Object.assign( event.data , response ) );
    });
   }
   else if( this.options.options.displayType == "activity" ){
    let items = [
     { "label" : "From Date" , "type"  : "date",    "icon" : "calendar" , "id" : "from"     , "default" : "" , "name" : "from"     , "help" : "" , "placeholder" : "From Date" },
     { "label" : "To Date",    "type"  : "date",    "icon" : "calendar" , "id" : "to"       , "default" : "" , "name" : "to"       , "help" : "" , "placeholder" : "To Date" },
     { "label" : "Cost",       "type"  : "number",  "icon" : "number" ,   "id" : "cost"     , "default" : "" , "name" : "cost"     , "help" : "" , "placeholder" : "Cost" },
     { "label" : "Duration",   "type"  : "number",  "icon" : "number" ,   "id" : "duration" , "default" : "" , "name" : "duration" , "help" : "" , "placeholder" : "Duration" }
    ];

    let myPopupDetails = this.getEntityPopupContent( items , 'Additional Info' );

    this.getPopup( myPopupDetails.title , myPopupDetails.body )
    .then( response => {
     // this.addItem( { ...event.data , ...response } );
     this.addItem( Object.assign( event.data , response ) );
    });    
   }
   else if( this.options.options.displayType == "transportation" ){
    // let myPopupDetails = this.getTransportationPopupContent();
    let items = [
     { "label" : "From Date" ,       "type"  : "date",     "icon" : "calendar" , "id" : "fromDate"  ,       "default" : "" , "name" : "fromDate"  ,       "help" : "" , "placeholder" : "From Date" },
     { "label" : "To Date",          "type"  : "date",     "icon" : "calendar" , "id" : "toDate" ,          "default" : "" , "name" : "toDate" ,          "help" : "" , "placeholder" : "To Date" },
     { "label" : "From Destination", "type"  : "autotext", "icon" : "text" ,     "id" : "fromDestination" , "default" : "" , "name" : "fromDestination" , "help" : "" , "placeholder" : "From Destination" },
     { "label" : "To Destination",   "type"  : "autotext", "icon" : "text" ,     "id" : "toDestination" ,   "default" : "" , "name" : "toDestination" ,   "help" : "" , "placeholder" : "To Destination" },
     { "label" : "Price",            "type"  : "number",   "icon" : "number" ,   "id" : "price" ,           "default" : "" , "name" : "price" ,           "help" : "" , "placeholder" : "Price" },
     { "label" : "Type",             "type"  : "select",   "icon" : "search" ,   "id" : "type" ,            "default" : "" , "name" : "type" ,            "help" : "" , "placeholder" : "Type" },
     { "label" : "Route",            "type"  : "text",     "icon" : "geo" ,      "id" : "route" ,           "default" : "" , "name" : "route" ,           "help" : "" , "placeholder" : "Route" }
    ];

    let popupTitle = 'Additional Info';
    let myPopupDetails = this.getEntityPopupContent( items , popupTitle );

    this.getPopup( myPopupDetails.title , myPopupDetails.body )
    .then( response => {
     // this.addItem( { ...event.data , ...response } );
     this.addItem( Object.assign( event.data , response ) );
    });    
   }
   else if( this.options.options.displayType == "traveler" ){
    this.addItem( event.data );
   }

  }

 }

 ifExists( itemID ){
  let exists = false;
  
  this.selectedItems.forEach( ( aItem ) => {
   if( aItem.id === itemID )
    exists = true;
  });

  return exists;
 }

 getIcon(type){
  var myIcon = "";
  switch(type){
   case "calendar" : 
    myIcon = '<svg class="bi bi-calendar" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
              '<path fill-rule="evenodd" d="M14 0H2a2 2 0 00-2 2v12a2 2 0 002 2h12a2 2 0 002-2V2a2 2 0 00-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z" clip-rule="evenodd"/>' + 
              '<path fill-rule="evenodd" d="M6.5 7a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd"/>' + 
             '</svg>';
   break;
   case "text" : 
    myIcon = '<svg class="bi bi-card-text" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
              '<path fill-rule="evenodd" d="M14.5 3h-13a.5.5 0 00-.5.5v9a.5.5 0 00.5.5h13a.5.5 0 00.5-.5v-9a.5.5 0 00-.5-.5zm-13-1A1.5 1.5 0 000 3.5v9A1.5 1.5 0 001.5 14h13a1.5 1.5 0 001.5-1.5v-9A1.5 1.5 0 0014.5 2h-13z" clip-rule="evenodd"/>' + 
              '<path fill-rule="evenodd" d="M3 5.5a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9a.5.5 0 01-.5-.5zM3 8a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9A.5.5 0 013 8zm0 2.5a.5.5 0 01.5-.5h6a.5.5 0 010 1h-6a.5.5 0 01-.5-.5z" clip-rule="evenodd"/>' + 
             '</svg>';
   break;
   case "email" : 
    myIcon = '<svg class="bi bi-at" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
              '<path fill-rule="evenodd" d="M13.106 7.222c0-2.967-2.249-5.032-5.482-5.032-3.35 0-5.646 2.318-5.646 5.702 0 3.493 2.235 5.708 5.762 5.708.862 0 1.689-.123 2.304-.335v-.862c-.43.199-1.354.328-2.29.328-2.926 0-4.813-1.88-4.813-4.798 0-2.844 1.921-4.881 4.594-4.881 2.735 0 4.608 1.688 4.608 4.156 0 1.682-.554 2.769-1.416 2.769-.492 0-.772-.28-.772-.76V5.206H8.923v.834h-.11c-.266-.595-.881-.964-1.6-.964-1.4 0-2.378 1.162-2.378 2.823 0 1.737.957 2.906 2.379 2.906.8 0 1.415-.39 1.709-1.087h.11c.081.67.703 1.148 1.503 1.148 1.572 0 2.57-1.415 2.57-3.643zm-7.177.704c0-1.197.54-1.907 1.456-1.907.93 0 1.524.738 1.524 1.907S8.308 9.84 7.371 9.84c-.895 0-1.442-.725-1.442-1.914z" clip-rule="evenodd"/>' + 
             '</svg>';
   break;
   case "password" : 
    myIcon = '<svg class="bi bi-eye-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
               '<path d="M10.5 8a2.5 2.5 0 11-5 0 2.5 2.5 0 015 0z"/>' + 
               '<path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 100-7 3.5 3.5 0 000 7z" clip-rule="evenodd"/>' + 
             '</svg>';
   break;
   case "phone" : 
    myIcon = '<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-telephone" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
              '<path fill-rule="evenodd" d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"/>' + 
             '</svg>';
   break;
   case "route" : 
    myIcon = '<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-cursor" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
               '<path fill-rule="evenodd" d="M14.082 2.182a.5.5 0 0 1 .103.557L8.528 15.467a.5.5 0 0 1-.917-.007L5.57 10.694.803 8.652a.5.5 0 0 1-.006-.916l12.728-5.657a.5.5 0 0 1 .556.103zM2.25 8.184l3.897 1.67a.5.5 0 0 1 .262.263l1.67 3.897L12.743 3.52 2.25 8.184z"/>' + 
             '</svg>';
   break;
   case "geo" : 
    myIcon = '<svg class="bi bi-geo-alt" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
               '<path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 002 6c0 4.314 6 10 6 10zm0-7a3 3 0 100-6 3 3 0 000 6z" clip-rule="evenodd"/>' + 
             '</svg>';
   break;
   case "contact" : 
    myIcon = '<svg class="bi bi-people-circle" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
               '<path d="M13.468 12.37C12.758 11.226 11.195 10 8 10s-4.757 1.225-5.468 2.37A6.987 6.987 0 008 15a6.987 6.987 0 005.468-2.63z"/>' + 
               '<path fill-rule="evenodd" d="M8 9a3 3 0 100-6 3 3 0 000 6z" clip-rule="evenodd"/>' + 
               '<path fill-rule="evenodd" d="M8 1a7 7 0 100 14A7 7 0 008 1zM0 8a8 8 0 1116 0A8 8 0 010 8z" clip-rule="evenodd"/>' + 
             '</svg>';
   break;
   case "search" : 
    myIcon = '<svg class="bi bi-search" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' +
               '<path fill-rule="evenodd" d="M10.442 10.442a1 1 0 011.415 0l3.85 3.85a1 1 0 01-1.414 1.415l-3.85-3.85a1 1 0 010-1.415z" clip-rule="evenodd"/>' +
               '<path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 100-11 5.5 5.5 0 000 11zM13 6.5a6.5 6.5 0 11-13 0 6.5 6.5 0 0113 0z" clip-rule="evenodd"/>' +
             '</svg>';
   break;
   case "number" : 
    myIcon = '<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-calculator-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
                '<path fill-rule="evenodd" d="M12 1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM4 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H4z"/>' + 
                '<path d="M4 2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-2zm0 4a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm3-6a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm3-6a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-4z"/>' + 
               '</svg>';
   break;
   default : 
    myIcon = '<svg class="bi bi-card-text" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
              '<path fill-rule="evenodd" d="M14.5 3h-13a.5.5 0 00-.5.5v9a.5.5 0 00.5.5h13a.5.5 0 00.5-.5v-9a.5.5 0 00-.5-.5zm-13-1A1.5 1.5 0 000 3.5v9A1.5 1.5 0 001.5 14h13a1.5 1.5 0 001.5-1.5v-9A1.5 1.5 0 0014.5 2h-13z" clip-rule="evenodd"/>' + 
              '<path fill-rule="evenodd" d="M3 5.5a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9a.5.5 0 01-.5-.5zM3 8a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9A.5.5 0 013 8zm0 2.5a.5.5 0 01.5-.5h6a.5.5 0 010 1h-6a.5.5 0 01-.5-.5z" clip-rule="evenodd"/>' + 
             '</svg>';    
   break;
  }

  return myIcon;
 }

 getPopup( popupTitle , popupBody ){
  return new Promise( ( resolve , reject ) => {
   jQuery.prompt({
    viewDates: {
     title   : popupTitle,
     html    : popupBody,
     buttons : { Cancel: false, Set: true },
     submit  :function(e,v,m,f){
      if(v){
       e.preventDefault();
       resolve( f );
      }
      jQuery.prompt.close();
     }
    }
   });
  });

 }

 addHotel( data ){
  this.box.insertAdjacentHTML(
   "afterBegin" , 
   '<div class="card" id="card-'+data.hotel_id+'" style="margin: 15px; background-image: url('+data.ht_image+');"> ' +
    '<div class="text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4">' +
      '<div>' +
        '<h3 class="card-title pt-2"><strong>'+data.ht_name+'</strong></h3>' +
        '<p>'+data.ht_country+' | '+data.ht_city+'<br>' + 
         '<span>'+data.check_in+'</span>' + 
         '<br>' + 
         '<span>'+data.check_out+'</span>' + 
         '<br>' + 
         '<span>'+data.food+'</span>' + 
         '<br>' + 
         '<span>'+data.type+'</span>' + 
         '<br>' + 
         '<span>'+data.amenities+'</span>' + 
        '</p>' +
        '<a class="btn btn-pink" id="delete-card-'+data.hotel_id+'">Delete</a>' +
      '</div>' +
    '</div>' +
   '</div>'
  );

  this.selectedItems.push({
   "node"           : document.getElementById('card-'+data.hotel_id),
   "id"             : 'card-'+data.hotel_id,
   "image"          : data.ht_image,
   "text"           : data.ht_country+' | '+data.ht_city,
   "name"           : data.ht_name,
   "food"           : data.food,
   "type"           : data.type,
   "amenities"      : data.amenities,
   "selecteditemid" : data.hotel_id,
   "check_in"       : data.check_in,
   "check_out"      : data.check_out
  });
 }

 addDestination( data ){
  this.box.insertAdjacentHTML(
   "afterBegin" , 
   '<div class="card" id="card-'+data.destination_id+'" style="margin: 15px; background-image: url();"> ' +
    '<div class="text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4">' +
      '<div>' +
        // '<h3 class="card-title pt-2"><strong>'+data.country+'</strong></h3>' +
        '<p>'+data.city+'<br>' + 
         '<span>'+data["from"].substr( 0 , 10 )+'</span>' + 
         '<br>' + 
         '<span>'+data["to"].substr( 0 , 10 )+'</span>' + 
        '</p>' +
        '<a class="btn btn-pink" id="delete-card-'+data.destination_id+'">Delete</a>' +
      '</div>' +
    '</div>' +
   '</div>'
  );

  this.selectedItems.push({
   "node"           : document.getElementById('card-'+data.destination_id),
   "id"             : 'card-'+data.destination_id,
   "city"           : data.city,
   "country"        : data.country,
   "longitude"      : data.longitude,
   "latitude"       : data.latitude,
   "from"           : data.from,
   "to"             : data.to,
   "selecteditemid" : data.destination_id
  });
 }

 addTransportation( data ){
  this.box.insertAdjacentHTML(
   "afterBegin" , 
   '<div class="card" id="card-'+data.transportation_id+'" style="margin: 15px; background-image: url();"> ' +
    '<div class="text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4">' +
      '<div>' +
        '<h3 class="card-title pt-2"><strong>'+data.transportation_company+'</strong></h3>' +
        '<p>'+data.transportation_type+'<br>' + 
         '<span>'+data.fromDate+'</span>' + 
         '<br>' + 
         '<span>'+data.toDate+'</span>' + 
         '<br>' + 
         '<span>'+data.fromDestination+'</span>' + 
         '<br>' + 
         '<span>'+data.toDestination+'</span>' + 
         '<br>' + 
         '<span>'+data.price+'</span>' + 
         '<br>' + 
         '<span>'+data.duration+'</span>' + 
         '<br>' + 
         '<span>'+data.route+'</span>' + 
         '<br>' + 
         '<span>'+data.type+'</span>' + 
        '</p>' +
        '<a class="btn btn-pink" id="delete-card-'+data.transportation_id+'">Delete</a>' +
      '</div>' +
    '</div>' +
   '</div>'
  );

  this.selectedItems.push({
   "node"                : document.getElementById('card-'+data.transportation_id),
   "id"                  : 'card-'+data.transportation_id,
   "destination_company" : data.transportation_company,
   "destination_type"    : data.transportation_type,
   "fromDate"            : data.fromDate,
   "toDate"              : data.toDate,
   "fromDestination"     : data.fromDestination,
   "toDestination"       : data.toDestination,
   "price"               : data.price,
   "duration"            : data.duration,
   "route"               : data.route,
   "type"                : data.type,
   "selecteditemid"      : data.transportation_id
  });
 }

 addActivity( data ){
  this.box.insertAdjacentHTML(
   "afterBegin" , 
   '<div class="card" id="card-'+data.activity_id+'" style="margin: 15px; background-image: url('+data.activity_image+');"> ' +
    '<div class="text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4">' +
      '<div>' +
        '<h3 class="card-title pt-2"><strong>'+data.activity_name+'</strong></h3>' +
        '<p>'+data.activity_location+'<br>' + 
         '<span>'+data.from+'</span>' + 
         '<br>' + 
         '<span>'+data.from+'</span>' + 
         '<br>' + 
         '<span>'+data.cost+'</span>' + 
         '<br>' + 
         '<span>'+data.duration+'</span>' + 
        '</p>' +
        '<a class="btn btn-pink" id="delete-card-'+data.activity_id+'">Delete</a>' +
      '</div>' +
    '</div>' +
   '</div>'
  );

  this.selectedItems.push({
   "node"           : document.getElementById('card-'+data.activity_id),
   "id"             : 'card-'+data.activity_id,
   "from"           : data.from,
   "to"             : data.to,
   "cost"           : data.cost,
   "duration"       : data.duration,
   "selecteditemid" : data.activity_id
  });
 }

 addTraveler( data ){
  this.box.insertAdjacentHTML(
   "afterBegin" , 
   '<div class="card" id="card-'+data.traveler_id+'" style="margin: 15px; background-image: url();"> ' +
    '<div class="text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4">' +
      '<div>' +
        // '<h3 class="card-title pt-2"><strong>'+data.traveler_first_name+'</strong></h3>' +
        '<p>'+data.traveler_last_name+' | '+data.traveler_first_name+'<br>' + 
         '<span>'+data.traveler_age+'</span>' + 
         '<br>' + 
         '<span>'+data.traveler_email+'</span>' + 
         '<br>' + 
         '<span>'+data.traveler_phone+'</span>' + 
        '</p>' +
        '<a class="btn btn-pink" id="delete-card-'+data.traveler_id+'">Delete</a>' +
      '</div>' +
    '</div>' +
   '</div>'
  );

  this.selectedItems.push({
   "node"                 : document.getElementById('card-'+data.traveler_id),
   "id"                   : 'card-'+data.traveler_id,
   "selecteditemid"       : data.traveler_id,
   "traveler_email"       : data.traveler_email,
   "traveler_age"         : data.traveler_age,
   "traveler_phone"       : data.traveler_phone,
   "traveler_last_name"   : data.traveler_last_name,
   "traveler_first_name"  : data.traveler_first_name
  });
 }

 addItem( item ){
  console.log( "[ comboGrid | addItem( item , uniqueID ) ]"  );
  console.log( "item" , item  );
  console.log( "uniqueID" , this.uniqueID  );
  console.log( "Node : " + 'card-'+item[this.uniqueID] );
  let myUniqueID = item[this.uniqueID];
  let myNode     = 'card-'+myUniqueID;

  if( !this.ifExists( myNode ) ){

   this.removedItems.forEach( ( aItem , idx ) => {
    if( myUniqueID === aItem ){
     this.removedItems.splice( idx , 1 );
    }
   });

   if( this.options.options.displayType == "hotel" ){
     this.addHotel( item );
     document.getElementById('delete-card-'+myUniqueID).addEventListener( "click" , ( event ) =>{
      this.removeItem( myNode );
     });
   }
   else if( this.options.options.displayType == "destination" ){
     this.addDestination( item );
     document.getElementById('delete-card-'+myUniqueID).addEventListener( "click" , ( event ) =>{
      this.removeItem( myNode );
     });
   }
   else if( this.options.options.displayType == "activity" ){
     this.addActivity( item );
     document.getElementById('delete-card-'+myUniqueID).addEventListener( "click" , ( event ) =>{
      this.removeItem( myNode );
     });
   }
   else if( this.options.options.displayType == "transportation" ){
     this.addTransportation( item );
     document.getElementById('delete-card-'+myUniqueID).addEventListener( "click" , ( event ) =>{
      this.removeItem( myNode );
     });
   }
   else if( this.options.options.displayType == "traveler" ){
     this.addTraveler( item );
     document.getElementById('delete-card-'+myUniqueID).addEventListener( "click" , ( event ) =>{
      this.removeItem( myNode );
     });
   }
  } 
  else{
   console.log( "item already exists" );
  }
 }

 removeItem( itemID ){

  let myNode = document.getElementById(itemID);
  if( myNode ){
   myNode.remove();

   this.selectedItems.forEach( ( aItem , idx ) => {
    if( aItem.id === itemID ){
     this.selectedItems.splice( idx , 1 );
     this.removedItems.push( aItem.id.replace('card-','') );
    }
   });
  }
 }

 getGrid(){
  return new amGrid( {
      "container"      : this.mainid,
      "uniqueId"       : "grid_"+this.mainid+"_" + this.modalID,
      "customAJAX"     : this.options.server.customAJAX,
      "uniqueRowId"    : this.uniqueID,
      "dispatcherPath" : "../ws/ws.grid.php", 
      "processtype"    : this.options.options.processtype,
      "scrollable"     : false,
      "height"         : "290px",
      "width"          : "200px",
      "collapse"       : true,
      "columnsDisplay" : this.options.options.columnsDisplay,
      "columnsearch"   : {
       "columns"  : this.options.options.columnsearch
      },
      "rowEvent"   : ( event ) =>{
       this.rowEvent( event );
      },
      "rowselect"  : true,
      "submit"      : ( data ) => {
       console.log( data );
      }
     });
 }

}
