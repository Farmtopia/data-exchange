function randomUID( length ) {
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt( Math.floor( Math.random( ) * charactersLength ) );
   }
   return "uid_"+result;
}

function getGridSelectedItem( containerID ){
 var myResponse = new Object( );

 console.log(
  document.querySelectorAll( containerID+'-selected' )
 );

 document.getElementById( containerID+'-selected' ).value.split( "," ).forEach( 
  ( item )=>{
   var myPair = item.split( "|" );
   myResponse[myPair[0]] = myPair[1];
  }
 );

 return myResponse;
}

function _getMultiComboEvent( options , response ){
 var myModalID       = randomUID( 10 );
 var myDataContainer = myModalID+"-body";
 var myGridDomID     = randomUID( 10 )+"-container";
 var combo;

 getModal(
  {
   "id"      : myModalID,
   "title"   : options.modalTitle,
   "buttons" : ["cancel"],
   "body"    : "<div id='"+myGridDomID+"' class='multi-combo-grid-form'></div><div id='"+myGridDomID+"-form-sub' class='multi-combo-grid-form'></div>"
  } , 
   ( ) => {
    combo = new comboFormGrid({
     "activateActionButtons" : options.activateActionButtons,
     "CRUD"                  : options.CRUD,
     "mainid"                : myGridDomID,
     "modalID"               : myModalID,
     "entity"                : options.entity,
     "formColumns"           : options.formColumns,
     "parentId"              : options.parentId,
     "parentEntity"          : options.parentEntity
    });
   }
 )
}

function getMultiComboEvent( options , response ){
 var myModalID       = randomUID( 10 );
 var myDataContainer = myModalID+"-body";
 var myPopupGrid;
 var myPopupGridB;
 var combo;

 var myGridDomID    = "myGridModalContainer";
 var myDisplayDomID = "myDisplayBox";

 getModal(
  {
   "id"      : myModalID,
   "title"   : options.modalTitle,
   "body"    : "<div id='"+myGridDomID+"' class='multi-combo-grid'></div><div id='"+myDisplayDomID+"' class='multi-combo-display'></div>",
   "action"  : {

    "submit" : ( event )=>{

     let myAjaxSettings = {
      "action"      : "updateData",
      // "proposal_id" : combo.parentid,
      "proposal_id" : response.data[response.options.uniqueid],
      "entity"      : options.entity,
      "remove"      : combo.removedItems,
      "add"         : combo.selectedItems
     }

     AJAX("../ws/ws.multicombo.php" , myAjaxSettings , 
     ( response ) => {
      if( response.error ){
       getModal({ "body" : "There was an error , try again." , "title" : "Error in Updating" });
      }
      else{
       if( ( response.res.addeditems.length > 0 ) || ( response.res.removeditems.length ) ){
        getModal({ "body" : options.entity + " have been updated succesfully." , "title" : "Success" }); 
       }
      }
     });

     $( '#'+myModalID ).modal( 'hide' );
    }
   }

  } , 
   ( ) => {
    combo = new comboGrid({
     "activateActionButtons" : options.activateActionButtons,
     "mainid"                : myGridDomID,
     "boxid"                 : myDisplayDomID,
     "modalID"               : myModalID,
     "options"               : options,
     // "parentid"     : response.data[response.options.uniqueid],
     // "parententity" : response.options.entity,
     "server"  : {
      "dispatcherPath" : "../ws/ws.grid.php",
      "customAJAX"     : {
       "entity"         : options.entity 
       // , 
       // "parentid"       : response.data[response.options.uniqueid] , 
       // "parententity"   : response.options.entity
      },
      "data" : {
       "dispatcherPath" : "../ws/ws.multicombo.php" , 
       "request" : {
        "action"         : "getData",
        "parentid"       : response.data[response.options.uniqueid] , 
        "parententity"   : response.options.entity,
        "entity"         : options.entity
       }
      }
     }
    });
   }
 )
}

function getComboEvent( inputItem , response ){
 var myModalID       = randomUID( 10 );
 var myDataContainer = myModalID+"-body";

 getModal(
  {
   "id"      : myModalID,
   "title"   : inputItem.modalTitle,
   "action"  : {
    "submit" : ( event )=>{
     var mySelectedItem   = getGridSelectedItem( myDataContainer );
     var myHiddenProperty = mySelectedItem[inputItem.hiddenProperty];
     var myReturnedValue  = mySelectedItem[inputItem.returnProperty];

  // Check if hidden input exists
  // If it exists set its value to the selected row row_id  
  // If it does not exist , proceed in creating it 
  // and setting the value to the selected row_id
     var myHiddenInputDomElement = document.getElementById( inputItem.hiddenProperty );

     if( myHiddenInputDomElement ){
      myHiddenInputDomElement.value = myHiddenProperty;
     }
     else{
      document
        // .getElementById( "amForm-"+response.options.entity+"-"+response.options.formid )
        .getElementById( inputItem.formID )
        .insertAdjacentHTML( 
          "afterBegin",
          "<input type='hidden' name='"+inputItem.hiddenProperty+"' id='"+inputItem.hiddenProperty+"' value='"+myHiddenProperty+"'></input>"
        );
     }

     if( typeof myReturnedValue != 'undefined' )
      document.getElementById( inputItem.returnProperty ).value = myReturnedValue;
     else
      console.log( "No matching return property found." );

     $( '#'+myModalID ).modal( 'hide' );
    }
   }
  } , 
   function( ){
    if( inputItem.type == "grid" ){
     let myPopupGrid = new amGrid( {
      "container"      : myDataContainer,
      "uniqueId"       : "grid_" + myModalID,
      "customAJAX"     : {"entity" : "account"},
      "uniqueRowId"    : "account_id",
      "dispatcherPath" : "../ws/ws.grid.php", 
      "processtype"    : inputItem.processtype,
      "scrollable"     : false,
      "height"         : "150px",
      "collapse"       : true,
      "columnsDisplay" : inputItem.columnsDisplay,
      "columnsearch"   : {
       "columns"  : inputItem.columnsearch
      },
      "rowEvent"   : true,
      "rowselect"  : true
      // ,"tableActions" : {
        // "addRow" : {
          // "action"   : "addRow",
          // "position" : "account-form-main",
          // "name"     : "Add Account",
          // "exclude"  : ["account_id","created_on"]
        // }
      // }
     });
    }
   }
 )
}

function getFormInputs( DataSet  , formID ){
 var myReturn = new Array( );

 DataSet.inputs.forEach( ( inputItem )=>{

  if( inputItem.type == "combo" ){

   inputItem.action = {
    "type"  : "click" ,
    "event" : ( event ) => {
     if( !event.readonly ){
      getComboEvent({
       "columnsearch" : {
        "acc_last_name"  : "string",
        "acc_first_name" : "string"
       },
       "modalTitle"     : inputItem.modalTitle,
       "type"           : "grid",
       "formID"         : formID,
       "dispatcher"     : "../ws/ws.grid.php",
       "returnProperty" : "acc_last_name",
       "hiddenProperty" : "account_id",
       "processtype"    : "server",
       "columnsDisplay" : [ "acc_last_name" , "acc_first_name" ]
      } , DataSet );
     }
     else{
      getModal({
       "title"    : "Information",
       "body"     : "Action available only on EDIT mode" ,
       "buttons"  : ["cancel"]
      });
      console.log( "Item in ReadOnly Status" );
     }
    },
    "action" : ( ev ) => {
     console.log( ev );
    }
   }
  }
  else if( inputItem.type == "combo-multi-form" ){
   let formColumns  = 2;
   let entity       = inputItem.entity;
   let modalTitle   = "Add " + inputItem.entity;
   let parentId     = DataSet.data.proposal_id;
   let parentEntity = DataSet.options.entity;

   inputItem.action = {
    "type"  : "click" ,
    "event" : ( event ) => {
     // if( !event.readonly ){
      _getMultiComboEvent({
       "activateActionButtons" : event.readonly === false ? true : false,
       "dispatcher"            : "../ws/ws.grid.php",
       "processtype"           : "server",
       "CRUD"                  : ( ( inputItem.type == "combo-multi-form" ) ? true : false ),
       "displayType"           : entity,  // This is used in [ File : class_combo_grid.js , Functions : addItem( item ) , rowEvent( event ) ]
       "entity"                : entity,
       "modalTitle"            : modalTitle,
       "formColumns"           : formColumns,
       "parentId"              : parentId,
       "parentEntity"          : parentEntity
      } , DataSet );
     // }
     // else{
      // getModal({
       // "title"    : "Information",
       // "body"     : "Action available only on EDIT mode" ,
       // "buttons"  : ["cancel"]
      // });
      // console.log( "Item in ReadOnly Status" );
     // }
    },
    "action" : ( ev ) => {
     console.log( ev );
    }
   }

  }
  else if( inputItem.type == "combo-multi" ){

   let columnsearch;
   let returnProperty;
   let hiddenProperty;
   let columnsDisplay;
   let entity;
   let modalTitle;

   if( inputItem.name == "ht_name" ) {
    entity     = "hotel";
    modalTitle = "Hotels";
    columnsearch = {
     "ht_name"    : "string",
     "ht_country" : "string",
     "ht_city"    : "string"
    };

    returnProperty = "ht_name";
    hiddenProperty = "hotel_id";
    columnsDisplay = [ "ht_name" , "ht_country" , "ht_city" , "ht_address" , "ht_url" ];
   }
   else if( inputItem.name == "city" ) {
    entity     = "destination";
    modalTitle = "Destinations";
    columnsearch = {
     "city"    : "string"
    };

    returnProperty = "city";
    hiddenProperty = "destination_id";
    columnsDisplay = [ "city" ];
   }
   else if( inputItem.name == "activity_name" ) {
    entity     = "activity";
    modalTitle = "Activities";
    columnsearch = {
     "activity_name"     : "string",
     "activity_location" : "string"
    };

    returnProperty = "activity_name";
    hiddenProperty = "activity_id";
    columnsDisplay = [ "activity_name" , "activity_type" , "activity_location" , "activity_period_of_availability" , "activity_description" , "activity_url" ];
   }
   else if( inputItem.name == "transportation_company" ) {
    entity     = "transportation";
    modalTitle = "Transportations";
    columnsearch = {
     "transportation_company" : "string",
     "transportation_type"    : "string"
    };

    returnProperty = "transportation_company";
    hiddenProperty = "transportation_id";
    columnsDisplay = [ "transportation_company" , "transportation_type" ];
   }
   else if( inputItem.name == "traveler_email" ) {
    entity     = "traveler";
    modalTitle = "Traveler";
    columnsearch = {
     "traveler_email"     : "string",
     "traveler_last_name" : "string"
    };

    returnProperty = "traveler_email";
    hiddenProperty = "traveler_id";
    columnsDisplay = [ "traveler_first_name" , "traveler_last_name" , "traveler_email" ];
   }

   inputItem.action = {
    "type"  : "click" ,
    "event" : ( event ) => {
     // if( !event.readonly ){
      getMultiComboEvent({
       "activateActionButtons" : event.readonly === false ? true : false,
       "dispatcher"            : "../ws/ws.grid.php",
       "processtype"           : "server",
       "displayType"           : entity,  // This is used in [ File : class_combo_grid.js , Functions : addItem( item ) , rowEvent( event ) ]
       "entity"                : entity,
       "modalTitle"            : modalTitle,
       "type"                  : "grid",
       "formID"                : formID,
       "columnsearch"          : columnsearch , 
       "returnProperty"        : returnProperty,
       "hiddenProperty"        : hiddenProperty,
       "columnsDisplay"        : columnsDisplay
      } , DataSet );
     // }
     // else{
      // getModal({
       // "title"    : "Information",
       // "body"     : "Action available only on EDIT mode" ,
       // "buttons"  : ["cancel"]
      // });
      // console.log( "Item in ReadOnly Status" );
     // }
    },
    "action" : ( ev ) => {
     console.log( ev );
    }
   }

  }

  myReturn.push( inputItem );

 });

 return myReturn;
}

function getFormHiddenInputs( DataSet ){
 var myReturn = new Array( );

     myReturn.push( 
      {
        "id"   : DataSet[ "options" ][ "uniqueid" ],
        "name" : DataSet[ "options" ][ "uniqueid" ]
      }
     );

  return myReturn;
}

function getFormTemplate( url , action ){

 return new Promise( ( success , reject )=>{
  AJAX( url , action , ( response )=>{
   if( typeof response.error !== 'undefined' && response.error ){
    reject( response );
   }
   else{
    success( response );
   }
  } );
 } );
 
}

