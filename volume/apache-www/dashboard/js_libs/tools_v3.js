/*
1) Need to add uniqueness so that multiple grids can exist on the same page
2) When there is an Action on the ROW or Column Collapse should be deactivated
   If there is no action then Collapse should be activated
3) Event on OnChange of Select
*/

function submitEnterKey(key){
 console.log(key);
}

function getViewDimensions(type){
 if(typeof type === 'undefined'){
  return {
   width  : Math.max(document.documentElement.clientWidth,  window.innerWidth  || 0),
   height : Math.max(document.documentElement.clientHeight, window.innerHeight || 0)
  }
 }else if(type === "height"){
  return Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
 }else if(type === "width"){
  return Math.max(document.documentElement.clientWidth,  window.innerWidth  || 0);
 }
}

function randomUID(length) {
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return "uid_"+result;
}

function getModal(options , callback){
 // console.log("main modal" , options);
   var validationResult;
   var myBodyHTML;

   if(document.getElementById(options.id)){
    document.getElementById(options.id).remove();
   }

// Generate a Random UID to assign as DOM element ID if no ID has been provided
   if(typeof options.id === 'undefined'){
    options.id = this.randomUID(10);
   }

// Add the Parent that will hold all the Modal DOM Elements
   document.body.insertAdjacentHTML(
    "beforeEnd",
    '<div class="modal fade" id="'+options.id+'" role="dialog" aria-labelledby="'+options.id+'" aria-hidden="true"></div>'
   );

// You either provide the BodyMessage or create it on the Callback.
// if either does not take place then the following message will be shown for notification reasons.
   myBodyHTML = options.body || "";

   let cancelButton;
   let acceptButton;
   let acceptButtonLocale = options.acceptButton ? options.acceptButton : "Accept";
   let cancelButtonLocale = options.cancelButton ? options.cancelButton : "Close";

   if( typeof options.buttons !== 'undefined' ){
    if( options.buttons.includes("cancel") )
     cancelButton = '<button type="button" id="am-'+options.id+'-close" class="btn btn-danger btn-sm" data-dismiss="modal">'+cancelButtonLocale+'</button>';
    else
     cancelButton = '';

    if( options.buttons.includes("accept") )
     acceptButton = '<button type="button" id="am-'+options.id+'-submit" class="btn btn-secondary btn-sm" data-dismiss="modal">'+acceptButtonLocale+'</button>';
    else
     acceptButton = '';
   }
   else{
     cancelButton = '<button type="button" id="am-'+options.id+'-close" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>';
     acceptButton = '<button type="button" id="am-'+options.id+'-submit" class="btn btn-primary btn-sm">Ok</button>';
   }
   
   let myModalSizeStyle = "modal-xl";
   if( typeof options.modalStyle !== 'undefined' ){
    switch( options.modalStyle ){
     case "extra-large" : myModalSizeStyle = "modal-xl"; break;
     case "large"       : myModalSizeStyle = "modal-l";  break;
     case "small"       : myModalSizeStyle = "modal-sm"; break;
     default            : myModalSizeStyle = "modal-xl"; break;
    }
   }

// Add on the Parent the rest of the Modal along with the Body of the Form [myBodyHTML]
   document.getElementById(options.id).insertAdjacentHTML(
    "beforeEnd",
    '<div id="" class="modal-dialog '+myModalSizeStyle+'" role="document">' +
                     '<div class="modal-content">' +
                       '<div class="modal-header">' +
                         '<h5 class="modal-title" id="'+options.id+'-title">'+options.title+'</h5>' +
                         '<button type="button" class="close btn-sm" data-dismiss="modal" aria-label="Close">' +
                           '<span aria-hidden="true">&times;</span>' +
                         '</button>' +
                       '</div>' +
                       '<div class="modal-body" id="'+options.id+'-body">' +
                       myBodyHTML + 
                       '</div>' +
                       '<div class="modal-footer">' + 
                       acceptButton + 
                       cancelButton + 
                       '</div>' +
                     '</div>' +
                   '</div>'
   );

// Add event Listener on Close Button to hide the Modal and remove the DOM Element (The delay is to let the fade out animation to finish)
   if( document.getElementById("am-"+options.id+"-close") ){
    document.getElementById("am-"+options.id+"-close").addEventListener('click' , (event)=>{
     setTimeout(()=>{
       if( document.getElementById(options.id) )
        document.getElementById(options.id).remove();
     },500);
    });
    
   }

// If we have a Custom Submit Function in our Configuration we need to set it up
   if(options.action){
    if(options.action.submit){

  // Assign a Listener on our Submit Buttong
     if( document.getElementById("am-"+options.id+"-submit") ){
       document.getElementById("am-"+options.id+"-submit").addEventListener('click' , (event)=>{
        
     // Serialize our Form.
        var myForm           = document.querySelector("#"+options.id+" form");
        console.log( myForm );
        var mySerializedForm = jQuery(myForm).serializeArray();

     // If we have Requested a Form Validation then we proceed in doing so.
     // If not then we simply sent the results back to the client.
        if(options.validate){
         myForm.reportValidity();

      // If our Form Validation has Succeded proceed in executing the submit function 
      // passing all the relevant form properties 
         if (myForm.checkValidity() === true){
          options.action.submit({
           "event"  : event,
           "form"   : document.querySelectorAll("#"+options.id+" [name]"),
           "values" : mySerializedForm
          });
          
       // Hide the Modal and also remove the Element from the DOM
          $('#'+options.id).modal('hide');
          setTimeout(()=>{ document.getElementById(options.id).remove(); },500);

         }
         else{
          // Do nothing...
         }

        }
        else{
      // Proceed in executing the submit function passing all the relevant form properties
         options.action.submit({
          "event"  : event,
          "form"   : document.querySelectorAll("#"+options.id+" [name]"),
          "values" : mySerializedForm
         });

      // Hide the Modal and also remove the Element from the DOM
         $('#'+options.id).modal('hide');
         setTimeout(()=>{ 
          if( document.getElementById(options.id) )
           document.getElementById(options.id).remove(); 
         },500);
        }
       });
     }
    }
   }
   else{
 // Add event Listener on Close Button to hide the Modal and remove the DOM Element (The delay is to let the fade out animation to finish)
    if( document.getElementById("am-"+options.id+"-submit") ) {
     document.getElementById("am-"+options.id+"-submit").addEventListener('click' , (event)=>{
      $('#'+options.id).modal("hide");
      setTimeout(()=>{
       if( document.getElementById(options.id) )
        document.getElementById(options.id).remove(); 
      },500);
     });
    }
   }

// If there is Autosubmit ,then when the user presses the Enter Key 
// The submit event will be fired [Default : true]
   if(options.autosubmit){
    console.log(document.getElementById(options.id+'-body'));
    document.getElementById(options.id+'-body').addEventListener('keydown', (e) => {
      if (!e.repeat){
       if(e.keyCode == 13){
        document.getElementById('am-'+options.id+'-submit').click();
       }
      }
    });
   }

// Bring up the Modal
   $('#'+options.id).modal();

// Remove Modal when user clicks on X
   $('#'+options.id).on('hidden.bs.modal', function (e) {
    document.getElementById(options.id).remove(); 
   });

// If there is a Callback that has to be executed after the Modal has been rendered proceed in calling it.
   if(typeof callback === 'function'){
    $('#'+options.id).on('shown.bs.modal', function (e) {
     callback();
    });
   }
 }

function getAlert(options){

  var myContainer;
  var myAlertElement;

  if(!options){
    console.log("No Provided Options for Alert");
   return;
  }

  if(typeof options === "string"){
   var tempVar = options;
   options = new Object();
   options.message = tempVar;
  }

  var myContainerID = options.container || "bs-alert-amanos-cnt";
  var myAlertType   = options.type      || "warning"; // Types : info , warning , success , danger , etc
  var myAlertMsg    = options.message   || "";
  var myAlertHTML   = "";
  var myIcon        = "";
  switch( myAlertType ){
   case "primary" : myIcon = '<i class="fas fa-check-circle"></i>';         break;
   case "warning" : myIcon = '<i class="fas fa-exclamation-triangle"></i>'; break;
   case "info"    : myIcon = '<i class="fas fa-exclamation-circle"></i>';   break;
   case "error"   : myIcon = '<i class="fas fa-ban"></i>';                  break;
   case "danger"  : myIcon = '<i class="fas fa-exclamation-triangle"></i>'; break;
   default        : myIcon = '<i class="fas fa-check-circle"></i>';         break;
  }

  if(document.getElementById(myContainerID)){
   document.getElementById(myContainerID).remove();
  }

  myAlertElement = document.createElement("div");
  myAlertElement.setAttribute("id", myContainerID);
  myAlertElement.setAttribute("class", "bs-alert-amanos-cnt");
  document.body.appendChild(myAlertElement);

  myContainer = document.getElementById(myContainerID);

  myAlertHTML += '<div class="alert alert-'+myAlertType+'">';
  myAlertHTML += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
  myAlertHTML += myIcon + " " + myAlertMsg;
  myAlertHTML += '</div>';

  // Set our Alert on the DOMContentLoaded
     myContainer.insertAdjacentHTML("beforeEnd" , myAlertHTML);

  // Slide In
     setTimeout(()=>{
      myAlertElement.style.top = "0px";
     },100);

  // Fade out
     setTimeout( ()=>{
       myAlertElement.style.top = "-500px";
     } , 2000);

}

function __amForm(formOptions){
   console.log("Form Initialization...");
   console.log(formOptions);

   if(typeof formOptions == 'undefined'){
    console.log("You need to provide settings.");
    return;
   }

// Set the Container
   var myContainer = getContainer(formOptions.container);

// Clear Container
   myContainer.innerHTML = "";

// Set the Form
   var myForm = setForm(formOptions.formid);

   function getContainer(containerID){
    return document.getElementById(containerID);
   }

   function setForm(formID){
    myContainer.insertAdjacentHTML("beforeEnd" , "<form id='"+formID+"'></form>");

    return document.getElementById(formOptions.formid);
   }

   function addInputs(myInputs){
    var nOfRows     = 0;
    var myInputHTML = "";
    var nOfColumns  = typeof formOptions.columns !== 'undefined' ? formOptions.columns : 1;

    myInputs.forEach((inputOptions)=>{
     if(typeof inputOptions.id       === 'undefined'){ console.log("Provide an Input ID");   return; }
     if(typeof inputOptions.name     === 'undefined'){ console.log("Provide an Input Name"); return; }
     if(typeof inputOptions.default  === 'undefined'){ inputOptions.default  = "";     }
     if(typeof inputOptions.type     === 'undefined'){ inputOptions.type     = "text"; }
     if(typeof inputOptions.rowspan  === 'undefined'){ inputOptions.rowspan  = false; }
     if(typeof inputOptions.options  === 'undefined'){ inputOptions.options  = false; }
     if(typeof inputOptions.sign     === 'undefined'){ inputOptions.sign     = false; }
     if(typeof inputOptions.required === 'undefined'){ inputOptions.required = false; }
     if(typeof inputOptions.action   === 'undefined'){ inputOptions.action   = false; }

     if(typeof inputOptions.label === 'undefined'){
      inputOptions.label = "";
     }
     else{
      inputOptions.label = '<label for="'+inputOptions.id+'">'+inputOptions.label+'</label>';
     }

     if(typeof inputOptions.help === 'undefined'){
      inputOptions.help = "";
     }
     else{
      inputOptions.help = '<small id="'+inputOptions.type+'Help" class="form-text text-muted">'+inputOptions.help+'</small>';
     }

     var myInputSpecificHTML = getInputHTML({
      "type"    : inputOptions.type,
      "id"      : inputOptions.id,
      "default" : inputOptions.default,
      "name"    : inputOptions.name,
      "options" : inputOptions.options,
      "label"   : inputOptions.label,
      "icon"    : inputOptions.icon,
      "sign"    : inputOptions.sign,
      "required": inputOptions.required,
      "action"  : inputOptions.action
     });

     if(nOfRows % nOfColumns == 0){
      myInputHTML += '<div class="form-row">';
     }

     if(inputOptions.rowspan){
      myInputHTML += '</div>';
      myInputHTML += '<div class="form-row">';
      nOfRows = nOfColumns;
     }else
      nOfRows = nOfRows + 1;

     myInputHTML += '<div class="col">' + 
                     inputOptions.label + 
                     '<div class="form-group">' + 
                      '<div class="input-group mb-3">' + 
                       myInputSpecificHTML + 
                      '</div>' + 
                      inputOptions.help + 
                     '</div>' + 
                    '</div>';

     if(nOfRows % nOfColumns == 0 || inputOptions.rowspan){
      nOfRows = 0;
      myInputHTML += '</div>';
     }
    });

    myForm.insertAdjacentHTML("afterBegin",myInputHTML);

    myInputs.forEach((inputOptions)=>{
     if(inputOptions.type == "date"){
      jQuery('#'+inputOptions.id)
       .datepicker(
        { 'format': 'yyyy-mm-dd' , 'todayHighlight': true, 'autoclose': true }
       );
     }else if(inputOptions.type == "datetime"){
      jQuery('#'+inputOptions.id)
       .datepicker(
        { 'format': 'yyyy-mm-dd' , 'todayHighlight': true, 'autoclose': true }
       );
     }

     if(inputOptions.action){
      document.getElementById(inputOptions.id).addEventListener(inputOptions.action.type,(event)=>{
       if(typeof inputOptions.default  === 'undefined'){ inputOptions.default  = "";     }
       if(typeof inputOptions.type     === 'undefined'){ inputOptions.type     = "text"; }
       if(typeof inputOptions.rowspan  === 'undefined'){ inputOptions.rowspan  = false; }
       if(typeof inputOptions.options  === 'undefined'){ inputOptions.options  = false; }
       if(typeof inputOptions.sign     === 'undefined'){ inputOptions.sign     = false; }
       if(typeof inputOptions.required === 'undefined'){ inputOptions.required = false; }
       if(typeof inputOptions.action   === 'undefined'){ inputOptions.action   = false; }

       inputOptions.action.event({
        "ev"      : event,
        "id"      : inputOptions.id,
        "type"    : inputOptions.type,
        "id"      : inputOptions.id,
        "default" : inputOptions.default,
        "name"    : inputOptions.name,
        "options" : inputOptions.options,
        "label"   : inputOptions.label,
        "icon"    : inputOptions.icon,
        "sign"    : inputOptions.sign,
        "required": inputOptions.required,
        "action"  : inputOptions.action
       });
      });
     }
    });

   }
   
   function getIcon(type){
    var myIcon = "";
    switch(type){
     case "calendar" : 
      myIcon = '<svg class="bi bi-calendar" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
                '<path fill-rule="evenodd" d="M14 0H2a2 2 0 00-2 2v12a2 2 0 002 2h12a2 2 0 002-2V2a2 2 0 00-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z" clip-rule="evenodd"/>' + 
                '<path fill-rule="evenodd" d="M6.5 7a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd"/>' + 
               '</svg>';
     break;
     case "text" : 
      myIcon = '<svg class="bi bi-card-text" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
                '<path fill-rule="evenodd" d="M14.5 3h-13a.5.5 0 00-.5.5v9a.5.5 0 00.5.5h13a.5.5 0 00.5-.5v-9a.5.5 0 00-.5-.5zm-13-1A1.5 1.5 0 000 3.5v9A1.5 1.5 0 001.5 14h13a1.5 1.5 0 001.5-1.5v-9A1.5 1.5 0 0014.5 2h-13z" clip-rule="evenodd"/>' + 
                '<path fill-rule="evenodd" d="M3 5.5a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9a.5.5 0 01-.5-.5zM3 8a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9A.5.5 0 013 8zm0 2.5a.5.5 0 01.5-.5h6a.5.5 0 010 1h-6a.5.5 0 01-.5-.5z" clip-rule="evenodd"/>' + 
               '</svg>';
     break;
     case "email" : 
      myIcon = '<svg class="bi bi-at" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
                '<path fill-rule="evenodd" d="M13.106 7.222c0-2.967-2.249-5.032-5.482-5.032-3.35 0-5.646 2.318-5.646 5.702 0 3.493 2.235 5.708 5.762 5.708.862 0 1.689-.123 2.304-.335v-.862c-.43.199-1.354.328-2.29.328-2.926 0-4.813-1.88-4.813-4.798 0-2.844 1.921-4.881 4.594-4.881 2.735 0 4.608 1.688 4.608 4.156 0 1.682-.554 2.769-1.416 2.769-.492 0-.772-.28-.772-.76V5.206H8.923v.834h-.11c-.266-.595-.881-.964-1.6-.964-1.4 0-2.378 1.162-2.378 2.823 0 1.737.957 2.906 2.379 2.906.8 0 1.415-.39 1.709-1.087h.11c.081.67.703 1.148 1.503 1.148 1.572 0 2.57-1.415 2.57-3.643zm-7.177.704c0-1.197.54-1.907 1.456-1.907.93 0 1.524.738 1.524 1.907S8.308 9.84 7.371 9.84c-.895 0-1.442-.725-1.442-1.914z" clip-rule="evenodd"/>' + 
               '</svg>';
     break;
     case "password" : 
      myIcon = '<svg class="bi bi-eye-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
                 '<path d="M10.5 8a2.5 2.5 0 11-5 0 2.5 2.5 0 015 0z"/>' + 
                 '<path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 100-7 3.5 3.5 0 000 7z" clip-rule="evenodd"/>' + 
               '</svg>';
     break;
     case "geo" : 
      myIcon = '<svg class="bi bi-geo-alt" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
                 '<path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 002 6c0 4.314 6 10 6 10zm0-7a3 3 0 100-6 3 3 0 000 6z" clip-rule="evenodd"/>' + 
               '</svg>';
     break;
     case "contact" : 
      myIcon = '<svg class="bi bi-people-circle" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
                 '<path d="M13.468 12.37C12.758 11.226 11.195 10 8 10s-4.757 1.225-5.468 2.37A6.987 6.987 0 008 15a6.987 6.987 0 005.468-2.63z"/>' + 
                 '<path fill-rule="evenodd" d="M8 9a3 3 0 100-6 3 3 0 000 6z" clip-rule="evenodd"/>' + 
                 '<path fill-rule="evenodd" d="M8 1a7 7 0 100 14A7 7 0 008 1zM0 8a8 8 0 1116 0A8 8 0 010 8z" clip-rule="evenodd"/>' + 
               '</svg>';
     break;
     case "search" : 
      myIcon = '<svg class="bi bi-search" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' +
                 '<path fill-rule="evenodd" d="M10.442 10.442a1 1 0 011.415 0l3.85 3.85a1 1 0 01-1.414 1.415l-3.85-3.85a1 1 0 010-1.415z" clip-rule="evenodd"/>' +
                 '<path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 100-11 5.5 5.5 0 000 11zM13 6.5a6.5 6.5 0 11-13 0 6.5 6.5 0 0113 0z" clip-rule="evenodd"/>' +
               '</svg>';
     break;
    }

    return myIcon;
   }

   function addSumbitButton(){
    myForm.insertAdjacentHTML("beforeEnd",
     '<button class="btn btn-primary" id="submit-'+myForm.id+'" type="button">'+
     'Submit'+
     '</button>'
    );

    document.getElementById("submit-"+myForm.id).addEventListener('click',(event)=>{
     var myFormSerialized = jQuery( document.getElementById(myForm.id) ).serializeArray();
     var validationResult = false;
     myForm.reportValidity();

     if (myForm.checkValidity() === false)
      validationResult = false;
     else
      validationResult = true;

     formOptions.submit({
      "validation"     : validationResult,
      "formid"         : myForm.id,
      "values"         : myFormSerialized
     });

    });
   }

   function setSpinner(ElementID , state){
    switch(state){
     case "on" : 
      document.getElementById(ElementID).style.visibility = "visible";
     break;
     case "off" : 
      document.getElementById(ElementID).style.visibility = "hidden";
     break;
     default:
      document.getElementById(ElementID).style.visibility = "visible";
     break;
    }
   }

   function setValidField(ElementID){
    var myTarget     = document.getElementById(ElementID);
    var myParent     = myTarget.parentElement;
    var myValidation = document.getElementById(ElementID+"-validation");
    if(myValidation)
     myValidation.remove();

    myTarget.className = "form-control is-valid";
    myParent.insertAdjacentHTML("beforeEnd",'<div id="'+ElementID+'-validation" class="valid-feedback">Looks good!</div>');
   }

   function setInValidField(ElementID){
    var myTarget = document.getElementById(ElementID);
    var myParent = myTarget.parentElement;
    var myValidation = document.getElementById(ElementID+"-validation");
    if(myValidation)
     myValidation.remove();

    myTarget.className = "form-control is-invalid";
    myParent.insertAdjacentHTML("beforeEnd",'<div id="'+ElementID+'-validation" class="invalid-feedback">Issue!</div>');
   }

   function getInputHTML(options){
    var myInputHTML = "";
    
    var inputRequired = options.required ? "required" : "";
    
    switch(options.type){
     case "text" : 
      myInputHTML +=  '<div class="input-group-prepend">' + 
                       '<span class="input-group-text" id="'+options.name+'-icon">' + getIcon(options.icon) + '</span>' + 
                      '</div>' + 
                      '<input type="text" class="form-control" ' + 
                      'name="'+options.name+'"' + 
                      'id="'+options.id+'" ' + 
                      'placeholder="'+options.default+'" ' + 
                      'aria-label="'+options.default+'" ' + 
                      'aria-describedby="'+options.default+'" '+inputRequired+'>';
     break;
     case "combo" : 
      myInputHTML +=  '<div class="input-group-prepend">' + 
                       '<span class="input-group-text" id="'+options.name+'-icon">' + getIcon(options.icon) + '</span>' + 
                      '</div>' + 
                      '<input type="text" class="form-control" ' + 
                      'name="'+options.name+'"' + 
                      'id="'+options.id+'" ' + 
                      'placeholder="'+options.default+'" ' + 
                      'aria-label="'+options.default+'" ' + 
                      'aria-describedby="'+options.default+'" '+inputRequired+'>';
     break;
     case "textarea" : 
      myInputHTML += '<div class="input-group-prepend">' + 
                       '<span class="input-group-text" id="'+options.name+'-icon">' + getIcon(options.icon) + '</span>' + 
                      '</div>' + 
                      '<textarea class="form-control" ' + 
                      'name="'+options.name+'"' + 
                      'id="'+options.id+'" ' + 
                      'aria-label="'+options.default+'" ' + 
                      'aria-describedby="'+options.default+'" rows="3" '+inputRequired+'>'+options.default+'</textarea>';
     break;
     case "select" : 
      myInputHTML += '<div class="input-group-prepend">' + 
                       '<span class="input-group-text" id="'+options.name+'-icon">' + getIcon(options.icon) + '</span>' + 
                      '</div>' + 
                      '<select class="form-control" ' + 
                      'name="'+options.name+'"' + 
                      'id="'+options.id+'" ' + 
                      'aria-label="'+options.default+'" ' + 
                      'aria-describedby="'+options.default+'" '+inputRequired+'>';
      Object.keys(options.options).forEach((item)=>{
       if(item == options.default)
        myInputHTML += '<option value="'+item+'" selected>'+options.options[item]+'</option>';
       else
        myInputHTML += '<option value="'+item+'">'+options.options[item]+'</option>';
      });
      myInputHTML += '</select>';
     break;
     case "checkbox" : 
      var isChecked = options.default == "checked" ? "checked" : "";
      myInputHTML += '<div class="form-check">' + 
                      '<input class="form-check-input" ' + 
                       'type="checkbox" ' + 
                       'name="'+options.name+'"' + 
                       'id="'+options.id+'" '+isChecked+' '+inputRequired+'>' + 
                       '<label class="form-check-label" for="'+options.id+'">' + options.sign + '</label>' + 
                     '</div>';
     break;
     default : 
      myInputHTML +=  '<div class="input-group-prepend">' + 
                       '<span class="input-group-text" id="'+options.name+'-icon">' + getIcon(options.icon) + '</span>' + 
                      '</div>' + 
                      '<input type="text" class="form-control" ' + 
                      'name="'+options.name+'"' + 
                      'id="'+options.id+'" ' + 
                      'placeholder="'+options.default+'" ' + 
                      'aria-label="'+options.default+'" ' + 
                      'aria-describedby="'+options.default+'" '+inputRequired+'>';
     break;
    }

    return myInputHTML;

   }

   addInputs(formOptions.inputs);

   addSumbitButton();
}

function __amGrid(options){
 var myGrid;
 var myHeaderObj;
 var mySearchNavID;

 if(typeof options == 'undefined'){
  console.log("You need to provide settings.");
  return;
 }

 if(typeof options.processtype == 'undefined') 
  options.processtype = 'client';
 
// Reset DOM Elements
   document.getElementById(options.container).innerHTML = ""; 

   if(typeof options.columnsearch !== 'undefined'){
    mySearchNavID = options.uniqueId+'-search-nav';
    if(options.columnsearch.position){
     mySearchNavID = options.columnsearch.position;
     document.getElementById(mySearchNavID).innerHTML = "";
    }
    else{
     mySearchNavID = options.uniqueId+'-search-nav';
     document.getElementById(options.container).innerHTML = "<div id='"+mySearchNavID+"'></div>";
    }
   }  

   if(typeof options.tableActions !== 'undefined'){
    Object.keys(options.tableActions).forEach((item)=>{
     document.getElementById(options.tableActions[item].position).innerHTML = "";
    });
   }

   function findHeaderKeyFromIndex(needle , type){
    var myResponse = "";
    // myHeaderObj.forEach((header)=>{
     // if(type == "property"){
      // if( header[ Object.keys(header)[0] ] == needle){
       // myResponse = Object.keys(header)[0];
      // }
     // }
     // else{
      // if(myHeaderObj[needle])
       // myResponse = Object.keys(myHeaderObj[needle])[0];
      // else
       // myResponse = false;
     // }
    // });

    if(myHeaderObj[needle])
     myResponse = myHeaderObj[needle]["column"];
    else
     myResponse = false;    

    return myResponse;
   }

   function findHeaderIndexFromKey(needle , type){
    var myResponse = "";
    myHeaderObj.forEach((header , idx)=>{
     if( header["column"] == needle){
      myResponse = idx;
     }
    });
    // myHeaderObj.forEach((header , idx)=>{
     // if(type == "property"){
      // if( Object.keys(header)[0] == needle){
       // myResponse = idx;
      // }
     // }
     // else{
      // if(myHeaderObj[needle])
       // myResponse = idx;
      // else
       // myResponse = false;
     // }
    // });
    return myResponse;
   } 

   function getRowData(grid , domElement , headersArray){
     var myRowData     = grid.row( domElement ).data();
     var myHeaders     = new Array();
     var myRowObject   = new Object();

     myHeaderObj.forEach((header)=>{
      // myHeaders.push( Object.keys(headerOBJ)[0] );
      myHeaders.push( header["column"] );
     });

     myRowData.forEach((columnValue , columnIndex)=>{
      myRowObject[ myHeaders[columnIndex] ] = columnValue;
     });

     return myRowObject;
   }

   function setHeaders(container , actionColumn){
     myContainer = document.getElementById(container);
     var myDtResponsive = "";
     if(options.collapse)
      myDtResponsive = "dt-responsive";
     else
      myDtResponsive = "";
      
     myHeaders   = '<table id="'+container+'_tbl" class="table '+myDtResponsive+' table-light table-striped table-bordered" style="width:100%">' + 
                    '<thead class="thead-light">' + 
                     '<tr>';

     myHeaderObj.forEach((headerOBJ)=>{
      // myHeaders += "<th>" + headerOBJ[ Object.keys(headerOBJ)[0] ] + "</th>";
      myHeaders += "<th>" + headerOBJ["locale"] + "</th>";
     });

     if(typeof actionColumn !== 'undefined'){
      myHeaders += "<th>Actions</th>";
     }

     myHeaders +='</tr>' + '</thead>';

     myHeaders += '</table>';
     
     myHeaders += '<input type="hidden" id="'+container+'-selected" name="'+container+'-selected" ></input>';

     myContainer.insertAdjacentHTML("beforeEnd",myHeaders);
   }

   function addRecord(aForm , grid , headers){

     var myURI        = new Array();
     var myValues     = new Array();
     var myTempObject = new Object();

  // Create our URI with the pairs of Key-Values from the Form
     Object.keys(aForm).forEach((item)=>{
      myURI.push(item+"="+aForm[item]);
     });

     AJAX( "./dispatcher.php" , 
           "action=addRecord&" + myURI.join("&") , 
           (response) => {
            if(response.error){
             jQuery.prompt.close();
             jQuery.prompt(response.error);
            }
            else{
             myHeaderObj.forEach((headerOBJ)=>{
              var Header = headerOBJ["column"];

              if( Header == options.uniqueId ){
               myTempObject[ Header ] = response[options.uniqueId];
              }
              else{
               myTempObject[ Header ] = aForm[ Header ]; 
              }
             });

             myHeaderObj.forEach((headerOBJ)=>{
              var Header = headerOBJ["column"];
              var aValue = myTempObject[Header] || null;
              myValues.push( aValue );
             });

             var newNode = grid.row.add( myValues )
                 .draw( true )
                 .node();

             jQuery.prompt.close();
            }
           }
         );
   }

   function deleteRecord(recordID , grid , targetRow){

    AJAX( "./dispatcher.php" , 
          "action=deleteRecord&id=" + recordID , 
          (response) => {
           if(response.error){
            jQuery.prompt.close();
            jQuery.prompt(response.error);
           }
           else{
            var newNode = grid.row( targetRow )
                .remove( )
                .draw( true );

            jQuery.prompt.close();
           }
          }
        );
   }

   function addRow(grid , headers , exclude){

    var HTML = "<table>";

    myHeaderObj.forEach((headerOBJ)=>{
     // var Header = Object.keys(headerOBJ)[0];
     // var Locale = headerOBJ[ Object.keys(headerOBJ)[0] ];
     var Header = headerOBJ["column"];
     var Locale = headerOBJ["locale"];
     if(!exclude.includes(Header)){
      HTML += "<tr>";
       HTML += "<td>" + Locale + " </td>";
       HTML += "<td><input type='text' name='"+Header+"'></input></td>";
      HTML += "</tr>";
     }

    });
    HTML += "</table>";

    jQuery.prompt({
     addRecord: {
      html:HTML,
      buttons: { Cancel: false, Add: true },
      focus: 1,
      submit:function(e,v,m,f){
       if(v){
        e.preventDefault();
        addRecord(f , grid , headers);
        return false;
       }
       $.prompt.close();
      }
     }
    });
   }

 AJAX(options.dispatcherPath , 
  "action=headers&processtype=" + options.processtype , 
  (headers)=>{
    var myGridOptions = {};
        myHeaderObj   = headers;
        myDataPeriod  = false;

    if(options.height){
     myGridOptions.scrollY        = options.height;
     myGridOptions.scrollCollapse = false;
    }

    myGridOptions.scrollCollapse = false;

    /* Adding the Headers First */
    setHeaders(options.container , options.rowButtons);

    /* AJAX Request EndPoint and Custom Values */
    myGridOptions.ajax = {
     "url" : options.dispatcherPath,
      "data" : function ( form ) {
          form.processtype = options.processtype;
          form.period      = myDataPeriod;
      }
    }

    /* The Grid should be Responsive (Upon collapse it creates an expandable Sub Tree */
    if(options.collapse)
     myGridOptions.responsive = true;

    if(options.rowselect)
     myGridOptions.select = {
             toggleable: false,
             blurable: true
         };

    /* Add Export Controls */
    if(options.exports){
     myGridOptions.dom          = 'Bfrtip';
     myGridOptions.buttons      = options.exports;
    }    

    /* 
      We either have Pagination or Scrolling.
      Scrolling is not supported in Server Side Processing.
      Server Side Processing is done to minimize overhead. 
      When you use Scroll you are need to send all data to the client.
      So there is no gain in using Server Side Processing.
    */
    if(options.scrollable && options.processtype != "server"){
     myGridOptions.scrollY         = options.height;
     myGridOptions.scrollX         = true;
     myGridOptions.scrollCollapse  = true;
     myGridOptions.fixedColumns    = { heightMatch: 'none' };
     myGridOptions.paging          = false;
    }

    /* Assign our Server Side Processing Properties */
    if(options.processtype == "server"){
         myGridOptions.processing  = true;
         myGridOptions.deferRender = true;
         myGridOptions.serverSide  = true;
         myGridOptions.pagingType  = "simple";
         myGridOptions.info        = false;
    }

    if(options.rowButtons){
     myGridOptions.columnDefs = new Array();
     var myButtons = "";
     Object.keys(options.rowButtons).forEach((btnKey)=>{
      myButtons += "<button class='"+btnKey+"_grid_btn'>"+options.rowButtons[btnKey].name+"</button>"
     });

      myGridOptions.columnDefs.push(
       {
        "targets"       : -1,
        "data"          : null,
        "defaultContent": myButtons
       }
      );
    }

 // Create our Custom Search Boxes in the DOM
    if(options.columnsearch){
     if(options.columnsearch.columns){
      var mySearchBoxesHTML = '<div class="input-group mb-3">';
       myHeaderObj.forEach((headerOBJ)=>{
        // var TempTitleName  = Object.keys(headerOBJ)[0];
        // var TempFieldType  = options.columnsearch.columns[TempTitleName];
        var TempTitleName  = headerOBJ["column"];
        var TempFieldType  = options.columnsearch.columns[TempTitleName];
        if(options.columnsearch.columns[ TempTitleName ]){
         if(TempFieldType == "date" ){
          mySearchBoxesHTML += '<div class="input-group-prepend">' + 
           '<span class="input-group-text" id="'+TempTitleName+'">' + 
            '<svg class="bi bi-calendar" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
             '<path fill-rule="evenodd" d="M14 0H2a2 2 0 00-2 2v12a2 2 0 002 2h12a2 2 0 002-2V2a2 2 0 00-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z" clip-rule="evenodd"/>' + 
             '<path fill-rule="evenodd" d="M6.5 7a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd"/>' + 
            '</svg>' + 
           '</span>' + 
          '</div>' + 
          '<input type="text" class="form-control datetimepicker-time" ' + 
           'id="'+options.container+'_tbl-'+TempTitleName+'-date" ' + 
           'placeholder="'+TempTitleName+'" aria-label="'+TempTitleName+'" aria-describedby="'+TempTitleName+'">';
         }
         else if(TempFieldType == "datetime" ){
          mySearchBoxesHTML += '<div class="input-group-prepend">' + 
           '<span class="input-group-text" id="'+TempTitleName+'">' + 
            '<svg class="bi bi-calendar" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
             '<path fill-rule="evenodd" d="M14 0H2a2 2 0 00-2 2v12a2 2 0 002 2h12a2 2 0 002-2V2a2 2 0 00-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z" clip-rule="evenodd"/>' + 
             '<path fill-rule="evenodd" d="M6.5 7a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd"/>' + 
            '</svg>' + 
           '</span>' + 
          '</div>' + 
          '<input type="text" class="form-control datetimepicker-datetime" ' + 
           'id="'+options.container+'_tbl-'+TempTitleName+'-date" ' + 
           'placeholder="'+TempTitleName+'" aria-label="'+TempTitleName+'" aria-describedby="'+TempTitleName+'">';
         }
         else if(
          TempFieldType == "period"
         ){
          mySearchBoxesHTML += '<div class="input-group-prepend">' + 
           '<span class="input-group-text" id="'+TempTitleName+'">' + 
            '<svg class="bi bi-calendar" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
             '<path fill-rule="evenodd" d="M14 0H2a2 2 0 00-2 2v12a2 2 0 002 2h12a2 2 0 002-2V2a2 2 0 00-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z" clip-rule="evenodd"/>' + 
             '<path fill-rule="evenodd" d="M6.5 7a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd"/>' + 
            '</svg>' + 
           '</span>' + 
          '</div>' + 
          '<input type="text" class="form-control datetimepicker-time" ' + 
           'id="'+options.container+'_tbl-'+TempTitleName+'-start-date" ' + 
           'placeholder="'+TempTitleName+'" aria-label="'+TempTitleName+'" aria-describedby="'+TempTitleName+'" value="From">' + 
          '<div class="input-group-prepend">' + 
           '<span class="input-group-text" id="'+TempTitleName+'">' + 
            '<svg class="bi bi-calendar" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
             '<path fill-rule="evenodd" d="M14 0H2a2 2 0 00-2 2v12a2 2 0 002 2h12a2 2 0 002-2V2a2 2 0 00-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z" clip-rule="evenodd"/>' + 
             '<path fill-rule="evenodd" d="M6.5 7a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd"/>' + 
            '</svg>' +             
           '</span>' + 
          '</div>' + 
          '<input type="text" class="form-control datetimepicker-time" ' + 
           'id="'+options.container+'_tbl-'+TempTitleName+'-end-date" ' + 
           'placeholder="'+TempTitleName+'" aria-label="'+TempTitleName+'" aria-describedby="'+TempTitleName+'" value="To">';
         }
         else
         {
         mySearchBoxesHTML += '<div class="input-group-prepend">' + 
          '<span class="input-group-text" id="basic-addon1">'+
           '<svg class="bi bi-card-text" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
             '<path fill-rule="evenodd" d="M14.5 3h-13a.5.5 0 00-.5.5v9a.5.5 0 00.5.5h13a.5.5 0 00.5-.5v-9a.5.5 0 00-.5-.5zm-13-1A1.5 1.5 0 000 3.5v9A1.5 1.5 0 001.5 14h13a1.5 1.5 0 001.5-1.5v-9A1.5 1.5 0 0014.5 2h-13z" clip-rule="evenodd"/>' + 
             '<path fill-rule="evenodd" d="M3 5.5a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9a.5.5 0 01-.5-.5zM3 8a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9A.5.5 0 013 8zm0 2.5a.5.5 0 01.5-.5h6a.5.5 0 010 1h-6a.5.5 0 01-.5-.5z" clip-rule="evenodd"/>' + 
           '</svg>' + 
          '</span>' + 
         '</div>' + 
         '<input type="text" class="form-control" ' + 
          'id="'+options.container+'_tbl-'+TempTitleName+'" ' + 
          'placeholder="'+TempTitleName+'" aria-label="'+TempTitleName+'" aria-describedby="'+TempTitleName+'">';
         }        
        }
       });
       mySearchBoxesHTML += '</div>';

       document.getElementById(mySearchNavID).insertAdjacentHTML( 
        "afterBegin", 
        mySearchBoxesHTML
       );
     }
    }

 // Hide columns not set for display
    if(options.columnsDisplay){
     if(typeof myGridOptions.columnDefs === "undefined"){
      myGridOptions.columnDefs = new Array();
     }

     myHeaderObj.forEach((header , idx)=>{
      if(!options.columnsDisplay.includes(header["column"])){
        myGridOptions.columnDefs.push({
         "targets"    : [idx],
         "visible"    : false,
         "searchable" : false
        });
       }
     });
    }

    // myGridOptions.pageLength = 5;
    // myGridOptions.width = '100%';

    /* Initiate our Grid */
    myGrid = $('#'+options.container+'_tbl')
             .DataTable( myGridOptions )
             .columns.adjust()
             .responsive.recalc();

 // Assign Event Listeners to our previously created Custom Search Boxes now that the Grid has been initialized
    if(options.columnsearch){
     if(options.columnsearch.columns){

   // If a custom Search Box has been set in the Configuration 
   // then proceed in adding an Event Listener on it.
      $('#'+mySearchNavID+' input').each( function (idx , el) {
       var FieldTargetName  = el.id.replace(options.container+'_tbl-',"")
                                    .replace("-date","")
                                    .replace("-start-date","")
                                    .replace("-start","")
                                    .replace("-end","")
                                    .replace("-end-date","");
       var FieldTargetIndex = findHeaderIndexFromKey(FieldTargetName , "property");

       if(options.columnsearch.columns[ FieldTargetName ] != "period"){
        $(this).on( 'keyup change clear', function (event) {
         myGrid.columns(FieldTargetIndex).search( event.target.value ).draw();
        });
       }
      });

   // Insert our DateTime Listeners so that if a Field Type is set to Date an appropriate Calendar will popup
      myGrid.columns().every( function (idx) {
       var that       = this;
       var ColumnType = options.columnsearch.columns[ findHeaderKeyFromIndex(idx) ];
       var myInputID  = options.container+'_tbl-'+findHeaderKeyFromIndex(idx);

       if(findHeaderKeyFromIndex(idx)){
        if( ColumnType == "date" ){
         
         jQuery('#'+myInputID+'-date')
          .datepicker(
           { 'format': 'yyyy-mm-dd' , 'todayHighlight': true, 'autoclose': true }
          );
        }
        else if( ColumnType == "datetime"){
         
         jQuery('#'+myInputID+'-date')
          .datepicker(
           { 'format': 'yyyy-mm-dd' , 'todayHighlight': true, 'autoclose': true }
          );
        }
        else if( ColumnType == "period"){

         
         jQuery('#'+myInputID+'-start-date')
          .datepicker( 
           { 'format': 'yyyy-mm-dd' , 'todayHighlight': true, 'autoclose': true }
          );

         jQuery('#'+myInputID+'-end-date')
          .datepicker( 
           { 'format': 'yyyy-mm-dd' , 'todayHighlight': true, 'autoclose': true }
          );
        }
       }

       if( ColumnType == "period"){

         var StartDateElement = options.container+'_tbl-'+findHeaderKeyFromIndex(idx)+'-start-date';
         var EndDateElement   = options.container+'_tbl-'+findHeaderKeyFromIndex(idx)+'-end-date';

         $.fn.dataTable.ext.search.push(
          function( settings, data, dataIndex ) {
           var min = Date.parse($('#'+StartDateElement).val()) / 1000;
           var max = Date.parse($('#'+EndDateElement).val()) / 1000;
           var cdt = Date.parse(data[idx]) / 1000;

           if ( ( isNaN( min ) && isNaN( max ) ) ||
                ( isNaN( min ) && cdt <= max )   ||
                ( min <= cdt   && isNaN( max ) ) ||
                ( min <= cdt   && cdt <= max ) )
           {
               return true;
           }
           return false;
          }
         );        

         $( '#'+ StartDateElement)
          .on( "change" , (event)=>{
            document.getElementById( StartDateElement ).blur();
           });

         $( '#'+ EndDateElement)
          .on( "change" , (event)=>{
            document.getElementById( EndDateElement ).blur();
            var myStartDate   = document.getElementById( StartDateElement ).value;
            var myEndDate     = document.getElementById( EndDateElement ).value;
            myDataPeriod      = { 
             "column" : findHeaderKeyFromIndex(idx) , 
             "range" : [myStartDate,myEndDate] 
            };
            myGrid.draw();
          });
       }
      } );
     }
    }

    if(options.rowEvent){
     $('#'+options.container+'_tbl tbody').on('click', 'tr', function (event) {
        // event.preventDefault();
        // event.stopPropagation();

        var myRowObject  = getRowData(myGrid , this , headers);
        var myInputArray = new Array();

        if(options.rowselect){
         myGrid.rows('.selected').deselect();

         myInputArray = new Array();

         Object.keys(myRowObject).forEach((property)=>{
          myInputArray.push(property+"|"+myRowObject[property]);
         });

         document.getElementById(options.container+'-selected').value = myInputArray.join(",");
        }

        if(typeof options.rowEvent.fn === 'function'){
         options.rowEvent.fn({
          "event": event,
          "grid" : myGrid , 
          "data" : myRowObject
         });
        }
        else{
         console.log("No function assigned on Row Event");
        }
     } );
    }

    if(options.fieldEvent){
     $('#'+options.container+'_tbl tbody').on( 'click', 'td', function (event) {
        event.preventDefault();
        event.stopPropagation();

        var myColumnIndex = myGrid.cell(this).index().column;
        var myColumnName  = headers[myColumnIndex].replace("_","");
        var myFieldData   = myGrid.cell( this ).data();
        var myRowObject   = getRowData(myGrid , this , headers);

        if(options.fieldEvent[myColumnName]){
         if(typeof options.fieldEvent[myColumnName].fn === 'function'){
          options.fieldEvent[myColumnName].fn({
           "colName"  : myColumnName,
           "fldValue" : myFieldData,
           "rowData"  : myRowObject,
           "grid"     : myGrid
          });
         }
         else{
          console.log("No function assigned on Row Event");
         }
        }

     } );
    }

    if(options.rowButtons){
     $('#'+options.container+'_tbl tbody').on( 'click', 'button', function (event) {
         event.preventDefault();
         event.stopPropagation();

         var myRowObject    = getRowData(myGrid , $(this).parents('tr') , headers);
         var myButtonObject = options.rowButtons[this.className.replace("_grid_btn","")];
         var myButtonAction = options.rowButtons[this.className.replace("_grid_btn","")].name;
         if(typeof myButtonObject.fn === 'function'){
          myButtonObject.fn(myGrid , myRowObject);
         }
         else{
           if(myButtonAction == "deleteRow"){
            deleteRecord(myRowObject[options.uniqueId] , myGrid , $(this).parents('tr') );
           }
           else{
            console.log("No function assigned on Button");
           }
         }
     } );
    }

    if(options.tableActions){

     Object.keys(options.tableActions).forEach((tblAction)=>{
      var ActionLocale = options.tableActions[tblAction].name;
      var ActionName   = tblAction;
      var ActionFN     = options.tableActions[tblAction].fn;
      var myContainer  = document.getElementById(options.tableActions[tblAction].position);

      myContainer.insertAdjacentHTML(
       "beforeEnd" , 
       "<input type='button' class='btn btn-dark btn-sm' id='"+tblAction+"_GridBtn' value='"+ActionLocale+"'></input>"
      );

      $('#'+tblAction+'_GridBtn').on( 'click', function (event) {
        event.preventDefault();
        event.stopPropagation();

        if(typeof ActionFN === 'function'){
         ActionFN(myGrid);
        }
        else{
         if(tblAction == "addRow"){
          console.log("no fn");
          addRow(myGrid , headers , options.tableActions["addRow"].exclude);
         }
         else if(tblAction == "addRow"){
          console.log("custom fn present");
         }

         console.log("No function assigned on Button");
        }
      } );

     });
    }

  }
 );

}

function __getFormInputs(containerID){
 return document.querySelectorAll("#"+containerID+" [name]");
}

function getDate( timestamp , time ){
  let cdts       = new Date( timestamp );
  let curMonth   = ( cdts.getUTCMonth()   + 1  ) < 10 ? "0" + ( cdts.getUTCMonth() + 1)  : cdts.getUTCMonth() + 1;
  let curSeconds = ( cdts.getUTCSeconds() < 10 )      ? "0" +   cdts.getUTCSeconds()     : cdts.getUTCSeconds();
  let curMinutes = ( cdts.getUTCMinutes() < 10 )      ? "0" +   cdts.getUTCMinutes()     : cdts.getUTCMinutes();
  let curHours   = ( cdts.getUTCHours()   < 10 )      ? "0" +   cdts.getUTCHours()       : cdts.getUTCHours();
  let curDayDate = ( cdts.getUTCDate()    < 10 )      ? "0" +   cdts.getUTCDate()        : cdts.getUTCDate();
  let curDate    = "";

  if( typeof time !== 'undefined' )
    curDate    = cdts.getFullYear() +"-"+ curMonth +"-"+ curDayDate +" "+ curHours +":"+ curMinutes +":"+ curSeconds;
  else
    curDate    = cdts.getFullYear() +"-"+ curMonth +"-"+ curDayDate;

  return curDate;
}