function amAutoComplete( value , targetNode , myOptions ){
  let cntID          = "amAutoComplete4j";
  let myElements     = getContainer( cntID );
  let container      = myElements["container"];
  let overlay        = myElements["overlay"];
  let minLen         = myOptions.minLen || 0;
  let myList;

  function randomUID(length){
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
  }

  function setContainerStyle( node ){
   node.style.position        = "absolute";
   node.style.maxHeight       = "200px";
   node.style.overflow        = "auto";
   node.style.backgroundColor = "#0000001f";
   node.style.display         = "none";
   node.style.zIndex          = "100000";
   node.style.display         = "block";
  }

  function hideNode( node ){
   node.style.display = "none";
  }

  function showNode( node ){
   node.style.display = "block";
  }

  function getContainer( cntID ){
   if( !document.getElementById( cntID+"_overlay" ) ){
    document.body.insertAdjacentHTML(
     "afterBegin" , 
     "<div style='z-index:10000;position:absolute;width:100%;height:100%;background-color:rgba(0,0,0,0.4);display:block;' id='"+cntID+"_overlay'>" + 
       "<div id='"+cntID+"'></div>" + 
     "</div>"
    );
   }
   
   document
    .getElementById( cntID+"_overlay" )
    .addEventListener( "click" , ( event ) => {
      document.getElementById( cntID+"_overlay" ).style.display = "none";
    });

   return { 
    "container" : document.getElementById( cntID ),
    "overlay"   : document.getElementById( cntID+"_overlay" )
   }
  }

  function setContainerAnchor( container , target ){
   let parentStyle = target.getBoundingClientRect();
   container.style.top             = ( parentStyle.top + parentStyle.height ) + "px";
   container.style.left            = parentStyle.left;
   container.style.width           = parentStyle.width;
   container.style.minHeight       = parentStyle.height;
  }

  function getContainerListNode( container ){
   let myListNode = document.getElementById("autocomplete-list");

   if( myListNode )
    myListNode.remove();

   container.insertAdjacentHTML( "afterBegin" , '<div class="list-group" id="autocomplete-list"></div>' );
   
   return document.getElementById("autocomplete-list");
  }

  if( value.length <= minLen ){
   hideNode( overlay );
   return;
  }
  else
   showNode( overlay );

  setContainerStyle( container );

  setContainerAnchor( container , targetNode );

  myList = getContainerListNode( container );

  fetch( myOptions.endpoint+value , {
     method : 'get'
    })
    .then( response => response.json() )
    .then((data) => {
      let myPath = typeof myOptions.path   !== 'undefined' ? data[ myOptions.path ] : data;
      let myProp = typeof myOptions.return !== 'undefined' ? myOptions.return : false;

      if( myPath.length <= 0 ){
       hideNode( overlay );
      }
      else{
       myPath.forEach( ( item ) => {
        let nodeID = randomUID(5);
        myList.insertAdjacentHTML( 
         "beforeEnd" , 
         '<a href="#" id="'+nodeID+'_item_cpl" class="list-group-item list-group-item-action">'+item[myProp]+'</a>' 
        );
        document
         .getElementById(nodeID+'_item_cpl')
         .addEventListener( "click" , ( event ) => {
           event.preventDefault();
           event.stopPropagation();
           targetNode.value = item[myProp];
           hideNode( overlay );
           if( typeof myOptions.callback === "function" ){
             myOptions.callback( item );
           }
         });
       });
      }
    })
    .catch((error) => {
     console.error('Error:', error);
    });
}

function setAmAutoComplete( options ){
  let selector = options.selector || ".amAutoComplete";
  let nodeList = document.querySelectorAll( selector );
  let myTarget = document.getElementById( options.target );

  let myOptions = {
    "endpoint" : ( options.url      || false ),
    "path"     : ( options.path     || false ),
    "return"   : ( options.return   || false ),
    "minLen"   : ( options.minLen   || 0 ),
    "callback" : ( options.callback || false )
  }

  myTarget.addEventListener( "keyup" , ( event ) => {
   event.preventDefault();
   amAutoComplete( event.target.value , myTarget , myOptions );
  });

}