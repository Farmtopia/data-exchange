class Datasources{

  constructor(){
   
    this.container;
    this.grid;
    this.selectedDataID;
    this.compareObject = {};
  }

  showDatasourceCompareContainer(){
    let myCompareContainer = document.getElementById( "datasource-compare-container" );
    let myWindow = document.getElementById( "nav-datasources" );
    let myList   = [];
    let self = this;
    Object.keys( this.compareObject ).forEach( ( datasourceName ) => {
      myList.push( "<tr>" + "<td>" + datasourceName  + "</td><td>" + this.compareObject[ datasourceName ][ "datasource_id" ] + "</td></tr>" );
    });
    let style = "right: 30px; top: 350px; overflow: auto; position: fixed; width: 400px; height: 150px; background-color: #79b5f5; border: 1px solid #00000040; z-index: 50;";
    if( !myCompareContainer ){
      myWindow.insertAdjacentHTML( "afterBegin" , "<div style='" + style + "'>" + 
                                                    "<div id='datasource-compare-container'>" + 
                                                      "<div class='container'>" + 
                                                        "<div class='row'>" + 
                                                          "<div class='col-8' id='datasource-compare-list-table'>" + 
                                                            "<table class='table'>" + 
                                                              "<thead>" + 
                                                                "<th>Name</td>" + 
                                                                "<th>ID</td>" + 
                                                              "</thead>" + 
                                                              "<tbody id='datasource-compare-list'>" + 
                                                                myList.join( "" ) + 
                                                              "</tbody>" + 
                                                            "</table>" + 
                                                          "</div>" + 
                                                          "<div class='col-4'>" + 
                                                            "<button type='button' class='btn-sm btn-primary' id='datasource-compare-button'>Compare</button" + 
                                                          "</div>" + 
                                                        "</div>" + 
                                                      "</div>" + 
                                                    "</div>" + 
                                                  "</div>" );
      myCompareContainer = document.getElementById( "datasource-compare-container" );
      myCompareContainer.display = "block";
      document.getElementById( "datasource-compare-button" )
              .addEventListener( "click" , ( event ) => {
                self.showCompareResults();
              });
    }
    else{
      document.getElementById( "datasource-compare-list" ).innerHTML = myList.join( "" );
    }
  }

  showCompareResults(){
    console.log( this.compareObject );
    
    this.response = [];
    let self = this;
    this.getDataFromDatasource(
          self.compareObject[ Object.keys( self.compareObject )[ 0 ] ][ "endpoint" ] , 
          self.compareObject[ Object.keys( self.compareObject )[ 0 ] ][ "datasource_id" ] 
        )
        .then( ( response1 ) => {
          self.response.push( response1 );
          return self.getDataFromDatasource( 
            self.compareObject[ Object.keys( self.compareObject )[ 1 ] ][ "endpoint" ] , 
            self.compareObject[ Object.keys( self.compareObject )[ 1 ] ][ "datasource_id" ] 
          );
        })
        .then( ( response2 ) => {
          self.response.push( response2 );
          console.log( self.response );
          getModal( {
            "title" : "whatever" , 
            "body"  : "<div id='presentation-of-data'></div>"
          } , () => {
            let mySeries = [];

                mySeries.push( {
                  data    : self.response[ 0 ][ "data" ] , 
                  name    : Object.keys( self.compareObject )[ 0 ] , 
                  tooltip : {
                      valueDecimals: 2
                  }
                } );

                mySeries.push( {
                  data    : self.response[ 1 ][ "data" ] , 
                  name    : Object.keys( self.compareObject )[ 1 ] , 
                  tooltip : {
                      valueDecimals: 2
                  }
                } );

            Highcharts.stockChart('presentation-of-data', {
                rangeSelector : { selected: 1 },
                legend        : { enabled: true  },
                title         : { text: "something" },
                xAxis         : { type: 'datetime' },
                series        : mySeries
            });
          });
        });

  }

  addDatasourceInCompareObject( datasourceName , datasourceData , endpoint ){
    this.compareObject[ datasourceName ] = { 
      "data" : [],
      "datasource_id" : datasourceData , 
      "endpoint" : endpoint 
    }
    console.log( this.compareObject );
    this.showDatasourceCompareContainer();
  }

  setContainer( container ){
    this.container = container;
  }

  getContainer(){
    return this.container;
  }

  getDataSourcesContainer(){
    return '<div class="row">' + 
             '<div class="col-12">' + 
               '<div id="datasource-table-container">' + 
               '</div>' + 
             '</div>' + 
           '</div>';
  }

  getToolsContainer(){
    return '<div class="row">' + 
             '<div class="col-12">' + 
               '<div id="datasource-tools-container">' + 
               '</div>' + 
             '</div>' + 
           '</div>';
  }

  getSubscriptionJWTContainer( response ){
    console.log( response );
    let myContainer = document.getElementById( "datasources-form-sub" );
    let self = this;
    let consumer_access_token = "";
    if( response.hasOwnProperty( "token" ) ){
      consumer_access_token = response[ "token" ];
    }
    myContainer.insertAdjacentHTML( "afterBegin" , '<div class="row">' + 
             '<div class="col-12">' + 
               '<div id="datasource-subscription-jwt-container">' + 
                 "<div class='row'>" + 
                   "<div class='col-12' style='padding: 0rem 2.5rem 0.0rem 2.5rem !important;'>" + 
                    '<form>' + 
                      '<div class="mb-3">' + 
                        '<label for="client-token-to-provider" class="form-label">JWT Received from Directory Listing for use with the Provider. It will be used for the subsequent requests to the specific Provider for Data retrieval.</label>' + 
                        '<textarea type="text" class="form-control" id="client-token-to-provider" aria-describedby="client-token-to-provider" readonly>' + consumer_access_token + '</textarea>' + 
                      '</div>' + 
                    '</form>' + 
                   "</div>" + 
                 "</div>" + 
               '</div>' + 
             '</div>' + 
           '</div>');


  }

  getAuthForm( username , password ){
    return '<form>' + 
             '<div class="row">' + 
               '<div class="col-12">' + 
                 '<h3>Farmtopia Datasharing Client Module</h3>' + 
                 '<hr>' + 
               '</div>' + 
             '</div>' + 
             '<div class="row">' + 
               '<div class="col-5">' + 
                 '<div data-mdb-input-init class="form-outline mb-4">' + 
                   '<label class="form-label" for="source-email">Email</label>' + 
                   '<input type="email" id="source-email" class="form-control" value="'+username+'"/>' + 
                 '</div>' + 
               '</div>' + 
               '<div class="col-5">' + 
                 '<div data-mdb-input-init class="form-outline mb-4">' + 
                   '<label class="form-label" for="source-password">Password</label>' + 
                   '<input type="password" id="source-password" class="form-control" value="'+password+'"/>' + 
                 '</div>' + 
               '</div>' + 
               '<div class="col-2">' + 
                 '<button type="button" class="btn btn-link btn-teal" style="height: 70px;" id="ds-submit-credentials">Not Connected</button>' + 
               '</div>' + 
             '</div>' + 
             '<div class="row">' + 
               '<div class="col-12">' + 
                 'Please fill your "Farmtopia Data Sources Directory" Credentials' + 
               '</div>' + 
             '</div>' + 
           '</form>' + 
           this.getToolsContainer() + 
           this.getDataSourcesContainer();
  }

  show( rowObject , evt ){
    console.log( rowObject );
  }

  addDatasourceTable( containerID ){
     let self = this;
     this.grid = new amGrid( {
      "container"      : containerID, 
      "uniqueRowId"    : containerID + "_id",
      "uniqueGridId"   : containerID + "-view",
      "columnsDisplay" : [ "name" , "service" , "swagger" ] , 
      "dispatcherPath" : "../ws/ws.grid.php", 
      "customAJAX"     : { "entity" : "datasources" , "action" : "getDatasources" },
      "processtype"    : "client",
      "orderby"        : { "column" : 0 , "dir" : "desc" } , 
      "searchBox"      : true,
      "CRUD"           : false,
      "infoBox"        : false,
      "rowselect"      : "single",
      "scrollable"     : false,
      "height"         : ( getViewDimensions("height") / 4 ) + "px",
      "collapse"       : true,
      /*
      "rowButtons"     : {
        "btn-toggle-station" : {
          // "name" : "toggle",
          // "icon" : '<i style="font-size:1rem" class="fa-solid fa-plus"></i>',
          "name" : "subscribe",
          "className" : "grid-override",
          "fn"   : ( event , rowObject ) => { 
            self.show( rowObject , event );
          }
        }
      },
      */
      "rowEvent" : ( rowEventObject ) => {
        self.selectedDataID = rowEventObject[ "data" ][ "id" ];
        self.getSubscriptionStatusForDatasource( rowEventObject[ "data" ] )
            .then( ( response ) => {
                 self.showDatasourceDetails( rowEventObject , response[ "res" ] );
                 self.getSubscriptionJWTContainer( response );
            })
            .catch( ( error ) => {
              if( error != "Could not authenticate with directory. Connect again." ){
                self.showDatasourceDetails( rowEventObject , false , false );
              }

              getAlert( error );
              console.log( error );
            });
      },
      "onrender" : ( grid ) => {
        this.grid.activateRow( 0 );
        this.grid.selectRow( 0 );
      }
     } );
  }

  updateToolsHTML( containerID , data ){
     console.log( data );
     let myContainer = document.getElementById( containerID );
     myContainer.innerHTML = '<form>' + 
                              '<div class="mb-3">' + 
                                '<label for="client-token" class="form-label">JWT Received from Directory Listing. It will be used for the subsequent request to the Directory.</label>' + 
                                '<textarea type="text" class="form-control" id="client-token" aria-describedby="client-token" readonly>' + data.data.access_token + '</textarea>' + 
                              '</div>' + 
                             '</form>';
  }

  getSubscriptionStatusForDatasource( datasource ){
    return AJAX( "../ws/ws.generic.php" , {
      "action"        : "getSubscriptionStatusForDatasource" , 
      "datasource_id" : datasource[ "id" ]
    });
  }

  updateSubscriptionToken( datasource_id ){
    return AJAX( "../ws/ws.generic.php" , {
      "action"        : "updateSubscriptionToken" , 
      "datasource_id" : datasource_id
    });
  }

  showDatasourceDetails( rowEventObject , subscriptionStatus , connectionWithProvider = true ){
    let myContainer = document.getElementById( "datasources-form-sub" );
    let myDetails   = [];
    let self = this;

    myDetails.push( "<div class='row'>" );
    Object.keys( rowEventObject[ "data" ] ).forEach( ( property , index ) => {
     if( ( index + 1 ) % 2 ) myDetails.push( "</div><div class='row'>" );

     myDetails.push( 
       '<div class="col-sm" style="padding: 0rem 2.5rem 0.0rem 2.5rem !important;" id="datasource-details-container" >' + 
         '<div class="form-group">' + 
           '<label for="datasource-' + property + '">' + lang( property ) + '</label>' + 
           '<input type="text" class="form-control" id="datasource-' + property + '" aria-describedby="" placeholder="" value="'+rowEventObject[ "data" ][ property ]+'" readonly>' + 
         '</div>' + 
       '</div>'
     );
    });
    myDetails.push( "</div>" );

    let myRandomButton = "";
    if( subscriptionStatus == "subscribed" )
      myRandomButton = '<div class="row"><div class="col-12" style="text-align: center;"><button class="btn btn-primary" id="update-token-for-dataset">Update Subscription Token</button></div></div>';

    myContainer.innerHTML = myRandomButton + myDetails.join( " " ) + this.getSubscribeContainer( subscriptionStatus , rowEventObject[ "data" ] , connectionWithProvider );

    this.assignListenerForSubscription( rowEventObject[ "data" ] );
    if( document.getElementById( 'update-token-for-dataset' ) ){
      document.getElementById( 'update-token-for-dataset' )
              .addEventListener( "click" , ( event ) => {
                self.updateSubscriptionToken( rowEventObject[ "data" ][ "id" ] )
                    .then( ( response ) => {
                      let myToken = response[ "res" ][ "data" ][ "token" ];
                      console.log( response );
                      document.getElementById( "client-token-to-provider" ).value = myToken;
                    });
              });
    }
  }

  assignListenerForSubscription( datasource ){
    console.log( datasource );
    let self = this;
    let mySubscribeButton               = document.getElementById( "ds-subscribe-to-datasource-" + datasource[ "id" ] );
    let myButtonToGetDataFromDS         = document.getElementById( "ds-get-data-from-datasource-" + datasource[ "id" ] );
    let myButtonToGetDataFromDSCalendar = document.getElementById( "ds-get-data-from-datasource-calendar-" + datasource[ "id" ] );
    let myButtonToAddToComparison       = document.getElementById( "ds-add-data-for-comparison-" + datasource[ "id" ] );
    
    mySubscribeButton.addEventListener( "click" , ( event ) => {
      self.subscribeToDataset( datasource[ "id" ] )
          .then( ( response ) => {
            if(
                response.hasOwnProperty( "res" ) 
                && response[ "res" ].hasOwnProperty( "data" ) 
                && response[ "res" ][ "data" ].hasOwnProperty( "res" ) 
                && response[ "res" ][ "data" ][ "res" ] == "Request has been submitted for evaluation. Provider will send email when evaluation has been completed."
              )
              {
                mySubscribeButton.classList.remove('btn-green');
                mySubscribeButton.classList.remove('btn-red');
                mySubscribeButton.classList.add('btn-teal');
                mySubscribeButton.innerText = "Pending acceptance from Provider";
              }
          });
    });

    if( myButtonToAddToComparison ){
      myButtonToAddToComparison.addEventListener( "click" , ( event ) => {
        self.addDatasourceInCompareObject( datasource[ "name" ] , datasource[ "id" ] , datasource[ "service" ] );
      });
    }

    if( myButtonToGetDataFromDSCalendar ){
      myButtonToGetDataFromDSCalendar.addEventListener( "click" , ( event ) => {
        self.getDataFromDatasourceCalendar( datasource[ "service" ] , datasource[ "id" ] )
            .then( ( response ) => {
                  console.log( response );
                  myParsedResponse = self.parseJSONLD( response );
                  console.log( myParsedResponse );
                  getModal( {
                    "title" : "whatever" , 
                    "body"  : "<div id='presentation-of-data'></div>"
                  } , () => {
                    let mySeries = [];

                        mySeries.push( {
                          data    : response , 
                          name    : "test" , 
                          tooltip : {
                              valueDecimals: 2
                          }
                        } );

                    Highcharts.stockChart('presentation-of-data', {
                        rangeSelector : { selected: 1 },
                        legend        : { enabled: true  },
                        title         : { text: "something" },
                        xAxis         : { type: 'datetime' },
                        series        : mySeries
                    });
                  });
            })
            .catch( ( error ) => {
              console.log( error );
            });
      });
    }

    if( myButtonToGetDataFromDS ){
      myButtonToGetDataFromDS.addEventListener( "click" , ( event ) => {
        self.getDataFromDatasource( datasource[ "service" ] , datasource[ "id" ] )
            .then( ( response ) => {
                  console.log( response );
                  if( response.hasOwnProperty( "error" ) ){
                    getModal( {
                      "title" : "Warning" , 
                      "body"  : "<p>" + response.error + "</p>"
                    });
                  }
                  else{
                    getModal( {
                      "title" : "whatever" , 
                      "body"  : "<div id='presentation-of-data'></div><div id='presentation-jsonld'></div>"
                    } , () => 
                    {

                      document.getElementById( "presentation-jsonld" )
                              .innerHTML = "<div class='row'>" + 
                                             "<div class='col-6'>" + 
                                               "<h4>JSONLD Raw Response </h4>" + 
                                               "<pre style='height: 340px; width: 100%; border: 1px solid #00000033; padding: 5px; background-color: #d6f9ff;'>" + 
                                                 JSON.stringify( response[ "raw" ][ "data" ] , undefined , 2 ) + 
                                               "</pre>" + 
                                             "</div>" + 
                                             "<div class='col-6'>" + 
                                               "<h4>JSONLD Rendered Response</h4>" + 
                                               "<pre style='height: 340px; width: 100%; border: 1px solid #00000033; padding: 5px; background-color: #d6f9ff;'>" + 
                                                 JSON.stringify( response[ "jsonld" ] , undefined , 2 ) + 
                                               "</pre>" + 
                                             "</div>";

                      let mySeries = [];

                          mySeries.push( {
                            data    : response[ "data" ] , 
                            name    : "API Results" , 
                            tooltip : {
                                valueDecimals: 2
                            }
                          } );

                      Highcharts.stockChart('presentation-of-data', {
                          rangeSelector : { selected: 1 },
                          legend        : { enabled: true  },
                          title         : { text: "something" },
                          xAxis         : { type: 'datetime' },
                          series        : mySeries
                      });
                    });
                  }
            })
            .catch( ( error ) => {
              console.log( error );
              getModal( {
                "title" : "Warning" , 
                "body"  : "<p>" + error + "</p>"
              });
            });
      });
    }
  }

  subscribeToDataset( datasource_id ){
    return AJAX( "../ws/ws.generic.php" , {
      "action"        : "subscribeToDataset" , 
      "datasource_id" : datasource_id
    });
  }

  getDataFromDatasource( endpoint , datasource_id ){
    return AJAX( "../ws/ws.generic.php" , {
      "action"        : "getDataFromDatasource" , 
      "datasource_id" : datasource_id , 
      "endpoint"      : endpoint
    });
  }

  getDataFromDatasourceCalendar( endpoint , datasource_id ){
    return AJAX( "../ws/ws.generic.php" , {
      "action"        : "getDataFromDatasourceCalendar" , 
      "datasource_id" : datasource_id , 
      "endpoint"      : endpoint + "/calendar"
    });
  }
  
  parseJSONLD( dataset ){
   
  }

  getSubscribeContainer( subscriptionStatus , rowData , connectionWithProvider ){
    let subscribeStatusText  = "UnSubscribed";
    let dataButtonText       = "data";
    let subscribeButtonClass = "red";
    let disabled             = "";
    let showDataButton       = false;

    if( subscriptionStatus == "subscribed" ) {
      subscribeStatusText  = "Subscribed";
      subscribeButtonClass = "green";
      showDataButton       = true;
    }
    else if( subscriptionStatus == "needs subscription" ) {
      subscribeStatusText  = "UnSubscribed";
      subscribeButtonClass = "red";
      showDataButton       = false;
    }
    else if( subscriptionStatus == "pending" ) {
      subscribeStatusText  = "Pending acceptance from Provider";
      subscribeButtonClass = "teal";
      showDataButton       = false;
    }

    if( connectionWithProvider === false ){
      subscribeStatusText  = "Refresh Subscription (Expired)";
      subscribeButtonClass = "blue";
      showDataButton       = false;
    }
    
    console.log( subscriptionStatus );
    console.log( connectionWithProvider );

    return "<div class='row'>" + 
             "<div class='col-2' style='padding: 0rem 2.5rem 0.0rem 2.5rem !important;'>" + 
               "<span>" + 
                 "Subscription Status" + 
               "</span>" + 
             "</div>" + 
             "<div class='col-4' style='padding: 0rem 2.5rem 0.0rem 2.5rem !important;'>" + 
              '<button type="button" style="height: 40px;padding: 12px;" class="btn btn-link btn-'+subscribeButtonClass+'" id="ds-subscribe-to-datasource-'+rowData["id"]+'" '+disabled+'>' + 
               subscribeStatusText + 
              '</button>' + 
             "</div>" + ( showDataButton ? (
                 "<div class='col-2' style='padding: 0rem 2.5rem 0.0rem 2.5rem !important;'>" + 
                  '<button type="button" style="height: 40px;padding: 12px;" class="btn btn-link btn-info" id="ds-get-data-from-datasource-'+rowData["id"]+'" '+disabled+'>' + 
                   dataButtonText + 
                  '</button>' + 
                 "</div>" + 
                 "<div class='col-2' style='padding: 0rem 2.5rem 0.0rem 2.5rem !important;'>" + 
                  '<button type="button" style="height: 40px;padding: 12px;" class="btn btn-link btn-dark" id="ds-get-data-from-datasource-calendar-'+rowData["id"]+'" '+disabled+'>' + 
                   "Calendar" + 
                  '</button>' + 
                 "</div>" + 
                 "<div class='col-2' style='padding: 0rem 2.5rem 0.0rem 2.5rem !important;'>" + 
                  '<button type="button" style="height: 40px;padding: 12px;" class="btn btn-link btn-success" id="ds-add-data-for-comparison-'+rowData["id"]+'" '+disabled+'>' + 
                   "+Compare" + 
                  '</button>' + 
                 "</div>" 
               ) : "" 
             ) + 
           "</div>";
  }

  authenticateWithDirectory(){
    let username = document.getElementById( "source-email" ).value;
    let password = document.getElementById( "source-password" ).value;

    return AJAX( "../ws/ws.generic.php" , {
      "action" : "authWithDirectory" , 
      "username" : username , 
      "password" : password
    });
  }

  assignListeners(){
    let myButtonToSaveCredentials = document.getElementById( "ds-submit-credentials" );
    let self = this;
    myButtonToSaveCredentials.addEventListener( "click" , ( event ) =>{
      event.preventDefault();
      this.authenticateWithDirectory()
          .then( ( response ) => {
            if( response.hasOwnProperty( "data" ) && !response.data.hasOwnProperty( "error" ) ){
              document.getElementById( "ds-submit-credentials" ).classList.remove('btn-primary');
              document.getElementById( "ds-submit-credentials" ).classList.remove('btn-teal');
              document.getElementById( "ds-submit-credentials" ).classList.add('btn-green');
              document.getElementById( "ds-submit-credentials" ).innerHTML = 'Connected';

              self.updateToolsHTML(    "datasource-tools-container" , response );
              self.addDatasourceTable( "datasource-table-container" );
            }
            else{
              document.getElementById( "ds-submit-credentials" ).classList.remove('btn-primary');
              document.getElementById( "ds-submit-credentials" ).classList.remove('btn-green');              
              document.getElementById( "ds-submit-credentials" ).classList.add('btn-teal');
              document.getElementById( "ds-submit-credentials" ).innerHTML = 'Not Connected';
              
              document.getElementById( "datasources-form-sub" ).innerHTML = '';
              if( document.getElementById( "client-token" ) ){
                document.getElementById( "client-token" ).innerHTML = '';
              }

              document
                .getElementById( "datasource-table-container" )
                .innerHTML = '<div class="alert alert-danger" role="alert">Authentication with Directory failed. Check your provided inputs.</div>';
            }
          });
    });
  }

  getSavedCredentials(){
    return AJAX( "../ws/ws.generic.php" , {
      "action"   : "getSavedCredentials" , 
      "api_id"   : 1 
    });
  }

  __getLocationMeasurements( location_id , fromDate , toDate , dataset ){
    return AJAX( "../ws/ws.graph.php" , {
      "entity"      : dataset , 
      "locationid"  : location_id , 
      "fromdate"    : fromDate , 
      "todate"      : toDate
     });
  }

}