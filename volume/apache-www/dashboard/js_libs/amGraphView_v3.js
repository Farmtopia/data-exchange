class ViewData{

  constructor(){
    this.ajaxEndpoint = new AjaxEndpoints();
  }

  parseSeries( res ){
   let mySeries = [];

   Object.keys( res.data ).forEach( ( seriesName ) => {
    mySeries.push( { 
      name     : lang( seriesName ), 
      data     : res.data[ seriesName ], 
      tooltip  : { valueDecimals: 2 } 
    } ); 
   }); 
   
   return mySeries;
  }

  parseEventSeries( res , eventType ){
   let mySeries  = [];
   let myIcon    = "";
   let myLegend  = "";

   switch( eventType ){
     case "irrigations" : 
       myIcon   = 'url(../template/site_items/graph_event_icons/events_potisma.png)';
       myLegend = lang( "Irrigations" );
     break;
     case "sprays" : 
       myIcon   = 'url(../template/site_items/graph_event_icons/events_rantisma.png)';
       myLegend = lang( "Sprays" );
     break;
     case "harvest" : 
       myIcon   = 'url(../template/site_items/graph_event_icons/events_harvest.png)';
       myLegend = lang( "Harvest" );
     break;
     case "phaenological_stages" : 
       myIcon   = 'url(../template/site_items/graph_event_icons/events_phaenologic.png)';
       myLegend = lang( "phaenological_stages" );
     break;
     case "insects" : 
       myIcon   = 'url(../template/site_items/graph_event_icons/events_ptisi.png)';
       myLegend = lang( "Insects" );
     break;
     default : myIcon = 'url(../template/site_items/graph_event_icons/events_default.png)'; break;
   }

   Object.keys( res.data ).forEach( ( eventType ) => {

    let myEventData = new Array();

    res.data[ eventType ].forEach( ( eventDetail ) => {
     
     let myDate = eventDetail[ "date" ];
     if( eventType == "phaenological_stages" ){
      myDate = eventDetail[ "from" ];
     }

     let myTextObject  = {};
     let myEventObject = {
                          x     : Date.parse( myDate ),
                          title : " " , 
                          text  : new Array()
                         }

     Object.keys( eventDetail ).forEach( ( property ) => {
      myTextObject[ property ] = eventDetail[ property ];
     });

     myEventObject.text.push( myTextObject );

     myEventData.push( myEventObject );
    });

    mySeries.push( {
                  type  : 'flags' , 
                  name  : myLegend, 
                  data  : myEventData,
                  shape : myIcon
    } );
   });

   return mySeries;
  }

  getData( viewName , hasGrid ){
    switch( viewName ){
      case "models"                  : this.getDataCustomNew( viewName , "models"                  , hasGrid );  break;
      case "irrigation"              : this.getDataCustomNew( viewName , "irrigation_grid"         , hasGrid );  break;
      case "meteo"                   : this.getDataCustomNew( viewName , "stationmeasurement"      , hasGrid );  break;
      case "ndvi"                    : this.getDataCustomNew( viewName , "stationmeasurement"      , hasGrid );  break;
      case "forecast"                : this.getDataCustomNew( viewName , "stationmeasurement"      , hasGrid );  break;
      case "carbon_footprint"        : this.getDataCustomNew( viewName , "carbon_footprint"        , hasGrid );  break;
      case "calendar"                : this.getDataCustomNew( viewName , "calendar"                , hasGrid );  break;
      case "analytics_soil_moisture" : this.getDataCustomNew( viewName , "analytics_grid"          , hasGrid );  break;
      case "analytics_chilling_days" : this.getDataCustomNew( viewName , "analytics_chilling_days" , hasGrid );  break;
      case "station"                 : console.log( "default : " + viewName );                                   break;
      case "parcel"                  : console.log( "default : " + viewName );                                   break;
      default                        : console.log( "default : " + viewName );                                   break;
    }
  }

  getInfoPanel( icon , iconType , message , customStyle ){
    
    let myCustomStyle = typeof customStyle !== "undefined" ? customStyle : "";

    let myIcon = "<i style='line-height: 21px;' class='fa-brands fa-pagelines'></i>";
    if( iconType == "image" ){
      myIcon = '<img style="width:25px;" src="' + icon + '"></img>';
    }
    else{
      myIcon = "<i style='line-height: 21px;' class='" + icon + "'></i>";
    }

    return (
             "<div class='col-3'>" + 
               "<div class='grid-top-info-panels-container'>" + 
                 "<div class='grid-top-info-panels-icon-container' style='" + myCustomStyle + "'>" + 
                   myIcon + 
                 "</div>" + 
                 "<div style='float: left; padding: 0px 5px 0px 5px;'>" + 
                   message + 
                 "</div>" + 
               "</div>" + 
             "</div>" 
           )
  }

  getDataCustomNew( viewName , dataSet , hasGrid ){

    switch( viewName ){
      case "meteo" : 
        this.getDataForGraph_meteo(      
          store[ "location_id" ] , 
          store[ "startDate" ] , 
          store[ "endDate" ] , 
          viewName+"-graph-container" , 
          ()=>{ if( hasGrid ){ getGrid( viewName , viewName+"-grid-container" , dataSet ); } } 
        );
      break;
      case "irrigation" : 
        this.getDataForGraph_irrigation( 
          store[ "location_id" ] , 
          store[ "startDate" ] , 
          store[ "endDate" ] , 
          viewName+"-graph-container" , 
          ( response )=>{ if( hasGrid ){ getGrid( viewName , viewName+"-grid-container" , dataSet , response ); } } 
        );
      break;
      case "models"     : 
        this.getDataForGraph_models(     
          store[ "location_id" ] , 
          store[ "startDate" ] , 
          store[ "endDate" ] , 
          viewName+"-graph-container" , 
          ()=>{ if( hasGrid ){ getGrid( viewName , viewName+"-grid-container" , dataSet ); } } 
        );
      break;
      case "forecast": 
        this.getDataForGraph_forecast(   
          store[ "location_id" ] , 
          store[ "startDate" ] , 
          store[ "endDate" ] , 
          viewName+"-graph-container" , 
          ()=>{ if( hasGrid ){ getGrid( viewName , viewName+"-grid-container" , dataSet ); } } 
        );
      break;
      case "carbon_footprint"   : 
       this.getDataFor_carbon_footprint( 
         store[ "location_id" ] , 
         store[ "startDate" ] , 
         store[ "endDate" ] , 
         viewName+"-graph-container" , 
         ()=>{ if( hasGrid ){ getGrid( viewName , viewName+"-grid-container" , "carbon_footprint" ); } } 
       );
      break;
      case "calendar"   : 
       this.myCalendar = new View_calendar( {
         "location_id" : store[ "location_id" ] , 
         "parcel_id"   : store[ "parcel_id" ] , 
         "fromDate"    : store[ "startDate" ] , 
         "toDate"      : store[ "endDate" ] , 
         "containerID" : viewName+"-graph-container" , 
         "callback"    : ()=>{ if( hasGrid ){ getGrid( viewName , viewName+"-grid-container" , "calendar" ); } }
       } );
       this.myCalendar.init();
      break;
      case "analytics_soil_moisture" : 
       this.getDataForGraph_analytics(  
         store[ "location_id" ] , 
         store[ "startDate" ] , 
         store[ "endDate" ] , 
         viewName+"-graph-container" , 
         ( response )=>{ if( hasGrid ){ getGrid( viewName , viewName+"-grid-container" , "analytics_grid" , response ); } } 
       );
      break;
      case "analytics_chilling_days" : 
       this.getDataForGraph_analytics_chilling_days(  
         store[ "location_id" ] , 
         store[ "startDate" ] , 
         store[ "endDate" ] , 
         store[ "chilling_low_threshold" ] , 
         store[ "chilling_high_threshold" ] , 
         viewName+"-graph-container" , 
         ( response )=>{ if( hasGrid ){ getGrid( viewName , viewName+"-grid-container" , "analytics_chilling_days" , response ); } } 
       );
      break;
    }
  }

  getDataForGraph_meteo( location_id , myStartDate , myEndDate , targetElementId , callback ){
   let myResponse = {};
   this.ajaxEndpoint.getLocationMeasurements( location_id , myStartDate , myEndDate , "stationmeasurement" )
      .then( ( measurementsResponse ) => {
       myResponse[ "measurements" ] = measurementsResponse;
       return this.ajaxEndpoint.getDataFromServer( "generic" , "getEvents" , location_id , myStartDate , myEndDate , "sprays" );
      })
      .then( ( spraysResponse ) => {
       myResponse[ "sprays" ]  = spraysResponse;
       return this.ajaxEndpoint.getDataFromServer( "generic" , "getEvents" , location_id , myStartDate , myEndDate , "phaenological_stages" );
      })
      .then( ( pStagesResponse ) => {
       myResponse[ "phaenological_stages" ]  = pStagesResponse;
       return this.ajaxEndpoint.getDataFromServer( "generic" , "getEvents" , location_id , myStartDate , myEndDate , "irrigations" );
      })
      .then( ( irrigationsResponse ) => {
       myResponse[ "irrigations" ] = irrigationsResponse;
       return this.ajaxEndpoint.getDataFromServer( "generic" , "getEvents" , location_id , myStartDate , myEndDate , "harvest" );
      })
      .then( ( harvestResponse ) => {
       myResponse[ "harvest" ] = harvestResponse;

       let myCombinedSeries    = [];

       if( typeof myResponse[ "measurements" ] !== 'undefined' ){
         let myMeasurementSeries = this.parseSeries( myResponse[ "measurements" ] );
         myMeasurementSeries.forEach( ( aSeries ) => {
           aSeries.negativeColor = '#FF0000';
           myCombinedSeries.push( aSeries );
         });
       }

       if( typeof myResponse[ "sprays" ] !== 'undefined' ){
         let mySpraysSeries      = this.parseEventSeries( myResponse[ "sprays" ] , "sprays" );
         mySpraysSeries.forEach( ( bSeries ) => {
           myCombinedSeries.push( bSeries );
         });
       }

       if( typeof myResponse[ "irrigations" ] !== 'undefined' ){
         let myIrrigationSeries  = this.parseEventSeries( myResponse[ "irrigations" ] , "irrigations" );
         myIrrigationSeries.forEach( ( cSeries ) => {
           myCombinedSeries.push( cSeries );
         });
       }

       if( typeof myResponse[ "harvest" ] !== 'undefined' ){
         let myHarvestSeries  = this.parseEventSeries( myResponse[ "harvest" ] , "harvest" );
         myHarvestSeries.forEach( ( cSeries ) => {
           myCombinedSeries.push( cSeries );
         });
       }

       if( typeof myResponse[ "phaenological_stages" ] !== 'undefined' ){
         let myPStagesSeries  = this.parseEventSeries( myResponse[ "phaenological_stages" ] , "phaenological_stages" );
         myPStagesSeries.forEach( ( cSeries ) => {
           myCombinedSeries.push( cSeries );
         });
       }

       printGraph( myCombinedSeries , targetElementId , "meteo" );
       if( typeof callback === 'function' ){
         callback();
       }
      })
      .catch( ( error ) => {
       console.log( error );
      });

  }

  getDataForGraph_irrigation( location_id , myStartDate , myEndDate , targetElementId , callback ){
   let myResponse = {};
   this.ajaxEndpoint.getLocationMeasurements( location_id , myStartDate , myEndDate , "irrigation" )
      .then( ( measurementsResponse ) => {
       myResponse[ "measurements" ] = measurementsResponse;
       return this.ajaxEndpoint.getDataFromServer( "generic" , "getEvents" , location_id , myStartDate , myEndDate , "sprays" );
      })
      .then( ( spraysResponse ) => {
       myResponse[ "sprays" ]  = spraysResponse;
       return this.ajaxEndpoint.getDataFromServer( "generic" , "getEvents" , location_id , myStartDate , myEndDate , "harvest" );
      })
      .then( ( harvestResponse ) => {
       myResponse[ "harvest" ]  = harvestResponse;
       return this.ajaxEndpoint.getDataFromServer( "generic" , "getEvents" , location_id , myStartDate , myEndDate , "phaenological_stages" );
      })
      .then( ( pStagesResponse ) => {
       myResponse[ "phaenological_stages" ]  = pStagesResponse;
       return this.ajaxEndpoint.getDataFromServer( "generic" , "getEvents" , location_id , myStartDate , myEndDate , "irrigations" );
      })
      .then( ( irrigationsResponse ) => {
       myResponse[ "irrigations" ] = irrigationsResponse;

       let myCombinedSeries    = [];
       
       console.log( myResponse );

       if( typeof myResponse[ "measurements" ] !== 'undefined' ){
         let myMeasurementSeries = this.parseSeries( myResponse[ "measurements" ] );
         myMeasurementSeries.forEach( ( dSeries ) => {
           if( dSeries[ "data" ].length > 0 )
             myCombinedSeries.push( dSeries );
         });
       }

       if( typeof myResponse[ "irrigations" ] !== 'undefined' ){
         let myIrrigationSeries  = this.parseEventSeries( myResponse[ "irrigations" ] , "irrigations" );
         console.log( myIrrigationSeries );
         myIrrigationSeries.forEach( ( cSeries ) => {
           if( cSeries[ "data" ].length > 0 )
             myCombinedSeries.push( cSeries );
         });
       }

       if( typeof myResponse[ "harvest" ] !== 'undefined' ){
         let myHarvestSeries     = this.parseEventSeries( myResponse[ "harvest" ] , "harvest" );
         myHarvestSeries.forEach( ( bSeries ) => {
           if( bSeries[ "data" ].length > 0 )
             myCombinedSeries.push( bSeries );
         });
       }

       if( typeof myResponse[ "phaenological_stages" ] !== 'undefined' ){
         let myPStagetSeries = this.parseEventSeries( myResponse[ "phaenological_stages" ] , "phaenological_stages" );
         myPStagetSeries.forEach( ( aSeries ) => {
           if( aSeries[ "data" ].length > 0 )
             myCombinedSeries.push( aSeries );
         });
       }

       printGraph( myCombinedSeries , targetElementId , "irrigation" );
       if( typeof callback === 'function' ){
         callback( myResponse );
       }
      })
      .catch( ( error ) => {
       console.log( error );
      });

  }

  getDataForGraph_analytics( location_id , myStartDate , myEndDate , targetElementId , callback ){
   let myResponse = {};
   this.ajaxEndpoint.getLocationMeasurements( location_id , myStartDate , myEndDate , "irrigation" )
       .then( ( measurementsResponse ) => {
         myResponse[ "measurements" ] = measurementsResponse;
         return this.ajaxEndpoint.getDataFromServer( "generic" , "getEvents" , location_id , myStartDate , myEndDate , "sprays" );
       })
       .then( ( spraysResponse ) => {
         myResponse[ "sprays" ]  = spraysResponse;
         return this.ajaxEndpoint.getDataFromServer( "generic" , "getEvents" , location_id , myStartDate , myEndDate , "harvest" );
       })
       .then( ( harvestResponse ) => {
         myResponse[ "harvest" ]  = harvestResponse;
         return this.ajaxEndpoint.getDataFromServer( "generic" , "getEvents" , location_id , myStartDate , myEndDate , "phaenological_stages" );
       })
       .then( ( pStagesResponse ) => {
         myResponse[ "phaenological_stages" ]  = pStagesResponse;
         return this.ajaxEndpoint.getDataFromServer( "generic" , "getEvents" , location_id , myStartDate , myEndDate , "irrigations" );
       })
       .then( ( irrigationsResponse ) => {
         myResponse[ "irrigations" ] = irrigationsResponse;
         return this.ajaxEndpoint.getAggregates( store[ "parcel_id" ] , store[ "location_id" ] , myStartDate , myEndDate );
       })
       .then( ( analytics ) => {
         myResponse[ "analytics" ] = analytics;

         let myCombinedSeries    = [];
         let myGraphOptions      = {};
         
         if( typeof myResponse[ "measurements" ] !== 'undefined' ){
           let myMeasurementSeries = this.parseSeries( myResponse[ "measurements" ] );
           myMeasurementSeries.forEach( ( dSeries ) => {
             if( dSeries[ "data" ].length > 0 )
               myCombinedSeries.push( dSeries );
           });
         }

         if( typeof myResponse[ "analytics" ] !== 'undefined' ){
           let myMeasurementSeries = this.parseSeries( myResponse[ "analytics" ] );
           myMeasurementSeries.forEach( ( dSeries ) => {
             if( dSeries[ "data" ].length > 0 )
               myCombinedSeries.push( dSeries );
           });
           if( myResponse[ "analytics" ].hasOwnProperty( "info" ) ){
             myGraphOptions[ "analytics" ] = myResponse[ "analytics" ][ "info" ];
           }
         }

         if( typeof myResponse[ "irrigations" ] !== 'undefined' ){
           let myIrrigationSeries  = this.parseEventSeries( myResponse[ "irrigations" ] , "irrigations" );
           myIrrigationSeries.forEach( ( cSeries ) => {
             if( cSeries[ "data" ].length > 0 )
               myCombinedSeries.push( cSeries );
           });
         }

         if( typeof myResponse[ "harvest" ] !== 'undefined' ){
           let myHarvestSeries     = this.parseEventSeries( myResponse[ "harvest" ] , "harvest" );
           myHarvestSeries.forEach( ( bSeries ) => {
             if( bSeries[ "data" ].length > 0 )
               myCombinedSeries.push( bSeries );
           });
         }

         if( typeof myResponse[ "phaenological_stages" ] !== 'undefined' ){
           let myPStagetSeries = this.parseEventSeries( myResponse[ "phaenological_stages" ] , "phaenological_stages" );
           myPStagetSeries.forEach( ( aSeries ) => {
             if( aSeries[ "data" ].length > 0 )
               myCombinedSeries.push( aSeries );
           });
         }

         printGraph( myCombinedSeries , targetElementId , "irrigation_analytics" , myGraphOptions );
         if( typeof callback === 'function' ){
           callback( myResponse );
         }
       })
       .catch( ( error ) => {
         console.log( error );
       });

  }

  getChillingHoursThresholdsBasedOnCrop( crop ){

    let myThresholds = {
      "milia"                   : { "thresholds" : [ 1000 , 1600 ] ,  "limits" : [ 0 , 7  ] } , 
      "achladia"                : { "thresholds" : [ 500  , 1000 ] ,  "limits" : [ 0 , 7  ] } , 
      "kudonia"                 : { "thresholds" : [ 0    , 500 ]  ,  "limits" : [ 0 , 7  ] } , 
      "rodakinia"               : { "thresholds" : [ 400  , 800 ]  ,  "limits" : [ 0 , 7  ] } , 
      "kerasia"                 : { "thresholds" : [ 900  , 1200 ] ,  "limits" : [ 0 , 7  ] } , 
      "damaskinia_P._domestica" : { "thresholds" : [ 800  , 1200 ] ,  "limits" : [ 0 , 7  ] } , 
      "damaskinia_P._salicina"  : { "thresholds" : [ 0    , 800 ]  ,  "limits" : [ 0 , 7  ] } , 
      "verikokia"               : { "thresholds" : [ 200  , 400 ]  ,  "limits" : [ 0 , 7  ] } , 
      "amugdalia"               : { "thresholds" : [ 180  , 350 ]  ,  "limits" : [ 0 , 7  ] } , 
      "karudia"                 : { "thresholds" : [ 500  , 1500 ] ,  "limits" : [ 0 , 7  ] } , 
      "fistikia"                : { "thresholds" : [ 1000 , 1000 ] ,  "limits" : [ 0 , 7  ] } , 
      "sukia"                   : { "thresholds" : [ 0    , 400 ]  ,  "limits" : [ 0 , 7  ] } , 
      "fraoula"                 : { "thresholds" : [ 0    , 400 ]  ,  "limits" : [ 0 , 7  ] } , 
      "aktinidia"               : { "thresholds" : [ 0    , 400 ]  ,  "limits" : [ 0 , 7  ] } , 
      "rodia"                   : { "thresholds" : [ 0    , 400 ]  ,  "limits" : [ 0 , 7  ] } , 
      "ampeli"                  : { "thresholds" : [ 50   , 400 ]  ,  "limits" : [ 2 , 10 ] } ,// 2 - 10 , άριστα 3 - 7
      "olive"                   : { "thresholds" : [ 200  , 300 ]  ,  "limits" : [ 0 , 7  ] } , 
      "olive_tree"              : { "thresholds" : [ 200  , 300 ]  ,  "limits" : [ 0 , 7  ] } ,
      "Πατάτα"                  : { "thresholds" : [ 0    , 0 ]    ,  "limits" : [ 0 , 0  ] } 
    }

    if( myThresholds.hasOwnProperty( crop ) ){
      return {
               "plotLines": [
                {
                 "id": "low_limit",
                 "color": "green",
                 "label": {
                  "text": lang( "required hours" ) + 
                          " ( " + myThresholds[ crop ][ "limits" ][ 0 ] + "°C - " + 
                          myThresholds[ crop ][ "limits" ][ 1 ] + "°C ): " + 
                          lang( crop ) + " - " + lang( "low_limit" ) + " (" + myThresholds[ crop ][ "thresholds" ][ 0 ] + ")",
                  "style": {
                   "fill": "#000000",
                   "color": "#000000"
                  }
                 },
                 "value": myThresholds[ crop ][ "thresholds" ][ 0 ],
                 "width": 1,
                 "zIndex": 5,
                 "dashStyle": "solid"
                },
                {
                 "id": "high_limit",
                 "color": "green",
                 "label": {
                  "text": lang( "required hours" ) + 
                          " ( " + myThresholds[ crop ][ "limits" ][ 0 ] + "°C - " + 
                          myThresholds[ crop ][ "limits" ][ 1 ] + "°C ): " + 
                          lang( crop ) + " - " + lang( "high_limit" ) + " (" + myThresholds[ crop ][ "thresholds" ][ 1 ] + ")",
                  "style": {
                   "fill": "#000000",
                   "color": "#000000"
                  }
                 },
                 "value": myThresholds[ crop ][ "thresholds" ][ 1 ],
                 "width": 1,
                 "zIndex": 5,
                 "dashStyle": "solid"
                }
               ]
      }
    }
    else
      return false;
  }

  getDataForGraph_analytics_chilling_days( location_id , myStartDate , myEndDate , chilling_low_threshold , chilling_high_threshold , targetElementId , callback ){
   let myResponse = {};
   let myDates    = {
     "current" : {
        "fromDate" : myStartDate , 
        "toDate"   : myEndDate
     },
     "previous" : {}
   };

   var date = new Date( myStartDate );
   date.setFullYear( date.getFullYear() - 1 );
   var adate = new Date( myEndDate );
   adate.setFullYear( adate.getFullYear() - 1 );

   myDates[ "previous" ][ "fromDate" ] = date.toISOString().split( "T" )[ 0 ];
   myDates[ "previous" ][ "toDate" ]   = adate.toISOString().split( "T" )[ 0 ];

   this.ajaxEndpoint.getAgronomyData( store[ "parcel_id" ] )
       .then( ( agronomyData ) => {
         myResponse[ "agronomy" ]   = agronomyData;
         myResponse[ "thresholds" ] = this.getChillingHoursThresholdsBasedOnCrop( myResponse[ "agronomy" ][ "data" ][ 0 ][ "rocrDescription" ] );
         return this.ajaxEndpoint.getChillingDays( 
           store[ "parcel_id" ] , 
           store[ "location_id" ] , 
           myStartDate , 
           myEndDate , 
           chilling_low_threshold , 
           chilling_high_threshold 
         );
       })
       .then( ( chillingDays ) => {
         myResponse[ "analytics" ] = chillingDays;
         return this.ajaxEndpoint.getChillingDays( 
           store[ "parcel_id" ] , 
           store[ "location_id" ] , 
           myDates[ "previous" ][ "fromDate" ] , 
           myDates[ "previous" ][ "toDate" ] , 
           chilling_low_threshold , 
           chilling_high_threshold 
         );
       })
       .then( ( chilingHoursLastPeriod ) => {
         myResponse[ "analytics-last-period" ] = chilingHoursLastPeriod;
         return this.ajaxEndpoint.getLocationEvents( 
           store[ "location_id" ] , 
           myDates[ "previous" ][ "fromDate" ] , 
           myDates[ "previous" ][ "toDate" ] , 
           "harvest" 
         );
       })
       .then( ( previousHarvest ) => {
         myResponse[ "previous_harvest" ] = previousHarvest; 
         return this.ajaxEndpoint.getLocationEvents( 
           store[ "location_id" ] , 
           myStartDate , 
           myEndDate , 
           "harvest" 
         );
       })
       .then( ( currentHarvest ) => {
         myResponse[ "current_harvest" ]  = currentHarvest;

         let stremmata = parseFloat( myResponse[ "agronomy" ][ "data" ][ 0 ][ "gis" ][ 0 ][ "ektaria" ] );
         let variety   = myResponse[ "agronomy" ][ "data" ][ 0 ][ "rocvDescription" ];
         let crop      = myResponse[ "agronomy" ][ "data" ][ 0 ][ "rocrDescription" ];

         console.log( myResponse );

         let totalChillingHoursLastPeriod = 0;
         let totalChillingHoursThisPeriod = 0;

         if( myResponse[ "analytics-last-period" ].hasOwnProperty( "data" ) ){
           if( myResponse[ "analytics-last-period" ].data.hasOwnProperty( "index" ) ){
             totalChillingHoursLastPeriod = myResponse[ "analytics-last-period" ].data.index[ myResponse[ "analytics-last-period" ].data.index.length - 1 ][ 1 ];
           }
         }

         if( myResponse[ "analytics" ].hasOwnProperty( "data" ) ){
           if( myResponse[ "analytics" ].data.hasOwnProperty( "index" ) ){
             totalChillingHoursThisPeriod = myResponse[ "analytics" ].data.index[ myResponse[ "analytics" ].data.index.length - 1 ][ 1 ];
           }
         }

         let totalPreviousHarvest = 0;
         myResponse[ "previous_harvest" ][ "data" ][ "harvest" ].forEach( ( harvest ) => {
           totalPreviousHarvest = totalPreviousHarvest + harvest[ "quantity" ];
         });

         let totalCurrentHarvest = 0;
         myResponse[ "current_harvest" ][ "data" ][ "harvest" ].forEach( ( harvest ) => {
           totalCurrentHarvest = totalCurrentHarvest + harvest[ "quantity" ];
         });

         document
            .getElementById( targetElementId )
            .insertAdjacentHTML( "beforeBegin" , 
                                 "<div class='row' id='parent-node-for-analytics' style='text-align: center;'>" + 
                                   "<div class='col-md-12'>" + 
                                     "<div class='row' id='agronomy-data-analytics' style='text-align: center;'>" + 
                                     "</div>" + 
                                   "</div>" 
                               );

         document.getElementById( "agronomy-data-analytics" )
                 .innerHTML = ( 
                               "<div class='col-12'>" + 
                                 "<div class='row'>" + 
                                   this.getInfoPanel( "fa-regular fa-map" , "font" , ( lang( "stremmata" ) + " : " + stremmata ) ) + 
                                   (
                                     ( crop !== "" ) ? ( this.getInfoPanel( "fa-brands fa-pagelines" , "font" , ( lang( "crop" ) + " : " + crop ) ) ) : "" 
                                   ) + 
                                   (
                                     ( variety !== "" ) ? ( this.getInfoPanel( "fa-solid fa-tag" , "font" , ( lang( "variety" ) + " : " + variety ) ) ) : "" 
                                   ) + 
                                   (
                                     ( totalChillingHoursThisPeriod !== "" ) ? ( this.getInfoPanel( "fa-solid fa-snowflake" , "font" , ( lang( "chilling_hours" ) + " : " + totalChillingHoursThisPeriod ) , "background-color: rgb(248 249 250); color: rgb(0 123 255); border-right: 1px solid #0000001f;" ) ) : "" 
                                   ) + 
                                   // (
                                     // ( totalChillingHoursLastPeriod !== "" ) ? ( this.getInfoPanel( "fa-solid fa-snowflake" , "font" , ( lang( "previous_chilling_hours" ) + " : " + totalChillingHoursLastPeriod ) ) ) : "" 
                                   // ) + 
                                   // (
                                     // ( totalPreviousHarvest !== "" ) ? ( this.getInfoPanel( "../template/site_items/icons/ICONS_WHITE_SYGKOMIDH.png" , "image" , ( lang( "previous_harvest" ) + " : " + totalPreviousHarvest ) ) ) : "" 
                                   // ) + 
                                   // (
                                     // ( totalCurrentHarvest !== "" ) ? ( this.getInfoPanel( "../template/site_items/icons/ICONS_WHITE_SYGKOMIDH.png" , "image" , ( lang( "current_harvest" ) + " : " + totalCurrentHarvest ) ) ) : "" 
                                   // ) + 
                                 "</div>" + 
                               "</div>" 
                              );
         // let myAnalyticsTable = document.getElementById( "table-data-analytics" );
         let myAnalyticsTable = document.getElementById( "table-data-analytics" );
         if( typeof myAnalyticsTable !== "undefined" && myAnalyticsTable !== null ){
           myAnalyticsTable.remove();
         }
         document
            .getElementById( targetElementId.replace( "-graph-container" , "" ) + "-form-main" )
            .insertAdjacentHTML( "beforeEnd" , 
            // .getElementById( "parent-node-for-analytics" )            
            // .insertAdjacentHTML( "beforeEnd" , 
                                 "<div class='row' id='table-data-analytics' style='text-align: center;'>" + 
                                   "<div class='col-md-12'>" + 
                                    '<table class="table table-bordered">              ' + 
                                    '  <thead class="table-primary">       ' + 
                                    '    <tr>                           ' + 
                                    '      <th class="table-primary" scope="col">' + lang( "period_selection" ) + '</th>' + 
                                    '      <th class="table-primary" scope="col">' + lang( "chilling_hours" ) + '</th>' + 
                                    '      <th class="table-primary" scope="col">' + lang( "harvest" ) + '</th>' + 
                                    '    </tr>' + 
                                    '  </thead>' + 
                                    '  <tbody>' + 
                                    '    <tr>' + 
                                    '      <td>' + myDates[ "current" ][ "fromDate" ] + 
                                                   " to " + 
                                                   myDates[ "current" ][ "toDate" ] + '</td>' + 
                                    '      <td>' + totalChillingHoursThisPeriod + '</td>' + 
                                    '      <td>' + totalCurrentHarvest + '</td>' + 
                                    '    </tr>' + 
                                    '    <tr>' + 
                                    '      <td>' + myDates[ "previous" ][ "fromDate" ] + 
                                                   " to " + 
                                                   myDates[ "previous" ][ "toDate" ] + '</td>' + 
                                    '      <td>' + totalChillingHoursLastPeriod + '</td>' + 
                                    '      <td>' + totalPreviousHarvest + '</td>' + 
                                    '    </tr>' + 
                                    '  </tbody>' + 
                                    '</table>' + 
                                   "</div>" + 
                                 "</div>"
                               );

         let myCombinedSeries    = [];
         let myGraphOptions      = {};

         if( typeof myResponse[ "analytics" ] !== 'undefined' ){
           let myMeasurementSeries = this.parseSeries( myResponse[ "analytics" ] );
           myMeasurementSeries.forEach( ( dSeries ) => {
             if( dSeries[ "data" ].length > 0 )
               myCombinedSeries.push( dSeries );
           });
           if( myResponse[ "analytics" ].hasOwnProperty( "info" ) ){
             myGraphOptions[ "analytics" ] = myResponse[ "analytics" ][ "info" ];
           }
         }

         myGraphOptions[ "analytics" ] = myResponse[ "thresholds" ];

         printGraph( myCombinedSeries , targetElementId , "analytics_chilling_days" , myGraphOptions );
         if( typeof callback === 'function' ){
           callback( myResponse );
         }

       })
       .catch( ( error ) => {
         console.log( error );
       });
  }

  getDataForGraph_ndvi( viewName , targetElementId ){
   let myViewContainer = document.getElementById( viewName+'-form-main' );
   myViewContainer.innerHTML = "";

   myViewContainer.insertAdjacentHTML( "afterBegin" , "<div id='"+viewName+"-grid-container'></div>" );

   getAutoSearch( viewName+'-tool-container' , store[ "local_name" ] );

   let cTS         = new Date().getTime();
   let daysBack    = 180;
   let myStartDate = getDate( ( cTS - ( daysBack * 24 * 60 * 60 * 1000 ) ) );
   let myEndDate   = getDate( cTS );
   let self        = this;

   setAmAutoComplete( {
    "url"      : "../ws/ws.generic.php?action=getStations&value=" , 
    "path"     : "data" , 
    // "return"   : "name",
    "return"   : "toponym",
    "minLen"   : "0",
    "selector" : ".LocationAutoComplete" , 
    "target"   : viewName+"-tool-container-search-input",
    "callback" : ( res ) => {
      store[ "location_id" ]   = res.id;
      store[ "location_name" ] = res.name;
      store[ "local_name" ]    = res.toponym;

      self.ajaxEndpoint.getNDVI( res[ "id" ] , myStartDate , myEndDate )
         .then( ( ndviResponse ) => {
          printGraph( this.parseSeries( ndviResponse ) , targetElementId , "ndvi" );
         })
         .catch( ( error ) => {
          console.log( error );
         });
    }
   } );

   if( store[ "location_id" ] ){
    self.ajaxEndpoint.getNDVI( store[ "location_id" ] , "2021-01-01" , "2021-03-01" )
       .then( ( ndviResponse ) => {
        printGraph( this.parseSeries( ndviResponse ) , targetElementId , "ndvi" );
       })
       .catch( ( error ) => {
        console.log( error );
       });
   }
  }

  getDataForGraph_models( location_id , myStartDate , myEndDate , targetElementId , callback ){ 
   let myResponse = {};

   this.ajaxEndpoint.getLocationMeasurements( location_id , myStartDate , myEndDate , "models" )
      .then( ( measurementsResponse ) => {
        myResponse[ "measurements" ] = measurementsResponse;
        return this.ajaxEndpoint.getDataFromServer( "generic" , "getEvents" , location_id , myStartDate , myEndDate , "sprays" );
      })
      .then( ( spraysResponse ) => {
       myResponse[ "sprays" ]  = spraysResponse;
       return this.ajaxEndpoint.getDataFromServer( "generic" , "getEvents" , location_id , myStartDate , myEndDate , "harvest" );
      })
      .then( ( harvestResponse ) => {
       myResponse[ "harvest" ]  = harvestResponse;
       return this.ajaxEndpoint.getDataFromServer( "generic" , "getEvents" , location_id , myStartDate , myEndDate , "phaenological_stages" );
      })
      .then( ( pStagesResponse ) => {
       myResponse[ "phaenological_stages" ]  = pStagesResponse;

       let myCombinedSeries    = [];

       if( typeof myResponse[ "measurements" ] !== 'undefined' ){
        let myMeasurementSeries = this.parseSeries( myResponse[ "measurements" ] );
        myMeasurementSeries.forEach( ( aSeries ) => {
          myCombinedSeries.push( aSeries );
        });
       }

       if( typeof myResponse[ "sprays" ] !== 'undefined' ){
         let mySpraysSeries      = this.parseEventSeries( myResponse[ "sprays" ] , "sprays" );
         mySpraysSeries.forEach( ( bSeries ) => {
           myCombinedSeries.push( bSeries );
         });
       }

       if( typeof myResponse[ "harvest" ] !== 'undefined' ){
         let myHarvestSeries     = this.parseEventSeries( myResponse[ "harvest" ] , "harvest" );
         myHarvestSeries.forEach( ( bSeries ) => {
           myCombinedSeries.push( bSeries );
         });
       }

       if( typeof myResponse[ "phaenological_stages" ] !== 'undefined' ){
         let myPStagetSeries = this.parseEventSeries( myResponse[ "phaenological_stages" ] , "phaenological_stages" );
         myPStagetSeries.forEach( ( aSeries ) => {
           myCombinedSeries.push( aSeries );
         });
       }

       printGraph( myCombinedSeries , targetElementId , "models" );
       if( typeof callback === 'function' ){
         callback();
       }
      })
      .catch( ( error ) => {
       console.log( error );
      });
  }

  getDataForGraph_forecast( location_id , myStartDate , myEndDate , targetElementId , callback ){ 
   let myResponse = {};

   this.ajaxEndpoint.getLocationForecast( location_id , myStartDate , myEndDate , "forecast" )
      .then( ( forecastResponse ) => {
        myResponse[ "forecast" ] = forecastResponse;

        let myCombinedSeries = [];

        if( typeof myResponse[ "forecast" ] !== 'undefined' ){
         let myForecastMeasurements = this.parseSeries( myResponse[ "forecast" ] );
         myForecastMeasurements.forEach( ( aSeries ) => {
           myCombinedSeries.push( aSeries );
         });
        }

        printGraph( myCombinedSeries , targetElementId , "models" );

        if( typeof callback === 'function' ){
          callback();
        }
      })
      .catch( ( error ) => {
       console.log( error );
      });
  }

  getDataFor_carbon_footprint( location_id , myStartDate , myEndDate , targetElementId , callback ){ 
   let myResponse = {};

   this.ajaxEndpoint.getCarbonFootprintOutput( location_id , myStartDate , myEndDate )
      .then( ( response ) => {
        myResponse[ "carbonFootprintOutput" ] = response;
        return getCarbonFootprintInput( location_id , myStartDate , myEndDate );
      })
      .then( ( carbonFootprintInput ) => {
        myResponse[ "carbonFootprintInput" ] = carbonFootprintInput;

        if( myResponse[ "carbonFootprintOutput" ][ "err" ] !== "" && myResponse[ "carbonFootprintInput" ][ "err" ] !== "" ){
          document.getElementById( "toast-header" ).style.backgroundColor = "#ffc107";
          document.getElementById( "toast-title" ).innerText              = lang( "info" );
          document.getElementById( "toast-message" ).innerText            = lang( "no_carbon_footprint_available" );

          $('.toast').toast( { "autohide" : false } );
          $('.toast').toast( "show" );
          setTimeout( () => {
           $('.toast').toast( "hide" );
          } , 10000 );
        }
        else{

          document.getElementById( targetElementId ).innerHTML = "";

          if( !myResponse[ "carbonFootprintOutput" ][ "data" ][ "data" ] ){
           document.getElementById( targetElementId ).innerHTML = '<div class="container" style="height:250px;">' + 
                                                                    '<div class="alert alert-primary" role="alert">' +
                                                                      '<h4 class="alert-heading">' + lang( "carbon_no_data_title" ) + '</h4>' +
                                                                      '<p>' + lang( "carbon_no_data_body" ) + '</p>' +
                                                                      '<hr>' +
                                                                      '<p class="mb-0" style="font-size: 13px;font-weight: 600;">' + lang( "contact_admin" ) + '</p>' +
                                                                    '</div>' + 
                                                                  '</div>' 
          }
          else{
            // printCarbonFootprintTablesStackedColumns( targetElementId , myResponse );
            printCarbonFootprintSummary( targetElementId , myResponse );
            getCarbonAggregation( targetElementId , myResponse );
            printCarbonFootprintTables( targetElementId , myResponse );
          }
          
        }
        if( typeof callback === 'function' ){
          callback();
        }
      })
      .catch( ( error ) => {
       console.log( error );
      });
  }

}