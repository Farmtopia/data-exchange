class amDom_CardWithIcon{

 constructor( options ){
  console.log("amDom_CardWithIcon initializing...");
  if(typeof options == 'undefined'){
   console.log("You need to provide a Configuration Object.");
   return;
  }
  
  this.options = options;
  
  if( typeof this.options.cntheight === 'undefined' )
     this.options.cntheight = 150;

  this.myPageContainer = document.getElementById( options[ "container_id" ] );
  this.cardID = "amDom_CardWithIcon_" + this.randomUID( 6 );

  this.init();
  return this.myGrid;
 }
 
 getInputStyle( content , hasData ){
  let myHTML = "";

  if ( hasData === true ){
   content.forEach( ( item , idx ) => {
    let myTooltip = (typeof item[ "tooltip" ] !== 'undefined' && item[ "tooltip" ] !== false ) ? 
                      '<div class="input-group-append">' + 
                        '<span class="input-group-text" id="tooltip-card-id-'+idx+'">'+ 
                         '<i class="fas fa-info-circle" data-toggle="tooltip" data-placement="left" data-html="true" title="' + item[ "tooltip" ] + '"></i>' + 
                        '</span>' + 
                      '</div>'
                      : 
                      "";
    let columns = ( myTooltip !== "" ) ? 3 : 0;
    let iconWidth = ( typeof this.options[ "iconwidth" ] !== 'undefined' ) ? "width: "+this.options[ "iconwidth" ]+"px;" : "";
    myHTML += '<div class="col" style="padding:0px 0px 0px 0px; width: 100%;">' + 
               '<div class="input-group mb-'+columns+'">' + 
                 '<div class="input-group-prepend am-domcard-list-property" '+iconWidth+'">' + 
                   '<span style="width: 100%;" class="input-group-text" id="badge-id-'+idx+'">'+item[ "title" ]+'</span>' + 
                 '</div>' + 
                 '<input style="background-color: #ffffff;" type="text" class="form-control" placeholder="'+item[ "description" ]+'" aria-label="'+item[ "description" ]+'" aria-describedby="badge-id-'+idx+'" readonly>' + 
                 myTooltip + 
               '</div>' + 
              '</div>';
   });
  }
  else{
    myHTML += '<div class="col" style="padding:0px 0px 0px 0px;">' + 
                '<div style="text-align: center; line-height: 115px; background-color: #e9ecef; height: 100%; border: 1px solid #00000021; border-radius: 4px;">' + 
                  lang( 'no_data' ) + 
                '</div>' + 
              '</div>';
  }

  return myHTML;
 }

 init(){
   this.setIcon( this.options );
   this.setContent( this.options );
   this.setType( this.options ); // with-content | without-content
   this.addCard( this.options );
 }
 
 getStyle(){
  return "border-radius: 10px; " + 
         "border: 1px solid #0000001c; " + 
         // "width: 550px; " + 
         // "height: "+this.options.cntheight+"px; " + 
         "overflow: hidden; ";
 }

 getTooltip( content ){
  return '<button type="button" class="btn btn-secondary" data-toggle="tooltip" data-html="true" title="'+content+'">' + 
           '<i class="fas fa-info-circle"></i>' + 
         '</button>';
 }

 setContent( options ){
  this.content = typeof options.content !== 'undefined' ? options.content : "";
 }

 setType( options ){
   this.type = typeof options.type !== 'undefined' ? options.type : "with-content";
 }

 setIcon( options ){
  let iconColor = typeof options.color !== 'undefined' ? options.color : "black";
  let iconSize  = typeof options.size  !== 'undefined' ? parseInt( options.size ) : "50";
  let iconType  = typeof options.icon  !== 'undefined' ? options.icon  : "fas fa-times-circle";
  this.icon = '<i style="margin: 15px; font-size: '+iconSize+'px; color:'+iconColor+'" class="'+iconType+'"></i>';
 }

 addCard( options ){
  let iconBackGroundColor = typeof options.iconbg   !== 'undefined' ? options.iconbg   : "#ffffff";
  let iconBorderRadius    = typeof options.border   !== 'undefined' ? options.border   : "0px";
  let cssclass            = typeof options.cssclass !== 'undefined' ? options.cssclass : "";
  let position            = typeof options.position !== 'undefined' ? options.position : "afterBegin";
  let tooltip             = typeof options.position !== 'undefined' ? options.position : "afterBegin";
  let title               = typeof options.title    !== 'undefined' ? options.title    : "";
  let aStyle              = 'background-color:'+iconBackGroundColor+';border:'+iconBorderRadius;
  let sizeOfIcon          = this.type === 'without-content' ? "col-12" : "col-md-0 col-lg-2 p-0";
  let contentArea         = this.type === 'without-content' ? "" : "<div class='col-md-12 col-lg-10 p-0 am-domcard-right'>" + 
                                                                     this.getInputStyle( this.content , this.options.hasdata ) + 
                                                                   "</div>";

  let myCard = "<div class='p-2 "+cssclass+"' id='"+this.cardID+"'>" +
                  "<div class='row am-domcard-title'>" + 
                    "<div class='col-md-12 p-1 align-middle' style='background-color: #dc3545;color: white;'>" + 
                      "<div>" + title + "</div>" + 
                    "</div>" + 
                  "</div>" + 
                  "<div class='row'>" + 
                   "<div class='" + sizeOfIcon + " align-middle am-domcard-left' style='" + aStyle + "'>" + 
                     this.icon + 
                     "<div class='card-sub-title'>" + title + "</div>" + 
                   "</div>" + contentArea 
                  "</div>" + 
               "</div>";

  this.myPageContainer.insertAdjacentHTML( position , myCard );
 }

 randomUID( length ) {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return "uid_"+result;
 }

}