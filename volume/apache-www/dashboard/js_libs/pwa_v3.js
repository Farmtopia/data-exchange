/* Error Pane */
const myServiceWorkerVersion = 4;
  window.addEventListener('online' , connectionHandler);
  window.addEventListener('offline', connectionHandler);

  function handleConnectivity(){
   var myErrorPane = document.getElementById("error-pane");
   if(!navigator.onLine){
    if(myErrorPane){
     setTimeout(()=>{
      myErrorPane.innerHTML = "No Network Connection";
      myErrorPane.style.backgroundColor = "red";
      myErrorPane.style.display = "block";
     },100);
    }
    console.log("Device is Offline");
   }

   return navigator.onLine;
  }

  function connectionHandler(){
   var myErrorPane = document.getElementById("error-pane");
   var myConnInfo  = document.getElementById("connection-info");
   if(navigator.onLine){
    if(myErrorPane){
     myErrorPane.innerHTML = "Connection to Network Established";
     myErrorPane.style.backgroundColor = "#00c400";
     myErrorPane.style.display = "block";
     if(typeof myConnInfo !== 'undefined'){
      myConnInfo.style.backgroundColor = "#00c400";
      myConnInfo.innerHTML = "<span>Online</span>";
     }
    }
    console.log("Device is Online");
   }else{
    if(myErrorPane){
     myErrorPane.innerHTML = "Network connection lost";
     myErrorPane.style.backgroundColor = "red";
     myErrorPane.style.display = "block";
     if(typeof myConnInfo !== 'undefined'){
      myConnInfo.style.backgroundColor = "#ff0000";
      myConnInfo.innerHTML = "<span>Offline</span>";
     }
    }
    console.log("Device is Offline");
   }
  }

  document.body.addEventListener("click" , (event)=>{
   var myErrorPane = document.getElementById("error-pane");
   if(myErrorPane)
    myErrorPane.style.display = "none";
  });

 if ('serviceWorker' in navigator) {
   window.addEventListener('load', function() {
     navigator.serviceWorker.register('service-worker_v'+myServiceWorkerVersion+'.js')
       .then(reg => {
         console.log('Service worker registered! 😎');
       })
       .catch(err => {
         console.log('😥 Service worker registration failed: ', err);
       });
   });
 }

 let deferredPrompt;
 const addBtn = document.getElementById('install-pwa');

 window.addEventListener('beforeinstallprompt', event => {

   // Prevent Chrome 67 and earlier from automatically showing the prompt
   event.preventDefault();

   // Stash the event so it can be triggered later.
   deferredPrompt = event;
   
   // Update UI to notify the user they can add to home screen
   // addBtn.style.display = 'block';
   addBtn.style.display = 'none';
   
   // Attach the install prompt to a user gesture
   // document.querySelector('#installBtn').addEventListener('click', event => {
   addBtn.addEventListener('click', (e) => {
    
    // hide our user interface that shows our A2HS button
    addBtn.style.display = 'none';    

     // Show the prompt
     deferredPrompt.prompt();

     // Wait for the user to respond to the prompt
     deferredPrompt.userChoice
       .then((choiceResult) => {
         if (choiceResult.outcome === 'accepted') {
           console.log('User accepted the A2HS prompt');
         } else {
           console.log('User dismissed the A2HS prompt');
         }
         deferredPrompt = null;
       });
   });
 });
