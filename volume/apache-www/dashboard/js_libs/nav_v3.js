/* Main Graph Object */

let myChartObject = {};

/* Search Input */

function getAutoSearch( targetContainerID , defaultValue ){
  let myTargetContainer = document.getElementById( targetContainerID );
  let mySearchBoxId     = targetContainerID+"-search";
  let mySearchInputId   = targetContainerID+"-search-input";
  let vDefault          = typeof defaultValue !== "undefined" ? defaultValue : "";
  myTargetContainer.insertAdjacentHTML( "afterBegin" , '<div class="col-md-4 location-search-container" id="'+mySearchBoxId+'"></div>' );

  letMyLocationSearchContainer = document.getElementById( mySearchBoxId );
  letMyLocationSearchContainer.style.display = "block";

  letMyLocationSearchInput     = document.getElementById( mySearchInputId );

  if( !letMyLocationSearchInput ){
   let myStyle = "width: 200px; " + 
                 " height: 31px; background-color: rgb(252 252 252); color: black; " + 
                 " margin: 9px; text-align: center; line-height: 31px; " + 
                 " z-index: 15; box-shadow: 1px 1px 10px 0px #00000030; position: relative;";

   let mySearchHTML = '<label class="form-label">' + lang( "search_parcel" ) + '</label>';
       mySearchHTML += '<div class="input-group mb-3">' + 
                        '<input type="text" class="form-control LocationAutoComplete" id="'+mySearchInputId+'" data-target="#'+mySearchInputId+'" placeholder="Location" aria-label="Location" aria-describedby="Location" value="'+vDefault+'">' + 
                        '<div class="input-group-append">' +
                         '<span class="input-group-text" id="m_date">' + '<i class="fas fa-search"></i>' + '</span>' +
                        '</div>' + 
                       '</div>';

    letMyLocationSearchContainer.insertAdjacentHTML( "afterBegin" , mySearchHTML );
  }
}


/* Helpers */

function getViewLayout( viewName , myClickedElement ){

 switch( viewName ){
  // case "station"                 : setupView( viewName , myClickedElement ); getStationGrid( viewName );                                 break;
  // case "meteo"                   : setupView( viewName , myClickedElement ); getDataView( viewName , "stationmeasurement" , true );      break;
  case "datasources"             : setupView( viewName , myClickedElement ); getDatasources( viewName , "datasources" , true );      break;
  case "logout"                  : logout( );         break;
  case "settings"                : getSettingsView(); break;
  case "messages"                : getMessagesView(); break;
 }

}

function setupView( aSelectedTab , myClickedElement ){ 
 toggleButtonsStyling( myClickedElement );
 updateNavigationPath( aSelectedTab );
 store[ "currentView" ] = aSelectedTab.toLowerCase();
}

function toggleButtonsStyling( myTargetElement ){

  document
    .querySelectorAll( ".nav-link" )
    .forEach( ( node ) => {
      node.classList.remove( "waves-light" );
      node.classList.remove( "active" ); 
      node.style.backgroundColor = "rgba(0, 0, 0, 0)";
   });

  document
    .querySelectorAll( ".dropdown-item" )
    .forEach( ( node ) => {
      node.classList.remove("active");
      node.style.backgroundColor = "rgba(0, 0, 0, 0)";
   });

  myTargetElement.style.backgroundColor = "rgb(110 163 252)";
}

function updateNavigationPath( navName ){
 let myNavPath = "<div><i style='color: #007bff;margin-right: 5px;' class='fas fa-globe'></i></div>" + 
                 "<div>" + lang( store[ "nav_name" ] , myLanguage ) +  "</div>";

 if( store[ "nav_child" ] ){
  myNavPath += "<div><i style='color: #20c997;' class='fas fa-arrow-circle-right'></i></div>";
  myNavPath += "<div>" + lang( store[ "nav_child" ] , myLanguage ) +  "</div>";
 }

 document
   .getElementById( "nav-path" )
   .innerHTML = myNavPath;
}

function getNoDataAlert(){
  return '<div class="alert alert-success" role="alert" style="width: 100%;margin-top: 15px;">' + 
          '<h4 class="alert-heading">' + lang( "missing_data_header" ) + '</h4>' + 
          '<p>' + lang( "missing_data_body" ) + '</p>' + 
          '<hr>' + 
          '<p class="mb-0">' + lang( "missing_data_footer" ) + '</p>' + 
         '</div>';
}

/* Mini Views */

function getSettingsView(){
   getModal({
    "title" : "Info",
    "body"  : "Feature [Settings] is Under Construction" , 
    "buttons" : [ "cancel" ]
   } , () => {
    console.log( "Settings Loaded" );    
   });
}

function getMessagesView(){
   getModal({
    "title" : "Info",
    "body"  : "Feature [Messages] is Under Construction" , 
    "buttons" : [ "cancel" ]
   } , () => {
    console.log( "Messages Loaded" );    
   });
}

function showUnderConstruction( viewName ){
 let myContainer = document.getElementById( viewName+"-form-main" );
 myContainer.innerHTML = '<div class="container" style="padding: 60px;">' + 
                           '<div class="row">' + 
                             '<div clas="col-12" style="width: 100%;">' + 
                               '<div class="card">' + 
                                 '<div class="card-header">' + 
                                   'Info <i class="fa-solid fa-person-digging"></i>' + 
                                 '</div>' + 
                                 '<div class="card-body">' + 
                                   '<h5 class="card-title">Under Construction</h5>' + 
                                   '<p class="card-text">The "Carbon Footpring" feature is under construction.</p>' + 
                                 '</div>' + 
                               '</div>' + 
                             '</div>' + 
                           '</div>' + 
                         '</div>';
}

/* Station Grid */

function getStationGrid( viewName ){
   setDefaultDates();

// If a Grid is present , do not reinitialize it , 
// to prevent quering the WS again for Stations
   if( document.getElementById( viewName + "-form-main_tbl_wrapper" ) )
      return;

// HTML Clear the Detail container. (Below the Grid)
   if( document.getElementById( viewName + "-form-sub" ) ) 
     document.getElementById( viewName + "-form-sub" ).innerHTML = "";

// Init the Grid
   setTimeout( () => {
     var myGrid = new amGrid( {
      "container"      : viewName + "-form-main", 
      "uniqueRowId"    : viewName + "_id",
      "uniqueGridId"   : viewName + "-view",
      "dispatcherPath" : "../ws/ws.grid.php", 
      "customAJAX"     : { "entity" : "station" , "action" : "getStations" },
      "processtype"    : "client",
      "orderby"        : { "column" : 0 , "dir" : "desc" } , 
      "searchBox"      : true,
      "CRUD"           : false,
      "infoBox"        : false,
      "rowselect"      : "single",
      "scrollable"     : false,
      "height"         : ( getViewDimensions("height") / 4 ) + "px",
      "collapse"       : true,
      "rowButtons"     : {
        "btn-toggle-station" : {
          "name" : "toggle",
          "icon" : '<i style="font-size:1.5rem" class="fa-solid fa-toggle-on"></i>',
          "className" : "grid-override",
          "fn"   : ( event , rowObject ) => { console.log( "ToggleStatus" ); toggleStatus( rowObject.id , event ); }
        }
      },
      "rowEvent" : ( rowEventObject ) => {
        showNodeGraph( rowEventObject );
      },
      "onrender" : ( grid ) => { 
        myGrid.activateRow( 0 );
        myGrid.selectRow( 0 );
        setCorrectStatuses();
      }
     } );
     store[ "grid_obj" ] = myGrid;
   } , 100 );

}

/* Node Graph */
function showNodeGraph( rowEventObject ){
  console.log( rowEventObject );
  store[ "nodeid" ] = rowEventObject.data.name;
  AJAX( "../ws/ws.graph.php" , {
    "action"   : "getMeasurements" , 
    "nodeid"   : rowEventObject.data.name , 
    "fromDate" : store[ "startDate" ],
    "endDate"  : store[ "endDate" ],
    "entity"   : "station"
  })
  .then( ( response ) => {
    let myContainer = document.getElementById( "station-form-sub" );

    myContainer.innerHTML = "<div class='row'>" + 
                              "<div class='col-12' id='tools-container' >" + 
                                "<div id='my-period-container-station'></div>" + 
                              "</div>" + 
                            "</div>" +
                            "<div class='row'>" + 
                              "<div class='col-12' id='graph-container' ></div>" + 
                            "</div>";

    let mySeries = [];

    getPeriodSearchHTML( 
       'tools-container' , 
       'station' , 
       { 
         "startDate" : store[ "startDate" ] , 
         "endDate"   : store[ "endDate" ] 
       } , 
       false ,
       true 
    );

    Object.keys( response[ "data" ] ).forEach( ( entity ) => {
      mySeries.push( {
        data    : response[ "data" ][ entity ] , 
        name    : entity , 
        tooltip : {
            valueDecimals: 2
        }
      } );
    });

    Highcharts.stockChart('graph-container', {
        rangeSelector: {
            selected: 1
        },
        
        legend: {
            enabled: true 
        },

        title: {
            text: response[ "info" ][ "title" ]
        },

        xAxis : {
            type: 'datetime'
        },

        series : mySeries
    });
  })
  .catch( ( error ) => {
    console.log( error );
  });
}

function setCorrectStatuses(){
  AJAX( "../ws/ws.generic.php" , {
    "action"   : "getStatus" 
  })
  .then( ( response ) => {
    console.log( response );
    if( response.length > 0 ){
      response.forEach( ( command ) => {
        let myButton = document.getElementById( "btn-toggle-station-" + command[ "id" ] );
        if( typeof myButton !== "undefined" && myButton ){
          console.log( myButton );
          if( parseInt( command[ "state" ] ) <= 0 ){
             myButton.innerHTML = '<i style="font-size:1.5rem" class="fa-solid fa-toggle-off"></i>';
          }
          else{
             myButton.innerHTML = '<i style="font-size:1.5rem" class="fa-solid fa-toggle-on"></i>';
          }
        }
      } );
    }
  })
  .catch( ( error ) => {
    console.log( error );
  });
}

function toggleStatus( id , event ){
  AJAX( "../ws/ws.generic.php" , {
    "action"   : "toggleStatus" , 
    "nodeid"   : id 
  })
  .then( ( response ) => {
    console.log( response );

    return AJAX( "../ws/ws.generic.php" , { "action" : "getStatus" , "nodeid" : id });
  })
  .then( ( responseb ) => {
    let myButton = document.getElementById( "btn-toggle-station-" + id );
    console.log( id );
    // console.log( responseb );
    // console.log( responseb[ "state" ] );
    if( parseInt( responseb[ 0 ][ "state" ] ) <= 0 ){
       myButton.innerHTML = '<i style="font-size:1.5rem" class="fa-solid fa-toggle-off"></i>';
    }
    else{
       myButton.innerHTML = '<i style="font-size:1.5rem" class="fa-solid fa-toggle-on"></i>';
    }
  })
  .catch( ( error ) => {
    console.log( error );
  });
}

/* Data Grid */

function getHeadersFromResponse( dataset ){
  let headers = [];
  if( dataset.hasOwnProperty( "measurements" ) && dataset.measurements.hasOwnProperty( "data" ) ){
    Object.keys( dataset.measurements.data ).forEach( ( sensorName ) => {
      headers.push( { "column" : sensorName , "locale" : sensorName , "type" : "string" } );
    });
  }

  if( dataset.hasOwnProperty( "analytics" ) && dataset.measurements.hasOwnProperty( "data" ) ){
    Object.keys( dataset.analytics.data ).forEach( ( sensorName ) => {
      headers.push( { "column" : sensorName , "locale" : sensorName , "type" : "string" } );
    });
  }
  return headers;
}

function getGrid( viewName , targetContainerID , dataSet , response ){
  
  // let myHeaders = false;
  // if( typeof response !== "undefined" )
      // myHeaders = getHeadersFromResponse( response );
  
  // console.log( myHeaders );

  store[ "grid_obj" ] = new amGrid({
   "container"      : targetContainerID, 
   "uniqueRowId"    : viewName + "_id",
   "uniqueGridId"   : viewName + "-view",
   "dispatcherPath" : "../ws/ws.grid.php", 
   "exports"        : [ "excel" , "pdf" , "csv" , "copy" , "print" ],
   "customAJAX"     : {
     "entity"     : dataSet , 
     "locationid" : store[ "location_id" ] , 
     "startDate"  : store[ "startDate" ] , 
     "endDate"    : store[ "endDate" ] 
   },
   "processtype"    : "client",
   "orderby"        : { "column" : 0 , "dir" : "desc" } , 
   "searchBox"      : false,
   "CRUD"           : false,
   "infoBox"        : false,
   "rowselect"      : "single",
   "scrollable"     : false,
   "height"         : ( getViewDimensions( "height" ) / 4 ) + "px",
   "collapse"       : false,
   "onrender"       : () => { console.log( "Grid Rendered" ); }
  });
}

function setGridContainer( viewContainerID , viewName ){
 document.getElementById( viewContainerID ).innerHTML = "";
 document.getElementById( viewContainerID ).insertAdjacentHTML( "afterBegin" , "<div id='"+viewName+"-grid-container'></div>" );
}

function setGraphContainer( viewContainerID , viewName ){
 document.getElementById( viewContainerID ).innerHTML = "";
 document.getElementById( viewContainerID ).insertAdjacentHTML( "afterBegin" , "<div id='"+viewName+"-graph-container'></div>" );
}

function setToolsContainer( viewContainerID , viewName ){
 // document.getElementById( viewContainerID ).innerHTML = "";
 document.getElementById( viewContainerID ).insertAdjacentHTML( "afterBegin" , "<div id='"+viewName+"-tool-container' style='margin-bottom: 10px;' class='row'></div>" );
}

function setDefaultDates(){
 if( typeof store[ "startDate" ] === 'undefined' ){
  let cTS               = new Date().getTime();
  let daysBack          = 7;
  store[ "startDate" ]  = getDate( ( cTS - ( daysBack * 24 * 60 * 60 * 1000 ) ) );
  store[ "endDate" ]    = getDate( cTS );
  store[ "yearfilter" ] = new Date().getFullYear();
 }
}

/* Rest of Functions for Graphs/Grid */

function getPeriodSearchHTML( containerId , viewName , defaults , hasGrid , showPeriodSelection , customFields ){

 function getHTML_CommonAttributes( options ){
  var myIdentifier = options.identifier !== "" ? "-" + options.identifier : "";
  var myInputID    = options.container + '-' + options.title + myIdentifier;
  var myDataToggle = options.dataToggle !== "" ? " data-toggle='"+options.dataToggle+"'" : "";
  var myDataTarget = options.dataTarget !== "" ? " data-target='#"+options.dataTarget+"'" : " data-target='#"+myInputID+"'";
  return 'class="form-control '+options.cssclass+'" ' + 
         'id="' + myInputID + '" ' + 
         myDataToggle + 
         myDataTarget + 
         'placeholder="' + options.defaultValue + '" ' + 
         'aria-label="' + options.title + '" ' + 
         'aria-describedby="' + options.title + '" ' + 
         'value="' + options.defaultValue + '" ';
 }

 let myViewContainer   = document.getElementById( containerId );
 let today             = new Date();
 let myPrefix          = "period-box-"+viewName;
 let fromTitle         = "fromDate";
 let myFromID          = myPrefix + "_tbl-" + fromTitle + "-date";
 let toTitle           = "toDate";
 let myToID            = myPrefix + "_tbl-" + toTitle + "-date";
 let myInputStartDate  = typeof defaults !== 'undefined' ? defaults.startDate : "From";
 let myInputEndDate    = typeof defaults !== 'undefined' ? defaults.endDate : "To";
 let myHTML            =  '<div class="col-md-12" id="my-period-container-'+viewName+'">';

 myHTML += '<div class="row justify-content-end">';

 myHTML += '<div class="row" style="padding: 5px;">';
 myHTML +=  '<div class="col-md-4">' + 
              '<div class="input-group mb-3" style="margin-bottom: 0 !important;">' + 
               '<div class="input-group-prepend">' + 
                 '<span class="input-group-text" id="' + fromTitle + '">' + 
                   '<i class="far fa-calendar-alt"></i>' + 
                 '</span>' + 
               '</div>' + 
               '<input type="text" ' + 
                 getHTML_CommonAttributes( {
                   "title"        : fromTitle , 
                   "identifier"   : "start-date" , 
                   "cssclass"     : "datetimepicker-input" , 
                   "dataToggle"   : "datetimepicker" , 
                   "dataTarget"   : "" , 
                   "defaultValue" : myInputStartDate,
                   "container"    : myPrefix 
                 } ) + 
               '>' + 
              '</div>' + 
            '</div>' + 
            '<div class="col-md-4">' + 
             '<div class="input-group mb-3" style="margin-bottom: 0 !important;">' + 
              '<div class="input-group-prepend">' + 
                '<span class="input-group-text" id="' + toTitle + '">' + 
                  '<i class="far fa-calendar-alt"></i>' + 
                '</span>' + 
              '</div>' + 
              '<input type="text" ' + 
                getHTML_CommonAttributes( {
                  "title"        : toTitle , 
                  "identifier"   : "end-date" , 
                  "cssclass"     : "datetimepicker-input" , 
                  "dataToggle"   : "datetimepicker" , 
                  "dataTarget"   : "" , 
                  "defaultValue" : myInputEndDate,
                  "container"    : myPrefix 
                } ) + 
              '>' + 
             '</div>' + 
            '</div>';

 myHTML += '<div class="col-md-4">' + 
             '<button type="button" class="btn btn-primary btn-sm" style="margin:0; width: 100%; height: 100%;" id="mySearchButtonDates-'+viewName+'">'+lang("search") + '</button>' + 
           '</div>' + 
          '</div>';

 myViewContainer.insertAdjacentHTML( "beforeEnd" , myHTML );

 if( typeof customFields !== "undefined" ){
   customFields.forEach( ( item ) => {
     document.getElementById( myPrefix + "-" + item[ "title" ] + "-" + item[ "id" ] )
             .addEventListener( "keyup" , ( e ) => {
               store[ item[ "title" ] ] = e.target.value;
             });

     document.getElementById( myPrefix + "-" + item[ "title" ] + "-" + item[ "id" ] )
             .addEventListener( "change" , ( e ) => {
               store[ item[ "title" ] ] = e.target.value;
             });
   });
 }

 jQuery('#' + myPrefix + "-" + fromTitle + '-start-date')
  .datetimepicker(
   { 'format': 'YYYY-MM-DD' , "sideBySide" : false , "showToday" : true }
  );

 jQuery('#' + myPrefix + "-" + toTitle + '-end-date')
  .datetimepicker(
   { 'format': 'YYYY-MM-DD' , "sideBySide" : false , "showToday" : true , "maxDate" : today }
  );

 jQuery('#' + myPrefix + "-" + fromTitle + '-start-date')
       .on( "change.datetimepicker" , function ( e ) {
        store[ "startDate" ] = e.date.format("YYYY-MM-DD");
       });

 jQuery('#' + myPrefix + "-" + toTitle + '-end-date')
       .on( "change.datetimepicker" , function ( e ) {
         store[ "endDate" ] = e.date.format("YYYY-MM-DD");
       });

 if( typeof defaults !== "undefined" ){
   store[ "startDate" ] = defaults.startDate;
   store[ "endDate" ]   = defaults.endDate;
   Object.keys( defaults ).forEach( ( defaultProperty ) => {
     store[ defaultProperty ] = defaults[ defaultProperty ];
   });
 }

 document.getElementById( 'mySearchButtonDates-'+viewName )
         .addEventListener( "click" , ( event ) => {
            console.log( store );
            showNodeGraph( { "data" : { "id" : store[ "nodeid" ] } } );
         });

}

function getListSearchHTML( containerId , viewName , defaults , hasGrid , showListSelection ){

 function getHTML_CommonAttributes( options ){
  var myIdentifier = options.identifier !== "" ? "-" + options.identifier : "";
  var myInputID    = options.container + '-' + options.title + myIdentifier;
  return 'class="'+options.cssclass+'" ' + 
         'id="' + myInputID + '" ' + 
         'value="' + options.defaultValue + '" ';
 }

 if( document.getElementById( "my-period-container-"+viewName ) ) return;

 let myViewContainer   = document.getElementById( containerId );
 let today             = new Date();
 let myPrefix          = "period-box-"+viewName;
 let aTitle            = "yearfilter";
 let myInputYearFilter = typeof defaults !== 'undefined' ? defaults.yearfilter : "From";
 let myHTML =  '<div class="col-md-8" id="my-period-container-'+viewName+'">' + 
                 '<div class="row">';

 if( showListSelection ){
     myHTML +=  '<div class="col-md-8">' + 
                  '<div class="input-group mb-3" style="margin-bottom: 0 !important;">' + 
                   '<div class="input-group-prepend">' + 
                     '<span class="input-group-text" id="' + aTitle + '">' + 
                       '<i class="far fa-calendar-alt"></i>' + 
                     '</span>' + 
                   '</div>' + 
                   '<select ' + 
                     getHTML_CommonAttributes( {
                       "title"        : aTitle , 
                       "identifier"   : "yearfilter" , 
                       "cssclass"     : "custom-select" , 
                       "defaultValue" : myInputYearFilter,
                       "container"    : myPrefix 
                     } ) + 
                   '>' + 
                     '<option value="2020">2020</option>' +  
                     '<option value="2021">2021</option>' +  
                     '<option value="2022">2022</option>' +  
                     '<option value="2023">2023</option>' +  
                   '</select>' + 
                  '</div>' + 
                '</div>';
 }

 if( showListSelection ){
        myHTML += '<div class="col-md-4">' + 
                    '<button type="button" class="btn btn-primary btn-sm" style="margin:0; width: 100%; height: 100%;" id="mySearchButtonDates-'+viewName+'">'+lang("Search") + '</button>' + 
                  '</div>' + 
                 '</div>' + 
               '</div>';
 }
 else{
        myHTML += '<div class="col-md-4">' + 
                    '<button type="button" class="btn btn-primary btn-sm" style="margin:0; width: 100%; height: 38px; margin-right: 10px;" id="mySearchButtonDates-'+viewName+'">'+lang("Search") + '</button>' + 
                  '</div>' + 
                 '</div>' + 
               '</div>';
 }

 myViewContainer.insertAdjacentHTML( "beforeEnd" , myHTML );

 if( showListSelection ){

    if( typeof defaults !== "undefined" ){
      store[ "yearfilter" ] = defaults.yearfilter;
      document.getElementById( myPrefix + "-" + aTitle + '-' + aTitle ).value = defaults.yearfilter;;
    }

    document.getElementById( myPrefix + "-" + aTitle + '-' + aTitle )
            .addEventListener( "change" , ( event ) => {
              store[ "yearfilter" ] = event.target.value;
            });

    document.getElementById( 'mySearchButtonDates-'+viewName )
            .addEventListener( "click" , ( event ) => {
              if( typeof store[ "parcel_id" ] === 'undefined' ) { getAlert( lang( "No Location" ) );      return; }
              if( typeof store[ "yearfilter" ]  === 'undefined' ) { getAlert( lang( "No Year Selected" ) ); return; }

              store[ "startDate" ] = store[ "yearfilter" ] + "-01-01";
              store[ "endDate" ]   = store[ "yearfilter" ] + "-12-31";
              let myViewData = new ViewData();
              myViewData.getData( viewName , hasGrid );
            });
 }

}

function getDatasources(){
  let myMainContainer = document.getElementById( "datasources-form-main" );
  let mySubContainer  = document.getElementById( "datasources-form-sub" );

  myDSView = new Datasources();
  myDSView.setContainer( myMainContainer );
  console.log( myDSView.getContainer() );
  myDSView
      .getSavedCredentials()
      .then( ( response ) => {
         let username = "";
         let password = "";
         if( response.hasOwnProperty( "error" ) ){
           console.log( response.error );
         }
         else{
          if( response.length > 0 ){
            username = response[ 0 ].api_username;
            password = response[ 0 ].api_password;
          }
         }

         myDSView.getContainer().innerHTML = "";
         myDSView.getContainer().insertAdjacentHTML( "afterBegin" , myDSView.getAuthForm( username , password ) );
         // myDSView.getContainer().insertAdjacentHTML( "beforeEnd" , myDSView.getDatasets() );
         myDSView.assignListeners();
      });
}

function getDataView( viewName , dataSet , hasGrid ){
   let graphTargetElementId = viewName + "-form-main";
   let gridTargetElementId  = viewName + "-form-sub";

   setGraphContainer(  graphTargetElementId , viewName );
   setGridContainer(   gridTargetElementId  , viewName );
   setToolsContainer(  graphTargetElementId , viewName );

   getAutoSearch( viewName + '-tool-container' , store[ "parcel_id" ] );

   setDefaultDates();

   if( viewName === "station" )
     getPeriodSearchHTML( 
        viewName+'-tool-container' , 
        viewName , 
        { 
          "startDate" : store[ "startDate" ] , 
          "endDate"   : store[ "endDate" ] 
        } , 
        hasGrid ,
        true 
     );

   setAmAutoComplete( {
    "url"      : "../ws/ws.generic.php?action=getStations&value=" , 
    "path"     : "data" , 
    "return"   : "parcel_id",
    "minLen"   : "0",
    "selector" : ".LocationAutoComplete" , 
    "target"   : viewName+"-tool-container-search-input",
    "callback" : ( res ) => {
      console.log( res );

      store[ "station_id" ] = res.id;
      let myViewData = new ViewData();
      myViewData.getData( viewName , hasGrid );
    }
   } );

}

function getCustomDate(amDate , type , isObject){
 var curMonth,curSeconds,curMinutes,curHours,curDayDate,curDate,cdts;

 if(typeof amDate === 'undefined' || amDate === null)
  cdts = new Date();
 else
  cdts = new Date(amDate);

 switch(type){
   case 'db' : 
     curMonth = (cdts.getUTCMonth() + 1) < 10 ? "0" + (cdts.getUTCMonth() + 1) : cdts.getUTCMonth() + 1;
     curSeconds = (cdts.getUTCSeconds() < 10) ? "0" + cdts.getUTCSeconds() : cdts.getUTCSeconds();
     curMinutes = (cdts.getUTCMinutes() < 10) ? "0" + cdts.getUTCMinutes() : cdts.getUTCMinutes();
     curHours =  (cdts.getUTCHours() < 10) ? "0" + cdts.getUTCHours() : cdts.getUTCHours();
     curDayDate =  (cdts.getUTCDate() < 10) ? "0" + cdts.getUTCDate() : cdts.getUTCDate();
     curDate = curDayDate +"-"+ curMonth +"-"+ cdts.getFullYear() +" "+ curHours +":"+ curMinutes +":"+ curSeconds;
   break;
   case 'graphs' : 
     curMonth = (cdts.getUTCMonth() + 1) < 10 ? "0" + (cdts.getUTCMonth() + 1) : cdts.getUTCMonth() + 1;
     curSeconds = (cdts.getUTCSeconds() < 10) ? "0" + cdts.getUTCSeconds() : cdts.getUTCSeconds();
     curMinutes = (cdts.getUTCMinutes() < 10) ? "0" + cdts.getUTCMinutes() : cdts.getUTCMinutes();
     curHours =  (cdts.getUTCHours() < 10) ? "0" + cdts.getUTCHours() : cdts.getUTCHours();
     curDayDate =  (cdts.getUTCDate() < 10) ? "0" + cdts.getUTCDate() : cdts.getUTCDate();
     curDate = cdts.getFullYear() + "-" + curMonth +"-"+ curDayDate +" "+ curHours +":"+ curMinutes +":"+ curSeconds;
   break;
 }

 if(typeof isObject === 'undefined'){
  return curDate;
 }else{
   if(isObject === true)
    return new Date(curDate);
   else
    return curDate;
 }
}

function getFloatingTooltip( myHighChartObject , height ){
  let myXValue      = myHighChartObject.x;
  let myYValue      = myHighChartObject.y;
  let curDate       = getCustomDate( myXValue , 'db' );
  let myHTMLTooltip = "<div style='max-width:500px !important; max-height:500px !important; '>" + 
                      "<div>" + curDate + "</div><hr>";

  if( !myYValue && myYValue !== 0 ){
   if( myHighChartObject.point )
    if( myHighChartObject.point.text ){
     Object.keys( myHighChartObject.point.text[ 0 ] ).forEach( ( aProperty ) => {
      let myPropertyName  = aProperty;
      let myPropertyValue = myHighChartObject.point.text[ 0 ][ aProperty ];
      let mySeriesColor   = myHighChartObject.series.color;
      myHTMLTooltip += "<div>" + 
                         "<span style='font-size: 12px;color:" + mySeriesColor + "'>" + myPropertyName + "</span> " + 
                         " : " + 
                         "<span style='font-size: 12px;'>" + myPropertyValue + "</span>" + 
                       "</div>";
     });
    }
  }
  else{
     myHighChartObject.points.forEach( function( myPoint , idx ){
       let mySeriesColor   = myPoint.series.color;
       let mySeriesName    = myPoint.series.name;
       let mySeriesValue   = parseFloat( myPoint.y ).toFixed( 2 );

       myHTMLTooltip += "<div>" + 
                          "<span style='font-size: 12px;color:" + mySeriesColor + "'>" + mySeriesName + "</span> " + 
                          " : " + 
                          "<span style='font-size: 12px;'>" + mySeriesValue + "</span>" + 
                        "</div>";
     });
  }

  myHTMLTooltip += "</div>";

  return myHTMLTooltip;
}

function getHeightOfContainer( graphUniqueID ){
  var ContainerHeight = document.querySelector("#"+graphUniqueID+" .highcharts-plot-background");
  return ( ContainerHeight.getAttribute("height") / 1.5 );
}

function printGraph( mySeries , elementID , seriesType , myGraphOptions ){
  var myGraphContainer = (typeof elementID === 'undefined') ? 'graph' : elementID;
  
  store[ "graphOptions" ] = myGraphOptions;

  document.getElementById( myGraphContainer ).style.height = "60%";
  let myPlotLines = [];

  if( seriesType == "meteo" ){
   myPlotLines = [{
             color     : '#FF0000',
             dashStyle : "ShortDash",
             width     : 2,
             value     : 0
         }];
  }
  
  myChartObject = Highcharts.stockChart(myGraphContainer, {
      credits : {
       enabled : false
      },
      legend: {
          enabled: true
      },
      tooltip: {
       useHTML: true,
       formatter: function () {
        return getFloatingTooltip( this , getHeightOfContainer( myGraphContainer ));
       }
      },
      plotOptions: {
        series: {
          dataGrouping: {
            enabled: false
          }
        }
      },
      chart           : { type: 'spline' , zoomType: 'x' , backgroundColor : 'rgba(255,255,255,0)'},
      title           : { text: '' },
      xAxis           : { type: 'datetime' },
      yAxis           : { plotLines : myPlotLines },
      navigator       : { enabled : false },
      rangeSelector   : {
       enabled      : false,
       inputEnabled : false,
       selected     : 1 , 
       buttons: [{
           type  : 'all',
           text  : 'All',
           title : 'View all'
       }]
      },
      series          : mySeries
  });

  // Set PlotLines and PlotBands
     updatePlotInGraph();

  // Set the Buttons that appear on top of the Graph
     if( typeof seriesType !== 'undefined' ){
      if( seriesType == "irrigation" ){
       document.getElementById( myGraphContainer ).insertAdjacentHTML( "afterBegin" , 
         "<div style='position: absolute;z-index: 1;'>" + 
          "<table>" + 
           "<tr>" + 
            "<td><button type='button' style='margin:0;padding: 0.2rem 1.6rem;' class='btn btn-light btn-sm' onClick='turnSeriesOff();'><div style='position:relative;width:100%;float:left;'><i class='fas fa-eye-slash'></i></div><div style='position:relative;width:100%;float:left;'>"+lang("off")+"</div></button></td>" + 
            "<td><button type='button' style='margin:0;padding: 0.2rem 1.6rem;' class='btn btn-light btn-sm' onClick='turnSeriesOn();'><div style='position:relative;width:100%;float:left;'><i class='fas fa-eye'></i></div><div style='position:relative;width:100%;float:left;'>"+lang("on")+"</div></button></td>" + 
            "<td><button type='button' style='margin:0;padding: 0.2rem 1.6rem;' class='btn btn-light btn-sm' onClick='showSeriesGroup( \"salinity\" );'><div style='position:relative;width:100%;float:left;'><i class='fas fa-vial'></i></div><div style='position:relative;width:100%;float:left;'>"+lang("salinity")+"</div></button></td>" + 
            "<td><button type='button' style='margin:0;padding: 0.2rem 1.6rem;' class='btn btn-light btn-sm' onClick='showSeriesGroup( \"rh_soil\" );'><div style='position:relative;width:100%;float:left;'><i class='fas fa-tint'></i></div><div style='position:relative;width:100%;float:left;'>"+lang("rh_soil")+"</div></button></td>" + 
           "</tr>" + 
          "</table>" + 
         "</div>"
       );
      }
      else if( seriesType == "irrigation_analytics" ){
       document.getElementById( myGraphContainer ).insertAdjacentHTML( "afterBegin" , 
         "<div style='position: absolute;z-index: 1;'>" + 
          "<table>" + 
           "<tr>" + 
            "<td><button type='button' style='margin:0;padding: 0.2rem 1.6rem;' class='btn btn-light btn-sm' onClick='turnSeriesOff();'><div style='position:relative;width:100%;float:left;'><i class='fas fa-eye-slash'></i></div><div style='position:relative;width:100%;float:left;'>"+lang("off")+"</div></button></td>" + 
            "<td><button type='button' style='margin:0;padding: 0.2rem 1.6rem;' class='btn btn-light btn-sm' onClick='turnSeriesOn();'><div style='position:relative;width:100%;float:left;'><i class='fas fa-eye'></i></div><div style='position:relative;width:100%;float:left;'>"+lang("on")+"</div></button></td>" + 
            "<td><button type='button' style='margin:0;padding: 0.2rem 1.6rem;' class='btn btn-light btn-sm' onClick='showSeriesGroup( \"salinity\" );'><div style='position:relative;width:100%;float:left;'><i class='fas fa-vial'></i></div><div style='position:relative;width:100%;float:left;'>"+lang("salinity")+"</div></button></td>" + 
            "<td><button type='button' style='margin:0;padding: 0.2rem 1.6rem;' class='btn btn-light btn-sm' onClick='showSeriesGroup( \"rh_soil\" );'><div style='position:relative;width:100%;float:left;'><i class='fas fa-tint'></i></div><div style='position:relative;width:100%;float:left;'>"+lang("rh_soil")+"</div></button></td>" + 
            "<td><button type='button' style='margin:0;padding: 0.2rem 1.6rem;height: 32px;' class='btn btn-light btn-sm' onClick='setGraphSetting();'><div style='position:relative;width:100%;float:left;'></div><div style='position:relative;width:100%;float:left;'>"+lang("graph_bands")+"</div></button></td>" + 
           "</tr>" + 
          "</table>" + 
         "</div>"
       );
      }
     }
}

function getParameterList( type ){
 let myParameterList = [];

 switch( type ){
  case "rh_soil" : 
   myParameterList = [ 
    LANG_dnd_30_sdi12_sm_10,
    LANG_dnd_30_sdi12_sm_20,
    LANG_dnd_30_sdi12_sm_30,
    LANG_dnd_60_sdi12_sm_10,
    LANG_dnd_60_sdi12_sm_20,
    LANG_dnd_60_sdi12_sm_30,
    LANG_dnd_60_sdi12_sm_40,
    LANG_dnd_60_sdi12_sm_50,
    LANG_dnd_60_sdi12_sm_60,
    LANG_dnd_90_sdi12_sm_10,
    LANG_dnd_90_sdi12_sm_20,
    LANG_dnd_90_sdi12_sm_30,
    LANG_dnd_90_sdi12_sm_40,
    LANG_dnd_90_sdi12_sm_50,
    LANG_dnd_90_sdi12_sm_60,
    LANG_dnd_90_sdi12_sm_70,
    LANG_dnd_90_sdi12_sm_80,
    LANG_dnd_90_sdi12_sm_90,
    LANG_enviro_sdi12_sm_10, 
    LANG_enviro_sdi12_sm_20, 
    LANG_enviro_sdi12_sm_30, 
    LANG_enviro_sdi12_sm_40, 
    LANG_enviro_sdi12_sm_50, 
    LANG_enviro_sdi12_sm_60, 
    LANG_enviro_sdi12_sm_70, 
    LANG_enviro_sdi12_sm_80, 
    LANG_enviro_sdi12_sm_90 
   ];
  break;
  case "salinity" : 
   myParameterList = [ 
    LANG_dnd_30_sdi12_sal_10,
    LANG_dnd_30_sdi12_sal_20,  
    LANG_dnd_30_sdi12_sal_30,  
    LANG_dnd_60_sdi12_sal_10,  
    LANG_dnd_60_sdi12_sal_20,  
    LANG_dnd_60_sdi12_sal_30,  
    LANG_dnd_60_sdi12_sal_40,  
    LANG_dnd_60_sdi12_sal_50,  
    LANG_dnd_60_sdi12_sal_60,  
    LANG_dnd_90_sdi12_sal_10,  
    LANG_dnd_90_sdi12_sal_20,  
    LANG_dnd_90_sdi12_sal_30,  
    LANG_dnd_90_sdi12_sal_40,  
    LANG_dnd_90_sdi12_sal_50,  
    LANG_dnd_90_sdi12_sal_60,  
    LANG_dnd_90_sdi12_sal_70,  
    LANG_dnd_90_sdi12_sal_80,  
    LANG_dnd_90_sdi12_sal_90,
    LANG_enviro_sdi12_sal_10,
    LANG_enviro_sdi12_sal_20,
    LANG_enviro_sdi12_sal_30,
    LANG_enviro_sdi12_sal_40,
    LANG_enviro_sdi12_sal_50,
    LANG_enviro_sdi12_sal_60,
    LANG_enviro_sdi12_sal_70,
    LANG_enviro_sdi12_sal_80,
    LANG_enviro_sdi12_sal_90
  ];
  break;
  case "atmo" : 
   myParameterList = [ LANG_wsht30_temp ];
  break;
 }
 
 return myParameterList;

}

function turnSeriesOff(){
 myChartObject.series.forEach( function( series ){
  series.setVisible( false, false );
 });
}

function turnSeriesOn(){
 myChartObject.series.forEach( function( series ){
  series.setVisible( true, false );
 });
}

function showSeriesGroup( group ){
 let loaderContainer   = "analytics_soil_moisture-form-main";
 let containerText     = "loader-container-v2-text";
 let myEligibleSensors = getParameterList( group );
 toggleLoaderV2( loaderContainer );

 turnSeriesOff();

 setTimeout( () => {

  for( i=0; i<myChartObject.series.length; i++ ){
   let mySeriesName = myChartObject.series[ i ].name;
   if( myEligibleSensors.includes( mySeriesName ) ){
    myChartObject.series[ i ].setVisible( true, true );
    let myTextContainer = document.getElementById( "loader-container-v2-text" );
    if( myTextContainer ){
      myTextContainer.innerHTML = mySeriesName;
      myTextContainer.innerText = mySeriesName;
    }
   }
  }

  toggleLoaderV2( loaderContainer );

 } , 100 );
}

function updatePlotInGraph(){

  if( myChartObject && myChartObject.hasOwnProperty( "yAxis" ) ){
    myChartObject.yAxis[ 0 ].removePlotLine( "high_limit" );
    myChartObject.yAxis[ 0 ].removePlotLine( "low_limit" );
    myChartObject.yAxis[ 0 ].removePlotBand( "gap" );
  }

  if( store.hasOwnProperty( "graphOptions" ) && typeof store.graphOptions !== "undefined" ){
    Object.keys( store.graphOptions ).forEach( ( graphName ) => {
      if( graphName == "analytics" ){
        if( store.graphOptions[ graphName ].hasOwnProperty( "plotLines" ) ){
          store.graphOptions[ graphName ].plotLines.forEach( ( plotLine ) => {
            plotLine[ "label" ][ "text" ] = lang( plotLine[ "label" ][ "text" ] );
            myChartObject.yAxis[ 0 ].addPlotLine( plotLine );
          });
        }

        if( store.graphOptions[ graphName ].hasOwnProperty( "plotBands" ) ){
          store.graphOptions[ graphName ].plotBands.forEach( ( plotBand ) => {
            myChartObject.yAxis[ 0 ].addPlotBand( plotBand );
          });
        }
      }
    });
  }
}

function setGraphSetting(){
   let settingsHTMLForm = "<form>" + 
                            "<div class='container'>" + 
                              "<div class='row'>" + 
                                "<div class='col-12'>" + 
                                  '<div class="input-group">' + 
                                    '<div class="input-group-prepend">' + 
                                      '<span style="width: 150px;" class="input-group-text">' + lang( "high_limit" ) + '</span>' + 
                                    '</div>' + 
                                    '<input class="form-control" id="graph_setting_high_limit" name="graph_setting_high_limit" aria-label="High Limit"></input>' + 
                                  '</div>' + 
                                '</div>' + 
                              "</div>" + 
                              "<div class='row'>" + 
                                "<div class='col-12'>" + 
                                  '<div class="input-group">' + 
                                    '<div class="input-group-prepend">' + 
                                      '<span style="width: 150px;" class="input-group-text">' + lang( "low_limit" ) + '</span>' + 
                                    '</div>' + 
                                    '<input class="form-control" id="graph_setting_low_limit" name="graph_setting_low_limit" aria-label="Low Limit"></input>' + 
                                  '</div>' + 
                                '</div>' + 
                              "</div>" + 
                            "</div>" + 
                          "</form>";
   getModal({
     "title"        : lang( "Graph Settings" ),
     "body"         : settingsHTMLForm , 
     "acceptButton" : "Delete",
     "cancelButton" : "Cancel",
     "action" : {
       "submit" : ( ev ) => {
         if( ev.hasOwnProperty( "form" ) && ev.values.length > 0 ){
           AJAX( "../ws/ws.graph.php" , {
             "action" : "setGraphSettings" , 
             "form"   : ev.values
           })
           .then( ( response ) => {
             let userGraphSettings   = JSON.parse( response[ "res" ][ 0 ][ "setting" ] );
             store[ "graphOptions" ] = userGraphSettings[ "graph" ];
             updatePlotInGraph();
           }).
           catch( ( error ) => {
             console.log( error );
           });
         }
         else{
           console.log( "something is missing" );
         }
       }
     }
   } , () => {
     console.log( "Settings Loaded" );
   });
}

/* Control Panel */

function getControlPanel( container ){
  let myContainer = document.getElementById( container + '-form-main' );
  myContainer.innerHTML = "";
  initControlPanel( myContainer.id );
}
