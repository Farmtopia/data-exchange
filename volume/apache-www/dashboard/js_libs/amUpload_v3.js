class amUpload {
	constructor(options) {
		console.log(
			"Class amUpload Loaded.\n" +
				"Arguments : endpoint , dropzone , onProgress , onSelect , onSuccess , onHover , onClick \n"
		);

		this.endpoint      = options.endpoint    || false;
		this.params        = options.params      || false;
		this.onProgress    = options.onProgress  || false;
		this.onSuccess     = options.onSuccess   || false;
		this.onHover       = options.onHover     || false;
		this.onClick       = options.onClick     || false;
		this.onSelect      = options.onSelect    || false;
  this.onSucces      = options.onSucces    || false;
  this.onError       = options.onError     || false;
  this.fileInputName = options.inputName   || "fileElem";
		this.dropzone      = document.getElementById(options.dropzone) || false;
		this.filesArray    = new Array();

		this.setHiddenFileInput();
		this.setEventListeners();
	}

	setHiddenFileInput() {
		const myFileInput = document.getElementById( this.fileInputName );
		if (!myFileInput)
			this.dropzone.insertAdjacentHTML(
				"beforeBegin",
				'<input type="file" name="'+this.fileInputName+'" id="'+this.fileInputName+'" multiple accept="*/*" style="display:none;">'
			);
	}

	setEventListeners() {
  // console.log( "ondragover is set to : " , this.dropzone.ondragover );
  // console.log( "ondragenter is set to : " , this.dropzone.ondragenter );
  // console.log( "ondragleave is set to : " , this.dropzone.ondragleave );
  // console.log( "ondrop is set to : " , this.dropzone.ondrop );
  // console.log( "onclick is set to : " , this.dropzone.onclick );

		if (typeof this.dropzone.ondragover !== "undefined")
			this.dropzone.ondragover = (event) => {
				event.stopPropagation();
				event.preventDefault();
			};

		if (typeof this.dropzone.ondragenter !== "undefined")
			this.dropzone.ondragenter = (event) => {
				this.onHover({
					event: event,
					dropzone: this.dropzone,
					isover: true,
				});
			};

		if (typeof this.dropzone.ondragleave !== "undefined")
			this.dropzone.ondragleave = (event) => {
				this.onHover({
					event: event,
					dropzone: this.dropzone,
					isover: false,
				});
			};

		if (typeof this.dropzone.ondrop !== "undefined")
			this.dropzone.ondrop = (event) => {
    console.log("Drop event Triggered ");
				event.stopPropagation();
				event.preventDefault();

				this.filesArray = event.dataTransfer.files;

				if (typeof this.onSelect === "function")
					this.onSelect( event.dataTransfer.files );
    else
     for (let i=0; i<event.dataTransfer.files.length; i++){
      this.uploadFile( event.dataTransfer.files[i] )
      .then( res => {

       if( typeof res.response.error !== 'undefined' ){
        if( typeof this.onError === "function" )
         this.onError( res.response.error );
        else
         console.log( "[ Uploader Error ] : " , res );
       }
       else{
        if( typeof this.onSuccess === "function" )
         this.onSuccess( res );
        else
         console.log( "[ Uploader Success ] : " , res );
       }
      })
      .catch( error => {
       if( typeof this.onError === "function" ){
        this.onError( error );
       }
       else{
        console.log( "[ Uploader Error ] : " , error );
       }
      });

     }
			};

		if (typeof this.dropzone.onclick !== "undefined") {
			var fileElem = document.getElementById( this.fileInputName );

			this.dropzone.onclick = (event) => {
				event.stopPropagation();
				event.preventDefault();
				fileElem.click();
			};

			fileElem.addEventListener("change", (event) => {
    console.log("Change event Triggered ");
    this.filesArray = fileElem.files;

				if (typeof this.onSelect === "function")
					this.onSelect(fileElem.files);
    else
     for (let i=0; i<fileElem.files.length; i++){
      this.uploadFile( fileElem.files[i] )
      .then( res => {

       if( typeof res.response.error !== 'undefined' ){
        if( typeof this.onError === "function" )
         this.onError( res.response.error );
        else
         console.log( "[ Uploader Error ] : " , res );
       }
       else{
        if( typeof this.onSuccess === "function" )
         this.onSuccess( res );
        else
         console.log( "[ Uploader Success ] : " , res );
       }
      })
      .catch( error => {
       if( typeof this.onError === "function" ){
        this.onError( error );
       }
       else{
        console.log( "[ Uploader Error ] : " , error );
       }
      });

     }
			});
		}
	}

	uploadFile(file) {
		return new Promise((resolve, reject) => {
			const xhr = new XMLHttpRequest();
			const fd  = new FormData();

			xhr.open("POST", this.endpoint, true);

			xhr.upload.onprogress = (e) => {
				if (typeof this.onProgress === "function")
					this.onProgress({
						percentage: Math.round(100 / (e.total / e.loaded)),
						total: e.total,
						loaded: e.loaded,
					});
			};

			xhr.onreadystatechange = () => {
				if (xhr.readyState == 4 && xhr.status == 200) {
					resolve({
						response: JSON.parse(xhr.responseText),
						status: xhr.status,
      file : file
					});
				}

				if (xhr.status != 200) {
					reject({
						response: JSON.parse(xhr.responseText),
						status: xhr.status,
      file: file
					});
				}
			};

			fd.append("file", file);
			if (this.params) {
				Object.keys(this.params).forEach((param) => {
					fd.append(param, this.params[param]);
				});
			}

			xhr.send(fd);
		});
	}

	showProgress(progress) {
		if (typeof this.onProgress === "function") this.onProgress(progress);
	}

}

function showAttachmentSuccess( file ){
 console.log( file );
}

function toggleHover( dropzoneid ){
 let myContainer = document.getElementById( dropzoneid );
 let myClass     = myContainer.className;

 if( myClass.indexOf("upload-zone-inactive") > -1 ){
  myContainer.classList.remove("upload-zone-inactive");
  myContainer.classList.add("upload-zone-active");
 } 
 else if( myClass.indexOf("upload-zone-active") > -1 ){
  myContainer.classList.remove("upload-zone-active");
  myContainer.classList.add("upload-zone-inactive");
 } 
 else{
  myContainer.classList.remove("upload-zone-inactive");
  myContainer.classList.add("upload-zone-active");
 }

}
