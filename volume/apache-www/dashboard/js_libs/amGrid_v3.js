/*
1) When adding a row , it has no DeleteRow Function on it , till you refresh.
*/

class amGrid{

 constructor(options){
  this.myHeaderObj;
  this.mySearchNavID;
  this.options;
  this.myGridOptions;
  this.myGrid;
  this.selected;
  this.myForm;
  this.dataSet;

  console.log("amGrid initializing...");

  if(typeof options == 'undefined'){
   console.log("You need to provide a Configuration Object.");
   return;
  }

 // This Object is widely used by the Class. It consists of all the configuration properties needed 
 // to build our Grid. Check documentation for all the available properties.
    this.options = options;

  this.activateActionButtons = this.options.activateActionButtons === false ? false : true;

  this.init();
  return this.myGrid;
 }

 init(){

 // Set the Proccess Type. This can be either Server or Client. If Client then the whole Result will be sent to the 
 // Client and the Client will take care of Order , Searches , Pagination , etc. If it is set to Server , then everything 
 // will be processed by the server.
    this.setProcessType();

 // Clear all the DOM Elements that might already exist.
    this.resetDomElements();

 // Make a Request to the Server to get the Headers of the Grid
 // This is a Promise. When the Result returns , we proceed with 
 // creating the Grid along with one more request to get the DataSet.
    this.getHeaders()
        .then((myHeaders)=>{

       // These are the Options needed by the DataTables Library in order to create our Grid
       // In the following lines this Object will be filled with the appropriate properties.
          this.myGridOptions = new Object();

       // If there is going to be a DatePeriod Search in our Grid some changes need to take place , so i flag it.
          this.myDataPeriod  = false;

       // This is the AJAX Configuration for the Grid. 
       // It provides the Endpoint for the AJAX Request along 
       // with custom Fields , if any , that need to be sent to the server
       // Currently the "this.myDataPeriod" is one of the Custom Fields
          this.setAjaxEndpoint();

       // This is the Object with our Headers. It is widely used by the class.
       // This is an Array of Objects. This array provides the Order of the Columns in the Grid , 
       // and every element of the Array is an Object with the Header details.
          this.myHeaderObj = myHeaders;

       // Set the height and the scrollbars if a height is defined , if not then the Grid will take the whole ViewPort.
          this.setDimensions();

       // Add the Headers into the DOM. Also create the Primary Table Container.
          this.setHeaders(this.options.container , this.options.rowButtons); 

       // The Grid should be Responsive (Upon collapse it creates an expandable Sub Tree
          this.setCollapse();

       // Add Export Controls
          this.setExportControls();

       // We either have Pagination or Scrolling.
       // Scrolling is not supported in Server Side Processing.
       // Server Side Processing is done to minimize overhead. 
       // When you use Scroll you are need to send all data to the client.
       // So there is no gain in using Server Side Processing.
          this.setPagination();

       // Assign our Server Side Processing Properties */
          this.setProcessProperties();

       // Set the Buttons that will appear on the Last Column.
          this.setRowButtons();

       // Create our Custom Search Boxes in the DOM
          this.setColumnSearches();

       // Hide columns not set for display
          this.setHiddenColumns();

       // Set the order by
          this.setOrderBy();

       // Set if the Grid will have the Global Search box or not
          this.setSearchBox();

       // Set if the Grid will have the Global Search box or not
          this.setInfoBox();
       
       // Set if the drop down that allows for 5 , 10 , 15 results to be shown , will be visible or not.
          this.setLengthChange();

       // Set if the Grid will have selectable rows. When you click on them it will be highlighted.
          // this.setSelectableFlag();

       // This will be called on initilization finish.
          this.addInitCompleteFunction();

       // This will be called on initilization finish.
          if( typeof this.options.CRUD !== 'undefined' ) 
           if( this.options.CRUD == true ) 
            if( this.activateActionButtons )
             this.addCRUDButtons();

       // Set the onRender Callback
          this.setOnRender(  );

       // Initiate our Grid
          this.initiateGrid();

       /*********** Assign Listeners ***********/

       // Assign a Listener on each of our Custom Search Boxes.
          this.assignColumnSearchListeners();

       // Assign a Listener on each Row of the Table
          this.assignRowEventListener();

       // Assign a Listener on each Field of the Table
          this.assignFieldEventListener();

       // Assign a Listener on the Buttons of a Row. The Buttons at the last column.
          this.assignRowButtonsListener();

       // Assign a Listener on the Buttons of the Table. Like AddRow.
          this.assignTableActionsButtonsListener();

        })
        .catch( ( error ) => { 
         document.dispatchEvent( new CustomEvent('grid-error' , { detail: {
          "error" : error 
         } }) );

         console.log( error ) ;
        });
 
 }
 
 setOnRender(  ){
  if( typeof this.options.onrender === "function" )
    this.myGridOptions[ "initComplete" ] = (  ) => {
     this.options.onrender( this );
    }
 }
 
 setForm( form ){
  this.myForm = form;
 }

 addCRUDButtons( ){
  var myContainerHTML = "<div class='table-actions-buttons-container' id='table-actions-container-"+this.options.container+"'></div>";
  var myAddButton     = "<button togglestatus='inactive' style='position: relative; float: left;' id='add-record-"+this.options.container+"' type='button' class='btn btn-primary btn-sm rounded-0'>"+this.getIcon('add') + " " + lang( "Add" , myLanguage ) + "</button>";
  var myUpdateButton  = "<button togglestatus='inactive' style='position: relative; float: left;display:block;' id='update-record-"+this.options.container+"' type='button' class='btn btn-primary btn-sm rounded-0'>"+this.getIcon('edit') + " " + lang( "Edit" , myLanguage ) + "</button>";
  var myCancelButton  = "<button togglestatus='inactive' style='position: relative; float: left;display:none;' id='cancel-record-"+this.options.container+"' type='button' class='btn btn-danger btn-sm rounded-0'>"+this.getIcon('cancel') + " " + lang( "Cancel" , myLanguage ) + "</button>";
  let parentNode      = document.getElementById( this.options.container );
  var myContainer;

  parentNode.insertAdjacentHTML("afterBegin" , myContainerHTML);
  myContainer = document.getElementById('table-actions-container-'+this.options.container);
  myContainer.insertAdjacentHTML("afterBegin",myCancelButton);
  myContainer.insertAdjacentHTML("afterBegin",myUpdateButton);
  myContainer.insertAdjacentHTML("afterBegin",myAddButton);

  document.getElementById('cancel-record-'+this.options.container)
   .addEventListener('click' , (event)=>{
    this.toggleCRUDButton("cancel");

    console.log("clicked cancel");
   });

  document.getElementById('update-record-'+this.options.container)
   .addEventListener('click' , (event)=>{
    var ButtonStatus = document.getElementById('update-record-'+this.options.container).getAttribute("togglestatus");

    if(ButtonStatus == "active"){
     this.myForm.updateRecord()
     .then( () => {
      this.toggleCRUDButton("update");
     })
     .catch( ( error ) => console.log( error ) );
    }
    else{
     if( !this.myForm.options.data ){
       getModal({"title" : "No item selected" , "body" : "No item selected for Edit" });
      return;
     }
     else{
      this.toggleCRUDButton("update");  
     }
    }
   });

  document.getElementById('add-record-'+this.options.container)
   .addEventListener('click' , (event)=>{
    var ButtonStatus = document.getElementById('add-record-'+this.options.container).getAttribute("togglestatus");

    if(ButtonStatus == "active"){
     this.myForm.addRecord()
     .then( () => {
      this.toggleCRUDButton("add");
     })
     .catch( ( error ) => console.log( error ) );
    }
    else{
     this.toggleCRUDButton("add"); 
    }
   });
 }

 toggleCRUDButton(type){

  if( !this.activateActionButtons ) return;

  switch(type){
   case "cancel" : 
    if( typeof this.myForm !== 'undefined' ) {
     this.myForm.setReadonly(true);
     this.myForm.populate(this.myForm.formvalues);
    }

    document.getElementById('cancel-record-'+this.options.container).style.display = "none";

    document.getElementById('update-record-'+this.options.container).setAttribute("togglestatus","inactive");
    document.getElementById('update-record-'+this.options.container).className     = "btn btn-primary btn-sm rounded-0";
    document.getElementById('update-record-'+this.options.container).innerHTML     = this.getIcon('edit') + " " + lang( "Edit" , myLanguage );
    document.getElementById('update-record-'+this.options.container).style.display = "block";
    document.getElementById('update-record-'+this.options.container).blur();

    document.getElementById('add-record-'+this.options.container).setAttribute("togglestatus","inactive");
    document.getElementById('add-record-'+this.options.container).className        = "btn btn-primary btn-sm rounded-0";
    document.getElementById('add-record-'+this.options.container).innerHTML        = this.getIcon('add') + " " + lang( "Add" , myLanguage );
    document.getElementById('add-record-'+this.options.container).style.display    = "block";
    document.getElementById('add-record-'+this.options.container).blur();
   break;
   case "update":
    var ButtonStatus = document.getElementById('update-record-'+this.options.container).getAttribute("togglestatus");

    if(ButtonStatus == "inactive"){
     if( typeof this.myForm !== 'undefined' ) {
       this.myForm.setReadonly(false);
     }
     document.getElementById('update-record-'+this.options.container).setAttribute("togglestatus","active");
     document.getElementById('update-record-'+this.options.container).className     = "btn btn-warning btn-sm rounded-0";
     document.getElementById('update-record-'+this.options.container).innerHTML     = this.getIcon('save') + " " + lang( "Save" , myLanguage );
     document.getElementById('add-record-'+this.options.container).style.display    = "none";
     document.getElementById('cancel-record-'+this.options.container).style.display = "block";
     document.getElementById('cancel-record-'+this.options.container).blur();
    }
    else{
     if( typeof this.myForm !== 'undefined' ) {
       this.myForm.setReadonly(true);
       this.myForm.populate(this.myForm.formvalues);
     }
     document.getElementById('update-record-'+this.options.container).setAttribute("togglestatus","inactive");
     document.getElementById('update-record-'+this.options.container).className     = "btn btn-primary btn-sm rounded-0";
     document.getElementById('update-record-'+this.options.container).innerHTML     = this.getIcon('save') + " " + lang( "Save" , myLanguage );
     document.getElementById('add-record-'+this.options.container).style.display    = "block";
     document.getElementById('add-record-'+this.options.container).blur();
     document.getElementById('cancel-record-'+this.options.container).style.display = "none";
    }
    break;
    case "add" : 
     var ButtonStatus = document.getElementById('add-record-'+this.options.container).getAttribute("togglestatus");
     if(ButtonStatus == "inactive"){
      if( typeof this.myForm !== 'undefined' ) {
        this.myForm.setReadonly(false);
        this.myForm.resetFormFields();
      }
      document.getElementById('add-record-'+this.options.container).setAttribute("togglestatus","active");
      document.getElementById('add-record-'+this.options.container).className        = "btn btn-warning btn-sm rounded-0";
      document.getElementById('add-record-'+this.options.container).innerHTML        = this.getIcon('save') + " " + lang( "Save" , myLanguage );
      document.getElementById('update-record-'+this.options.container).style.display = "none";
      document.getElementById('cancel-record-'+this.options.container).style.display = "block";
      document.getElementById('cancel-record-'+this.options.container).blur();
     }
     else{
      if( typeof this.myForm !== 'undefined' ) {
        this.myForm.setReadonly(true);
        this.myForm.populate(this.myForm.formvalues);
      }
      document.getElementById('add-record-'+this.options.container).setAttribute("togglestatus","inactive");
      document.getElementById('add-record-'+this.options.container).className        = "btn btn-primary btn-sm rounded-0";
      document.getElementById('add-record-'+this.options.container).innerHTML        = this.getIcon('add') + " " + lang( "Add" , myLanguage );
      document.getElementById('add-record-'+this.options.container).blur();
      document.getElementById('update-record-'+this.options.container).style.display = "block";
      document.getElementById('cancel-record-'+this.options.container).style.display = "none";
     }
    break;
  }
 }

 setOrderBy(){
  if( this.options.orderby )
   this.myGridOptions.order = [[ this.options.orderby.column , this.options.orderby.dir ]];
 }

 setSearchBox(){
  if( typeof this.options.searchBox !== 'undefined' ){
   if( this.options.searchBox )
    this.myGridOptions.searching = true;
   else
    this.myGridOptions.searching = false;
  }
  else
   this.myGridOptions.searching = false;
 }

 setInfoBox(){
  if( typeof this.options.infoBox !== 'undefined' )
   this.myGridOptions.info = this.options.infoBox;
  else
   this.myGridOptions.info = false;
 }

 setLengthChange(){
  if( typeof this.options.lengthChange !== 'undefined' ){
   if( this.options.lengthChange )
    this.myGridOptions.lengthChange = true;
   else
    this.myGridOptions.lengthChange = false;
  }
  else
   this.myGridOptions.lengthChange = false;
 }

 addInitCompleteFunction(){
  if( this.options.onRender ){
   this.myGridOptions.drawCallback = ( settings ) => {
    this.options.onRender( this );
   }
  }
 }
 
 activateRow( index ){
  if( this.myGrid.rows().count() > 0 ){
   this.myGrid.row(':eq('+index+')', { page: 'current' }).node().click()
  }  
 }

 selectRow( index ){
  if( this.myGrid.rows().count() > 0 ){
   this.myGrid.row(':eq('+index+')', { page: 'current' }).select();
  }  
 }

 deselectRow( index ){
  if( this.myGrid.rows().count() > 0 ){
   this.myGrid.row(':eq('+index+')', { page: 'current' }).deselect();
  }  
 }

 resetDomElements(){
  document.getElementById(this.options.container).innerHTML = "";

  if(typeof this.options.columnsearch !== 'undefined'){
   this.mySearchNavID = this.options.uniqueGridId+'-search-nav';
   if(this.options.columnsearch.position){
    this.mySearchNavID = this.options.columnsearch.position;
    document.getElementById(this.mySearchNavID).innerHTML = "";
   }
   else{
    this.mySearchNavID = this.options.uniqueGridId+'-search-nav';
    document.getElementById(this.options.container).innerHTML = "<div id='"+this.mySearchNavID+"'></div>";
   }
  }  

  if(typeof this.options.tableActions !== 'undefined'){
   Object.keys(this.options.tableActions).forEach((item)=>{
    document.getElementById(this.options.tableActions[item].position).innerHTML = "";
   });
  }
 } 

 assignColumnSearchListeners(){
  var self  = this;
  var today = new Date();

  if(this.options.columnsearch){
   if(this.options.columnsearch.columns){

 // If a custom Search Box has been set in the Configuration 
 // then proceed in adding an Event Listener on it.
    $('#'+this.mySearchNavID+' input').each( function (idx , el) {
     var FieldTargetName  = el.id.replace(self.options.container+'_tbl-',"")
                                 .replace("-date","")
                                 .replace("-start-date","")
                                 .replace("-start","")
                                 .replace("-end","")
                                 .replace("-end-date","");
     var FieldTargetIndex = self.findHeaderIndexFromKey(FieldTargetName , "property");

     if(self.options.columnsearch.columns[ FieldTargetName ][ "type" ] != "period"){
      $(this).on( 'keyup change clear', function (event) {
       console.log("fired : " + event.target.value + " | " + FieldTargetIndex);
       self.myGrid.columns(FieldTargetIndex).search( event.target.value ).draw();
      });
     }
    });

 // Insert our DateTime Listeners so that if a Field Type 
 // is set to Date an appropriate Calendar will popup
    this.myGrid.columns().every( function ( idx ) {
     var myColumnName = self.findHeaderKeyFromIndex( idx );
     // var ColumnType   = self.options.columnsearch.columns[ myColumnName ][ "type" ];
     var myInputID    = self.options.container + '_tbl-' + myColumnName;

     if( myColumnName ){

      if( typeof self.options.columnsearch.columns[ myColumnName ] !== 'undefined' ){
         var ColumnType   = self.options.columnsearch.columns[ myColumnName ][ "type" ];

      // Assign the correct type of DatePicker to each Input Element
         if( ColumnType == "date" ){
          jQuery('#'+myInputID+'-date')
           .datetimepicker(
            { 'format': 'YYYY-MM-DD' , "sideBySide" : false , "showToday" : true , 'todayHighlight': true, 'autoclose': true }
           );
         }
         else if( ColumnType == "time" ){
          jQuery('#'+myInputID+'-date')
           .datetimepicker(
            { 'format': 'HH:mm' , "sideBySide" : false , "showToday" : true , 'todayHighlight': true, 'autoclose': true }
           );
         }
         else if( ColumnType == "period" ){
          jQuery('#'+myInputID+'-start-date')
           .datetimepicker(
            { 'format': 'YYYY-MM-DD' , "sideBySide" : false , "showToday" : true }
           );

          jQuery('#'+myInputID+'-end-date')
           .datetimepicker(
            { 'format': 'YYYY-MM-DD' , "sideBySide" : false , "showToday" : true , "maxDate" : today }
           );
         }
         else{
          jQuery('#'+myInputID+'-date')
           .datetimepicker(
            { 'format': 'YYYY-MM-DD HH:mm' , "sideBySide" : false , "showToday" : true , 'todayHighlight': true, 'autoclose': true }
           );
         }

      // If the ColumnType is Period , i need to create a linked EventListener
      // That will redraw the Grid in case the EndDate has been changed to a different Value
         if( ColumnType == "period"){

           var StartDateElement = document.getElementById( self.options.container + '_tbl-' + myColumnName + '-start-date' );
           var EndDateElement   = document.getElementById( self.options.container + '_tbl-' + myColumnName + '-end-date'   );

           $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
             var min = Date.parse( StartDateElement.value ) / 1000;
             var max = Date.parse( EndDateElement.value ) / 1000;
             var cdt = Date.parse( data[ idx ] ) / 1000;

             if ( ( isNaN( min ) && isNaN( max ) ) ||
                  ( isNaN( min ) && cdt <= max )   ||
                  ( min <= cdt   && isNaN( max ) ) ||
                  ( min <= cdt   && cdt <= max ) )
             {
                 return true;
             }
             return false;
            }
           );

           jQuery( '#' + myInputID + '-end-date' )
                 .on( "change.datetimepicker" , function ( e ) {
                  let StartDateElement = document.getElementById( self.options.container + '_tbl-' + myColumnName + '-start-date' );
                  let EndDateElement   = document.getElementById( self.options.container + '_tbl-' + myColumnName + '-end-date'   );
                  let myStartDate      = StartDateElement.value == "" ? StartDateElement.getAttribute( "value" ) : StartDateElement.value;
                  let myEndDate        = EndDateElement.value   == "" ? EndDateElement.getAttribute( "value" )   : EndDateElement.value;

                  document.dispatchEvent(
                   new CustomEvent( 'grid-date-change' , { 
                     detail: {
                      "startDateElementId" : myInputID + '-start-date' ,
                      "endDateElementId"   : myInputID + '-end-date' ,
                      "pickerEvent"        : e , 
                      "grid"               : self
                     }
                   })
                  );

                  if( typeof e.oldDate === 'undefined' ) {
                   console.log( "old date is null" );
                   return;
                  }

                  if( e.date != e.oldDate ){
                   EndDateElement.blur();
                   self.myDataPeriod      = { 
                    "column" : self.findHeaderKeyFromIndex( idx ) , 
                    "range"  : [ myStartDate , myEndDate ] 
                   };
                   console.log( self.myDataPeriod );
                   self.myGrid.draw( true );
                  }

                 });
         }
      }
     }
    } );
   }
  }
 }

 assignFieldEventListener(){
  var self = this;
  if(this.options.fieldEvent){
   $('#'+this.options.container+'_tbl tbody').on( 'click', 'td', function (event) {
      event.preventDefault();
      event.stopPropagation();

      var myColumnIndex = self.myGrid.cell(this).index().column;
      var myColumnName  = self.headerOBJ[myColumnIndex].replace("_","");
      var myFieldData   = self.myGrid.cell( this ).data();
      var myRowObject   = self.getRowData(this);

      if(self.options.fieldEvent[myColumnName]){
       if(typeof self.options.fieldEvent[myColumnName].fn === 'function'){
        self.options.fieldEvent[myColumnName].fn({
         "colName"  : myColumnName,
         "fldValue" : myFieldData,
         "rowData"  : myRowObject,
         "grid"     : self.myGrid
        });
       }
       else{
        console.log("No function assigned on Row Event");
       }
      }

   } );
  }  
 }

 assignRowEventListener(){
  var self = this;
  this.selected = new Array();
  if(this.options.rowEvent){
   $('#'+this.options.container+'_tbl tbody').on('click', 'tr', function (event) {
      var myRowObject  = self.getRowData(this);
      var myInputArray = new Array();

      self.selected.push( myRowObject );

      if(self.options.rowselect){
       Object.keys(myRowObject).forEach((property)=>{
        myInputArray.push(property+"|"+myRowObject[property]);
       });

       document.getElementById( self.options.container+'-selected' ).value = myInputArray.join(",");
      }

      if(typeof self.options.rowEvent === 'function'){
       self.options.rowEvent({
        "event": event,
        "grid" : self.myGrid, 
        "data" : myRowObject
       });
      }
      else{
       console.log("No function assigned on Row Event");
      }
   } );
  }
 }

 assignTableActionsButtonsListener(){
  if(this.options.tableActions){
   Object.keys(this.options.tableActions).forEach((tblAction)=>{
    var ActionLocale = this.options.tableActions[tblAction].name;
    var ActionName   = tblAction;
    var ActionFN     = this.options.tableActions[tblAction].fn;
    var myContainer  = document.getElementById(this.options.tableActions[tblAction].position);
    var self         = this;
    var ButtonUID    = this.options.uniqueGridId+"_"+tblAction+"_GridBtn";

    myContainer.insertAdjacentHTML(
     "beforeEnd" , 
     "<input type='button' class='btn btn-dark btn-sm' id='"+ButtonUID+"' value='"+ActionLocale+"'></input>"
    );

    $('#'+ButtonUID).on( 'click', function (event) {

      event.preventDefault();
      event.stopPropagation();

      if(typeof ActionFN === 'function'){
       ActionFN(self);
      }
      else{
       if(tblAction == "addRow"){
        console.log("no custom fn. Executing build-in [addRow]");
        self.addRow(self.options.tableActions["addRow"].exclude);
       }
       else if(tblAction == "addRow"){
        console.log("custom fn present");
       }
       else{
        console.log("No function assigned on Button");
       }
      }
    } );

   });  

  }

 }

 assignRowButtonsListener(){
  var self = this;
  if(this.options.rowButtons){
   $('#'+this.options.container+'_tbl tbody').on( 'click', 'button' , function (event) {
       event.preventDefault();
       event.stopPropagation();

       var myRowObject    = self.getRowData($(this).parents('tr'));
       var myButtonObject = self.options.rowButtons[this.getAttribute('amkey')];
       var myButtonAction = this.getAttribute('amaction');
       var myButtonNode   = "";
       
       if( event.target.nodeName === "BUTTON" ){
         myButtonNode = event.target;
       }
       else if( event.target.nodeName === "I" ){
         myButtonNode = event.target.parentNode;
       }

       if(typeof myButtonObject.fn === 'function'){
        myButtonObject.fn(self.myGrid , myRowObject , myButtonNode );
       }
       else{
         if(myButtonAction == "deleteRow"){
          getModal(
           {
            "title"        : "Caution",
            "body"         : "Are you sure you want to delete this record?",
            "acceptButton" : "Delete",
            "cancelButton" : "Cancel",
            "action"     : {
             "submit"    : (ev)=>{
              self.deleteRecord(myRowObject[self.options.uniqueRowId] , $(this).parents('tr') );
             }
            }
           }
          )
         }
         else{
          console.log("No function assigned on Button");
         }
       }
   } );
  }
 }

 initiateGrid(){
  this.myGrid = $('#'+this.options.container+'_tbl')
                  .DataTable( this.myGridOptions )
                  .columns.adjust()
                  .responsive.recalc();
 }

 setHiddenColumns(){
  if(this.options.columnsDisplay){
   if(typeof this.myGridOptions.columnDefs === "undefined"){
    this.myGridOptions.columnDefs = new Array();
   }

   this.myHeaderObj.forEach((header , idx)=>{
    if(!this.options.columnsDisplay.includes(header["column"])){
      this.myGridOptions.columnDefs.push({
       "targets"    : [idx],
       "visible"    : false,
       "searchable" : false
      });
     }
   });
  }  
 }

 getIconFieldSVG(type){
  var mySVGPath;

  switch(type){
   case "date" :
    mySVGPath = this.getIcon("calendar");
    // mySVGPath = '<svg class="bi bi-calendar" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
                 // '<path fill-rule="evenodd" d="M14 0H2a2 2 0 00-2 2v12a2 2 0 002 2h12a2 2 0 002-2V2a2 2 0 00-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z" clip-rule="evenodd"/>' + 
                 // '<path fill-rule="evenodd" d="M6.5 7a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd"/>' + 
                // '</svg>';
   break;
   case "datetime" : 
    mySVGPath = this.getIcon("calendar");
    // mySVGPath = '<svg class="bi bi-calendar" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
                 // '<path fill-rule="evenodd" d="M14 0H2a2 2 0 00-2 2v12a2 2 0 002 2h12a2 2 0 002-2V2a2 2 0 00-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z" clip-rule="evenodd"/>' + 
                 // '<path fill-rule="evenodd" d="M6.5 7a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd"/>' + 
                // '</svg>';
   break;
   case "period" : 
     mySVGPath = this.getIcon("calendar");   
     // mySVGPath = '<svg class="bi bi-calendar" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
                  // '<path fill-rule="evenodd" d="M14 0H2a2 2 0 00-2 2v12a2 2 0 002 2h12a2 2 0 002-2V2a2 2 0 00-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z" clip-rule="evenodd"/>' + 
                  // '<path fill-rule="evenodd" d="M6.5 7a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd"/>' + 
                 // '</svg>';
   break;
   case "email" : 
     mySVGPath = this.getIcon("email");
     // mySVGPath = '<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-at" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
                  // '<path fill-rule="evenodd" d="M13.106 7.222c0-2.967-2.249-5.032-5.482-5.032-3.35 0-5.646 2.318-5.646 5.702 0 3.493 2.235 5.708 5.762 5.708.862 0 1.689-.123 2.304-.335v-.862c-.43.199-1.354.328-2.29.328-2.926 0-4.813-1.88-4.813-4.798 0-2.844 1.921-4.881 4.594-4.881 2.735 0 4.608 1.688 4.608 4.156 0 1.682-.554 2.769-1.416 2.769-.492 0-.772-.28-.772-.76V5.206H8.923v.834h-.11c-.266-.595-.881-.964-1.6-.964-1.4 0-2.378 1.162-2.378 2.823 0 1.737.957 2.906 2.379 2.906.8 0 1.415-.39 1.709-1.087h.11c.081.67.703 1.148 1.503 1.148 1.572 0 2.57-1.415 2.57-3.643zm-7.177.704c0-1.197.54-1.907 1.456-1.907.93 0 1.524.738 1.524 1.907S8.308 9.84 7.371 9.84c-.895 0-1.442-.725-1.442-1.914z"/>' + 
                 // '</svg>';
   break;
   case "number" : 
     mySVGPath = this.getIcon("number");
     // mySVGPath = '<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-calculator-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
                  // '<path fill-rule="evenodd" d="M12 1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM4 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H4z"/>' + 
                  // '<path d="M4 2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-2zm0 4a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm3-6a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm3-6a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-4z"/>' + 
                 // '</svg>';
   break;
   default :
     mySVGPath = this.getIcon("text");
   break;
  }
  
  return mySVGPath;
 }

 setColumnSearches(){
  if( this.options.columnsearch ){
   if( this.options.columnsearch.columns ){
  
  // Create our Parent Search Container.
  // All SearchBoxes will be added in it
     var mySearchBoxesHTML = '<div class="input-group mb-3">';

  // Iterate through the Headers 
  // If a Header Name exists inside the ColumnSearch Object
  // We need to create the appropriate Search Input Elements on the Grid
     this.myHeaderObj.forEach( ( headerOBJ ) => {

      var myInputTitle = headerOBJ[ "column" ];

   // Check if header is set as Searchable in our Config
      // var isSearchable = typeof this.options.columnsearch.columns[ myInputTitle ] !== 'undefined' ? true : false;
      var isSearchable = this.options.columnsearch.columns.hasOwnProperty( myInputTitle );

      if( isSearchable ){

       var myInputType      = this.options.columnsearch.columns[ myInputTitle ][ "type" ];
       var myHTML_inputIcon = '<div class="input-group-prepend">' + 
                                '<span class="input-group-text" id="' + myInputTitle + '">' + 
                                  this.getIconFieldSVG( myInputType ) + 
                                '</span>' + 
                              '</div>';
       var myDefaultValue   = this.options.columnsearch.columns[ myInputTitle ][ "default" ];

       function getHTML_CommonAttributes( options ){
        var myIdentifier = options.identifier !== "" ? "-" + options.identifier : "";
        var myInputID    = options.container + '_tbl-' + options.title + myIdentifier;
        var myDataToggle = options.dataToggle !== "" ? " data-toggle='"+options.dataToggle+"'" : "";
        var myDataTarget = options.dataTarget !== "" ? " data-target='#"+options.dataTarget+"'" : " data-target='#"+myInputID+"'";
        return 'class="form-control '+options.cssclass+'" ' + 
               'id="' + myInputID + '" ' + 
               myDataToggle + 
               myDataTarget + 
               'placeholder="' + options.defaultValue + '" ' + 
               'aria-label="' + options.title + '" ' + 
               'aria-describedby="' + options.title + '" ' + 
               'value="' + options.defaultValue + '" ';
       }

       if( myInputType == "date" ){
        mySearchBoxesHTML += myHTML_inputIcon + 
                             '<input type="text" ' + 
                              getHTML_CommonAttributes( {
                               "title"        : myInputTitle , 
                               "identifier"   : "date" , 
                               "cssclass"     : "datetimepicker-time" , 
                               "toggle"       : "datetimepicker" , 
                               "defaultValue" : "",
                               "container"    : this.options.container 
                              } ) + 
                              '>';
       }
       else if( myInputType == "datetime" ){
        mySearchBoxesHTML += myHTML_inputIcon + 
                             '<input type="text" ' + 
                              getHTML_CommonAttributes( {
                               "title"        : myInputTitle , 
                               "identifier"   : "date" , 
                               "cssclass"     : "datetimepicker-datetime" , 
                               "toggle"       : "datetimepicker" , 
                               "defaultValue" : "",
                               "container"    : this.options.container 
                              } ) + 
                             '>';
       }
       else if( myInputType == "period" ){
        let myInputStartDate = ( typeof myDefaultValue[ "start-date" ] !== 'undefined' ? myDefaultValue[ "start-date" ] : "From" );
        let myInputEndDate   = ( typeof myDefaultValue[ "end-date" ]   !== 'undefined' ? myDefaultValue[ "end-date" ]   : "To" );
        mySearchBoxesHTML += myHTML_inputIcon + 
                             '<input type="text" ' + 
                              getHTML_CommonAttributes( {
                               "title"        : myInputTitle , 
                               "identifier"   : "start-date" , 
                               "cssclass"     : "datetimepicker-input" , 
                               "dataToggle"   : "datetimepicker" , 
                               "dataTarget"   : "" , 
                               "defaultValue" : myInputStartDate,
                               "container"    : this.options.container 
                              } ) + 
                             '>' + 
                             myHTML_inputIcon + 
                             '<input type="text" ' + 
                              getHTML_CommonAttributes( {
                               "title"        : myInputTitle , 
                               "identifier"   : "end-date" , 
                               "cssclass"     : "datetimepicker-input" , 
                               "dataToggle"   : "datetimepicker" , 
                               "dataTarget"   : "" , 
                               "defaultValue" : myInputEndDate,
                               "container"    : this.options.container 
                              } ) + 
                             '>';
        if( myInputStartDate !== "From" && myInputEndDate !== "To" ){
         this.myDataPeriod = {
          "column" : "m_date" , 
          "range"  : [ myInputStartDate , myInputEndDate ]
         }
        }
       }
       else if( myInputType == "list" ){
       mySearchBoxesHTML += '<div class="input-group-prepend">' + 
         '<span class="input-group-text" id="basic-addon1">'+
          this.getIconFieldSVG(myInputType) + 
         '</span>' + 
        '</div>' + 
        '<select class="form-control" ' + 
        'id="'+this.options.container+'_tbl-'+myInputTitle+'" ' + 
        'placeholder="'+myInputTitle+'" ' + 
        'aria-label="'+myInputTitle+'" ' + 
        'aria-describedby="'+myInputTitle+'" >' + 
        '<option>Whatever</option>' + 
        '</select>';
       }
       else{
       mySearchBoxesHTML += myHTML_inputIcon + 
                             '<input type="text" ' + 
                              getHTML_CommonAttributes( {
                               "title"        : myInputTitle , 
                               "identifier"   : "" , 
                               "cssclass"     : "" , 
                               "toggle"       : "" , 
                               "defaultValue" : "",
                               "container"    : this.options.container 
                              } ) + 
                             '>';
       }
      }
     });
     mySearchBoxesHTML += '</div>';

     document.getElementById( this.mySearchNavID ).insertAdjacentHTML( 
      "afterBegin", 
      mySearchBoxesHTML
     );
   }
  }
 }

 setRowButtons(){
  if(this.options.rowButtons){
   this.myGridOptions.columnDefs = new Array();
   let myOptions = this.options;

    this.myGridOptions.pageLength = 5000;

    this.myGridOptions.columnDefs.push(
     {
      "targets"       : -1,
      "data"          : null,
      // "defaultContent": myButtons,
      "render"        : function ( data, type, row, meta ) {
        var myButtons = "";

        Object.keys(myOptions.rowButtons).forEach((btnKey)=>{
         let myClass = myOptions.rowButtons[btnKey].hasOwnProperty( "styleClass" ) ? myOptions.rowButtons[btnKey][ "styleClass" ] : "primary";
         if( typeof myOptions.rowButtons[btnKey].icon !== 'undefined' ) 
          myButtons += "<button id='"+btnKey+"-"+data[ 0 ]+"' style='padding: 5px;' amkey='"+btnKey+"' amaction='"+myOptions.rowButtons[btnKey].name+"' class='btn btn-"+myClass+" btn-sm'>"+myOptions.rowButtons[btnKey].icon+"</button>"
         else
          myButtons += "<button id='"+btnKey+"-"+data[ 0 ]+"' style='padding: 5px;' amkey='"+btnKey+"' amaction='"+myOptions.rowButtons[btnKey].name+"' class='btn btn-"+myClass+" btn-sm'>"+myOptions.rowButtons[btnKey].name+"</button>"
        });

        return myButtons;
      }
     }
    );
  }
 }

 setProcessProperties(){
  if(this.options.processtype == "server"){
   this.myGridOptions.processing  = true;
   this.myGridOptions.deferRender = true;
   this.myGridOptions.serverSide  = true;
   this.myGridOptions.pagingType  = "simple";
   this.myGridOptions.info        = false;
  }
 }

 setPagination(){
  if(this.options.scrollable && this.options.processtype != "server"){
   this.myGridOptions.scrollY         = this.options.height;
   this.myGridOptions.scrollX         = true;
   this.myGridOptions.scroller        = true;
   this.myGridOptions.scrollCollapse  = true;
   this.myGridOptions.fixedColumns    = { heightMatch: 'none' };
   this.myGridOptions.paging          = false;
  }
  else{
   this.myGridOptions.scrollY         = this.options.height;
   this.myGridOptions.scrollX         = true;
   this.myGridOptions.scroller        = true;   
  }
 }

 setExportControls(){
  if(this.options.exports){
   this.myGridOptions.dom     = 'Bfrtip';
   this.myGridOptions.buttons = this.options.exports;
  }
 }

 setAjaxEndpoint(){
  this.myGridOptions.ajax = {
   "url"   : this.options.dispatcherPath,
   "error" : ( jqXHR, textStatus, errorThrown ) => {
       console.log( jqXHR );
       console.log( textStatus );
       console.log( errorThrown );
       document.dispatchEvent( new CustomEvent('ajaxFailed' , { detail: {
        "url"   : this.options.dispatcherPath , 
        "error" : jqXHR.responseText 
       } }) );
   },
   "complete" : ( objectResolution , textResolution ) => {
    if( textResolution == "success" ){
     if( typeof objectResolution.responseJSON[ "err" ] !== 'undefined' ){
      if( typeof objectResolution.responseJSON[ "err" ] !== '' ){
       this.dataSet = objectResolution.responseJSON.data;
       document.dispatchEvent( new CustomEvent('grid-ajax-complete' , { detail: {
        "url"     : this.options.dispatcherPath , 
        "state"   : objectResolution , 
        "res"     : textResolution , 
        "options" : this.options
       } }) );
      }
     }
    }
   },
   "data"  : ( form ) => {
    form.processtype       = this.options.processtype;
    let myCustomProperties = this.options.customAJAX || false;

    if(myCustomProperties){
     Object.keys(myCustomProperties).forEach((property)=>{
      form[property] = myCustomProperties[property];
     });
    }

    form.period = this.myDataPeriod;

   }
  }
 }

 setCollapse(){
  if(this.options.collapse)
   this.myGridOptions.responsive = true;

  if(this.options.rowselect){
   if(this.options.rowselect == "multi"){
    this.myGridOptions.select = {
     selectable : true,
     toggleable : true,
     blurable   : false,
     style      : "multi"
    };
   }
   else{
    this.myGridOptions.select = {
     toggleable: false,
     selectable: true,
     blurable  : false,
     style     : "single"
    };    
   }
  }
 }

 setDimensions(){
  if(this.options.height){
   this.myGridOptions.scrollY        = this.options.height;
   this.myGridOptions.scrollCollapse = false;
   if(this.options.width){
    this.myGridOptions.scrollX        = this.options.width;
   }   
  }
  else{
   this.myGridOptions.scrollCollapse = false;
  }
 }

 getHeaders(){
  var myBasic      = {"action":"headers" , "processtype" : this.options.processtype };
  var myCustom     = this.getCustomAJAXProperties();
  var myParameters = {};
  
  Object.keys( myCustom ).forEach( ( property ) => {
    if( myBasic.hasOwnProperty( property ) )
      myParameters[ property ] = myBasic[ property ];
    else
      myParameters[ property ] = myCustom[ property ];
  });

  return new Promise( ( resolve , reject ) => {
    AJAX( this.options.dispatcherPath , myParameters )
     .then( (headers)=>{
      if(headers.error){
       reject(headers.error);
      }
      else{
       resolve(headers);
      }
     })
     .catch( ( error ) => {
      reject( error );
     });
  });
 }

 setProcessType(){
  if(typeof this.options.processtype == 'undefined') 
   this.options.processtype = 'client';  
 }

 findHeaderKeyFromIndex(needle , type){
  var myResponse = "";

  if(this.myHeaderObj[needle])
   myResponse = this.myHeaderObj[needle]["column"];
  else
   myResponse = false;    

  return myResponse;
 }

 findHeaderIndexFromKey(needle , type){
  var myResponse = "";
  this.myHeaderObj.forEach((header , idx)=>{
   if( header["column"] == needle){
    myResponse = idx;
   }
  });

  return myResponse;
 }

 getRowData(domElement){
   var myRowData     = this.myGrid.row( domElement ).data();
   var myHeaders     = new Array();
   var myRowObject   = new Object();

   this.myHeaderObj.forEach( ( header ) => {
     myHeaders.push( header[ "column" ] );
   });

   myRowData.forEach((columnValue , columnIndex)=>{
    myRowObject[ myHeaders[columnIndex] ] = columnValue;
   });

   return myRowObject;
 }

 setHeaders(container , actionColumn){
   var myContainer = document.getElementById(container);
   var myDtResponsive = "";
   var myHeaders;

   if(this.options.collapse)
    myDtResponsive = "dt-responsive";
   else
    myDtResponsive = "";

   myHeaders   = '<table id="'+container+'_tbl" class="table '+myDtResponsive+' table-hover table-light table-bordered" style="width:100%">' + 
                  '<thead class="thead-light">' + 
                   '<tr>';

   this.myHeaderObj.forEach( ( headerOBJ ) => {
     myHeaders += "<th>" + lang( headerOBJ["locale"] , myLanguage ) + "</th>";
   });

   if(typeof actionColumn !== 'undefined'){
     myHeaders += "<th>" + lang( "Actions" , myLanguage ) + "</th>";
   }

   myHeaders +='</tr>' + '</thead>';

   myHeaders += '</table>';
   
   myHeaders += '<input type="hidden" id="'+container+'-selected" name="'+container+'-selected" ></input>';

   myContainer.insertAdjacentHTML("beforeEnd",myHeaders);
 } 

 addRecord(aForm){

   var myValues     = new Array();
   var myTempObject = new Object();
   var customProps  = this.getCustomAJAXProperties();
   // var myRequest    = { ...aForm , ...customProps };
   var myRequest    = Object.assign( aForm , customProps );

   myRequest.action = "addRecord";

   AJAX( this.options.dispatcherPath , myRequest)
     .then(
         (response) => {
          if(response.error){
           jQuery.prompt.close();
           jQuery.prompt(response.error);
          }
          else{
           this.myHeaderObj.forEach((headerOBJ)=>{
            var Header = headerOBJ["column"];

            if( Header == this.options.uniqueRowId ){
             myTempObject[ Header ] = response[this.options.uniqueRowId];
            }
            else{
             myTempObject[ Header ] = aForm[ Header ]; 
            }
           });

           this.myHeaderObj.forEach((headerOBJ)=>{
            var Header = headerOBJ["column"];
            var aValue = myTempObject[Header] || null;
            myValues.push( aValue );
           });

           var newNode = this.myGrid.row.add( myValues )
               .draw( true )
               .node();

           jQuery.prompt.close();
           if( this.options.onAddRecord ){
            this.myGridOptions.drawCallback = ( settings ) => {
             this.options.onAddRecord( this );
            }
           }
          }
         }
       );
 }

 getCustomAJAXProperties(){
  return ( this.options.customAJAX || "" );
 }

 deleteRecord(recordID , targetRow){
  var customProps  = this.getCustomAJAXProperties();
  var myRequest    = Object.assign( {"action" : "deleteRecord" , "id" : recordID} , customProps );

  AJAX( this.options.dispatcherPath , myRequest)
        .then( (response) => {
         if(response.error){
          jQuery.prompt.close();
          jQuery.prompt(response.error);
         }
         else{
          var newNode = this.myGrid.row( targetRow )
              .remove( )
              .draw( true );

          jQuery.prompt.close();
          if( this.options.onDeleteRecord ){
           this.myGridOptions.drawCallback = ( settings ) => {
            this.options.onDeleteRecord( this );
           }
          }
         }
        }
      );
 }

 getIcon(type){
  var myIcon = "";
  switch(type){
   case "calendar" : 
    myIcon = '<svg class="bi bi-calendar" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
              '<path fill-rule="evenodd" d="M14 0H2a2 2 0 00-2 2v12a2 2 0 002 2h12a2 2 0 002-2V2a2 2 0 00-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z" clip-rule="evenodd"/>' + 
              '<path fill-rule="evenodd" d="M6.5 7a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd"/>' + 
             '</svg>';
   break;
   case "text" : 
    myIcon = '<svg class="bi bi-card-text" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
              '<path fill-rule="evenodd" d="M14.5 3h-13a.5.5 0 00-.5.5v9a.5.5 0 00.5.5h13a.5.5 0 00.5-.5v-9a.5.5 0 00-.5-.5zm-13-1A1.5 1.5 0 000 3.5v9A1.5 1.5 0 001.5 14h13a1.5 1.5 0 001.5-1.5v-9A1.5 1.5 0 0014.5 2h-13z" clip-rule="evenodd"/>' + 
              '<path fill-rule="evenodd" d="M3 5.5a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9a.5.5 0 01-.5-.5zM3 8a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9A.5.5 0 013 8zm0 2.5a.5.5 0 01.5-.5h6a.5.5 0 010 1h-6a.5.5 0 01-.5-.5z" clip-rule="evenodd"/>' + 
             '</svg>';
   break;
   case "email" : 
    myIcon = '<svg class="bi bi-at" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
              '<path fill-rule="evenodd" d="M13.106 7.222c0-2.967-2.249-5.032-5.482-5.032-3.35 0-5.646 2.318-5.646 5.702 0 3.493 2.235 5.708 5.762 5.708.862 0 1.689-.123 2.304-.335v-.862c-.43.199-1.354.328-2.29.328-2.926 0-4.813-1.88-4.813-4.798 0-2.844 1.921-4.881 4.594-4.881 2.735 0 4.608 1.688 4.608 4.156 0 1.682-.554 2.769-1.416 2.769-.492 0-.772-.28-.772-.76V5.206H8.923v.834h-.11c-.266-.595-.881-.964-1.6-.964-1.4 0-2.378 1.162-2.378 2.823 0 1.737.957 2.906 2.379 2.906.8 0 1.415-.39 1.709-1.087h.11c.081.67.703 1.148 1.503 1.148 1.572 0 2.57-1.415 2.57-3.643zm-7.177.704c0-1.197.54-1.907 1.456-1.907.93 0 1.524.738 1.524 1.907S8.308 9.84 7.371 9.84c-.895 0-1.442-.725-1.442-1.914z" clip-rule="evenodd"/>' + 
             '</svg>';
   break;
   case "password" : 
    myIcon = '<svg class="bi bi-eye-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
               '<path d="M10.5 8a2.5 2.5 0 11-5 0 2.5 2.5 0 015 0z"/>' + 
               '<path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 100-7 3.5 3.5 0 000 7z" clip-rule="evenodd"/>' + 
             '</svg>';
   break;
   case "geo" : 
    myIcon = '<svg class="bi bi-geo-alt" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
               '<path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 002 6c0 4.314 6 10 6 10zm0-7a3 3 0 100-6 3 3 0 000 6z" clip-rule="evenodd"/>' + 
             '</svg>';
   break;
   case "contact" : 
    myIcon = '<svg class="bi bi-people-circle" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
               '<path d="M13.468 12.37C12.758 11.226 11.195 10 8 10s-4.757 1.225-5.468 2.37A6.987 6.987 0 008 15a6.987 6.987 0 005.468-2.63z"/>' + 
               '<path fill-rule="evenodd" d="M8 9a3 3 0 100-6 3 3 0 000 6z" clip-rule="evenodd"/>' + 
               '<path fill-rule="evenodd" d="M8 1a7 7 0 100 14A7 7 0 008 1zM0 8a8 8 0 1116 0A8 8 0 010 8z" clip-rule="evenodd"/>' + 
             '</svg>';
   break;
   case "search" : 
    myIcon = '<svg class="bi bi-search" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' +
               '<path fill-rule="evenodd" d="M10.442 10.442a1 1 0 011.415 0l3.85 3.85a1 1 0 01-1.414 1.415l-3.85-3.85a1 1 0 010-1.415z" clip-rule="evenodd"/>' +
               '<path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 100-11 5.5 5.5 0 000 11zM13 6.5a6.5 6.5 0 11-13 0 6.5 6.5 0 0113 0z" clip-rule="evenodd"/>' +
             '</svg>';
   break;
   case "number" : 
    myIcon = '<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-calculator-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
                '<path fill-rule="evenodd" d="M12 1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM4 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H4z"/>' + 
                '<path d="M4 2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-2zm0 4a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm3-6a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm3-6a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-4z"/>' + 
               '</svg>';
   break;
   default : 
    myIcon = '<svg class="bi bi-card-text" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
              '<path fill-rule="evenodd" d="M14.5 3h-13a.5.5 0 00-.5.5v9a.5.5 0 00.5.5h13a.5.5 0 00.5-.5v-9a.5.5 0 00-.5-.5zm-13-1A1.5 1.5 0 000 3.5v9A1.5 1.5 0 001.5 14h13a1.5 1.5 0 001.5-1.5v-9A1.5 1.5 0 0014.5 2h-13z" clip-rule="evenodd"/>' + 
              '<path fill-rule="evenodd" d="M3 5.5a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9a.5.5 0 01-.5-.5zM3 8a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9A.5.5 0 013 8zm0 2.5a.5.5 0 01.5-.5h6a.5.5 0 010 1h-6a.5.5 0 01-.5-.5z" clip-rule="evenodd"/>' + 
             '</svg>';    
   break;
  }

  return myIcon;
 }

 addRow(exclude){
   var self      = this;
   var HTML      = "";
   var myModalID = this.randomUID(10);
   var myFormID  = "amForm-"+myModalID;
   // var myModalID = "my-grid-modal-id-";
   // var myFormID  = "my-grid-modal-form-id-";

   this.getModal(
    {
     "id"         : myModalID,
     "title"      : "Add",
     "validate"   : true,
     "autosubmit" : true,
     "action"     : {
      "submit"    : (ev)=>{
       var myForm             = document.getElementById( myFormID );
       var myFormSerialized   = jQuery( myForm ).serializeArray();
       var myFormReSerialized = new Object();
       var validationResult   = false;
       var myFormFields;

       myFormSerialized.forEach( ( item ) =>{
        myFormReSerialized[ item["name"] ] = item["value"];
       });

       myForm.reportValidity();

       if( myForm.checkValidity() === false ){
        validationResult = false;
       }
       else{
        validationResult = true;
        self.addRecord( myFormReSerialized );
       }
      }
     }
    }
    , 
     ()=>{
      var myOptions = {
       "container" : myModalID + "-body",
       "columns"   : 2,
       "formid"    : myFormID,
       "inputs"    : new Array()
      };

      self.myHeaderObj.forEach( ( inputItem ) =>{
       if( !exclude.includes( inputItem.column ) ){
        myOptions.inputs.push({
          id       : inputItem.column,
          name     : inputItem.column,
          type     : inputItem.style,
          icon     : inputItem.icon,
          label    : inputItem.locale,
          required : true,
          rowspan  : false
        });
       }
      });
      let myRenderedForm = new amForm( myOptions );
     }
   )
 }

 randomUID(length) {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return "uid_"+result;
 }

 getModal(options , callback){
   var validationResult;
   var myBodyHTML;

   if(document.getElementById(options.id)){
    document.getElementById(options.id).remove();
   }

// Generate a Random UID to assign as DOM element ID if no ID has been provided
   if(typeof options.id === 'undefined'){
    options.id = this.randomUID(10);
   }

// Add the Parent that will hold all the Modal DOM Elements
   document.body.insertAdjacentHTML(
    "beforeEnd",
    '<div class="modal fade" id="'+options.id+'" tabindex="-1" role="dialog" aria-labelledby="'+options.id+'" aria-hidden="true"></div>'
   );

// You either provide the BodyMessage or create it on the Callback.
// if either does not take place then the following message will be shown for notification reasons.
   myBodyHTML = options.body || "";

// Add on the Parent the rest of the Modal along with the Body of the Form [myBodyHTML]
   document.getElementById(options.id).insertAdjacentHTML(
    "beforeEnd",
    '<div id="" class="modal-dialog modal-xl" role="document">' +
                     '<div class="modal-content">' +
                       '<div class="modal-header">' +
                         '<h5 class="modal-title" id="'+options.id+'-title">'+options.title+'</h5>' +
                         '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
                           '<span aria-hidden="true">&times;</span>' +
                         '</button>' +
                       '</div>' +
                       '<div class="modal-body" id="'+options.id+'-body">' +
                       myBodyHTML + 
                       '</div>' +
                       '<div class="modal-footer">' +
                         '<button type="button" id="am-'+options.id+'-close" class="btn btn-secondary" data-dismiss="modal">Close</button>' +
                         '<button type="button" id="am-'+options.id+'-submit" class="btn btn-primary">Ok</button>' +
                       '</div>' +
                     '</div>' +
                   '</div>'
   );

// Add event Listener on Close Button to hide the Modal and remove the DOM Element (The delay is to let the fade out animation to finish)
   document.getElementById("am-"+options.id+"-close").addEventListener('click' , (event)=>{
    setTimeout(()=>{
     document.getElementById(options.id).remove(); 
    },500);
   });

// If we have a Custom Submit Function in our Configuration we need to set it up
   if(options.action.submit){

 // Assign a Listener on our Submit Buttong
    document.getElementById("am-"+options.id+"-submit").addEventListener('click' , (event)=>{
     
  // Serialize our Form.
     var myForm           = document.querySelector("#"+options.id+" form");
     var mySerializedForm = jQuery(myForm).serializeArray();

  // If we have Requested a Form Validation then we proceed in doing so.
  // If not then we simply sent the results back to the client.
     if(options.validate){
      myForm.reportValidity();

   // If our Form Validation has Succeded proceed in executing the submit function 
   // passing all the relevant form properties 
      if (myForm.checkValidity() === true){
       options.action.submit({
        "event"  : event,
        "form"   : document.querySelectorAll("#"+options.id+" [name]"),
        "formid" : options.id,
        "values" : mySerializedForm
       });
       
    // Hide the Modal and also remove the Element from the DOM
       $('#'+options.id).modal('hide');
       setTimeout(()=>{ document.getElementById(options.id).remove(); },500);

      }
      else{
       // Do nothing...
      }

     }
     else{
   // Proceed in executing the submit function passing all the relevant form properties
      options.action.submit({
       "event"  : event,
       "form"   : document.querySelectorAll("#"+options.id+" [name]"),
       "values" : mySerializedForm
      });

   // Hide the Modal and also remove the Element from the DOM
      $('#'+options.id).modal('hide');
      setTimeout(()=>{ document.getElementById(options.id).remove(); },500);
     }
    });
   }
   else{
 // Add event Listener on Close Button to hide the Modal and remove the DOM Element (The delay is to let the fade out animation to finish)
    document.getElementById("am-"+options.id+"-submit").addEventListener('click' , (event)=>{
     $('#'+options.id).modal("hide");
     setTimeout(()=>{
      document.getElementById(options.id).remove(); 
     },500);
    });
   }

// If there is Autosubmit ,then when the user presses the Enter Key 
// The submit event will be fired [Default : true]
   if(options.autosubmit){
    document.getElementById(options.id+'-body').addEventListener('keydown', (e) => {
      if (!e.repeat){
       if(e.keyCode == 13){
        document.getElementById('am-'+options.id+'-submit').click();
       }
      }
    });
   }

// Bring up the Modal
   $('#'+options.id).modal();

// If there is a Callback that has to be executed after the Modal has been rendered proceed in calling it.
   if(typeof callback === 'function'){
    $('#'+options.id).on('shown.bs.modal', function (e) {
     callback();
    });
   }
 }

}