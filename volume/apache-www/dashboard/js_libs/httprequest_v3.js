var myAjaxController;
var isRequestRunning = false;
// var   myAjaxStatus     = 0;
 
function AJAX( url , params ) {
 return new Promise( ( resolve , reject ) => {
  
  // document
      // .querySelectorAll( ".nav-link" )
      // .classList.add( "disabled" );
  
  isRequestRunning = true;
  myAjaxController = new AbortController();
  const signal     = myAjaxController.signal;
  
  var server_url;
  var myParams;
  var myMethod;
  var myHeader;
  var myOptions = {
   mode           : 'cors', 
   cache          : 'no-cache', 
   redirect       : 'follow', 
   referrerPolicy : 'no-referrer',
   signal         : signal
  };

  if( typeof params === 'object' ){
   myOptions.method  = "POST";
   myOptions.headers = { 'Content-Type': 'application/json' };
   myOptions.body    = JSON.stringify( params );
   server_url        = url;
  }
  else{
   myOptions.method  = "GET";
   myOptions.headers = { 'Content-Type': 'application/x-www-form-urlencoded' };
   server_url        = url + "?" + params;
  }

  document.dispatchEvent( new CustomEvent('ajax-initiate' , { detail: {
   "url" : server_url , 
   "res" : myOptions 
  } }) );

  fetch( server_url , myOptions )
    .then( response => response.json() )
    .then( data => { 

      // document
          // .querySelectorAll( ".nav-link" )
          // .classList.remove( "disabled" );

      if( data.error ){
       isRequestRunning = false;

       if( data.error === "Invalid Access" ){
        alert( "You have been disconnected from the Server" );
        top.window.location.href="../views/login.php";
       }
       else{
        console.log( "Generic Error" );
        document.dispatchEvent( new CustomEvent('ajax-success' , { detail: {
         "url" : server_url , 
         "res" : data.error 
        } }) );
        reject( data.error ); 
       }
      }
      else{
       isRequestRunning = false;

       document.dispatchEvent( new CustomEvent('ajax-success' , { detail: {
        "url" : server_url , 
        "res" : data 
       } }) );
       
       resolve( data ); 
      }
    })
    .catch( ( error ) => {

      // document
          // .querySelectorAll( ".nav-link" )
          // .classList.remove( "disabled" );

      isRequestRunning = false;
      if( error.name === "AbortError" ){
       document.dispatchEvent( new CustomEvent('ajax-aborted' , { detail: {
        "url" : server_url , 
        "res" : error 
       } }) );
      }
      else{
       document.dispatchEvent( new CustomEvent('ajax-fail' , { detail: {
        "url" : server_url , 
        "res" : error 
       } }) );

       reject(error);
      }
    });
 });
}