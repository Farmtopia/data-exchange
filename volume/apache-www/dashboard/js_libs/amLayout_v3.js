class amLayout{

 constructor( containerID , options ){
  console.log("amLayout loaded");
  
  this.container    = document.getElementById( containerID );
  this.options      = options;
  this.rows         = new Array();
  this.columns      = new Array();
  this.grid         = new Object();
  this.lastRowAdded;

  this.setContainerToFluid();

  return this;
 }

 getConstantsMapping( attribute , type ){
  let myConstantMap;
  
  switch( type ) {
   case "row" : 
    myConstantMap = {
     "left"   : "justify-content-start",
     "right"  : "justify-content-end",
     "center" : "justify-content-between",
     "around" : "justify-content-around"
    }
   break;
   case "column" : 
    myConstantMap = {
     "left"   : "align-self-start",
     "right"  : "align-self-end",
     "center" : "align-self-center"
    }
   break;
  }
  
  return myConstantMap[attribute];
 }

 addRowTo( rowID , options ){
  let RowID       = "row-"+(this.rows.length+1);
  let rowAlign    = typeof options !== 'undefined' ? ( options.align ? " "+this.getConstantsMapping( options.align , "row" ) : "" ) : "";
  let myCssStyle  = typeof options !== 'undefined' ? ( options.style ? this.getStyle( options.style ) : "" ) : "";
  let myCssClass  = typeof options !== 'undefined' ? ( options.class ? options.class : "" ) : "";
  let myTargetRow = document.getElementById( rowID );
  myTargetRow.insertAdjacentHTML("beforeEnd" , "<div style='"+myCssStyle+"' class='row"+rowAlign+"' id='"+RowID+"'></div>");
  // myTargetRow.insertAdjacentHTML("beforeEnd" , "<div style='"+myCssStyle+"' class='"+myCssClass+"' id='"+RowID+"'></div>");
  let RowNode = document.getElementById( RowID );
  this.rows.push( RowNode );
  this.lastRowAdded = RowNode;
  return this;
 }

 addRow( options , callback ){
  let RowID      = "row-"+(this.rows.length+1);
  let rowAlign   = typeof options !== 'undefined' ? ( options.align ? " "+this.getConstantsMapping( options.align , "row" ) : "" ) : "";
  let myCssStyle = typeof options !== 'undefined' ? ( options.style ? this.getStyle( options.style ) : "" ) : "";
  let myCssClass  = typeof options !== 'undefined' ? ( options.class ? options.class : "" ) : "";
  // this.container.insertAdjacentHTML("beforeEnd" , "<div style='"+myCssStyle+"' class='row"+rowAlign+"' id='"+RowID+"'></div>");
  this.container.insertAdjacentHTML("beforeEnd" , "<div style='"+myCssStyle+"' class='"+myCssClass+"' id='"+RowID+"'></div>");
  let RowNode = document.getElementById( RowID );
  this.rows.push( RowNode );
  this.lastRowAdded = RowNode;

  if( typeof callback === 'function' ){
   callback( RowNode );
  }

  return this;
 }

 addColumn( options , callback ){
  console.log( options.class );
  let clmnID      = typeof options !== 'undefined' ? ( options.id ? options.id : "col-"+this.rows.length+"-"+(this.columns.length+1) ) : "col-"+this.rows.length+"-"+(this.columns.length+1);
  let clmnSize    = options.size ? ("-"+options.size) : "";
  let clmnContent = options.content || "";
  let myCssStyle = typeof options !== 'undefined' ? ( options.style ? this.getStyle( options.style ) : "" ) : "";
  let myCssClass  = typeof options !== 'undefined' ? ( options.class ? options.class : "" ) : "";
  let clmnAlign   = options.align ? " "+this.getConstantsMapping( options.align , "column" ) : "";
  // this.lastRowAdded.insertAdjacentHTML("beforeEnd" , "<div style='"+myCssStyle+"' class='col"+clmnSize+clmnAlign+" tab-content' id='"+clmnID+"'>"+clmnContent+"</div>");
  this.lastRowAdded.insertAdjacentHTML("beforeEnd" , "<div style='"+myCssStyle+"' class='"+myCssClass+" col"+clmnSize+clmnAlign+"' id='"+clmnID+"'>"+clmnContent+"</div>");
  let colNode = document.getElementById( clmnID );
  // this.lastRowAdded.insertAdjacentHTML("beforeEnd" , "<div style='"+myCssStyle+"' class='tab-content' id='"+clmnID+"'>"+clmnContent+"</div>");
  this.columns.push( colNode );
  
  if( typeof callback === 'function' ){
   callback( colNode );
  }

  return this;
 }
 
 getStyle( style ){
  let myCssArray = new Array();
  Object.keys( style ).forEach( ( attribute ) => {
   myCssArray.push( attribute + ":" + style[attribute] );
  });
  
  return myCssArray.join(";") + ";";
 }

 setContainerToFluid(){
  this.container.classList.add("container-fluid");
 }

}

// var myGrid = new amLayout( 'amLayout-cnt' );
// myGrid.addRow( );
// myGrid.addColumn( { "id" : "header" , "size" : 12 } );

// myGrid.addRowTo( "header" , { "align" : "center" } );
// myGrid.addColumn( { "id" : "settings-btn" , "size" : 3 , "content" : "settings" } );
// myGrid.addColumn( { "id" : "profile-btn" , "size" : 2 , "content" : "profile" } );

// myGrid.addRow( { "style" : { "height" : "500px" } } );
// myGrid.addColumn( { "id" : "nav-bar" , "size" : 3 , "content" : "Nav-Bar" , "style" : { "padding" : 0 , "height" : "100%" , "overflow" : "auto" }  } );
// myGrid.addColumn( { "id" : "main-cnt" , "size" : 9 } );

// myGrid.addRowTo( "main-cnt" , { "align" : "right" , "style" : { "height" : "50px" } } );
// myGrid.addColumn( { "id" : "btn-1" , "size" : 3 , "content" : "Button 1" , "style" : { "padding" : 0  }  } );
// myGrid.addColumn( { "id" : "btn-1" , "size" : 3 , "content" : "Button 2" , "style" : { "padding" : 0  }  } );
// myGrid.addColumn( { "id" : "btn-1" , "size" : 3 , "content" : "Button 3" , "style" : { "padding" : 0  }  } );

// myGrid.addRow( );
// myGrid.addColumn( { "id" : "footer" , "size" : 12 , "content" : "Footer" } );

/* Documentation */
/*

*/