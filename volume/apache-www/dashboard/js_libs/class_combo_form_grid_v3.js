class comboFormGrid{

 constructor(options){
  // console.log( " [ Class comboFormGrid : options ] " , options );
  this.modalID               = options.myModalID;
  this.mainid                = options.mainid;
  this.formColumns           = options.formColumns;
  this.entity                = options.entity;
  this.parentEntity          = options.parentEntity;
  this.parentId              = options.parentId;
  this.options               = options;
  this.activateActionButtons = options.activateActionButtons === false ? false : true;
  this.myForm;
  this.myGrid;
  // console.log( "this.activateActionButtons : " + this.activateActionButtons );
  // console.log( options );

  this.getGrid();
 }

 getGrid(){
   this.myGrid = new amGrid({
    "container"              : this.mainid, 
    "activateActionButtons"  : this.activateActionButtons , 
    "uniqueRowId"            : this.entity+"_id",
    "uniqueGridId"           : this.entity+"-view",
    "dispatcherPath"         : "../ws/ws.grid.php", 
    "CRUD"                   : this.options["CRUD"],
    "customAJAX"             : { "entity" : this.entity , "parententity" : this.parentEntity , "parentid" : this.parentId },
    "processtype"            : "server",
    "rowselect"              : "single",
    "searchBox"              : true,
    "infoBox"                : false,
    "orderby"                : { "column" : 0 , "dir" : "desc" } , 
    "onAddRecord"       : ( gridObject ) => {
      gridObject.activateRow(0);
    },
    "onDeleteRecord"    : ( gridObject ) => {
      gridObject.activateRow(0);
    },
    "onRender"       : ( gridObject ) => {
     if( gridObject.myGrid.rows().count() > 0 ){
      gridObject.activateRow(0);
     }
     else{
      let AJAXRequest = new Object();
          AJAXRequest["action"]          = "getFormTemplate";
          AJAXRequest["template"]        = this.entity;
          AJAXRequest["entity"]          = this.entity;
          AJAXRequest[this.entity+"_id"] = false;

      getFormTemplate( "../ws/ws.form.php" , AJAXRequest )
      .then( formTemplate => {
       let aFormID = "amForm-"+this.entity+"-" + formTemplate.options.formid;
       this.myForm = new amForm({
        "customAJAX"   : { "parententity" : this.parentEntity , "parentid" : this.parentId },
        "container"    : this.mainid+"-form-sub",
        "columns"      : this.formColumns,
        "readonly"     : true,
        "entity"       : this.entity,
        "data"         : false,
        "formid"       : aFormID,
        "inputs"       : getFormInputs( formTemplate , aFormID ),
        "hiddeninputs" : getFormHiddenInputs( formTemplate ),
        "event"        : (response)=>{ // Fires upon changes on Form like UpdateRecord and AddRecord
         if(response.event == "addrecord"){
          this.myGrid.myGrid.draw();
         }else if(response.event == "updaterecord"){
          this.myGrid.myGrid.draw();
         }
        }
       });
       this.myGrid.setForm( this.myForm );
      });
     }
    },
    "scrollable"     : false,
    "height"         : (getViewDimensions("height") / 4)+"px",
    "collapse"       : false,
    "rowButtons" : {
     "btn1" : {
      "name" : "deleteRow",
      "icon" : '<i class="far fa-trash-alt"></i>'
     }
    },
    "rowEvent" : ( rowEventObject ) => {
     let AJAXRequest = new Object();
         AJAXRequest["action"]          = "getFormTemplate";
         AJAXRequest["template"]        = this.entity;
         AJAXRequest["entity"]          = this.entity;
         AJAXRequest[this.entity+"_id"] = rowEventObject.data[this.entity+"_id"];
     getFormTemplate( "../ws/ws.form.php" , AJAXRequest )
     .then( formTemplate => {
      let aFormID = "amForm-"+this.entity+"-" + formTemplate.options.formid;
      this.myForm = new amForm({
       "customAJAX"   : { "parententity" : this.parentEntity , "parentid" : this.parentId },
       "container"    : this.mainid+"-form-sub",
       "columns"      : this.formColumns,
       "readonly"     : true,
       "entity"       : this.entity, // Designates the Object that will be loaded on the server
       "data"         : formTemplate.data || false,
       "formid"       : aFormID,
       "inputs"       : getFormInputs( formTemplate , aFormID ),
       "hiddeninputs" : getFormHiddenInputs( formTemplate ),
       "event"        : (response)=>{ // Fires upon changes on Form like UpdateRecord and AddRecord
        if(response.event == "addrecord"){
         rowEventObject.grid.draw();
        }else if(response.event == "updaterecord"){
         rowEventObject.grid.draw();
        }
       }
      });
      this.myGrid.setForm( this.myForm );
     });
    }
   });
 }

}