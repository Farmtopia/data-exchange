class amList{

 constructor( containerID , options ){
  console.log("amList loaded");

  this.options      = options || false;
  this.containerID  = containerID;
  this.list         = new Array();
  this.myListNodeID = "amList-1";
  this.myListNode   = this.createListNode( this.myListNodeID );

  if( this.options ) {
   this.populateListFromOptions( this.options );
  }

  return this;
 }

 createListNode( myListNodeID ){
  if( document.getElementById( myListNodeID ) ){
   return document.getElementById( myListNodeID );
  }
  else{
   document.getElementById( this.containerID ).innerHTML = "<div class='list-group list-group-flush' id='"+myListNodeID+"'></div>";
   return document.getElementById( myListNodeID );
  }
 }

 populateListFromOptions( options ){
  let isArray = Array.isArray(options);
  let myItem;

  if( isArray ) {
   options.forEach( ( item ) =>{
    myItem = this.creatListItem( item );
    this.list.push( myItem );
   });   
  }
  else{
   myItem = this.creatListItem( options );
   this.list.push( myItem );
  }
 }

 creatListItem( item ){
   let myProperty = item.name.toLowerCase();
   let buttonID   = item.id;
   let btnStatus  = item.status ? item.status : "";
   let btnStyle   = item.style ? "list-group-item-" + item.style : "";
   let btnOnClick = item.onClick ? item.onClick : false;
   let btnIcon    = item.icon ? item.icon : false;

   return {
    "id"       : item.id , 
    "property" : myProperty , 
    "name"     : item.name,
    "style"    : btnStyle,
    "status"   : btnStatus,
    "onClick"  : btnOnClick,
    "icon"     : btnIcon
   };
 }

 getButton( item ){
  if( item.icon ){
   return '<a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center '+item.status+' '+item.style+'" id="'+item.id+'" data-toggle="list" href="#list-'+item.property+'" role="tab" aria-controls="'+item.property+'">' + 
   item.name + 
   this.getIconHTML( item.id , item.icon ) + 
   '</a>';
  }
  else{
   return '<a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center '+item.status+' '+item.style+'" id="'+item.id+'" data-toggle="list" href="#list-'+item.property+'" role="tab" aria-controls="'+item.property+'">'+item.name+'</a>'; 
  }
  
 }

 activateItem( event ){
  let myNodeID = event.target.id;

  this.list.forEach( ( item ) =>{
   if( item.id == myNodeID ){
    this.setListAttribute( myNodeID , "status" , "active" );
    if( !this.getElement( myNodeID ).classList.contains( "active" ) ){
     this.getElement( myNodeID ).classList.add("active");
    }
   }
   else{
    this.getElement( item.id ).classList.remove("active");
    this.setListAttribute( item.id , "status" , "inactive" );
   }
  });
 }

 render(){
  this.myListNode.innerHTML = "";
  this.list.forEach( (item) => {

   this.myListNode.insertAdjacentHTML( "beforeEnd" , this.getButton( item ) );

   document.getElementById( item.id ).addEventListener( 'click' , ( event ) => {
    this.activateItem( event );

    if( typeof item.onClick === 'function')
     item.onClick(  );
   });

  });

  return this;
 }

 add( options ){
  this.populateListFromOptions( options ) 
  return this;
 }
 
 remove( itemID ){
  let myNewArray;

  if( Array.isArray( itemID ) ){
   itemID.forEach( ( id ) => {
    this.list.forEach( ( item , idx ) => {
     if( id == item.id ){
      this.list.splice( idx , 1);
     }
    });
   });
  }
  else{
   this.list.forEach( ( item , idx ) => {
    if( itemID == item.id ){
     this.list.splice( idx , 1);
    }
   });
  }
  
  return this;
 }

 getElement( itemID ){
  return document.getElementById( itemID );
 }

 setStatus( itemID , itemOptions ){
  let myStatus   = (itemOptions.value == "active") ? "active" : "inactive";
  let myItemNode = this.getElement( itemID );
  
  if( !myItemNode ){
   console.log( "Node with id '"+itemID+"' does not Exist" );
   return this;
  }

  if( myStatus === "inactive" ){
   this.getElement( itemID ).classList.remove("active");
  }else{
   this.getElement( itemID ).classList.add(myStatus);
  }

  this.setListAttribute( itemID , "status" , myStatus );

  return this;
 }
 
 setListAttribute( itemID , attribute , value ){
  this.list.forEach( ( item ) => {
   if( value === null ){
    if( itemID == item.id ) {
     delete item[attribute];
    }
   }
   else{
    if( itemID == item.id ) {
     item[attribute] = value;
    }
   }
  });
 }

 getIconHTML( itemID , iconOptions ){
  let myType      = iconOptions.type  || "pill";
  let myColor     = iconOptions.color || "primary";
  let myValue     = typeof iconOptions.value !== 'undefined' ? iconOptions.value : "No Value";

  return '<span class="badge badge-'+myColor+' badge-'+myType+'" id="badge-'+itemID+'">'+myValue+'</span>'
 }

 setIcon( itemID , itemOptions ){
  let myBudgeNode = this.getElement( "badge-"+itemID );

  if( typeof itemOptions === 'undefined' ){
    myBudgeNode.remove();
    this.setListAttribute( itemID , "icon" , null );
  }
  else{
   if( myBudgeNode )
    myBudgeNode.remove();

   this.getElement( itemID ).insertAdjacentHTML( "beforeEnd" ,  this.getIconHTML( itemID , itemOptions ) ); 
   this.setListAttribute( itemID , "icon" , itemOptions );
  }

  return this;
 }

}

/*
var myNavBar = new amList( 'navbar-cnt' , [
 { "name" : "House" , "id" : "id1" , "onClick" : ( event ) => { console.log("House"); } , "style" : "primary" , "status" : "active"   },
 { "name" : "Car"   , "id" : "id2" , "onClick" : ( event ) => { console.log("Car");   } , "style" : "primary" , "status" : "disabled" },
 { "name" : "Test"  , "id" : "id3" , "onClick" : ( event ) => { console.log("Test");  } , "style" : "primary" },
 { "name" : "Tree"  , "id" : "id4" , "onClick" : ( event ) => { console.log("Tree");  } , "style" : "primary" }
]).render();

myNavBar.render();
*/
/* Documentation */
/*

Some Methods are directly affecting the DOM and some are settings that will need re-rendering of our List.
The DOM or LIST designate in which category each method falls. 
The DOM methods , can not be executed if no ".render()" method has been called prior to their call.
The LIST methods , need a ".render()" call at the end in order for changes to take place.
You can chain methods , if you follow the above rule.

Simple Initialization 
>> var myNavBar = new amList( DOM_ELEMENT_ID );

Add Item
Method Name : add
Category    : LIST
>> myNavBar.add({
>>  "name"    : "DISPLAYED_NAME" , 
>>  "id"      : "DOM_ID_THAT_WILL_BE_CREATED" , 
>>  "onClick" : ( event ) => { console.log("Clicked"); } ,
>>  "status"  : "active",  // Available Values ["active" , "inactive"] defaults to "inactive"
>>  "style"   : "primary"  // Available Values [ "primary" , "secondary" , "success" , "danger" , "warning" , "info" , "light" , "dark" ] defaults to "primary"
>> });

Remove Item
Method Name : remove
Category    : LIST
>> myNavBar.remove( "DOM_ID_OF_ITEM" );

Change Status
Method Name : setStatus
Category    : DOM
>> myNavBar.setStatus( "DOM_ID_OF_ITEM" , {"value" : "inactive"});

Set Icon
Method Name : setIcon
Category    : DOM
>> myNavBar.setIcon( "DOM_ID_OF_ITEM" , { 
>>   "value" : 666,       // Can be anything. Even HTML.
>>   "color" : "primary", // Available Values [ "primary" , "secondary" , "success" , "danger" , "warning" , "info" , "light" , "dark" ] defaults to "primary"
>>   "type"  : "pill"     // No other options available at the moment. Defualts to "pill".
>> });

Remove Icon
Method Name : setIcon
Category    : DOM
>> myNavBar.setIcon( "DOM_ID_OF_ITEM" );

Render the List
Method Name : render
Category    : LIST
>> myNavBar.render();

Note : You need to call Render whenever you make changes in the layout like Adding or Removing items from the list.
       If you change Styling or Icons there is no need for render cause those changes go directly on the DOM.

Multi Initialization

One Call
>> var myNavBar = new amList( 'navbar-cnt' , [
>>  { "name" : "House" , "id" : "id1" , "onClick" : ( event ) => { console.log("House"); } , "icon" : { "value" : 15 , "color" : "danger"} , "style" : "primary" , "status" : "active"   },
>>  { "name" : "Car"   , "id" : "id2" , "onClick" : ( event ) => { console.log("Car");   } , "icon" : { "value" : 1  , "color" : "info" } , "style" : "primary" , "status" : "disabled" },
>>  { "name" : "Test"  , "id" : "id3" , "onClick" : ( event ) => { console.log("Test");  } , "icon" : { "value" : "ok" } , "style" : "primary" },
>>  { "name" : "Tree"  , "id" : "id4" , "onClick" : ( event ) => { console.log("Tree");  } , "style" : "primary" }
>> ]).render();

Chaining
>> var myNavBar = new amList( 'navbar-cnt' );
>> 
>> myNavBar
>>  .add({ "name" : "A"  , "id" : "id1" , "onClick" : ( event ) => { console.log("1");  } })
>>  .add({ "name" : "B"  , "id" : "id2" , "onClick" : ( event ) => { console.log("2");  } })
>>  .add({ "name" : "C"  , "id" : "id3" , "onClick" : ( event ) => { console.log("3");  } })
>>  .remove( "id2" )
>>  .add({ "name" : "D"  , "id" : "id4" , "onClick" : ( event ) => { console.log("4");  } })
>>  .render()
>>  .setIcon( "id4" , { "value" : "this is a test" })
>>  .setStatus( "id1" , { "value" : "active" } );

*/