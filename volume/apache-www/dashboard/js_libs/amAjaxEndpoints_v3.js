class AjaxEndpoints{

  constructor(){
   
  }

  getLocationMeasurements( location_id , fromDate , toDate , dataset ){
   return AJAX( "../ws/ws.graph.php" , {
     "entity"      : dataset , 
     "locationid"  : location_id , 
     "fromdate"    : fromDate , 
     "todate"      : toDate
    });
  }

  getAggregates( parcel_id , location_id , fromDate , toDate ){
   return AJAX( "../ws/ws.graph.php" , { 
     "entity"       : "analytics_irrigation" , 
     "analytics_id" : 15,
     "locationid"   : location_id , 
     "parcel"       : parcel_id , 
     "fromdate"     : fromDate,
     "todate"       : toDate
   })
  }

  getChillingDays( parcel_id , location_id , fromDate , toDate , chilling_low_threshold , chilling_high_threshold ){
   return AJAX( "../ws/ws.graph.php" , { 
     "entity"                  : "analytics_chilling_days" , 
     "chilling_low_threshold"  : chilling_low_threshold ,
     "chilling_high_threshold" : chilling_high_threshold ,
     "locationid"              : location_id , 
     "parcel"                  : parcel_id , 
     "fromdate"                : fromDate,
     "todate"                  : toDate
   })
  }

  getLocationForecast( location_id , fromDate , toDate , dataset ){
   return AJAX( "../ws/ws.graph.php" , {
     "entity"      : dataset , 
     "locationid"  : location_id , 
     "fromdate"    : fromDate , 
     "todate"      : toDate
    });
  }

  getCarbonFootprintOutput( location_id , fromDate , toDate ){
   return AJAX( "../ws/ws.generic.php" , {
     "action"      : "getCarbonFootprintOutput" , 
     "locationid"  : location_id , 
     "fromdate"    : fromDate , 
     "todate"      : toDate
    });
  }

  getCarbonFootprintInput( location_id , fromDate , toDate ){
   return AJAX( "../ws/ws.generic.php" , {
     "action"      : "getCarbonFootprintInput" , 
     "locationid"  : location_id , 
     "fromdate"    : fromDate , 
     "todate"      : toDate
    });
  }

  getNDVI( location_id , fromDate , toDate ){
   return AJAX( "../ws/ws.graph.php" , {
     "entity"      : "ndvi" , 
     "locationid"  : location_id , 
     "fromdate"    : fromDate , 
     "todate"      : toDate
    });
  }

  getLocationEvents( location_id , fromDate , toDate , event_type ){
   return AJAX( "../ws/ws.generic.php" , {
     "action"      : "getEvents" , 
     "locationid"  : location_id , 
     "fromdate"    : fromDate , 
     "todate"      : toDate , 
     "event_type"  : event_type 
    });
  }

  getDataFromServer( serviceName , serviceAction , location_id , fromDate , toDate , event_type ){
   return AJAX( "../ws/ws." + serviceName + ".php" , {
     "action"      : serviceAction , 
     "locationid"  : location_id , 
     "fromdate"    : fromDate , 
     "todate"      : toDate , 
     "event_type"  : event_type 
    });
  }

  getParcelDetails( parcel_id ){
   return AJAX( "../ws/ws.generic.php" , {
     "action"    : "getParcelDetails" , 
     "parcel_id" : parcel_id 
    });
  }

  getAgronomyData( parcel_id ){
   return AJAX( "../ws/ws.generic.php" , {
     "action"    : "getAgronomyData" , 
     "parcelid" : parcel_id 
    });
  }

}