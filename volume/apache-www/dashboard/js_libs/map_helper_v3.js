var GameLoop    = 0;
var currentZoom = 0;
var GlobalSpeed = 1;

class amMapTools{
  constructor( options ){
    console.log( "Parser Initiated" );
  }

  getPointArrayFromPGSQLPolygon( polygon ){
    let myParsedPolygon  = "";
    let myTempArray      = new Array();
    let myGeometry       = new Array();

    myTempArray = polygon
                    .replace( /POLYGON/g , "")
                    .replace( /\(\(/g , "")
                    .replace( /\)\)/g , "")
                    .split( "," );

    myTempArray.forEach( ( pointPair ) => {
     let mySplitPair = pointPair.split( " " );
     let myLongitude = mySplitPair[ 0 ];
     let myLatitude  = mySplitPair[ 1 ];
     myGeometry.push( { "lat" : parseFloat( myLatitude ), "lng" : parseFloat( myLongitude ) } );
    });

    return myGeometry;
  }

  getHighsAndLowsFromPolygons( polygonArray ){

    let myResponse = { "xmin" : [] , "ymin" : [] , "xmax" : [] , "ymax" : []  };

    polygonArray.forEach( ( polygon ) => {
      let polygonHighsAndLows = this.getHighsAndLowsFromPolygon( polygon );
      myResponse[ "xmin" ].push( polygonHighsAndLows[ "xmin" ] );
      myResponse[ "ymin" ].push( polygonHighsAndLows[ "ymin" ] );
      myResponse[ "xmax" ].push( polygonHighsAndLows[ "xmax" ] );
      myResponse[ "ymax" ].push( polygonHighsAndLows[ "ymax" ] );
    });

    return myResponse;
  }

  getHighsAndLowsFromPolygon( polygon ){
    let myPolygonPath = this.getPolygonPath( polygon );
    let myArray       = {
      "x" : new Array(),
      "y" : new Array()
    };

    myPolygonPath.forEach( ( item ) => {
      myArray[ "x" ].push( parseFloat( item[ 1 ] ) );
      myArray[ "y" ].push( parseFloat( item[ 0 ] ) );
    });

    return {
      "xmax" : Math.max( ...myArray[ "x" ] ),
      "ymax" : Math.max( ...myArray[ "y" ] ),
      "xmin" : Math.min( ...myArray[ "x" ] ),
      "ymin" : Math.min( ...myArray[ "y" ] )
    }
  }

  getPGSQLPolygonFromBounds( bounds ){
    return "POLYGON((" +
      bounds[ "xmin" ] + " " + bounds[ "ymin" ] + "," +
      bounds[ "xmin" ] + " " + bounds[ "ymax" ] + "," +
      bounds[ "xmax" ] + " " + bounds[ "ymax" ] + "," +
      bounds[ "xmax" ] + " " + bounds[ "ymin" ] + "," +
      bounds[ "xmin" ] + " " + bounds[ "ymin" ]
    "))";
  }

  getMaxValues( myNewArray ){
    let response = {};

    response[ "xmax" ] = Math.max( ...myNewArray[ "xmax" ] );
    response[ "xmin" ] = Math.min( ...myNewArray[ "xmin" ] );
    response[ "ymax" ] = Math.max( ...myNewArray[ "ymax" ] );
    response[ "ymin" ] = Math.min( ...myNewArray[ "ymin" ] );

    return response;
  }

  getPolygonPath( polygon ){
   let myPointArray    = new Array();
   let myPolygonPoints = polygon.getPath().getArray();
   
   myPolygonPoints.forEach( ( point ) => {
     myPointArray.push( [ point.lat() , point.lng() ] );
   });

   return myPointArray;
  } 

  getPQSLPolygonFromGooglePolygon( googlePath , type ){
    let myArray = [];
    if( typeof type === "undefined" ){
        googlePath.forEach( ( point ) => {
          myArray.push( point.lng() + " " + point.lat() );
        });
        myArray.push( myArray[ 0 ] );
        
        return "POLYGON((" + myArray.join( "," ) + "))";     
    }
    else{
      if( type == "box" ){
        googlePath.forEach( ( point ) => {
          myArray.push( point.lng() + "," + point.lat() );
        });
        return myArray.join( "," );
      }
      else if( type == "polygon" ){
        googlePath.forEach( ( point ) => {
          myArray.push( point.lng() + " " + point.lat() );
        });
        myArray.push( myArray[ 0 ] );
        
        return "POLYGON((" + myArray.join( "," ) + "))";
      }
    }
  }
}

class amMap{
  constructor( options ){
    console.log( "map loaded" );
    this.mType     = typeof options.mapType  === 'undefined' ? 'SATELLITE' : options.mapType;
    this.mZoom     = typeof options.zoom     === 'undefined' ? 14          : Number(options.zoom);
    this.targetID  = typeof options.targetID === 'undefined' ? "map"       : options.targetID;
    this.aCenter   = typeof options.center   === 'undefined' ? { lat: -34.397, lng: 150.644 } : options.center;
    this.polygons  = {};
    this.paths     = [];
    this.markers   = [];

    this.map       = new google.maps.Map(
       document.getElementById( this.targetID ), 
       {
         zoom              : this.mZoom,
         disableDefaultUI  : true,
         center            : this.aCenter,
         mapTypeId         : google.maps.MapTypeId[ this.mType ] // TERRAIN , ROADMAP
       }
    );
  }

  addMarkerToMap( marker , id , markerOptions ){
    var myMarker = marker;
    myMarker[ "id" ] = id;

    if( typeof markerOptions !== "undefined" && markerOptions !== "" && typeof markerOptions === "object" ){
      myMarker[ "properties" ] = markerOptions[ "properties" ];
    }

    myMarker.setMap( this.map );

    if( typeof markerOptions.callback === 'function' ){
      myMarker.addListener( 'click', ( e ) => {
        markerOptions.callback( myMarker );
      });
    }
    
    this.polygons[ id ] = myMarker;

    return this;
  }

  getMarkerFromPoint( coords , options ){
    let icon = ( typeof options !== 'undefined' && typeof options[ "icon" ] !== "undefined" ) ? options[ "icon" ] : "";

    return new google.maps.Marker({
      position : { 
       lat: Number( coords.lat ), 
       lng: Number( coords.lng ) 
      },
      icon : icon
    });
  }

  addPolygonToMap( polygonOptions ){
    var myPolygon = polygonOptions[ "polygon" ];
    myPolygon[ "id" ] = polygonOptions[ "id" ];

    if( typeof polygonOptions !== "undefined" && polygonOptions !== "" && typeof polygonOptions === "object" ){
      myPolygon[ "properties" ] = polygonOptions[ "properties" ];
    }

    myPolygon.setMap( this.map );

    if( typeof polygonOptions.callback === 'function' ){
      myPolygon.addListener( 'click', ( e ) => {
        polygonOptions.callback( myPolygon );
      });
    }

    this.polygons[ polygonOptions[ "id" ] ] = myPolygon;

    return this;
  }

  getPolygonFromPointArray( coords , options ){
    let myPolygonOptions = { "paths" : coords };

    if( typeof options !== "undefined" ){
      Object.keys( options ).forEach( ( property ) => {
        myPolygonOptions[ property ] = options[ property ];
      });
    }

    return new google.maps.Polygon( myPolygonOptions );
  }

  addPathToMap( path , id , pathOptions ){
    var myPath     = path;
    myPath[ "id" ] = id;

    if( typeof pathOptions !== "undefined" && pathOptions !== "" && typeof pathOptions === "object" ){
      myPath[ "properties" ] = pathOptions[ "properties" ];
    }

    myPath.setMap( this.map );

    if( typeof pathOptions.callback === 'function' ){
      myPath.addListener( 'click', ( e ) => {
        pathOptions.callback( myPath );
      });
    }

    this.paths[ id ] = myPath;

    return this;
  }

  remove( googleGeoObject ){
    googleGeoObject.setMap( null );
  }

  getPathFromPointArray( coords , options ){
    let myPathOptions = { 
      "path" : coords 
    };

    if( typeof options !== "undefined" ){
      Object.keys( options ).forEach( ( property ) => {
        myPathOptions[ property ] = options[ property ];
      });
    }

    return new google.maps.Polyline(myPathOptions);
  }

  focusOnPolygon( polygonID ){
    this.polygons[ polygonID ].map.fitBounds( this.polygons[ polygonID ].getBounds() );
  }

  getPolygon( polygonID ){
    if( this.polygons.hasOwnProperty( polygonID ) )
       return this.polygons[ polygonID ];
    else
       return false;
  }
}

google.maps.Polygon.prototype.getBounds = function() {
    var bounds = new google.maps.LatLngBounds();
    var paths = this.getPaths();
    var path;        
    for (var i = 0; i < paths.getLength(); i++) {
        path = paths.getAt(i);
        for (var ii = 0; ii < path.getLength(); ii++) {
            bounds.extend(path.getAt(ii));
        }
    }
    return bounds;
}

/*

// Examples : 

myMarkers.push(
 addMarker(
  {
   lat        : Number(37.9297300), 
   lng        : Number(22.6793400),
   "callback" : (markerDetails , markerData)=>{ console.log(markerDetails , markerData); }
  } , map1 )
);

myPolygons.push(
 addPolygon({
  "geometry" : [
   { lat: 37.9297300 , lng: 22.6793400 } ,
   { lat: 37.9587300 , lng: 22.6893400 } ,
   { lat: 37.9577300 , lng: 22.7593400 } ,
   { lat: 37.9297300 , lng: 22.6793400 }
  ],
  "callback" : (data)=>{ console.log(data); }
 }, map1 )
);

*/