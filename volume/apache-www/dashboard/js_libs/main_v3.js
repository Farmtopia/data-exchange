const DEBUG = false;

var store = {
 "grid" : {
  "station" : {
   "hasdata" : false
  }
 }
};

var clock = 0;

$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
  // e.target // newly activated tab
  // e.relatedTarget // previous active tab
  if( DEBUG ) console.log( e );
})


/* Google Prototype */

// google.maps.Polygon.prototype.getBounds = function() {
    // var bounds = new google.maps.LatLngBounds();
    // var paths = this.getPaths();
    // var path;        
    // for (var i = 0; i < paths.getLength(); i++) {
        // path = paths.getAt(i);
        // for (var ii = 0; ii < path.getLength(); ii++) {
            // bounds.extend(path.getAt(ii));
        // }
    // }
    // return bounds;
// }


/* Global Loop */

setInterval( () => { clock = clock + 1; } , 100 );

document.addEventListener('ajaxFailed', function (e) { 
 if( DEBUG ) console.log( 'ajaxFailed' );
 getAlert( e.detail[ "error" ] );
}, false);

document.addEventListener('grid-date-change', function (e) { 
 if( DEBUG ) console.log( "grid-date-change" , e );
 let myEntity = e.detail.grid.options.customAJAX.entity;

 if( typeof store[ "frame" ] === 'undefined' )
  store[ "frame" ] = false;

 if ( store[ "frame" ] === false ){
  store[ "frame" ] = true;
  setTimeout( () => {
   store[ "frame" ] = false;

   try{
     let myStartDate = document.getElementById( e.detail.startDateElementId ).value == "" ? document.getElementById( e.detail.startDateElementId ).getAttribute( "value" ) : document.getElementById( e.detail.startDateElementId ).value;
     let myEndDate   = document.getElementById( e.detail.endDateElementId   ).value == "" ? document.getElementById( e.detail.endDateElementId   ).getAttribute( "value" ) : document.getElementById( e.detail.endDateElementId   ).value;
     let myResponse  = [];

     if( myStartDate == "" || !myStartDate ) return;
     switch( myEntity ){
       case "stationmeasurement" : 
         myResponse = getDataForGraph_meteo( store[ "location_id" ] , myStartDate , myEndDate );
       break;
       case "irrigation" : 
         myResponse = getDataForGraph_irrigation( store[ "location_id" ] , myStartDate , myEndDate );
       break;
     }
     // let myResponse = getDataForGraph_meteo( store[ "location_id" ] , myStartDate , myEndDate );
   }
   catch ( error ){
     console.log( "No data in grid" , error );
   }
  } , 500 );
 }

});

document.addEventListener('grid-ajax-complete', function (e) {
 if( DEBUG ) console.log( 'grid-ajax-complete' );

 let myEntity = getEntityFromGridEvent( e );

 if( myEntity ){

  switch( myEntity ){
   case "station" : 
    updateGridDataStatus( e );
   break;
   default:
    console.log( "No defined Entity" );
   break;
  }

 }
 
}, false);

document.addEventListener('grid-error', function (e) { 
 if( DEBUG ) console.log( 'grid-error' );
}, false);

document.addEventListener('ajax-initiate', function (e) { 
 if( DEBUG ) console.log( 'ajax-initiate' );
 toggleLoader( "on" );
}, false);

document.addEventListener('ajax-fail', function (e) { 
 if( DEBUG ) console.log( 'ajax-fail' );
 toggleLoader( "off" );
 getAlert( "Something went wrong while retrieving data. If the problem persists , please contact the system administrator" );
}, false);

document.addEventListener('ajax-aborted', function (e) { 
 if( DEBUG ) console.log( 'ajax-aborted' );
 toggleLoader( "off" );
 // getAlert( "Something went wrong while retrieving data. If the problem persists , please contact the system administrator" );
}, false);

document.addEventListener('ajax-success', function (e) { 
 if( DEBUG ) console.log( 'ajax-success' );
 toggleLoader( "off" );
}, false);

document.addEventListener('parcel-aggregation-requested', function (e) { 
 if( DEBUG ) console.log( e.detail );
}, false);

document.addEventListener('group-aggregation-requested', function (e) { 
 if( DEBUG ) console.log( e.detail );
}, false);

function getEntityFromGridEvent( gridEvent ){
 let myEntity = false;
 
 if( gridEvent.detail.options.hasOwnProperty( "customAJAX" ) ){
  if( gridEvent.detail.options[ "customAJAX" ].hasOwnProperty( "entity" ) ){
   myEntity = gridEvent.detail.options[ "customAJAX" ][ "entity" ];
  }
 }

 return myEntity;
}

function updateGridDataStatus( gridEvent ){
 if( gridEvent.detail.state.hasOwnProperty( "responseJSON" ) ){
  if( typeof gridEvent.detail.state[ "responseJSON" ][ "data" ] !== 'undefined' ){
   if( gridEvent.detail.state[ "responseJSON" ][ "data" ].length > 0 ){
    store[ "grid" ][ "station" ][ "hasdata" ] = true;
   }
   else{
    store[ "grid" ][ "station" ][ "hasdata" ] = false;
   }
  }
 }
}

function toggleLoader( status ){
 let myLoader = document.getElementById( "loader-container" );
 let myNavLinks = document
                    .querySelectorAll( ".nav-link" );

 if( myLoader )
  if( status == "off" ){
   myLoader.style.display = "none";
   myNavLinks.forEach( ( node ) => {
    node.classList.remove( "disabled" );
   });
  }
  else{
   myLoader.style.display = "block";
   myNavLinks.forEach( ( node ) => {
    node.classList.add( "disabled" );
   });
  }

}

function toggleLoaderV2( targetElementID ){
  if( DEBUG ) console.log( "Toggle Loader" );
  let myContainer  = document.getElementById( targetElementID );
  let myLoader     = document.getElementById( "loader-container-v2" );
  let myLoaderHTML = '<div class="loader-container" id="loader-container-v2" style="z-index:1; display: block; position: absolute; top: 30%; width: 100%;">' + 
                      '<div class="d-flex justify-content-center" style="background-color: var(--info); color: white; padding: 10px; width: 150px; margin: auto; border-radius: 4px;">' + 
                        '<span id="loader-container-v2-text" style="line-height: 30px; margin-right: 10px;">Loading...</span>' + 
                        '<div class="spinner-border" role="status">' + 
                        '</div>' + 
                      '</div>' + 
                     '</div>';
  if( myLoader ){
   if( DEBUG ) console.log( "Removing Loader" );
   myLoader.remove();
  }
  else{
   if( DEBUG ) console.log( "Adding Loader" );
   myContainer.insertAdjacentHTML( "afterBegin" , myLoaderHTML ); 
  }

}

function toggleLoaderV2SetText( targetElementID , text ){
  let myContainer  = document.getElementById( targetElementID );
  if( myContainer )
    myContainer.innerHTML = text;
  else
    console.log( "No element with id ["+targetElementID+"] can be found" );
}

function login(){
 var myUsername = document.getElementById("usr").value;
 var myPassword = document.getElementById("pw").value;

 if(!myUsername || !myPassword){
  showError("Missing Credentials");
 }

 AJAX("../ws/ws.usrmgmt.php" , 
      { "action" : "login" , "un" : myUsername , "pw" : myPassword })
      .then( (data) => {
        if( data.error ){
         getAlert("Invalid Password. Please try again.");
         return;
        }
        else{
         top.window.location.href = "./index.php";
        }
      })
      .catch( ( error ) => {
       getAlert("Invalid Password. Please try again.");
      });
}

function logout(){
 AJAX("../ws/ws.usrmgmt.php" , 
      { "action" : "logout" } )
      .then( (data) => {
        if( data.error ){
         top.window.location.href = "./login.php";
        }
        else
         top.window.location.href = "./login.php";
      });
}

function getUserTabs( user_access ){  
  var myFormsEntityArray     = [ ];
  var myMenuItemsArray       = [ ];
  myFormsEntityArray = [ 
    "datasources"
  ];
  myMenuItemsArray = [
   { "datasources"          : { "translation" : "Test Smart Farming Application" } } 
   // ,
   // {
    // "aggregation" : {
     // "translation" : "Aggregation" , 
     // "children" : [
       // { "compare_panel"    : { "translation" : "compare_panel" } },
       // { "control_panel"    : { "translation" : "control_panel" } }
     // ]
    // }
   // }
  ];

  return{
    "myFormsEntityArray" : myFormsEntityArray,
    "myMenuItemsArray"   : myMenuItemsArray
  }  
}

function getSingleLi( options ){
  return '<li class="nav-item">' + 
  '<a class="nav-link '+options.active+'" ' + 
  'id="nav-'+options.name+'-tab" ' + 
  'data-toggle="tab" ' + 
  'navname="'+options.name+'" ' + 
  'navaction="'+options.name+'" ' + 
  'href="#nav-'+options.name+'" ' + 
  'role="tab" ' + 
  'aria-controls="nav-'+options.name+'" ' + 
  'aria-selected="true">'+lang(options.locale , myLanguage )+'</a>' + 
  '</li>';
}

function getMultiLi( options ){
 let myHTML = '<li class="nav-item dropdown">';
     myHTML += '<a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" navname="'+options.parentLocale+'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + 
     lang( options.parentLocale , myLanguage )
     '</a>';
     myHTML += '<div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">';
      options.children.forEach( ( childItem ) => {
       Object.keys( childItem ).forEach( ( childName ) => {
        myHTML += '<a class="dropdown-item" aria-selected="false" aria-controls="nav-'+childName+'" data-toggle="tab" role="tab" navname="'+childName+'" navaction="'+childName+'" href="#nav-'+childName+'">' + 
         lang( childItem[childName]["translation"] , myLanguage ) + 
        '</a>';
       });
      });
     myHTML += '</div>';
     myHTML += '</li>';

 return myHTML;
}

function addTabs(){
 var myMainNavContainer     = document.getElementById( "nav-tab" );
 var myMainContentContainer = document.getElementById( "nav-tabContent" );
 var myFormsEntityArray     = [ ];
 var myMenuItemsArray       = [ ];
 var userTabs;

 AJAX("../ws/ws.usrmgmt.php" , 
      { "action" : "getProfileId" })
      .then( (data) => {
        if( data.error ){
         getAlert("Invalid ProfileID. Please try again.");
         return;
        }
        else{
          userTabs = getUserTabs( data );
          myFormsEntityArray = userTabs[ "myFormsEntityArray" ];
          myMenuItemsArray   = userTabs[ "myMenuItemsArray" ];

          myMenuItemsArray.forEach( ( menuItemObject ) => {

           Object.keys( menuItemObject ).forEach( ( parentName ) => {

            if( typeof menuItemObject[parentName]["children"] !== 'undefined' ){
             myMainNavContainer.insertAdjacentHTML( "beforeEnd" , 
              getMultiLi( { 
               "parentName"   : parentName , 
               "parentLocale" : menuItemObject[parentName]["translation"] , 
               "children"     : menuItemObject[parentName]["children"]
              } )
             );
            }
            else{

             myMainNavContainer.insertAdjacentHTML( "beforeEnd" , 
              getSingleLi( { 
               "locale" : menuItemObject[parentName]["translation"] , 
               "name"   : parentName , 
               "active" : false , 
               "multi"  : false
              } )
             );

            }

           });

          });

          myFormsEntityArray.forEach((tabName , idx)=>{
           var isActive = (idx === 0) ? "show active" : "";
           myMainContentContainer.insertAdjacentHTML(
            "beforeEnd" , 
            "<div class='tab-pane fade "+isActive+"' id='nav-"+tabName+"' role='tabpanel' aria-labelledby='nav-"+tabName+"-tab'>" + 
             "<div class='am-search-nav' id='"+tabName+"-search-nav' class='"+tabName+"-search-nav'></div>" + 
             "<div class='am-form-main' id='"+tabName+"-form-main'></div>" + 
             "<div class='am-form-sub' id='"+tabName+"-form-sub'></div>" + 
            "</div>");
          });

          // All Menu Items Listener [Main NavBar Menus]
             $( '.nav-link' ).on( 'click' , function(){
               let myClickedElement = $( this )[ 0 ];
               let aSelectedTab     = myClickedElement.getAttribute( "navname" );
               store[ "nav_name" ]  = aSelectedTab;
               store[ "nav_child" ] = false;
               
               // if( isRequestRunning ) myAjaxController.abort();

               getViewLayout( aSelectedTab , myClickedElement );

             });

          // Menu Settings Menu Listener [Top Right Avatar Menu]
             $( '.dropdown-item' ).on( 'click' , function( e ){
              let myClickedElement = $( this )[ 0 ];
              let aSelectedTab     = myClickedElement.getAttribute( "navname" );
              store[ "nav_child" ] = aSelectedTab;

              getViewLayout( aSelectedTab , myClickedElement );

             });


        }
      })
      .catch( ( error ) => {
       getAlert("Invalid ProfileID. Please try again.");
      });


}

function setLanguageStyle( myLanguage ){
  document.getElementById( 'language-selection' ).querySelectorAll( "img" ).forEach( ( item ) => {
   item.style.opacity = 0.3;
   item.style.border  = "0px solid #00000080";
  });
  document.getElementById( 'lang-flag-' + myLanguage ).style.opacity = 1;
  document.getElementById( 'lang-flag-' + myLanguage ).style.border  = "1px solid #00000080";
}

function setLanguage( locale ){

  if( myLanguage === locale ){
   getModal({
     "title"        : lang("info"),
     "body"         : lang("change_language_no_available"),
     "buttons"      : [ "cancel" ],
     "cancelButton" : lang("cancel"),
     "modalStyle"   : "small"
    })
  }
  else{
   getModal(
    {
     "title"        : lang("caution"),
     "body"         : lang("change_language_prompt"),
     "buttons"      : [ "accept" , "cancel" ],
     "acceptButton" : lang("change"),
     "cancelButton" : lang("cancel"),
     "modalStyle"   : "small",
     "action"     : {
      "submit"    : ( ev )=>{
       AJAX("../ws/ws.usrmgmt.php" , 
            { "action" : "setLocale" , "locale" : locale })
            .then( (data) => {
              if( data.error ){
               getAlert("Changing Locale Failed.");
               return;
              }
              else
               top.window.location.href = "./index.php";
            })
            .catch( ( error ) => {
             getAlert("Changing Locale Failed.");
            });
      }
     }
    }
   )
 
  }


}

function lang(myString){
 var myTempString     = new String(myString).toLowerCase();
 var translatedString = "";
 var myFirstLetter;
 var LangMapping = {
  "α" : "a", "β" : "b", "γ" : "g", "δ" : "d", "ε" : "e", "ζ" : "z",  "η" : "i",   "θ" : "th",
  "ι" : "i", "κ" : "k", "λ" : "l", "μ" : "m", "ν" : "n", "ξ" : "x",  "ο" : "o",   "π" : "p",
  "ρ" : "r", "σ" : "s", "τ" : "t", "υ" : "u", "φ" : "f", "χ" : "ch", "ψ" : "ps",  "ω" : "o",
  "ή" : "i", "ί" : "i", "ό" : "o", "ώ" : "o", "ά" : "a", "έ" : "e",  "ύ" : "u",   "ς" : "s"
 };

 try{
  translatedString = eval("LANG_"+ ( myTempString.replace(/ /g, '_') ) );
 }catch(err){
   if(typeof myLanguage === 'undefined' || myLanguage === 'el') return myString.replace(/_/g, ' ');

   myFirstLetter = LangMapping[myTempString[0].toLowerCase()];

  if(typeof myFirstLetter !== 'undefined'){
   Object.keys(myTempString).forEach(function(letter , index){
    if(index === 0){
     if(typeof LangMapping[myTempString[letter]] === 'undefined'){
       translatedString += myTempString[letter];
     }else{
       translatedString += LangMapping[myTempString[letter]].toUpperCase();
     }
    }else
     if(typeof LangMapping[myTempString[letter]] === 'undefined'){
       translatedString += myTempString[letter];
     }else{
       translatedString += LangMapping[myTempString[letter]].toUpperCase();
     }
   });

  }
  else
   translatedString = myTempString;
 }

 return translatedString;
}

function togglePasswordVisibility( e ){
 e.preventDefault();
 
 let closeEye = '<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-slash-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
                 '<path d="M10.79 12.912l-1.614-1.615a3.5 3.5 0 0 1-4.474-4.474l-2.06-2.06C.938 6.278 0 8 0 8s3 5.5 8 5.5a7.029 7.029 0 0 0 2.79-.588zM5.21 3.088A7.028 7.028 0 0 1 8 2.5c5 0 8 5.5 8 5.5s-.939 1.721-2.641 3.238l-2.062-2.062a3.5 3.5 0 0 0-4.474-4.474L5.21 3.089z"/>' + 
                 '<path d="M5.525 7.646a2.5 2.5 0 0 0 2.829 2.829l-2.83-2.829zm4.95.708l-2.829-2.83a2.5 2.5 0 0 1 2.829 2.829z"/>' + 
                 '<path fill-rule="evenodd" d="M13.646 14.354l-12-12 .708-.708 12 12-.708.708z"/>' + 
                '</svg>';

 let openEye = '<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
                '<path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>' + 
                '<path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>' + 
               '</svg>';

 let myPasswordElement = document.getElementById('pw');
 let myType            = myPasswordElement.getAttribute("type");
 let myIconElement     = document.getElementById('pw-toggler');

 if( myType == "password" ){
  myPasswordElement.setAttribute("type" , "text");
  myIconElement.innerHTML = openEye;
 }else{
  myPasswordElement.setAttribute("type" , "password");
  myIconElement.innerHTML = closeEye;
 }
}