class amForm{

  constructor(options){
   this.myContainer;
   this.myForm;
   this.options;
   this.readonly;
   this.formvalues;
   this.uniqueFormID   = options.formid || "amForm-"+this.randomUID(5);
   this.customAJAX     = options.customAJAX || false;
   this.hasCRUDButtons = false;

   // console.log("Form Initialization... ID : " + this.uniqueFormID);

   if(typeof options == 'undefined'){
    console.log("You need to provide settings.");
    return;
   }

// This Object is widely used by the Class. It consists of all the configuration properties needed 
// to build our Form. Check documentation for all the available properties.
   this.options = options;

// Clear all the DOM Elements that might already exist.
   this.resetDomElements();   

// Set the Form "this.options.formid" on the DOM "this.options.container"
   this.myForm = this.setForm();

// Create all our Input Fields
   this.addInputs(this.options.inputs);

// Create all our Input Fields
   // if(typeof this.options.tableActions === 'undefined')
    // this.addCRUDButtons();

// Add our Hidden Inputs
   if(typeof this.options.hiddeninputs !== 'undefined')
    this.addHiddenInputs(this.options.hiddeninputs);

// Add our Submit Button
   // this.addSumbitButton();

// Set readonly state
   if(typeof this.options.readonly !== 'undefined')
    this.setReadonly(this.options.readonly);

// Populate with Data if provided
   if(typeof this.options.data !== 'undefined')
     this.populate(this.options.data);

  }

  randomUID(length) {
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
  }

  resetDomElements(){
   document.getElementById(this.options.container).innerHTML = "";
  }

  setForm(){
   this.myContainer = document.getElementById(this.options.container);
   this.myContainer.insertAdjacentHTML("beforeEnd" , "<form id='"+this.uniqueFormID+"'></form>");

   return document.getElementById(this.uniqueFormID);
  }

  addInputs(myInputs){
   var nOfRows     = 0;
   var self        = this;
   var myInputHTML = "";
   var nOfColumns  = typeof this.options.columns !== 'undefined' ? this.options.columns : 1;

   myInputs.forEach((inputOptions)=>{
    if(typeof inputOptions.id           === 'undefined'){ console.log("Provide an Input ID"   , inputOptions);   }
    if(typeof inputOptions.name         === 'undefined'){ console.log("Provide an Input Name" , inputOptions);   }
    if(typeof inputOptions.default      === 'undefined'){ inputOptions.default      = "";       }
    if(typeof inputOptions.placeholder  === 'undefined'){ inputOptions.placeholder  = "";       }
    if(typeof inputOptions.type         === 'undefined'){ inputOptions.type         = "text";   }
    if(typeof inputOptions.rowspan      === 'undefined'){ inputOptions.rowspan      = false;    }
    if(typeof inputOptions.options      === 'undefined'){ inputOptions.options      = false;    }
    if(typeof inputOptions.sign         === 'undefined'){ inputOptions.sign         = false;    }
    if(typeof inputOptions.required     === 'undefined'){ inputOptions.required     = false;    }
    if(typeof inputOptions.action       === 'undefined'){ inputOptions.action       = false;    }
    if(typeof inputOptions.validation   === 'undefined'){ inputOptions.validation   = false;    }
    
    inputOptions.id = inputOptions.id + "_" + this.uniqueFormID;

    if(typeof inputOptions.label === 'undefined'){
     inputOptions.label = "";
    }
    else{
     let myTranslation = lang( inputOptions.label , myLanguage );
     inputOptions.label = '<label for="'+inputOptions.id+'">'+myTranslation+'</label>';
    }

    if(typeof inputOptions.help === 'undefined'){
     inputOptions.help = "";
    }
    else{
     inputOptions.help = '<small id="'+inputOptions.type+'Help" class="form-text text-muted">'+inputOptions.help+'</small>';
    }

    var myInputSpecificHTML = this.getInputHTML({
     "type"        : inputOptions.type,
     "id"          : inputOptions.id,
     "default"     : inputOptions.default,
     "placeholder" : inputOptions.placeholder,
     "name"        : inputOptions.name,
     "options"     : inputOptions.options,
     "label"       : inputOptions.label,
     "icon"        : inputOptions.icon,
     "sign"        : inputOptions.sign,
     "required"    : inputOptions.required,
     "action"      : inputOptions.action,
     "validation"  : inputOptions.validation
    });

    if(nOfRows % nOfColumns == 0){
     myInputHTML += '<div class="form-row">';
    }

    if(inputOptions.rowspan){
     myInputHTML += '</div>';
     myInputHTML += '<div class="form-row">';
     nOfRows = nOfColumns;
    }else
     nOfRows = nOfRows + 1;

    myInputHTML += '<div class="col">' + 
                    inputOptions.label + 
                    '<div class="form-group">' + 
                     '<div class="input-group mb-3">' + 
                      myInputSpecificHTML + 
                     '</div>' + 
                     inputOptions.help + 
                    '</div>' + 
                   '</div>';

    if(nOfRows % nOfColumns == 0 || inputOptions.rowspan){
     nOfRows = 0;
     myInputHTML += '</div>';
    }
   });

   this.myForm.insertAdjacentHTML("afterBegin",myInputHTML);

   myInputs.forEach((inputOptions)=>{

    if(inputOptions.type == "date"){
     jQuery('#'+inputOptions.id)
      .datetimepicker(
       { 'format': 'YYYY-MM-DD' , "sideBySide" : false , "showToday" : true }
      );
    }
    else if(inputOptions.type == "time"){
     jQuery('#'+inputOptions.id)
      .datetimepicker(
       { 'format': 'HH:mm' , "sideBySide" : false , "showToday" : true }
      );
    }
    else{
     jQuery('#'+inputOptions.id)
      .datetimepicker(
       { 'format': 'YYYY-MM-DD HH:mm' , "sideBySide" : false , "showToday" : true }
      );
    }

    if(inputOptions.action){
     let myNodeID;
     let myNode = document.getElementById(inputOptions.id);
     if( !myNode ){
      myNode = document.getElementById(inputOptions.id+"-frmbtn");
     }

     myNode.addEventListener(inputOptions.action.type,(event)=>{
      event.preventDefault();

      if(typeof inputOptions.default      === 'undefined'){ inputOptions.default      = "";     }
      if(typeof inputOptions.placeholder  === 'undefined'){ inputOptions.placeholder  = "";     }
      if(typeof inputOptions.type         === 'undefined'){ inputOptions.type         = "text"; }
      if(typeof inputOptions.rowspan      === 'undefined'){ inputOptions.rowspan      = false;  }
      if(typeof inputOptions.options      === 'undefined'){ inputOptions.options      = false;  }
      if(typeof inputOptions.sign         === 'undefined'){ inputOptions.sign         = false;  }
      if(typeof inputOptions.required     === 'undefined'){ inputOptions.required     = false;  }
      if(typeof inputOptions.action       === 'undefined'){ inputOptions.action       = false;  }

      inputOptions.action.event({
       "ev"          : event,
       "id"          : inputOptions.id,
       "type"        : inputOptions.type,
       "default"     : inputOptions.default,
       "placeholder" : inputOptions.placeholder,
       "name"        : inputOptions.name,
       "options"     : inputOptions.options,
       "label"       : inputOptions.label,
       "icon"        : inputOptions.icon,
       "sign"        : inputOptions.sign,
       "required"    : inputOptions.required,
       "action"      : inputOptions.action,
       "readonly"    : self.readonly
      });
     });
    }

   });

   let acUrl    = "http://195.181.243.78:5050/destinations_autocomplete?q=";
   let acPath   = ["matching_destinations"];
   let acReturn = "name";
   setAmAutoComplete({
    "url"    : acUrl,
    "path"   : acPath,
    "return" : acReturn,
    "minLen" : 0
   });

  }

  setReadonly(status){
   var ent = this.myForm.querySelectorAll("input , select , textarea");

   if(status){
    this.readonly = true;
    ent.forEach((formItem)=>{
     formItem.readOnly = true;
     formItem.disabled = true;
    });
   }
   else{
    this.readonly = false;
    ent.forEach((formItem)=>{
     formItem.readOnly = false;
     formItem.disabled = false;
    });    
   }
  }

  resetFormFields(){
   var ent = this.myForm.querySelectorAll("input , select , textarea , email , number");

   ent.forEach((formItem)=>{
    switch(formItem.type){
     case "text"       : formItem.value   = "";      break;
     case "textarea"   : formItem.value   = "";      break;
     case "checkbox"   : formItem.checked = false;   break;
     case "select-one" : formItem.value   = "";      break;
     case "number"     : formItem.value   = "";      break;
     case "email"      : formItem.value   = "";      break;
    }
   });

  }

  toggleCRUDButton(type){
   if( !this.hasCRUDButtons ) return;

   switch(type){
    case "cancel" : 
     this.setReadonly(true);
     this.populate(this.formvalues);

     document.getElementById('cancel-record-'+this.options.container).style.display = "none";

     document.getElementById('update-record-'+this.options.container).setAttribute("togglestatus","inactive");
     document.getElementById('update-record-'+this.options.container).className     = "btn btn-primary btn-sm rounded-0";
     document.getElementById('update-record-'+this.options.container).innerHTML     = this.getIcon('edit') + " " + lang( "Edit" , myLanguage );
     document.getElementById('update-record-'+this.options.container).style.display = "block";
     document.getElementById('update-record-'+this.options.container).blur();

     document.getElementById('add-record-'+this.options.container).setAttribute("togglestatus","inactive");
     document.getElementById('add-record-'+this.options.container).className        = "btn btn-primary btn-sm rounded-0";
     document.getElementById('add-record-'+this.options.container).innerHTML        = this.getIcon('add') + " " + lang( "Add" , myLanguage );
     document.getElementById('add-record-'+this.options.container).style.display    = "block";
     document.getElementById('add-record-'+this.options.container).blur();
    break;
    case "update":
     var ButtonStatus = document.getElementById('update-record-'+this.options.container).getAttribute("togglestatus");

     if(ButtonStatus == "inactive"){
      this.setReadonly(false);
      document.getElementById('update-record-'+this.options.container).setAttribute("togglestatus","active");
      document.getElementById('update-record-'+this.options.container).className     = "btn btn-warning btn-sm rounded-0";
      document.getElementById('update-record-'+this.options.container).innerHTML     = this.getIcon('save') + " " + lang( "Save" , myLanguage );
      document.getElementById('add-record-'+this.options.container).style.display    = "none";
      document.getElementById('cancel-record-'+this.options.container).style.display = "block";
      document.getElementById('cancel-record-'+this.options.container).blur();
     }
     else{
      this.setReadonly(true);
      this.populate(this.formvalues);
      console.log(this.formvalues);
      document.getElementById('update-record-'+this.options.container).setAttribute("togglestatus","inactive");
      document.getElementById('update-record-'+this.options.container).className     = "btn btn-primary btn-sm rounded-0";
      document.getElementById('update-record-'+this.options.container).innerHTML     = this.getIcon('save') + " " + lang( "Save" , myLanguage );
      document.getElementById('add-record-'+this.options.container).style.display    = "block";
      document.getElementById('add-record-'+this.options.container).blur();
      document.getElementById('cancel-record-'+this.options.container).style.display = "none";
     }
     break;
     case "add" : 
      var ButtonStatus = document.getElementById('add-record-'+this.options.container).getAttribute("togglestatus");
      if(ButtonStatus == "inactive"){
       this.setReadonly(false);
       this.resetFormFields();
       document.getElementById('add-record-'+this.options.container).setAttribute("togglestatus","active");
       document.getElementById('add-record-'+this.options.container).className        = "btn btn-warning btn-sm rounded-0";
       document.getElementById('add-record-'+this.options.container).innerHTML        = this.getIcon('save') + " " + lang( "Save" , myLanguage );
       document.getElementById('update-record-'+this.options.container).style.display = "none";
       document.getElementById('cancel-record-'+this.options.container).style.display = "block";
       document.getElementById('cancel-record-'+this.options.container).blur();
      }
      else{
       this.setReadonly(true);
       this.populate(this.formvalues);
       document.getElementById('add-record-'+this.options.container).setAttribute("togglestatus","inactive");
       document.getElementById('add-record-'+this.options.container).className        = "btn btn-primary btn-sm rounded-0";
       document.getElementById('add-record-'+this.options.container).innerHTML        = this.getIcon('add') + " " + lang( "Add" , myLanguage );
       document.getElementById('add-record-'+this.options.container).blur();
       document.getElementById('update-record-'+this.options.container).style.display = "block";
       document.getElementById('cancel-record-'+this.options.container).style.display = "none";
      }
     break;
   }
  }

  addCRUDButtons(){
   this.hasCRUDButtons = true;
   var myContainerHTML = "<div class='table-actions-buttons-container' id='table-actions-container-"+this.options.container+"'></div>";
   var myAddButton     = "<button togglestatus='inactive' style='position: relative; float: left;' id='add-record-"+this.options.container+"' type='button' class='btn btn-primary btn-sm rounded-0'>"+this.getIcon('add')+" Add</button>";
   var myUpdateButton  = "<button togglestatus='inactive' style='position: relative; float: left;display:block;' id='update-record-"+this.options.container+"' type='button' class='btn btn-primary btn-sm rounded-0'>"+this.getIcon('edit')+" Edit</button>";
   var myCancelButton  = "<button togglestatus='inactive' style='position: relative; float: left;display:none;' id='cancel-record-"+this.options.container+"' type='button' class='btn btn-danger btn-sm rounded-0'>"+this.getIcon('cancel')+" Cancel</button>";
   var myContainer;

   this.myForm.insertAdjacentHTML("afterBegin" , myContainerHTML);
   myContainer = document.getElementById('table-actions-container-'+this.options.container);
   myContainer.insertAdjacentHTML("afterBegin",myCancelButton);
   myContainer.insertAdjacentHTML("afterBegin",myUpdateButton);
   myContainer.insertAdjacentHTML("afterBegin",myAddButton);

   document.getElementById('cancel-record-'+this.options.container)
    .addEventListener('click' , (event)=>{
     this.toggleCRUDButton("cancel");

     console.log("clicked cancel");
    });

   document.getElementById('update-record-'+this.options.container)
    .addEventListener('click' , (event)=>{
     var ButtonStatus = document.getElementById('update-record-'+this.options.container).getAttribute("togglestatus");

     if(ButtonStatus == "active"){
      this.updateRecord()
      .then( () => console.log( "Item Updated") )
      .catch( ( error ) => console.log( error ) );      
     }
     else{
      if( !this.options.data ){
        getModal({"title" : "No item selected" , "body" : "No item selected for Edit" , "buttons" : ["cancel"] });
       return;
      }
      else{
       this.toggleCRUDButton("update");  
      }
     }
    });

   document.getElementById('add-record-'+this.options.container)
    .addEventListener('click' , (event)=>{
     var ButtonStatus = document.getElementById('add-record-'+this.options.container).getAttribute("togglestatus");

     if(ButtonStatus == "active"){
      this.addRecord()
      .then( () => console.log( "Item Added") )
      .catch( ( error ) => console.log( error ) );
     }
     else{
      this.toggleCRUDButton("add"); 
     }
    });
  }

  addHiddenInputs(myInputs){

   myInputs.forEach((inputOptions)=>{
    if(typeof inputOptions.id           === 'undefined'){ console.log("Missing ID for hidden input with name [ "+inputOptions.name+" ]"); }
    if(typeof inputOptions.name         === 'undefined'){ console.log("Missing Name for hidden input with id [ "+inputOptions.id+" ]"); }
    if(typeof inputOptions.default      === 'undefined'){ inputOptions.default = ""; }

    let inputID = inputOptions.id+"_"+this.uniqueFormID;

    var myInputHTML = "<input type='hidden' name='"+inputOptions.name+"' id='"+inputID+"' value='"+inputOptions.default+"'></input>";
    this.myForm.insertAdjacentHTML("afterBegin",myInputHTML);
   });
  }

  addRecord(){
   console.log( "adding" );
   return new Promise ( ( resolve , reject ) => {
     console.log( "adding again" );
     var myFormSerialized = jQuery( document.getElementById(this.myForm.id) ).serializeArray();
     var validationResult = false;
     var myURL            = "";
     var myRequest        = {};

     this.myForm.reportValidity();

     if (this.myForm.checkValidity() === false)
      validationResult = false;
     else
      validationResult = true;

     myFormSerialized.forEach((item)=>{
      myRequest[ item[ "name" ] ] = item[ "value" ];
     });

     myFormSerialized.forEach((item)=>{
       myURL += "&"+item["name"]+"="+item["value"];
     });

     myFormSerialized.forEach((input)=>{
      var myInput = document.querySelector("#frm1 [name='"+input['name']+"']");
      if(myInput)
       myInput.style.backgroundColor = "rgba(255, 0, 0, 0.2)";
     });

     if(validationResult === true){
       AJAX("../ws/ws.form.php",
            // { ...{"entity" : this.options.entity , "action" : "addData" } , ...myRequest , ...this.customAJAX } , 
            Object.assign( {"entity" : this.options.entity , "action" : "addData" } , myRequest , this.customAJAX ) , 
            (response) => {
             console.log(response);
             if(response.error){
              getModal({"title":"Error Info" , "body" : response.error , "buttons" : ["cancel"]});
             }
             else{
              getModal({"title":"Addition Info" , "body" : "Succesfully Added" , "buttons" : ["cancel"] });
              if(typeof this.options.event == "function"){
               this.options.event({"event" : "addrecord"});
              }
              this.toggleCRUDButton("add");
              resolve();
             }
            }
           );
     }
     else{
      reject( "Failed form validation" );
     }
   });
  }

  updateRecord(){
   return new Promise( ( resolve , reject ) => {
     var myFormSerialized = jQuery( document.getElementById(this.myForm.id) ).serializeArray();
     var validationResult = false;
     var myURL            = "";
     var myRequest        = {};

     this.myForm.reportValidity();

     if (this.myForm.checkValidity() === false)
      validationResult = false;
     else
      validationResult = true;

     myFormSerialized.forEach((item)=>{
      myRequest[ item[ "name" ] ] = item[ "value" ];
     });

     myFormSerialized.forEach((input)=>{
      var myInput = document.querySelector("#frm1 [name='"+input['name']+"']");
      if(myInput)
       myInput.style.backgroundColor = "rgba(255, 0, 0, 0.2)";
     });

     if(validationResult == true){
      // return new Promise( ( resolve , reject ) => {
       AJAX("../ws/ws.form.php",
            // { ...{"entity" : this.options.entity , "action" : "updateData" } , ...myRequest , ...this.customAJAX } , 
            Object.assign( {"entity" : this.options.entity , "action" : "updateData" } , myRequest , this.customAJAX ) , 
            (response) => {
             if(response.error){
              getModal({"title":"Error Info" , "body" : response.error , "buttons" : ["cancel"] });
             }
             else{
              getModal({"title":"Update Info" , "body" : "Succesfully Updated" , "buttons" : ["cancel"]});
              this.populate(myFormSerialized);
              if(typeof this.options.event == "function"){
               this.options.event({"event" : "updaterecord"});
              }
              this.toggleCRUDButton("update");
              resolve();
             }
            }
           );
      // });
     }
     else{
      reject( "Form Validation Failed" );
     }
   });
  }

  getIcon(type){
   var myIcon = "";
   switch(type){
    case "calendar" : 
     myIcon = '<svg class="bi bi-calendar" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
               '<path fill-rule="evenodd" d="M14 0H2a2 2 0 00-2 2v12a2 2 0 002 2h12a2 2 0 002-2V2a2 2 0 00-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z" clip-rule="evenodd"/>' + 
               '<path fill-rule="evenodd" d="M6.5 7a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd"/>' + 
              '</svg>';
    break;
    case "text" : 
     myIcon = '<svg class="bi bi-card-text" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
               '<path fill-rule="evenodd" d="M14.5 3h-13a.5.5 0 00-.5.5v9a.5.5 0 00.5.5h13a.5.5 0 00.5-.5v-9a.5.5 0 00-.5-.5zm-13-1A1.5 1.5 0 000 3.5v9A1.5 1.5 0 001.5 14h13a1.5 1.5 0 001.5-1.5v-9A1.5 1.5 0 0014.5 2h-13z" clip-rule="evenodd"/>' + 
               '<path fill-rule="evenodd" d="M3 5.5a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9a.5.5 0 01-.5-.5zM3 8a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9A.5.5 0 013 8zm0 2.5a.5.5 0 01.5-.5h6a.5.5 0 010 1h-6a.5.5 0 01-.5-.5z" clip-rule="evenodd"/>' + 
              '</svg>';
    break;
    case "email" : 
     myIcon = '<svg class="bi bi-at" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
               '<path fill-rule="evenodd" d="M13.106 7.222c0-2.967-2.249-5.032-5.482-5.032-3.35 0-5.646 2.318-5.646 5.702 0 3.493 2.235 5.708 5.762 5.708.862 0 1.689-.123 2.304-.335v-.862c-.43.199-1.354.328-2.29.328-2.926 0-4.813-1.88-4.813-4.798 0-2.844 1.921-4.881 4.594-4.881 2.735 0 4.608 1.688 4.608 4.156 0 1.682-.554 2.769-1.416 2.769-.492 0-.772-.28-.772-.76V5.206H8.923v.834h-.11c-.266-.595-.881-.964-1.6-.964-1.4 0-2.378 1.162-2.378 2.823 0 1.737.957 2.906 2.379 2.906.8 0 1.415-.39 1.709-1.087h.11c.081.67.703 1.148 1.503 1.148 1.572 0 2.57-1.415 2.57-3.643zm-7.177.704c0-1.197.54-1.907 1.456-1.907.93 0 1.524.738 1.524 1.907S8.308 9.84 7.371 9.84c-.895 0-1.442-.725-1.442-1.914z" clip-rule="evenodd"/>' + 
              '</svg>';
    break;
    case "password" : 
     myIcon = '<svg class="bi bi-eye-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
                '<path d="M10.5 8a2.5 2.5 0 11-5 0 2.5 2.5 0 015 0z"/>' + 
                '<path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 100-7 3.5 3.5 0 000 7z" clip-rule="evenodd"/>' + 
              '</svg>';
    break;
    case "geo" : 
     myIcon = '<svg class="bi bi-geo-alt" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
                '<path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 002 6c0 4.314 6 10 6 10zm0-7a3 3 0 100-6 3 3 0 000 6z" clip-rule="evenodd"/>' + 
              '</svg>';
    break;
    case "contact" : 
     myIcon = '<svg class="bi bi-people-circle" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
                '<path d="M13.468 12.37C12.758 11.226 11.195 10 8 10s-4.757 1.225-5.468 2.37A6.987 6.987 0 008 15a6.987 6.987 0 005.468-2.63z"/>' + 
                '<path fill-rule="evenodd" d="M8 9a3 3 0 100-6 3 3 0 000 6z" clip-rule="evenodd"/>' + 
                '<path fill-rule="evenodd" d="M8 1a7 7 0 100 14A7 7 0 008 1zM0 8a8 8 0 1116 0A8 8 0 010 8z" clip-rule="evenodd"/>' + 
              '</svg>';
    break;
    case "search" : 
     myIcon = '<svg class="bi bi-search" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' +
                '<path fill-rule="evenodd" d="M10.442 10.442a1 1 0 011.415 0l3.85 3.85a1 1 0 01-1.414 1.415l-3.85-3.85a1 1 0 010-1.415z" clip-rule="evenodd"/>' +
                '<path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 100-11 5.5 5.5 0 000 11zM13 6.5a6.5 6.5 0 11-13 0 6.5 6.5 0 0113 0z" clip-rule="evenodd"/>' +
              '</svg>';
    break;
    case "number" : 
     myIcon = '<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-calculator-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
                 '<path fill-rule="evenodd" d="M12 1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM4 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H4z"/>' + 
                 '<path d="M4 2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-2zm0 4a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm3-6a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm3-6a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-4z"/>' + 
                '</svg>';
    break;
    case "add" : 
     myIcon = '<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
                '<path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>' + 
                '<path fill-rule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>' + 
              '</svg>';
    break;
    case "edit" : 
     myIcon = '<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
                '<path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>' + 
                '<path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>' + 
              '</svg>';
    break;
    case "save" : 
     myIcon = '<i class="far fa-save"></i>';
    break;
    case "cancel" : 
     myIcon = '<i class="fas fa-ban"></i>';
    break;
    default : 
     myIcon = '<svg class="bi bi-card-text" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' + 
               '<path fill-rule="evenodd" d="M14.5 3h-13a.5.5 0 00-.5.5v9a.5.5 0 00.5.5h13a.5.5 0 00.5-.5v-9a.5.5 0 00-.5-.5zm-13-1A1.5 1.5 0 000 3.5v9A1.5 1.5 0 001.5 14h13a1.5 1.5 0 001.5-1.5v-9A1.5 1.5 0 0014.5 2h-13z" clip-rule="evenodd"/>' + 
               '<path fill-rule="evenodd" d="M3 5.5a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9a.5.5 0 01-.5-.5zM3 8a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9A.5.5 0 013 8zm0 2.5a.5.5 0 01.5-.5h6a.5.5 0 010 1h-6a.5.5 0 01-.5-.5z" clip-rule="evenodd"/>' + 
              '</svg>';    
    break;
   }

   return myIcon;
  }

  addSumbitButton(){
   var self = this;

   if(this.options.submit){
    this.myForm.insertAdjacentHTML("beforeEnd",
     '<button class="btn btn-primary" id="submit-'+this.myForm.id+'" type="button">'+
      'Submit'+
     '</button>'
    );

    document.getElementById("submit-"+this.myForm.id).addEventListener('click',(event)=>{
     var myFormSerialized = jQuery( document.getElementById(self.myForm.id) ).serializeArray();
     var validationResult = false;
     self.myForm.reportValidity();

     if (self.myForm.checkValidity() === false)
      validationResult = false;
     else
      validationResult = true;

     self.options.submit({
      "validation"     : validationResult,
      "formid"         : self.myForm.id,
      "values"         : myFormSerialized
     });

    });
   }
  }

  setSpinner(ElementID , state){
   switch(state){
    case "on" : 
     document.getElementById(ElementID).style.visibility = "visible";
    break;
    case "off" : 
     document.getElementById(ElementID).style.visibility = "hidden";
    break;
    default:
     document.getElementById(ElementID).style.visibility = "visible";
    break;
   }
  }

  setValidField(ElementID){
   var myTarget     = document.getElementById(ElementID);
   var myParent     = myTarget.parentElement;
   var myValidation = document.getElementById(ElementID+"-validation");
   if(myValidation)
    myValidation.remove();

   myTarget.className = "form-control is-valid";
   myParent.insertAdjacentHTML("beforeEnd",'<div id="'+ElementID+'-validation" class="valid-feedback">Looks good!</div>');
  }

  setInValidField(ElementID){
   var myTarget = document.getElementById(ElementID);
   var myParent = myTarget.parentElement;
   var myValidation = document.getElementById(ElementID+"-validation");
   if(myValidation)
    myValidation.remove();

   myTarget.className = "form-control is-invalid";
   myParent.insertAdjacentHTML("beforeEnd",'<div id="'+ElementID+'-validation" class="invalid-feedback">Issue!</div>');
  }  

  getInputHTML(options){
   var myInputHTML = "";
   var inputRequired = options.required ? "required" : "";
   var myValidation = "";
   let myPlaceHolder = lang( options.placeholder , myLanguage );
   
   if( options.validation ){
    myValidation = options.validation;
   }

   switch(options.type){
    case "text" : 
     myInputHTML +=  '<div class="input-group-prepend">' + 
                      '<span class="input-group-text" id="'+options.name+'-icon">' + this.getIcon(options.icon) + '</span>' + 
                     '</div>' + 
                     '<input type="text" class="form-control" ' + 
                     myValidation + 
                     'name="'+options.name+'"' + 
                     'id="'+options.id+'" ' + 
                     'placeholder="'+myPlaceHolder+'" ' + 
                     'aria-label="'+myPlaceHolder+'" ' + 
                     'aria-describedby="'+myPlaceHolder+'" '+inputRequired+' value="'+options.default+'">';
    break;
    case "autotext" : 
     myInputHTML +=  '<div class="input-group-prepend">' + 
                      '<span class="input-group-text" id="'+options.name+'-icon">' + this.getIcon(options.icon) + '</span>' + 
                     '</div>' + 
                     '<input type="text" class="form-control amAutoComplete" ' + 
                     myValidation + 
                     'name="'+options.name+'"' + 
                     'id="'+options.id+'" ' + 
                     'placeholder="'+myPlaceHolder+'" ' + 
                     'aria-label="'+myPlaceHolder+'" ' + 
                     'aria-describedby="'+myPlaceHolder+'" '+inputRequired+' value="'+options.default+'">';
    break;
    case "date" : 
     myInputHTML +=  '<div class="input-group-prepend">' + 
                       '<span class="input-group-text" id="'+options.name+'-icon">' + this.getIcon(options.icon) + '</span>' + 
                      '</div>' + 
                      '<input type="text" class="form-control datetimepicker-input" ' + 
                      myValidation + 
                      'name="'+options.name+'"' + 
                      'id="'+options.id+'" ' + 
                      'data-toggle="datetimepicker" ' +
                      'data-target="#'+options.id+'" ' +
                      'placeholder="'+myPlaceHolder+'" ' + 
                      'aria-label="'+myPlaceHolder+'" ' + 
                      'aria-describedby="'+myPlaceHolder+'" '+inputRequired+' value="'+options.default+'">';
    break;
    case "datetime" : 
     myInputHTML +=  '<div class="input-group-prepend">' + 
                       '<span class="input-group-text" id="'+options.name+'-icon">' + this.getIcon(options.icon) + '</span>' + 
                      '</div>' + 
                      '<input type="text" class="form-control datetimepicker-input" ' + 
                      myValidation + 
                      'name="'+options.name+'"' + 
                      'id="'+options.id+'" ' + 
                      'data-toggle="datetimepicker" ' +
                      'data-target="#'+options.id+'" ' +
                      'placeholder="'+myPlaceHolder+'" ' + 
                      'aria-label="'+myPlaceHolder+'" ' + 
                      'aria-describedby="'+myPlaceHolder+'" '+inputRequired+' value="'+options.default+'">';
    break;
    case "time" : 
     myInputHTML +=  '<div class="input-group-prepend">' + 
                       '<span class="input-group-text" id="'+options.name+'-icon">' + this.getIcon(options.icon) + '</span>' + 
                      '</div>' + 
                      '<input type="text" class="form-control datetimepicker-input" ' + 
                      myValidation + 
                      'name="'+options.name+'"' + 
                      'id="'+options.id+'" ' + 
                      'data-toggle="datetimepicker" ' +
                      'data-target="#'+options.id+'" ' +
                      'placeholder="'+myPlaceHolder+'" ' + 
                      'aria-label="'+myPlaceHolder+'" ' + 
                      'aria-describedby="'+myPlaceHolder+'" '+inputRequired+' value="'+options.default+'">';
    break;
    case "number" : 
     myInputHTML +=  '<div class="input-group-prepend">' + 
                      '<span class="input-group-text" id="'+options.name+'-icon">' + this.getIcon(options.icon) + '</span>' + 
                     '</div>' + 
                     '<input type="number" class="form-control" ' + 
                     myValidation + 
                     'name="'+options.name+'"' + 
                     'id="'+options.id+'" ' + 
                     'placeholder="'+myPlaceHolder+'" ' + 
                     'aria-label="'+myPlaceHolder+'" ' + 
                     'aria-describedby="'+myPlaceHolder+'" '+inputRequired+' value="'+options.default+'">';
    break;
    case "email" : 
     myInputHTML +=  '<div class="input-group-prepend">' + 
                      '<span class="input-group-text" id="'+options.name+'-icon">' + this.getIcon(options.icon) + '</span>' + 
                     '</div>' + 
                     '<input type="email" class="form-control" ' + 
                     myValidation + 
                     'name="'+options.name+'"' + 
                     'id="'+options.id+'" ' + 
                     'placeholder="'+myPlaceHolder+'" ' + 
                     'aria-label="'+myPlaceHolder+'" ' + 
                     'aria-describedby="'+myPlaceHolder+'" '+inputRequired+' value="'+options.default+'">';
    break;
    case "combo" : 
     myInputHTML +=  '<div class="input-group-prepend">' + 
                      '<span class="input-group-text" id="'+options.name+'-icon">' + this.getIcon(options.icon) + '</span>' + 
                     '</div>' + 
                     '<input type="text" class="form-control" ' + 
                     myValidation + 
                     'name="'+options.name+'"' + 
                     'id="'+options.id+'" ' + 
                     'placeholder="'+myPlaceHolder+'" ' + 
                     'aria-label="'+myPlaceHolder+'" ' + 
                     'aria-describedby="'+myPlaceHolder+'" '+inputRequired+'>';
    break;
    case "combo-multi" : 
     myInputHTML +=  '<button class="btn btn-block btn-primary btn-sm" id="'+options.id+'-frmbtn" '+inputRequired+'><i class="fas fa-plus-circle"> ' + options.label+'</i></button>';
    break;
    case "combo-multi-form" : 
     myInputHTML +=  '<button class="btn btn-block btn-primary btn-sm" id="'+options.id+'-frmbtn" '+inputRequired+'><i class="fas fa-plus-circle"> ' + options.label+'</i></button>';
    break;
    case "textarea" : 
     myInputHTML += '<div class="input-group-prepend">' + 
                      '<span class="input-group-text" id="'+options.name+'-icon">' + this.getIcon(options.icon) + '</span>' + 
                     '</div>' + 
                     '<textarea class="form-control" ' + 
                     myValidation + 
                     'name="'+options.name+'"' + 
                     'id="'+options.id+'" ' + 
                     'aria-label="'+myPlaceHolder+'" ' + 
                     'aria-describedby="'+myPlaceHolder+'" rows="3" '+inputRequired+'>'+options.default+'</textarea>';
    break;
    case "select" : 
     myInputHTML += '<div class="input-group-prepend">' + 
                      '<span class="input-group-text" id="'+options.name+'-icon">' + this.getIcon(options.icon) + '</span>' + 
                     '</div>' + 
                     '<select class="form-control" ' + 
                     'name="'+options.name+'"' + 
                     'id="'+options.id+'" ' + 
                     'aria-label="'+myPlaceHolder+'" ' + 
                     'aria-describedby="'+myPlaceHolder+'" '+inputRequired+'>';

     myInputHTML += '<option value=""></option>';

     Object.keys(options.options).forEach((item)=>{
      if(item == options.default)
       myInputHTML += '<option value="'+item+'" selected>'+options.options[item]+'</option>';
      else
       myInputHTML += '<option value="'+item+'">'+options.options[item]+'</option>';
     });
     myInputHTML += '</select>';
    break;
    case "checkbox" : 
     var isChecked = options.default == "checked" ? "checked" : "";
     myInputHTML += '<div class="form-check">' + 
                     '<input class="form-check-input" ' + 
                      'type="checkbox" ' + 
                      'name="'+options.name+'"' + 
                      'id="'+options.id+'" '+isChecked+' '+inputRequired+'>' + 
                      '<label class="form-check-label" for="'+options.id+'">' + options.sign + '</label>' + 
                    '</div>';
    break;
    default : 
     myInputHTML +=  '<div class="input-group-prepend">' + 
                      '<span class="input-group-text" id="'+options.name+'-icon">' + this.getIcon(options.icon) + '</span>' + 
                     '</div>' + 
                     '<input type="text" class="form-control" ' + 
                     myValidation + 
                     'name="'+options.name+'"' + 
                     'id="'+options.id+'" ' + 
                     'placeholder="'+myPlaceHolder+'" ' + 
                     'aria-label="'+myPlaceHolder+'" ' + 
                     'aria-describedby="'+myPlaceHolder+'" '+inputRequired+' value="'+options.default+'">';
    break;
   }

   return myInputHTML;

  }

  randomUID(length) {
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return "uid_"+result;
  }

  getModal(options , callback){
    var validationResult;
    var myBodyHTML;
    
    console.log("calliong:" , options);

    if(document.getElementById(options.id)){
     document.getElementById(options.id).remove();
    }

    myBodyHTML = options.body || "";

    if(typeof options.id === 'undefined'){
     options.id = this.randomUID(10);
    }

    document.body.insertAdjacentHTML(
     "beforeEnd",
     '<div class="modal fade" id="'+options.id+'" tabindex="-1" role="dialog" aria-labelledby="'+options.id+'" aria-hidden="true"></div>'
    );

    document.getElementById(options.id).insertAdjacentHTML(
     "beforeEnd",
     '<div id="" class="modal-dialog modal-xl" role="document">' +
                      '<div class="modal-content">' +
                        '<div class="modal-header">' +
                          '<h5 class="modal-title" id="'+options.id+'-title">'+options.title+'</h5>' +
                          '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
                            '<span aria-hidden="true">&times;</span>' +
                          '</button>' +
                        '</div>' +
                        '<div class="modal-body" id="'+options.id+'-body">' +
                        myBodyHTML + 
                        '</div>' +
                        '<div class="modal-footer">' +
                          '<button type="button" id="am-'+options.id+'-close" class="btn btn-secondary" data-dismiss="modal">Close</button>' +
                          '<button type="button" id="am-'+options.id+'-submit" class="btn btn-primary">Ok</button>' +
                        '</div>' +
                      '</div>' +
                    '</div>'
    );

    document.getElementById("am-"+options.id+"-close").addEventListener('click' , (event)=>{
     setTimeout(()=>{
      document.getElementById(options.id).remove(); 
     },500);
    });

    if(options.action.submit){
     document.getElementById("am-"+options.id+"-submit").addEventListener('click' , (event)=>{
      var myForm           = document.querySelector("#"+options.id+" form");
      var mySerializedForm = jQuery(myForm).serializeArray();

      if(options.validate){
       myForm.reportValidity();

       if (myForm.checkValidity() === false){
        validationResult = false;
       }else{
        options.action.submit({
         "event"  : event,
         "form"   : document.querySelectorAll("#"+options.id+" [name]"),
         "values" : mySerializedForm
        });
        $('#'+options.id).modal('hide');
        setTimeout(()=>{
         document.getElementById(options.id).remove(); 
        },500);
       }

      }
      else{
       options.action.submit({
        "event"  : event,
        "form"   : document.querySelectorAll("#"+options.id+" [name]"),
        "values" : mySerializedForm
       });
       $('#'+options.id).modal('hide');
       setTimeout(()=>{
        document.getElementById(options.id).remove(); 
       },500);
      }
     });
    }

    $('#'+options.id).modal();

    if(typeof callback === 'function'){
     callback();
    }
  }

  populate(data){

   this.formvalues = data;

   Object.keys(this.formvalues).forEach((item)=>{
    let itemID = item+"_"+this.uniqueFormID;

    this.options.inputs.forEach((input)=>{
     let myInputID = input["id"];

     if(itemID == myInputID){

      if(input["type"] == "checkbox"){
       if(this.formvalues[item] == "on"){
        document.getElementById(myInputID).checked = true;
       }
       else{
        document.getElementById(myInputID).checked = false;
       }
      }
      else{
       if( document.getElementById(myInputID) )
        document.getElementById(myInputID).value = this.formvalues[item];
      }

     }
    });

    if(this.options.hiddeninputs){
     this.options.hiddeninputs.forEach((input)=>{
      let myInputID = input["id"]+"_"+this.uniqueFormID;
      if(itemID == myInputID){
        document.getElementById(myInputID).value = this.formvalues[item];
      }
     });    
    }
   });

  }
}