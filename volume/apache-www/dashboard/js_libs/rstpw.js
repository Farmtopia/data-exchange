/* Reset Password */

function validateEmail( email ){
 let regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
 if( email.match( regex ) ) 
   return true;
 else
   return false;
}

function requestReset(){
  let email = window.prompt( "Please provide your Email" );
  if( validateEmail( email ) ){
    AJAX( "https://gaiasense.neuropublic.gr/resetpassword/ws/ws.resetpassword.php" , 
          "action=request_reset" + 
          "&email=" + email , 
          ( response ) => {
            console.log( response );
            if( response.hasOwnProperty( "error" ) ){
              console.log( "Error : " , response );
              alert( "Failed to send email. Please try again later." );
            }
            else{
              console.log( "Success : " , response );
              alert( "Successfully requested a reset. Check your Email Box for a reset link." );
              document.getElementById( "request-reset-form-button" ).disabled = true;
            }
          },'','',true
    );
  }
  else{
    alert( "Invalid Email" );
  }
}