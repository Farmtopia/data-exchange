const myWorkerVersion = 4;
const myHost = 'http://localhost/directory/';
const myRootPath = 'directory';
const myDispatchers      = myHost+'/dispatcher/';
const myCacheName        = 'cache-v1';
const myDynamicCacheName = 'v1-dynamic';
const myCachedResources  = [
  myHost,
  'service-worker_v'+myWorkerVersion+'.js',
  'template/default/css/main_v'+myWorkerVersion+'.css',
  'js_libs/pwa_v'+myWorkerVersion+'.js',
  'js_libs/map_helper_v'+myWorkerVersion+'.js',
  'js_libs/main_v'+myWorkerVersion+'.js',
  'js_libs/high_graphs_v'+myWorkerVersion+'.js',
  'views/js_libs/custom_v'+myWorkerVersion+'.js',
  'template/default/css/amgrid_v'+myWorkerVersion+'.css',
  'template/default/site_items/loading.gif',
  'template/default/site_items/home_btn.png',
  'template/default/site_items/timezone.png',
  'template/default/site_items/logout_btn.png',
  'template/default/site_items/lang-flags.png',
  'template/default/site_items/settings_icon.png',
  'template/default/site_items/logos/greece.png',
  'template/default/site_items/logos/espa.png',
  'template/default/site_items/logos/gaiasense2.png',
  'template/default/site_items/logos/eu.png',
  'template/default/site_items/logos/attiki.png',
  'index.php',
  'views/default.php',
  'views/homepage.php',
  'views/login.php',
  'views/enemies-view.php',
  'views/profile-view.php',
  'views/remote-view.php',
  'views/thresholds-view.php',
  'localization/lang.el.js',
  'localization/lang.en.js',
  'js_libs/jquery_ui.js',
  'js_libs/highjs/highstock.js',
  'js_libs/highjs/highcharts-3d.js',
  'views/js_libs/dropdown.js',
  'views/js_libs/quick_graph.js',
  'views/js_libs/widgets.js',
  'template/default/css/jquery-ui.css',
  'template/default/css/nanoscroller.css'
];

self.addEventListener('install', event => {
  event.waitUntil(
    caches.open(myCacheName)
      .then(cache => {
        return cache.addAll(myCachedResources);
      }).then(()=>{
       // console.log("success in loading cache");
      })
  );
});

self.addEventListener('fetch', event => {
  // if (event.request.url.startsWith(myDispatchers)) {
    event.respondWith(
      caches.open(myDynamicCacheName).then((cache) => {
        return fetch(event.request)
          .then((response) => {
            cache.put(event.request, response.clone());
            return response;
          }).catch(() => {
           // console.log("Retrieving from cache : " , event.request);
           return caches.match(event.request);
          });
      })
    );
});

self.addEventListener('activate', event => {
  var expectedmyCacheNames = Object.keys(myCachedResources).map(function(key) {
    return myCachedResources[key];
  });

  event.waitUntil(
    caches.keys().then(function(myCacheNames) {
      return Promise.all(
        myCacheNames.map(function(myCacheName) {
          if (expectedmyCacheNames.indexOf(myCacheName) === -1) {
            return caches.delete(myCacheName);
          }
        })
      );
    })
  );
});
