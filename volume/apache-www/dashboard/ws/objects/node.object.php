<?php
/*
- It provides the headers of the Grid
- It provides where the Global Search will be executed on
*/

// Options unique on each Entity
   $myOptions = array(
    "entity"        => "node",
    "uniqueIndex"   => "id",
    "searchableColumns" => array( "id" , "name" , "deployment" ),
    "headers" => array(
      "id" => array(
       "locale"    =>"id" ,
       "type"      => "string",
       "style"     => "text"
      ),
      "name" => array(
       "locale"    =>"name" ,
       "type"      => "string",
       "style"     => "text"
      ),
      "deployment" => array(
       "locale"    =>"deployment" ,
       "type"      => "string",
       "style"     => "text"
      ),
      "sensor_name" => array(
       "locale"    =>"sensor_name" ,
       "type"      => "string",
       "style"     => "text"
      ),
      "sensor_quantitykind" => array(
       "locale"    =>"sensor_quantitykind" ,
       "type"      => "string",
       "style"     => "text"
      ),
      "description" => array(
       "locale"    =>"description" ,
       "type"      => "string",
       "style"     => "text"
      ),
      "state" => array(
       "locale"    =>"state" ,
       "type"      => "string",
       "style"     => "text"
      )
    )
   );
?>

