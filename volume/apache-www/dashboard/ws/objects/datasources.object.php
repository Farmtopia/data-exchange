<?php
/*
- It provides the headers of the Grid
- It provides where the Global Search will be executed on
*/

// Options unique on each Entity
   $myOptions = array(
    "entity"        => "node",
    "uniqueIndex"   => "id",
    "searchableColumns" => array( "id" , "name" , "service" ),
    "headers" => array(
      "id" => array(
       "locale"    =>"id" ,
       "type"      => "string",
       "style"     => "text"
      ),
      "name" => array(
       "locale"    =>"name" ,
       "type"      => "string",
       "style"     => "text"
      ),
      "description" => array(
       "locale"    =>"description" ,
       "type"      => "string",
       "style"     => "text"
      ),
      "service" => array(
       "locale"    =>"service" ,
       "type"      => "string",
       "style"     => "text"
      ),
      "data" => array(
       "locale"    =>"data" ,
       "type"      => "string",
       "style"     => "text"
      ),
      "swagger" => array(
       "locale"    =>"swagger" ,
       "type"      => "string",
       "style"     => "text"
      ),
      "openapi" => array(
       "locale"    =>"openapi" ,
       "type"      => "string",
       "style"     => "text"
      ),
      "metadata" => array(
       "locale"    =>"metadata" ,
       "type"      => "string",
       "style"     => "text"
      )
    )
   );
?>

