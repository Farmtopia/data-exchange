<?php
  if( !isset($_SESSION) ) { session_start(); }

  require_once("../php_libs/lib.main.php");
    $Settings = new main();

  require_once("../php_libs/lib.db.php");
    $db = new getDBO();
    if( $db->error ){
     echo json_encode( [ "error" => $db->error ] );
     return;
    }

  require_once("../php_libs/lib.ws.php");
    $ws = new amWS();

  require_once("../php_libs/lib.user.php");
    $user = new amUser( $_SESSION );
    if( !$user->id ){
     echo json_encode( [ "error" => "Invalid Access" ] );
     return;
    }

  require_once('../localization/lang.'.$user->getLocale().'.php');

 $action = $Settings->getVar( "action" );

 switch( $action ){
   case "toggleStatus"                       : toggleStatus();                                           break;
   case "getStatus"                          : getStatus();                                              break;
   case "getSavedCredentials"                : echo json_encode( getSavedCredentials() );                break;
   case "saveCredentials"                    : echo json_encode( saveCredentials() );                    break;
   case "authWithDirectory"                  : echo authWithDirectory();                                 break;
   case "getSubscriptionStatusForDatasource" : echo json_encode( getSubscriptionStatusForDatasource() ); break;
   case "subscribeToDataset"                 : echo json_encode( subscribeToDataset() );                 break;
   case "getDataFromDatasource"              : echo json_encode( getDataFromDatasource() );              break;
   case "getDataFromDatasourceCalendar"      : echo json_encode( getDataFromDatasourceCalendar() );      break;
   case "updateSubscriptionToken"            : echo json_encode( updateSubscriptionToken() );      break;
   
   default : echo json_encode( [ "error" => "Invalid Action" ] ); break;
 }

 function pr( $object ){
  print( "<pre>" ); 
  print_r( $object ); 
  print( "</pre>" );
 }

 function authWithDirectory(){
   global $ws;
   global $Settings;
   global $user;

   $username = $Settings->getVar( "username" );
   $password = $Settings->getVar( "password" );

   $ws->setEndPoint( $Settings->ws_directory[ "endpoint" ] );
   $ws->setMethod( "POST" );
   $ws->setEndPointPath( "token" );
   $ws->setParameters( "username={$username}&password={$password}" );
   $ws->setHeaders( [
     'Content-Type: application/x-www-form-urlencoded',
     'Accept: application/json'   
   ] );

   $wsResponse = $ws->cUrl( );

   if( isset( $wsResponse[ "data" ] ) && isset( $wsResponse[ "data" ]->access_token ) ){
     saveCredentials( $username , $password , $wsResponse[ "data" ]->access_token , 1 );
     return json_encode( $wsResponse );
   }
   else{
     return json_encode( [ "data" => [ "error" => "Failed to Authenticate" ] ] );
   }
 }

 function subscribeToDataset(){
   global $ws;
   global $Settings;
   global $user;

   $datasource_id = $Settings->getVar( "datasource_id" );
   $apiConnection = $user->getDirectoryJWT();
   $jwt           = $apiConnection[ "apikey" ];

   $ws->setEndPoint( $Settings->ws_directory[ "endpoint" ] );
   $ws->setMethod( "POST" );
   // $ws->setEndPointPath( "administration/subscribe?dataset_id={$datasource_id}" );
   $ws->setEndPointPath( "administration/user/subscription/dataset_id/{$datasource_id}" );
   
   // $ws->setEndPointPath( "administration/subscription/dataset_id/{$datasource_id}" );
   $ws->setHeaders( [
     'Content-Type: application/json',
     'Accept: application/json' , 
     'Authorization: Bearer ' . $jwt
   ] );

   $wsResponse = $ws->cUrl( );

   if( $wsResponse[ "status_code" ] == "401" ){
     return [ "error" => "Could not authenticate with directory. Connect again." , "status_code" => $wsResponse[ "status_code" ] ];
   }

   if( isset( $wsResponse[ "data" ] ) ){
     return [ "res" => $wsResponse ];
   }
   else{
     return [ "error" => "Could not subscribe to Dataset" ];
   }
 }

 function getDataFromDatasource(){
   global $ws;
   global $Settings;
   global $user;

   $datasource_id       = $Settings->getVar( "datasource_id" );
   $datasource_endpoint = $Settings->getVar( "endpoint" );
   $apiConnection       = $user->getDirectoryJWT();
   $jwt                 = $apiConnection[ "apikey" ];
   $mySubscription      = getSubscriptionForDatasource( $datasource_id );

   $ws->setEndPoint( $datasource_endpoint );
   $ws->setMethod( "GET" );
   $ws->setEndPointPath( "" );
   $ws->setHeaders( [
     'Content-Type: application/json',
     'Accept: application/json' , 
     'Authorization: Bearer ' . $mySubscription[ "data" ]->token
   ] );
   $wsResponseB = $ws->cUrl( );
   
   // print( "<pre>" );
   // print_r( $wsResponseB );
   // print( "</pre>" );
   
   if( isset( $wsResponseB[ "error" ] ) && $wsResponseB[ "error" ] != "" ){
     return array(
       "error" => json_encode( $wsResponseB[ "error" ] )
     );
   }

   if( isset( $wsResponseB[ "status_code" ] ) && $wsResponseB[ "status_code" ] != 200 ){
     return array(
       "error"       => $wsResponseB[ "data" ]->detail,
       "status_code" => $wsResponseB[ "status_code" ]
     );
   }

   $myParsedResponse = parseJSONLD( $wsResponseB );
   return array( 
     "data"   => $myParsedResponse[ "results" ] , 
     "jsonld" => $myParsedResponse[ "jsonld" ] , 
     "raw"    => $wsResponseB
   );

   // if( isset( $wsResponseB[ "data" ] ) ){
     // if( isset( $wsResponseB[ "data" ]->measurements ) ){
       // return $wsResponseB[ "data" ]->measurements;
     // }
     // else{
       // return [];
     // }
   // }
 }

 function updateSubscriptionToken(){
   global $ws;
   global $Settings;
   global $user;

   $datasource_id = $Settings->getVar( "datasource_id" );
   $apiConnection = $user->getDirectoryJWT();
   $jwt           = $apiConnection[ "apikey" ];

   $ws->setEndPoint( $Settings->ws_directory[ "endpoint" ] );
   $ws->setMethod( "POST" );
   $ws->setEndPointPath( "administration/user/subscription/dataset_id/{$datasource_id}/update" );
   $ws->setHeaders( [
     'Content-Type: application/json',
     'Accept: application/json' , 
     'Authorization: Bearer ' . $jwt
   ] );

   $wsResponse = $ws->cUrl( );

   if( $wsResponse[ "status_code" ] == "401" ){
     return [ "error" => "Could not updateT token. Connect again." , "status_code" => $wsResponse[ "status_code" ] ];
   }

   if( isset( $wsResponse[ "data" ] ) ){
     return [ "res" => $wsResponse ];
   }
   else{
     return [ "error" => "Could not Update Token" ];
   }
 }

 function getDataFromDatasourceCalendar(){
   global $ws;
   global $Settings;
   global $user;

   $datasource_id       = $Settings->getVar( "datasource_id" );
   $datasource_endpoint = $Settings->getVar( "endpoint" );
   $apiConnection       = $user->getDirectoryJWT();
   $jwt                 = $apiConnection[ "apikey" ];
   $mySubscription      = getSubscriptionForDatasource( $datasource_id );

   $ws->setEndPoint( $datasource_endpoint );
   $ws->setMethod( "GET" );
   $ws->setEndPointPath( "" );
   $ws->setHeaders( [
     'Content-Type: application/json',
     'Accept: application/json' , 
     'Authorization: Bearer ' . $mySubscription[ "data" ]->token
   ] );
   $wsResponseB = $ws->cUrl( );
   
   return $wsResponseB;

   // if( isset( $wsResponseB[ "data" ] ) ){
     // if( isset( $wsResponseB[ "data" ]->measurements ) ){
       // return $wsResponseB[ "data" ]->measurements;
     // }
     // else{
       // return [];
     // }
   // }
 }

 function getSubscriptionStatusForDatasource(){
   global $ws;
   global $Settings;
   global $user;

   $datasource_id       = $Settings->getVar( "datasource_id" );
   $apiConnection       = $user->getDirectoryJWT();
   $jwt                 = $apiConnection[ "apikey" ];

   $ws->setEndPoint( $Settings->ws_directory[ "endpoint" ] );
   $ws->setMethod( "GET" );
   $ws->setEndPointPath( "administration/user/subscription/dataset_id/" . $datasource_id );
   $ws->setHeaders( [
     'Content-Type: application/json',
     'Accept: application/json' , 
     'Authorization: Bearer ' . $jwt
   ] );

   $wsResponse = $ws->cUrl( );
   // print( "<pre>" ); print_r( $wsResponse ); print( "</pre>" );

   if( $wsResponse[ "status_code" ] == "401" ){
     return [ "error" => "Could not authenticate with directory. Connect again." , "status_code" => $wsResponse[ "status_code" ] ];
   }

   if( isset( $wsResponse[ "data" ] ) && isset( $wsResponse[ "data" ]->detail ) ){
     return [ "error" => $wsResponse[ "data" ]->detail ];
   }

   $hasDirectoryRecord    = false;
   $hasProviderRecord     = false;
   $hasProviderAcceptance = false;
   $myDirectoryResult     = [];
   $token                 = "";

   if( isset( $wsResponse[ "data" ] ) && isset( $wsResponse[ "data" ]->dataset_id ) ){
     if( (int)$wsResponse[ "data" ]->dataset_id === (int)$datasource_id ){
       $hasDirectoryRecord = true;
       $token              = $wsResponse[ "data" ]->token;
       if( (int)$wsResponse[ "data" ]->status == 3 ){
         $hasProviderRecord     = true;
         $hasProviderAcceptance = true;
       }
       else if( (int)$wsResponse[ "data" ]->status != 3 ){
         $hasProviderRecord     = true;
         $hasProviderAcceptance = false;
       }
     }
   }

/*
   // Old code when response would be an array. Now response is the Object of the Dataset.
   if( isset( $wsResponse[ "data" ] ) && sizeOf( $wsResponse[ "data" ] ) > 0 ){
     foreach( $wsResponse[ "data" ] as $key => $value ){
       if( (int)$value->dataset_id === (int)$datasource_id ){
         $hasDirectoryRecord = true;
         $token              = $value->token;
         if( (int)$value->is_active > 0 ){
           $hasProviderRecord     = true;
           $hasProviderAcceptance = true;
         }
         else if( (int)$value->is_active <= 0 ){
           $hasProviderRecord     = true;
           $hasProviderAcceptance = false;
         }
       }
     }
   }
*/

    // 1) User has no record in Directory = Unsubscribed
    // 2) User has    record in Directory but no record in provider = Unsubscribed
    // 3) User has    record in Directory and on provider is -1 = Pending
    // 4) User has    record in Directory and on provider is 1  = Sunscribed
    
    // var_dump( $hasDirectoryRecord );
    // var_dump( $hasProviderRecord );
    // var_dump( $hasProviderAcceptance );
    $ClientToken = ( isset( $wsResponse[ "data" ] ) && isset( $wsResponse[ "data" ]->token ) ) ? $wsResponse[ "data" ]->token : "";

    if( !$hasDirectoryRecord ){
      return [ "res" => "needs subscription" , "token" => $ClientToken ];
    }
    else{
      if( !$hasProviderRecord ){
        return [ "res" => "needs subscription" , "token" => $ClientToken ];
      }
      else{
        if( !$hasProviderAcceptance ){
          return [ "res" => "pending" , "token" => $ClientToken ];
        }
        else{
          return [ "res" => "subscribed" , "token" => $ClientToken ];
        }
      }
    }
 }

 function getSubscriptionForDatasource(){
   global $ws;
   global $Settings;
   global $user;

   $datasource_id       = $Settings->getVar( "datasource_id" );
   $apiConnection       = $user->getDirectoryJWT();
   $jwt                 = $apiConnection[ "apikey" ];

   $ws->setEndPoint( $Settings->ws_directory[ "endpoint" ] );
   $ws->setMethod( "GET" );
   $ws->setEndPointPath( "administration/user/subscription/dataset_id/" . $datasource_id );
   $ws->setHeaders( [
     'Content-Type: application/json',
     'Accept: application/json' , 
     'Authorization: Bearer ' . $jwt
   ] );

   $wsResponse = $ws->cUrl( );

   if( $wsResponse[ "status_code" ] == "401" ){
     return [ "error" => "Could not authenticate with directory. Connect again." , "status_code" => $wsResponse[ "status_code" ] ];
   }

   if( isset( $wsResponse[ "data" ] ) && isset( $wsResponse[ "data" ]->detail ) ){
     return [ "error" => $wsResponse[ "data" ]->detail ];
   }
   
   return $wsResponse;
 }

 function getDatasetJWT( $datasource_id ){
   global $db;
   global $user;
   global $Settings;

   // $api_id = $Settings->getVar( "api_id" );
   
   // if( $api_id == "" ){
     // return [ "error" => "Missing Properties in request" ];
   // }

   $myApiConnections = $db->loadAssocList(
     "select * from ".$Settings->AUTH_SCHEMA.".map_user_to_dataset where user_id = ? and dataset_id = ?" , 
     [ $user->id , $datasource_id ]
   );

   if( $db->error ){
     return [ "error" => $db->error ];
   }
   else{
     return $myApiConnections;
   }
 }

 function getSavedCredentials(){
   global $db;
   global $user;
   global $Settings;

   $api_id = $Settings->getVar( "api_id" );
   
   if( $api_id == "" ){
     return [ "error" => "Missing Properties in request" ];
   }

   $myApiConnections = $db->loadAssocList(
     "select * from ".$Settings->AUTH_SCHEMA.".api_connection where user_id = ? and id = ?" , 
     [ $user->id , $api_id ]
   );

   if( $db->error ){
     return [ "error" => $db->error ];
   }
   else{
     return $myApiConnections;
   }
 }

 function saveCredentials( $api_username , $api_password , $api_jwt , $api_id ){
   global $db;
   global $user;
   global $Settings;

   // $api_id       = $Settings->getVar( "api_id" );
   // $api_username = $Settings->getVar( "api_username" );
   // $api_password = $Settings->getVar( "api_password" );

   if( $api_id == "" || $api_username == "" || $api_password == "" ){
     return [ "error" => "Missing Properties in request" ];
   }

   $myApiConnections = $db->loadAssocList(
     "select * from ".$Settings->AUTH_SCHEMA.".api_connection where user_id = ?" , 
     [ $user->id ]
   );

   if( $db->error ){
     return [ "error" => $db->error ];
   }
   else{
     if( sizeOf( $myApiConnections ) > 0 ){
       $myApiUpdate = $db->loadAssocList(
         "update ".$Settings->AUTH_SCHEMA.".api_connection set api_username = ? , api_password = ? , apikey = ? where user_id = ? and id = ?" , 
         [ $api_username , $api_password , $api_jwt , $user->id , $api_id  ]
       );
       
       if( $db->error ){
         return [ "error" => $db->error ];
       }
       else{
         return [ "res" => "success" ];
       }
     }
     else{
       $myApiUpdate = $db->loadAssocList(
         "insert into ".$Settings->AUTH_SCHEMA.".api_connection " . 
         "( user_id , api_username , api_password , api_name , apikey ) values " . 
         "( ? , ? , ? , ? , ? ) " , 
         [ $user->id , $api_username , $api_password , "directory" , $api_jwt ]
       );
       
       if( $db->error ){
         return [ "error" => $db->error ];
       }
       else{
         return [ "res" => "success" ];
       }
     }
   }
 }

 function parseJSONLD( $JSONLD ){
   /*
   "@graph" : []
     "datasetSlices": []
       Object with property => "@id": "measurementsSlice"
       has a "sliceContents": []
         Object with property => "@id": "WeatherStation_1Observations"
           has a "sliceContents": []
             Objects with property => "@id" have arrays in "sliceContents" 
             with different measurements.
               - "@id": "AtmosphereObservationCollection_1"
               - "@id": "SurfaceObservationCollection_1"
               - "@id": "SoilObservationCollection_1"
               - "@id": "CanopyObservationCollection_1"
                 has a "sliceContents": []
                   Object has property => "sourceSensor": { "@id": "wsht30_temp", "@type": "sosa:Sensor" }
                   Object has property => "contents": []
                     Objects : {
                       "@id": "TemperatureAirObservation1",
                       "@type": "fsm:Observation",
                       "dateTime": "2024-02-26T16:00:00+03:00",
                       "value": "12.93"
                     }
   */
   
   $myCuratedJSON = array(
     "results" => [],
     "jsonld"  => []
   );
   
   // print( "<pre>" ); print_r( $JSONLD ); print( "</pre>" );

   $mySlices = $JSONLD[ "data" ]->{'@graph'};
   foreach( $mySlices as $key => $sliceValue ){
     // Issue. A lot of arrays making accessing properties inside the JSONLD very hard.
     // You need to goiterate arrays , search inside the response for the "@id" that you want 
     // and then continue again and again and again. You can not access properties instantly 
     // even when you know exactly what you want to find.

     // Graph is an array and has exactly one element :
     // "NP_gaiasense_serviceResource_representation_instance_1"
     // NOTE : I dont understand why it should be an array and not an object.
     // If the elements are predefined. Are they though?
     if( $sliceValue->{"@id"} == "NP_gaiasense_serviceResource_representation_instance_1" ){
       foreach( $sliceValue->datasetSlices as $sliceB => $sliceBValue ){

         // This array has the following two elements :
         // "locationsSlice"
         // "measurementsSlice"
         // NOTE : I dont understand why it should be an array and not an object.
         // If the elements are predefined. Are they though?
         if( $sliceBValue->{"@id"} == "measurementsSlice" ){
           foreach( $sliceBValue->sliceContents as $sliceC => $sliceCValue ){

             // This array has the following single element :
             // "WeatherStation_1Observations"
             // NOTE : I dont understand why it should be an array and not an object.
             // Probably obesrvations from multiple stations in the same JSONLD?
             if( $sliceCValue->{"@id"} == "WeatherStation_1Observations" ){
               foreach( $sliceCValue->sliceContents as $sliceD => $sliceDValue ){

                 // Whatever , its the same story again and again ...
                 if( $sliceDValue->{"@id"} == "AtmosphereObservationCollection_1" ){
                   foreach( $sliceDValue->sliceContents as $sliceE => $sliceEValue ){

                     if( $sliceEValue->{"@id"} == "AirTemperatureObservationCollection_1" ){
                       foreach( $sliceEValue->contents as $sliceF => $measurement ){
                         // $dataObject  = date_parse( $measurement->dateTime );
                         $myCuratedJSON[ "jsonld" ][] = $measurement;
                         $myCuratedJSON[ "results" ][] = array( 
                           (int)(strtotime( str_replace( "T" , "" , substr( $measurement->dateTime , 0 , 19 ) ) ) . "000") , 
                           (float)$measurement->value 
                         );
                       }
                     }

                   }
                 }

               }
             }

           }
         }

       }
     }
   }

   return $myCuratedJSON;
 }

?>