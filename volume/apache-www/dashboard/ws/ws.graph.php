<?php
  if( !isset($_SESSION) ) { session_start(); }

  require_once("../php_libs/lib.main.php");
    $Settings = new main();

  require_once("../php_libs/lib.db.php");
    $db = new getDBO();
    if( $db->error ){
     echo json_encode( [ "error" => $db->error ] );
     return;
    }

  require_once("../php_libs/lib.ws.php");
    $ws = new amWS();

  require_once("../php_libs/lib.user.php");
    $user = new amUser( $_SESSION );
    if( !$user->id ){
     echo json_encode( [ "error" => "Invalid Access" ] );
     return;
    }

  require_once('../localization/lang.'.$user->getLocale().'.php');

  $action = $Settings->getVar("action","str");
  $entity = $Settings->getVar("entity","str");
  require_once("./objects/".$entity.".object.php");

  function pr( $object ){
   print( "<pre>" ); 
   print_r( $object ); 
   print( "</pre>" );
  }

  function getHeaders(){
    global $myOptions;
    $myResponse = [];
    
    foreach( $myOptions[ "headers" ] as $property => $detail ){
      $detail[ "column" ] = $property;
      $myResponse[] = $detail;
    }

    return $myResponse;
  }

  function getHeadersFlatArray(){
    global $myOptions;
    $myResponse = [];

    foreach( $myOptions[ "headers" ] as $property => $detail ){
      $myResponse[] = $property;
    }

    return $myResponse;
  }

  function parseUserDates(){
   global $Settings;
    $defaultDate = new Datetime();
    $toDate      = $defaultDate->format( "Y-m-d" );
    $defaultDate->sub( new DateInterval( "P3D" ) );
    $fromDate    = $defaultDate->format( "Y-m-d" );

    $hasPeriod  = $Settings->getVar( "startDate" ) && $Settings->getVar( "endDate" ) ? true : false;

    if( $hasPeriod ){
      $fromDate = $Settings->getVar( "startDate" );
      $toDate   = $Settings->getVar( "endDate" );
    }
    
    return [
     "fromDate" => $fromDate , 
     "toDate"   => $toDate
    ];
  }

  function parseWsDataForGraph( $data , $graphTitle ){
   $myParsedDataset = [];
   $myData            = $data[ "data" ];
   $myInfo            = "";
   $headers           = [ 
    "temperature",
    "humidity"
   ];

   $myResponseDataset = array(
    "data" => array(),
    "info" => array(
      "title" => $graphTitle
    )
   );

   foreach( $myData as $row => $detail ){
    foreach( $headers as $header ){
     $myResponseDataset[ "data" ][ $header ][] = array( 
       (int)( strtotime( $detail->timestamp )."000" ) ,
       (float)$detail->$header 
     );
    }
   }

   return $myResponseDataset;
  }

  function getMeasurements(){
   global $ws;
   global $Settings;

   $headers    = getHeadersFlatArray();
   $graphTitle = "measurement";
   $nodeID     = $Settings->getVar( "nodeid" );
   $fromDate   = $Settings->getVar( "fromDate" );
   $toDate     = $Settings->getVar( "endDate" );

   $myResponse = array(
    "recordsTotal"    => 10000,
    "recordsFiltered" => 10000,
    "data"            => array(),
    "err"             => ""
   );

   $ws->setEndPoint( $Settings->ws_main[ "endpoint" ] );
   $ws->setAuth( $Settings->ws_main[ "auth" ] );
   $ws->setMethod( "GET" );
   $ws->setEndPointPath( $graphTitle );
   // $ws->setParameters( [ "locationid" => implode( "," , $myLocationArray ) , "limit" => sizeOf( $myLocationArray ) , "offset" => 0 , "include_icm" => "false" ] );
   $ws->setParameters( [ 
     "name"                    => $nodeID ,
     "datetimestamp_from_date" => $fromDate . "%2000:00:00",
     "datetimestamp_to_date"   => $toDate . "%2023:59:59"
   ] );

   $wsResponse        = $ws->cUrl( );
   $myParsedResponse  = array();
   // pr( $wsResponse );

   return parseWsDataForGraph( $wsResponse , $graphTitle );
  }

  switch( $action ){
      case "getMeasurements" : echo json_encode( getMeasurements() ); break;
  }


?>