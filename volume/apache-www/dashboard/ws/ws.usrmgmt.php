<?php
if( session_status() != PHP_SESSION_ACTIVE ){
 session_start();
}

 require_once("../php_libs/lib.main.php");
  $Settings = new main();
 require_once("../php_libs/lib.db.php");
 require_once("../php_libs/lib.user.php");

 $user       = new amUser( $_SESSION );
 $action     = $Settings->getVar( "action" );
 $password   = $Settings->getVar( "pw" );
 $username   = $Settings->getVar( "un" );

 if( !$action ){
  echo json_encode(["error" => "Invalid Request"]);
  return;
 }

 switch( $action ){
  case "login" : 
    if( !$password || !$username )
     $user->error = "Missing Credentials";
    else
     $res = $user->login( $password , $username );
  break;
  case "logout" : 
    $res = $user->logout();
  break;
  case "setLocale" : 
   if( !$user->isLogged() )
    $user->error = "No connected";
   else
    $res = $user->setLocale( $Settings->getVar( "locale" ) ); 
  break;
  case "getProfileId" : 
   $profile_id = $user->getProfileId();
   if( !$profile_id )
    $user->error = "No connected";
   else
    $res = [ 
     "profile_id" => $profile_id , 
     "access_ui"  => $user->getUIAccessId()
    ];
  break;
 }

 if( $user->error )
  echo json_encode( [ "error" => $user->error ] );
 else
  switch( $action ){
   case "login" : 
    $_SESSION[ "isLogged" ] = $res;
    echo json_encode( [ "success" => "valid user", "ssid" => $res , "page" => $Settings->HTDOC_NAME ] );
   break;
   case "logout" : 
    echo json_encode( [ "success" => "logged out", "ssid" => $res , "page" => $Settings->HTDOC_NAME ] );
    session_destroy();
   break;
   case "setLocale" : 
     echo json_encode( $res );
   break;
   case "getProfileId" : 
     echo json_encode( $res );
   break;
  }

?>