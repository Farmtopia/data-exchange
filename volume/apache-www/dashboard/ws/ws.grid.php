<?php
  if( !isset($_SESSION) ) { session_start(); }

  require_once("../php_libs/lib.main.php");
    $Settings = new main();

  require_once("../php_libs/lib.db.php");
    $db = new getDBO();
    if( $db->error ){
     echo json_encode( [ "error" => $db->error ] );
     return;
    }

  require_once("../php_libs/lib.ws.php");
    $ws = new amWS();

  require_once("../php_libs/lib.user.php");
    $user = new amUser( $_SESSION );
    if( !$user->id ){
     echo json_encode( [ "error" => "Invalid Access" ] );
     return;
    }

  require_once('../localization/lang.'.$user->getLocale().'.php');

  $action = $Settings->getVar("action","str");
  $entity = $Settings->getVar("entity","str");
  require_once("./objects/".$entity.".object.php");

  switch( $action ){
    case "headers" : echo json_encode( getHeaders() ); break;
    case "getStations" :  echo json_encode( getStations() ); break;
    case "getDatasources" :  echo json_encode( getDatasources() ); break;
  }

  function pr( $object ){
   print( "<pre>" ); 
   print_r( $object ); 
   print( "</pre>" );
  }

  function getHeaders(){
    global $myOptions;
    $myResponse = [];
    
    foreach( $myOptions[ "headers" ] as $property => $detail ){
      $detail[ "column" ] = $property;
      $myResponse[] = $detail;
    }

    return $myResponse;
  }

  function getHeadersFlatArray(){
    global $myOptions;
    $myResponse = [];

    foreach( $myOptions[ "headers" ] as $property => $detail ){
      $myResponse[] = $property;
    }

    return $myResponse;
  }

  function parseUserDates(){
   global $Settings;
    $defaultDate = new Datetime();
    $toDate      = $defaultDate->format( "Y-m-d" );
    $defaultDate->sub( new DateInterval( "P3D" ) );
    $fromDate    = $defaultDate->format( "Y-m-d" );

    $hasPeriod  = $Settings->getVar( "startDate" ) && $Settings->getVar( "endDate" ) ? true : false;

    if( $hasPeriod ){
      $fromDate = $Settings->getVar( "startDate" );
      $toDate   = $Settings->getVar( "endDate" );
    }
    
    return [
     "fromDate" => $fromDate , 
     "toDate"   => $toDate
    ];
  }

  function getStations(){
   global $ws;
   global $Settings;
   global $amGrid;
   global $processType;
   global $user;

   $headers    = getHeadersFlatArray();

   $myResponse = array(
    "recordsTotal"    => 10000,
    "recordsFiltered" => 10000,
    "data"            => array(),
    "err"             => ""
   );

   $ws->setEndPoint( $Settings->ws_main[ "endpoint" ] );
   $ws->setAuth( $Settings->ws_main[ "auth" ] );
   $ws->setMethod( "GET" );
   $ws->setEndPointPath( "device" );
   // $ws->setParameters( [ "locationid" => implode( "," , $myLocationArray ) , "limit" => sizeOf( $myLocationArray ) , "offset" => 0 , "include_icm" => "false" ] );
   $ws->setParameters( [] );

   $wsResponse        = $ws->cUrl( );
   $myParsedResponse  = array();

   foreach( $wsResponse[ "data" ] as $row => $data ){
     $myTempArray = [];
     foreach( $headers as $key => $header ){
       $myTempArray[] = $data->$header;
     }

     $myResponse[ "data" ][] = $myTempArray;
   }

   return $myResponse;

  }

  function getDataSources(){
   global $ws;
   global $Settings;
   global $amGrid;
   global $processType;
   global $user;

   $headers       = getHeadersFlatArray();
   $apiConnection = $user->getDirectoryJWT();
   $jwt           = $apiConnection[ "apikey" ];

   $myResponse = array(
    "recordsTotal"    => 10000,
    "recordsFiltered" => 10000,
    "data"            => array(),
    "err"             => ""
   );

   $ws->setEndPoint( $Settings->ws_directory[ "endpoint" ] );
   $ws->setMethod( "GET" );
   $ws->setEndPointPath( "administration/datasets" );
   $ws->setHeaders( [
     'Content-Type: application/json',
     'Accept: application/json' , 
     'Authorization: Bearer ' . $jwt
   ] );

   $wsResponse = $ws->cUrl( );

   // print( "<pre>" );print_r($wsResponse);print( "</pre>" );

   if( !isset( $wsResponse[ "data" ] ) ){
     return [ "error" => "Invalid Response from Server" ];
   }

   if( isset( $wsResponse[ "data" ]->error ) ){
     return [ "error" => $wsResponse[ "data" ]->error ];
   }

   if( isset( $wsResponse[ "status_code" ] ) && $wsResponse[ "status_code" ] != 200 ){
     return [ "error" => $wsResponse[ "data" ]->detail , "status_code" => $wsResponse[ "status_code" ] ];
   }

   foreach( $wsResponse[ "data" ] as $row => $data ){
     $myTempArray = [];
     foreach( $headers as $key => $header ){
       $myTempArray[] = $data->$header;
     }

     $myResponse[ "data" ][] = $myTempArray;
   }

   return $myResponse;

  }


?>