<?php
// namespace CFG\local;
// use \Datetime;
// use \DateInterval;

require_once('../php_libs/lib.main.php');

class amGetDataFromExternalWS{

 function __construct(){
  $this->Settings = new main();
 }

 public function getData( $options ){
  $myResponse = [];

  return $myResponse;
 }

 public function help(){
  $myHelp = <<<HELP
   ---Initialize the Class
   \$amGetDataFromExternalWS = new amGetDataFromExternalWS();

   ---Get the Depths with Location_ID
   \$myRes = \$amGetDataFromExternalWS->getQuery([
    "params"         => PARAMETERS ["wsht30_temp","rain","wsht30_rh" , etc],
    "from"           => FROM_DATE [Y-m-d H:i:s],
    "to"             => TO_DATE [Y-m-d H:i:s],
    "location_id"    => LOCATION_ID,
    "resolution"     => TIME_RESOLUTION [ 60 || 1440 ] (optional)
   ]);

   --OUTPUT 
   A Numeric Array with Associative Arrays , that have Sensor as Property and Measurement as Value.
HELP;
   echo "<pre>".$myHelp."</pre>";
 }

}

?>