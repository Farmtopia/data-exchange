<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

  if(!isset($_SESSION)) session_start();

// Build common Security Headers
   date_default_timezone_set('Europe/Athens');
   header("Content-Type: text/html; charset=utf-8");
   header("X-XSS-Protection: 1");
   header("X-Frame-Options: SAMEORIGIN");
   header("X-Content-Type-Options: nosniff");
   header("Cache-Control: no-cache");
   header("Cache-Control: no-store, must-revalidate");
   header("Date:".date("D M j G:i:s T Y"));
   $date = new DateTime();
   $date->add(new DateInterval('P30D'));
   $ExpirationDate = $date->format('D M j G:i:s T Y');
   header("Expires: {$ExpirationDate}");

  $SERVER_SITE   = substr( __DIR__ , strlen($_SERVER['DOCUMENT_ROOT']));
  $relative_path = $_SERVER["DOCUMENT_ROOT"]."/".$SERVER_SITE;
  require_once($relative_path . '/php_libs/lib.main.php');

  $Settings = new main();
  $Settings->amLog( "Header : " . $_SERVER['REQUEST_URI'] , 'uri' , 'a');
  $Settings->amLog( "Request : " . json_encode($_REQUEST) , 'uri' , 'a');

  require_once($relative_path . '/php_libs/lib.db.php');
  require_once($relative_path . '/php_libs/lib.user.php');
   $amUser = new amUser( $_SESSION );

   if( !$amUser->id ){
    session_unset();
    session_destroy();
    echo " <script> ";
     echo " alert(' Invalid Access '); ";
     echo " top.window.location.href=\"".$Settings->SERVER_URL."views/login.php\" ";
    echo " </script> ";
    exit();
  }
  else{
   require_once($relative_path . '/localization/lang.'.$amUser->getLocale().'.php');
  }
?>