/* GENERIC */
const LANG_no_data               = "No Available Data";
const LANG_limit                 = "Limit";
const LANG_first_true_evaluation = "Started On";
const LANG_contact_admin         = "Contact System Administrator";
const LANG_from_date             = "From";
const LANG_to_date               = "To";
const LANG_high_limit            = "High Limit";
const LANG_low_limit             = "Low Limit";
const LANG_graph_bands           = "Limits";
const LANG_sum_depths            = "Soil Moisture Sum";
const LANG_chilling_thresholds   = "Chilling Hours Limits (°C)";
const LANG_period_selection      = "Date Period";
const LANG_search_parcel         = "Parcel Search";
const LANG_datasources           = "TEST";

/* RULES */
const LANG_sum_hours_temp_below_seven = "Sum of Hours , where Temperature was below 7 Celsius Degrees";
const LANG_lithargic                  = "Lithargic";
const LANG_frost_description          = "Air temperature is below 0°C";
const LANG_frost                      = "Frost";
const LANG_heat_description           = "Air temperature is above 40°C";
const LANG_heat                       = "Heat";
const LANG_flood_description          = "Precipitation values exceed 15mm";
const LANG_flood                      = "Flood";
const LANG_windstorm_description      = "Wind speed exceeds 62km/h";
const LANG_windstorm                  = "Windstorm";

/* Alerts */ 
const LANG_no_location            = "No Location has been selected";
const LANG_no_start_date          = "No Start Date has been selected";
const LANG_no_end_date            = "No End Date has been selected";
const LANG_change_language        = "Change Language";
const LANG_change_language_prompt = "Changing the Language will reload the Web Page immediately after the change has been completed. Are you sure you want to proceed";
const LANG_change                 = "Change";
const LANG_cancel                 = "Cancel";
const LANG_accept                 = "Accept";
const LANG_caution                = "Caution";
const LANG_next                   = "Next";
const LANG_back                   = "Back";
const LANG_submit                 = "Submit";
const LANG_info                   = "Info";
const LANG_locale_change_failed   = "Changing Language Failed";
const LANG_change_language_no_available   = "The selected Language is already active.";
const LANG_no_forecast_location_available = "No calculations present for the exact Location. " + 
                                            "Therefore Forecast is calculated based on data from the next closest location." + 
                                            "which is at ";
const LANG_recent_warnings        = "Hazardous Events past 7 Days";
const LANG_latest_warnings        = "Latest Hazardous Events";
const LANG_warnings               = "Warnings";
const LANG_warning                = "Warning";
const LANG_no_warning_yet         = "No Warnings";
const LANG_current_measurements   = "Latest";
const LANG_missing_data_header    = "Missing Data!";
const LANG_missing_data_body      = "The location you selected appears to be missing relevant data for display.";
const LANG_missing_data_footer    = "Make sure that you are not selecting an inactive location.";

/* Station View */
const LANG_station_details     = "Details for Station";
const LANG_station_name        = "Location Name of Station";
const LANG_station_description = "Details relevant to the parcel will follow.";
const LANG_show_parcel         = "Show Parcel";

/* Menu Items */
const LANG_home                    = "Dashboard";
const LANG_station                 = "Parcels";
const LANG_data                    = "Data";
const LANG_meteo                   = "Atmospheric";
const LANG_irrigation              = "Irrigations";
const LANG_models                  = "Pests";
const LANG_forecast                = "Forecast";
const LANG_carbon_footprint        = "Carbon Footprint";
const LANG_calendar                = "Calendar";
const LANG_soil_moisture           = "Soil Moisture";
const LANG_analytics               = "Analytics";
const LANG_analytics_soil_moisture = "Soil Moisture";
const LANG_analytics_chilling_days = "Chilling Hours";
const LANG_chilling_hours          = "Chilling Hours";

/* Carbon Footprint */
const LANG_total_area             = "Total Area";
const LANG_agricultural_period    = "Agricultural Period";
const LANG_total_carbon_footprint = "Total Carbon Footprint";
const LANG_crop                   = "Crop";
const LANG_api_field              = "Api Field";
const LANG_flow_name              = "Flow Name";
const LANG_flow_property          = "Flow Property";
const LANG_In_Out                 = "In/Out";
const LANG_is_reference_flow      = "Is Reference Flow";
const LANG_type                   = "Type";
const LANG_uuid_dataset           = "Unique ID";
const LANG_category               = "Category";
const LANG_product_system         = "Product System";
const LANG_value                  = "Value";
const LANG_output_values          = "Output Values";
const LANG_input_values           = "Input Values";
const LANG_carbon_no_data_body    = "There are currently not enough data to calculate Carbon Footprint for the specific location.";
const LANG_carbon_no_data_title   = "Not enough Inputs";
const LANG_stremmata              = "Stremma";
const LANG_amount                 = "Amount";

/* Agronomy Cards */
const LANG_last_name              = "Last Name";
const LANG_first_name             = "First Name";
const LANG_phone                  = "Phone";
const LANG_email                  = "Email";
const LANG_subscriber             = "Subscriber";
const LANG_toponym                = "Toponym";
const LANG_station_loc            = "Station";
const LANG_prefecture             = "Prefecture";
const LANG_crop_type              = "Crop Type";
const LANG_crop_variety           = "Crop Variety";
const LANG_variety                = "Variety";
const LANG_product_direction      = "Product Direction";
const LANG_sensor_card_title      = "Sensors";
const LANG_warnings_card_title    = "Warnings";
const LANG_sum_warning_card_title = "Analysis";
const LANG_field_card_card_title  = "Field";
const LANG_parcel_card_title      = "Parcel";
const LANG_subscriber_card_title  = "Producer";

/* Sensors */
const LANG_index                  = 'Index';
const LANG_temp                   = 'Temperature';
const LANG_agc                    = 'agc';
const LANG_winddir                = 'Wind Dir.';
const LANG_vr_winddir             = 'Wind Dir.';
const LANG_rainmm_10min           = 'mm rain 10 min';
const LANG_btemp                  = 'Box Temperature';
const LANG_brh                    = 'Box Relative Humidity';
const LANG_leafw1                 = 'Leaf Wetness 1 (h)';
const LANG_nplwtemp1              = 'Leaf Temperature 1 (oC)';
const LANG_nplwrh1                = 'Leaf Relative Humidity 1 (%)';
const LANG_leafw2                 = 'Leaf Wetness 2 (h)';
const LANG_nplwtemp2              = 'Leaf Temperature 2 (oC)';
const LANG_nplwrh2                = 'Leaf Relative Humidity 2 (%)';
const LANG_np_consumption_voltage = 'Voltage Consumption';
const LANG_np_consumption_energy  = 'Energy Consumption';
const LANG_np_consumption_cal     = 'Cal Consumption';
const LANG_pv_energy_in_voltage   = 'PV Voltage';
const LANG_pv_energy_in_energy    = 'PV Energy In';
const LANG_pv_energy_in_cal       = 'PV Energy Calib Factor';
const LANG_ax                     = 'Acceleration X';
const LANG_ay                     = 'Acceleration Y';
const LANG_az                     = 'Acceleration Z';
const LANG_ticks                  = 'Wind cup rotations-ticks in 10 min';
const LANG_mean_msec              = 'Mean Msec';
const LANG_min_msec               = 'Min msec';
const LANG_velocity               = 'Velocity (km/h)';
const LANG_gust                   = 'Gust';
const LANG_itemp                  = 'Internal Temperature (oC)';
const LANG_pressure               = 'Atm. Pressure (mbar)';
const LANG_etemp                  = 'Temperature (oC)';
const LANG_wsht30_temp            = 'Temperature (oC)';
const LANG_wsht30_rh              = 'Humidity (%)';
const LANG_msht30_temp            = 'Temperature (oC)';
const LANG_msht30_rh              = 'Humidity (%)';
const LANG_main_ovp_cnt           = 'Over Power Notifications';
const LANG_atmos14rh              = 'Humidity (atmos14)';
const LANG_vapor_pressure         = 'Vapor Pressure';
const LANG_atmos14vp              = 'Vapor Pressure (atmos14)';
const LANG_atmos14temp            = 'Ext. Temperature (atmos14)';
const LANG_atmos14press           = 'Atm. Pressure (atmos14)';
const LANG_rain_sensor            = 'Rain';
const LANG_ext_copernicus         = 'Solar Radiation ( Copernicus )';
const LANG_nplwlw1                = 'Leaf Wetness 1';
const LANG_nplwlw2                = 'Leaf Wetness 2';
const LANG_vr_leafw_norm          = 'Leaf Wetness VR';
const LANG_rain                   = 'Rain';
const LANG_pyranometer            = 'Pyranometer';
const LANG_vr_nn_model_pyr        = 'Solar Radiation ( Model )';

/* DND 60 */
const LANG_dnd_30_sdi12_temp_10 = 'Soil Temperature 10cm (°C) (DND 30)';
const LANG_dnd_30_sdi12_temp_20 = 'Soil Temperature 20cm (°C) (DND 30)';
const LANG_dnd_30_sdi12_temp_30 = 'Soil Temperature 30cm (°C) (DND 30)';

const LANG_dnd_30_sdi12_sal_10  = 'Soil Salinity 10cm (DND 30)';
const LANG_dnd_30_sdi12_sal_20  = 'Soil Salinity 20cm (DND 30)';
const LANG_dnd_30_sdi12_sal_30  = 'Soil Salinity 30cm (DND 30)';

const LANG_dnd_30_sdi12_sm_10   = 'Soil Moisture 10cm (%) (DND 30)';
const LANG_dnd_30_sdi12_sm_20   = 'Soil Moisture 20cm (%) (DND 30)';
const LANG_dnd_30_sdi12_sm_30   = 'Soil Moisture 30cm (%) (DND 30)';

/* DND 60 */
const LANG_dnd_60_sdi12_temp_10 = 'Soil Temperature 10cm (°C) (DND 60)';
const LANG_dnd_60_sdi12_temp_20 = 'Soil Temperature 20cm (°C) (DND 60)';
const LANG_dnd_60_sdi12_temp_30 = 'Soil Temperature 30cm (°C) (DND 60)';
const LANG_dnd_60_sdi12_temp_40 = 'Soil Temperature 40cm (°C) (DND 60)';
const LANG_dnd_60_sdi12_temp_50 = 'Soil Temperature 50cm (°C) (DND 60)';
const LANG_dnd_60_sdi12_temp_60 = 'Soil Temperature 60cm (°C) (DND 60)';

const LANG_dnd_60_sdi12_sal_10  = 'Soil Salinity 10cm (DND 60)';
const LANG_dnd_60_sdi12_sal_20  = 'Soil Salinity 20cm (DND 60)';
const LANG_dnd_60_sdi12_sal_30  = 'Soil Salinity 30cm (DND 60)';
const LANG_dnd_60_sdi12_sal_40  = 'Soil Salinity 40cm (DND 60)';
const LANG_dnd_60_sdi12_sal_50  = 'Soil Salinity 50cm (DND 60)';
const LANG_dnd_60_sdi12_sal_60  = 'Soil Salinity 60cm (DND 60)';

const LANG_dnd_60_sdi12_sm_10   = 'Soil Moisture 10cm (%) (DND 60)';
const LANG_dnd_60_sdi12_sm_20   = 'Soil Moisture 20cm (%) (DND 60)';
const LANG_dnd_60_sdi12_sm_30   = 'Soil Moisture 30cm (%) (DND 60)';
const LANG_dnd_60_sdi12_sm_40   = 'Soil Moisture 40cm (%) (DND 60)';
const LANG_dnd_60_sdi12_sm_50   = 'Soil Moisture 50cm (%) (DND 60)';
const LANG_dnd_60_sdi12_sm_60   = 'Soil Moisture 60cm (%) (DND 60)';

/* DND 90 */
const LANG_dnd_90_sdi12_temp_10 = 'Soil Temperature 10cm (°C) (DND 90)';
const LANG_dnd_90_sdi12_temp_20 = 'Soil Temperature 20cm (°C) (DND 90)';
const LANG_dnd_90_sdi12_temp_30 = 'Soil Temperature 30cm (°C) (DND 90)';
const LANG_dnd_90_sdi12_temp_40 = 'Soil Temperature 40cm (°C) (DND 90)';
const LANG_dnd_90_sdi12_temp_50 = 'Soil Temperature 50cm (°C) (DND 90)';
const LANG_dnd_90_sdi12_temp_60 = 'Soil Temperature 60cm (°C) (DND 90)';
const LANG_dnd_90_sdi12_temp_70 = 'Soil Temperature 70cm (°C) (DND 90)';
const LANG_dnd_90_sdi12_temp_80 = 'Soil Temperature 80cm (°C) (DND 90)';
const LANG_dnd_90_sdi12_temp_90 = 'Soil Temperature 90cm (°C) (DND 90)';

const LANG_dnd_90_sdi12_sal_10  = 'Soil Salinity 10cm (DND 90)';
const LANG_dnd_90_sdi12_sal_20  = 'Soil Salinity 20cm (DND 90)';
const LANG_dnd_90_sdi12_sal_30  = 'Soil Salinity 30cm (DND 90)';
const LANG_dnd_90_sdi12_sal_40  = 'Soil Salinity 40cm (DND 90)';
const LANG_dnd_90_sdi12_sal_50  = 'Soil Salinity 50cm (DND 90)';
const LANG_dnd_90_sdi12_sal_60  = 'Soil Salinity 60cm (DND 90)';
const LANG_dnd_90_sdi12_sal_70  = 'Soil Salinity 70cm (DND 90)';
const LANG_dnd_90_sdi12_sal_80  = 'Soil Salinity 80cm (DND 90)';
const LANG_dnd_90_sdi12_sal_90  = 'Soil Salinity 90cm (DND 90)';

const LANG_dnd_90_sdi12_sm_10   = 'Soil Moisture 10cm (%) (DND 90)';
const LANG_dnd_90_sdi12_sm_20   = 'Soil Moisture 20cm (%) (DND 90)';
const LANG_dnd_90_sdi12_sm_30   = 'Soil Moisture 30cm (%) (DND 90)';
const LANG_dnd_90_sdi12_sm_40   = 'Soil Moisture 40cm (%) (DND 90)';
const LANG_dnd_90_sdi12_sm_50   = 'Soil Moisture 50cm (%) (DND 90)';
const LANG_dnd_90_sdi12_sm_60   = 'Soil Moisture 60cm (%) (DND 90)';
const LANG_dnd_90_sdi12_sm_70   = 'Soil Moisture 70cm (%) (DND 90)';
const LANG_dnd_90_sdi12_sm_80   = 'Soil Moisture 80cm (%) (DND 90)';
const LANG_dnd_90_sdi12_sm_90   = 'Soil Moisture 90cm (%) (DND 90)';

/* EnviroScan */

const LANG_enviro_sdi12_temp_10 = 'Soil Temperature 10cm (°C) (DND 90)';
const LANG_enviro_sdi12_temp_20 = 'Soil Temperature 20cm (°C) (DND 90)';
const LANG_enviro_sdi12_temp_30 = 'Soil Temperature 30cm (°C) (DND 90)';
const LANG_enviro_sdi12_temp_40 = 'Soil Temperature 40cm (°C) (DND 90)';
const LANG_enviro_sdi12_temp_50 = 'Soil Temperature 50cm (°C) (DND 90)';
const LANG_enviro_sdi12_temp_60 = 'Soil Temperature 60cm (°C) (DND 90)';
const LANG_enviro_sdi12_temp_70 = 'Soil Temperature 70cm (°C) (DND 90)';
const LANG_enviro_sdi12_temp_80 = 'Soil Temperature 80cm (°C) (DND 90)';
const LANG_enviro_sdi12_temp_90 = 'Soil Temperature 90cm (°C) (DND 90)';

const LANG_enviro_sdi12_sal_10  = 'Soil Salinity 10cm (DND 90)';
const LANG_enviro_sdi12_sal_20  = 'Soil Salinity 20cm (DND 90)';
const LANG_enviro_sdi12_sal_30  = 'Soil Salinity 30cm (DND 90)';
const LANG_enviro_sdi12_sal_40  = 'Soil Salinity 40cm (DND 90)';
const LANG_enviro_sdi12_sal_50  = 'Soil Salinity 50cm (DND 90)';
const LANG_enviro_sdi12_sal_60  = 'Soil Salinity 60cm (DND 90)';
const LANG_enviro_sdi12_sal_70  = 'Soil Salinity 70cm (DND 90)';
const LANG_enviro_sdi12_sal_80  = 'Soil Salinity 80cm (DND 90)';
const LANG_enviro_sdi12_sal_90  = 'Soil Salinity 90cm (DND 90)';

const LANG_enviro_sdi12_sm_10   = 'Soil Moisture 10cm (%) (DND 90)';
const LANG_enviro_sdi12_sm_20   = 'Soil Moisture 20cm (%) (DND 90)';
const LANG_enviro_sdi12_sm_30   = 'Soil Moisture 30cm (%) (DND 90)';
const LANG_enviro_sdi12_sm_40   = 'Soil Moisture 40cm (%) (DND 90)';
const LANG_enviro_sdi12_sm_50   = 'Soil Moisture 50cm (%) (DND 90)';
const LANG_enviro_sdi12_sm_60   = 'Soil Moisture 60cm (%) (DND 90)';
const LANG_enviro_sdi12_sm_70   = 'Soil Moisture 70cm (%) (DND 90)';
const LANG_enviro_sdi12_sm_80   = 'Soil Moisture 80cm (%) (DND 90)';
const LANG_enviro_sdi12_sm_90   = 'Soil Moisture 90cm (%) (DND 90)';

/* Grid Data */
const LANG_parcelid             = "Parcel Id";
const LANG_date                 = "Date";
const LANG_startdatetime        = "Start Date";
const LANG_enddatetime          = "End Date";
const LANG_osdecode             = "OSDE Code";
const LANG_waterquantity        = "Water Quantity";
const LANG_irrigationsystem     = "Irrigation System";
const LANG_remarks              = "Remarks";
const LANG_irighour             = "Irrigation Hour";
const LANG_drugname             = "Drug Name";
const LANG_dose                 = "Dose";
const LANG_unit                 = "Unit";
const LANG_activesubstance      = "Active Substance";
const LANG_target               = "Target";
const LANG_sprays               = "Sprays";
const LANG_irrigations          = "Irrigations";
const LANG_harvest              = "Harvest";
const LANG_phaenological_stages = "Phaenologic Stages";
const LANG_land_management      = "Land Management";
const LANG_fertilization        = "Fertilization";
const LANG_fertilizations       = "Fertilizations";

/* Grid Headers */
const LANG_longitude               = "Longitude";
const LANG_latitude                = "Latitude";
const LANG_altitude                = "Altitude";
const LANG_name                    = "Name";
const LANG_active                  = "Active";
const LANG_id                      = "Id";
const LANG_on                      = "On";
const LANG_off                     = "Off";
const LANG_salinity                = "Salinity";
const LANG_rh_soil                 = "Moisture";
const LANG_radius                  = "Station Distance (m)";
const LANG_location_name           = "Location Name";
const LANG_local_name              = "Local Name";
const LANG_previous_chilling_hours = "Last Ch.Hours";
const LANG_current_chilling_hours  = "Current Ch.Hours";
const LANG_previous_harvest        = "Prev. Harvest";
const LANG_current_harvest         = "Last Harvest";
const LANG_required_hours          = "Required Hours";

/* Models */
const LANG_leaf_curl_generic                          = "Leaf Curl (Generic)";
const LANG_leaf_curl_regression                       = "Leaf Curl (Regression)";
const LANG_monilia_fructicola                         = "Monilia Fructicola";
const LANG_pstages                                    = "Phaenologic Stages";
const LANG_downy_mildew                               = "Downy Mildew (Generic)";
const LANG_powedery_mildew                            = "Powedery Mildew (Generic)";
const LANG_powedery_mildew_grapes_regression          = "Powedery Mildew (Regression)";
const LANG_powedery_mildew_grapes_regression_conidial = "Powedery Mildew";
const LANG_erysiphe_negator_regression_2              = "Powedery Mildew 2 (Regression)";
const LANG_erysiphe_negator_regression_3              = "Powedery Mildew 3 (Regression)";

/* Calendar */
const LANG_no_calendar_data = "No available entries for ";
const LANG_get_aggregations = "Aggregations";
const LANG_aggregation      = "Aggregation";
const LANG_compare_panel    = "Templates";
const LANG_control_panel    = "Groups";
const LANG_select_group     = "Select Group";
const LANG_created_on       = "Created On";
const LANG_select_period    = "Select Period";
const LANG_is_active        = "Active";
const LANG_is_default       = "Default";
const LANG_actions          = "Actions";
const LANG_location_id      = "Location Id";
const LANG_parcel_id        = "Parcel Id";
const LANG_station_id       = "Station ID";
const LANG_icon             = "Active";
const LANG_create_group     = "Create Group";
const LANG_clear_selections = "Clear Selections";
const LANG_group_name       = "Group Name";
const LANG_parcel           = "Parcel";

const LANG_totalwaterperstremma   = "Total water (m3/Stremma)";
const LANG_totalwaterperparcel    = "Total water (m3)";
const LANG_period                 = "Period";
const LANG_total_parcels          = "Total Parcels";
const LANG_average_parcel_area    = "Avg Parcel Area (Stremma)";
const LANG_total_events           = "Total Events";
const LANG_top_three              = "Top 3";
const LANG_no_aggregation_data    = "No available aggregated data for the selection.";
const LANG_graphs                 = "Graphs";
const LANG_details                = "Details";
const LANG_aggregations           = "Aggregations";
const LANG_group                  = "Group";
const LANG_own_parcel             = "My Parcel";
const LANG_own_parcel_against     = "Parcel of comparison";
const LANG_agg_analysis_graph     = "Aggregates";
const LANG_details_are_in_stremma = "Analysis is per Stremma";
const LANG_total                  = "Total";
const LANG_stremma                = "Stremma";
const LANG_avg                    = "Avg";
const LANG_min                    = "Min";
const LANG_max                    = "Max";
const LANG_water                  = "Water";
const LANG_hour                   = "Hour";
const LANG_hours                  = "Hours";
const LANG_search                 = "Search";

const LANG_adminlastname          = "Last Name";
const LANG_adminfrstname          = "First Name";
const LANG_adminphone             = "Phone";
const LANG_adminsubscriber        = "Subscriber";
const LANG_rocrdescription        = "Crop";
const LANG_rocvdescription        = "Variety";
const LANG_parcelPrefecture       = "Prefecture";
const LANG_parcelToponym          = "Toponym";
