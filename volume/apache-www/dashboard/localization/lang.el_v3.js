﻿/* GENERIC */
const LANG_no_data               = "Δεν υπάρχουν δεδομένα";
const LANG_limit                 = "Όριο";
const LANG_first_true_evaluation = "Ξεκίνησε στις";
const LANG_contact_admin         = "Επικοινωνήστε με τον διαχειριστή";
const LANG_from_date             = "Από";
const LANG_to_date               = "Εώς";
const LANG_high_limit            = "Άνω Όριο";
const LANG_low_limit             = "Κάτω Όριο";
const LANG_graph_bands           = "Όρια";
const LANG_sum_depths            = "Σύνολο Υγρασίας Εδάφους";
const LANG_chilling_thresholds   = "Όρια Ψύχους (°C)";
const LANG_period_selection      = "Περίοδος";
const LANG_search_parcel         = "Αναζήτηση Αγροτεμαχίου";

/* RULES */
const LANG_sum_hours_temp_below_seven = "Σύνολο ωρών με την θερμοκρασία να είναι κάτω απο τους 7 βαθμούς";
const LANG_lithargic                  = "Λήθαργος";
const LANG_frost_description          = "Θερμοκρασία αέρα κάτω απο 0°C";
const LANG_frost                      = "Παγωνιά";
const LANG_heat_description           = "Θερμοκρασία αέρα πάνω απο 40°C";
const LANG_heat                       = "Καύσωνας";
const LANG_flood_description          = "Βροχόπτωση μεγαλύτερη απο 15mm";
const LANG_flood                      = "Πλημμύρα";
const LANG_windstorm_description      = "Ταχύτητα Ανέμου μεγαλύτερη απο 62km/h";
const LANG_windstorm                  = "Ανεμοθύελλα";

/* Alerts */ 
const LANG_no_location                    = "Δεν έχει επιλεχθεί τοποθεσία";
const LANG_no_start_date                  = "Δεν έχει επιλεχθεί αρχική ημερομηνία";
const LANG_no_end_date                    = "Δεν έχει επιλεχθεί τελική ημερομηνία";
const LANG_change_language                = "Αλλαγή Γλώσσας";
const LANG_change_language_prompt         = "H αλλαγή στην γλώσσα , θα κάνει αυτόματη ανανέωση της σελίδας μετα την ολοκλήρωση της. Είσαστε σίγουροι ότι θέλετε να προχωρήσετε?";
const LANG_change                         = "Αλλαγή";
const LANG_cancel                         = "Ακύρωση";
const LANG_accept                         = "Αποδοχή";
const LANG_caution                        = "Προσοχή";
const LANG_next                           = "Επόμενο";
const LANG_back                           = "Προηγούμενο";
const LANG_submit                         = "Επιλογή";
const LANG_info                           = "Πληροφορίες";
const LANG_locale_change_failed           = "Η αλλαγή γλώσσας απέτυχε";
const LANG_change_language_no_available   = "Η γλώσσα είναι ήδη ενεργή.";
const LANG_no_forecast_location_available = "Δεν υπάρχουν προβλέψεις για την συγκεκριμένη τοποθεσία." + 
                                            "Οι προβλέψεις που εμφανίζονται έχουν υπολογισθεί " + 
                                            "απο παρακείμενη τοποθεσία που βρίσκετε στα ";
const LANG_recent_warnings        = "Επικίνδυνες συνθήκες τις τελευταίες 7 ημέρες";
const LANG_latest_warnings        = "Τελευταίες επικίνδυνες συνθήκες";
const LANG_warnings               = "Ειδοποιήσεις";
const LANG_warning                = "Ειδοποίηση";
const LANG_no_warning_yet         = "Καμία ειδοποίηση";
const LANG_current_measurements   = "Τελευταίες";
const LANG_missing_data_header    = "Έλλειψη δεδομένων!";
const LANG_missing_data_body      = "Δεν υπάρχουν δεδομένα για την συγκεκριμένη τοποθεσία.";
const LANG_missing_data_footer    = "Σιγουρευτείτε οτι η τοποθεσία είναι ενεργή.";

/* Station View */
const LANG_station_details     = "Πληροφορίες σχετικά με τον σταθμό";
const LANG_station_name        = "Ονομασία τοποθεσίας σταθμού";
const LANG_station_description = "Ακολουθουν πληροφορίες σχετικά με το αγροτεμάχιο στο οποίο έχει τοποθετηθεί ο σταθμός";
const LANG_show_parcel         = "Δείτε στον Χάρτη";

/* Menu Items */
const LANG_home                    = "Dashboard";
const LANG_station                 = "Αγροτεμάχια";
const LANG_data                    = "Δεδομένα";
const LANG_meteo                   = "Ατμοσφαιρικά";
const LANG_irrigation              = "Άρδευση";
const LANG_models                  = "Φυτοπροστασία";
const LANG_forecast                = "Προβλέψεις";
const LANG_carbon_footprint        = "Αποτύπωμα Άνθρακα";
const LANG_calendar                = "Ημερολόγιο";
const LANG_soil_moisture           = "Υγρασία Εδάφους";
const LANG_analytics               = "Ανάλυση";
const LANG_analytics_soil_moisture = "Υγρασία Εδάφους";
const LANG_analytics_chilling_days = "Ψύχος";
const LANG_chilling_hours          = "Ψύχος";

/* Carbon Footprint */
const LANG_total_area             = "Συνολική έκταση";
const LANG_agricultural_period    = "Καλλιεργητική Περίοδος";
const LANG_total_carbon_footprint = "Συνολικό Αποτύπωμα άνθρακα";
const LANG_crop                   = "Καλλιέργεια";
const LANG_api_field              = "Api Field";
const LANG_flow_name              = "Όνομα";
const LANG_flow_property          = "Ιδιότητα";
const LANG_In_Out                 = "In/Out";
const LANG_is_reference_flow      = "Αναφορά";
const LANG_type                   = "Τύπος";
const LANG_uuid_dataset           = "Μοναδικός Αριθμός";
const LANG_category               = "Κατηγορία";
const LANG_product_system         = "Σύστημα";
const LANG_value                  = "Τιμή";
const LANG_output_values          = "Τιμές Εξόδου";
const LANG_input_values           = "Τιμές Εισόδου";
const LANG_carbon_no_data_body    = "Αυτή τη στιγμή , στο σύστημα , δεν υπάρχουν αρκετά δεδομένα για τον υπολογισμό Αποτυπώματος Άνθρακα.";
const LANG_carbon_no_data_title   = "Έλλειψη δεδομένων.";
const LANG_stremmata              = "Στρέμματα";
const LANG_amount                 = "Ποσότητα";

/* Agronomy Cards */
const LANG_last_name              = "Επώνυμο";
const LANG_first_name             = "Όνομα";
const LANG_phone                  = "Τηλέφωνο";
const LANG_email                  = "Email";
const LANG_subscriber             = "Συνδρομητής";
const LANG_toponym                = "Τοπωνύμιο";
const LANG_station_loc            = "Σταθμός";
const LANG_prefecture             = "Νομός";
const LANG_crop_type              = "Τύπος Καλλιέργειας";
const LANG_crop_variety           = "Ποικιλία Καλλιέργειας";
const LANG_variety                = "Ποικιλία";
const LANG_product_direction      = "Χρήση";
const LANG_sensor_card_title      = "Αισθητήρες";
const LANG_warnings_card_title    = "Ειδοποιήσεις";
const LANG_sum_warning_card_title = "Ανάλυση";
const LANG_field_card_card_title  = "Καλλιέργεια";
const LANG_parcel_card_title      = "Αγροτεμάχιο";
const LANG_subscriber_card_title  = "Παραγωγός";

/* Sensors */
const LANG_index                  = 'Δείκτης';
const LANG_temp                   = 'Θερμοκρασία';
const LANG_agc                    = 'agc';
const LANG_winddir                = 'Κατέυθυνση Ανέμου [Προς κατήργηση]';
const LANG_vr_winddir             = 'Κατέυθυνση Ανέμου';
const LANG_rainmm_10min           = 'mm rain 10 min';
const LANG_btemp                  = 'Θερμοκρασία Κουτιού';
const LANG_brh                    = 'Σχετική Υγρασία Κουτιού';
const LANG_leafw1                 = 'Διύγρανση Φύλλου 1 (h)';
const LANG_nplwtemp1              = 'Θερμοκρασία Φύλλου 1 (oC)';
const LANG_nplwrh1                = 'Σχετική Υγρασία Φύλλου 1 (%)';
const LANG_leafw2                 = 'Διύγρανση Φύλλου 2 (h)';
const LANG_nplwtemp2              = 'Θερμοκρασία Φύλλου 2 (oC)';
const LANG_nplwrh2                = 'Σχετική Υγρασία Φύλλου 2 (%)';
const LANG_np_consumption_voltage = 'Κατανάλωση Μπαταρίας';
const LANG_np_consumption_energy  = 'Κεντρική Κατανάλωση';
const LANG_np_consumption_cal     = 'Βαθμονόμηση Κεντρικής Κατανάλωσης';
const LANG_pv_energy_in_voltage   = 'PV Voltage';
const LANG_pv_energy_in_energy    = 'PV Energy In';
const LANG_pv_energy_in_cal       = 'PV Energy Calib Factor';
const LANG_ax                     = 'Επιτάχυνση X';
const LANG_ay                     = 'Επιτάχυνση Y';
const LANG_az                     = 'Επιτάχυνση Z';
const LANG_ticks                  = 'Wind cup rotations-ticks in 10 min';
const LANG_mean_msec              = 'Μέσος όρος περιόδου περιστροφής';
const LANG_min_msec               = 'Ελάχιστο όρος περιόδου περιστροφής';
const LANG_velocity               = 'Ανέμος (km/h)';
const LANG_gust                   = 'Μέγιστη ταχύτητα ανέμου';
const LANG_itemp                  = 'Εσωτερική Θερμοκρασία (oC)';
const LANG_pressure               = 'Ατμοσφαιρική Πίεση (mbar)';
const LANG_etemp                  = 'Θερμοκρασία (oC)';
const LANG_wsht30_temp            = 'Θερμοκρασία (oC)';
const LANG_wsht30_rh              = 'Υγρασία (%)';
const LANG_msht30_temp            = 'Θερμοκρασία (oC)';
const LANG_msht30_rh              = 'Υγρασία (%)';
const LANG_main_ovp_cnt           = 'Over Power Notifications';
const LANG_atmos14rh              = 'Σχετική Υγρασία Αναφοράς';
const LANG_vapor_pressure         = 'Τάση Ατμών';
const LANG_atmos14vp              = 'Τάση Ατμών Αναφοράς';
const LANG_atmos14temp            = 'Εξωτερική Θερμοκρασία Αναφοράς';
const LANG_atmos14press           = 'Ατμοσφαιρική Πίεση Αναφοράς';
const LANG_rain_sensor            = 'Βροχόπτωση (mm)';
const LANG_ext_copernicus         = 'Ηλιακή Ακτινοβολία ( Copernicus )';
const LANG_nplwlw1                = 'Δ.Φύλλου';
const LANG_nplwlw2                = 'Δ.Φύλλου';
const LANG_vr_leafw_norm          = 'Δ.Φύλλου VR';
const LANG_rain                   = 'Βροχή (mm)';
const LANG_pyranometer            = 'Πυρανόμετρο';
const LANG_vr_nn_model_pyr        = 'Ηλιακή Ακτινοβολία ( Μοντέλο )';

/* DND 30 */
const LANG_dnd_30_sdi12_temp_10 = 'Θερμ/σία Εδάφους 10cm (°C) (DND 30)';
const LANG_dnd_30_sdi12_temp_20 = 'Θερμ/σία Εδάφους 20cm (°C) (DND 30)';
const LANG_dnd_30_sdi12_temp_30 = 'Θερμ/σία Εδάφους 30cm (°C) (DND 30)';

const LANG_dnd_30_sdi12_sal_10  = 'Αλατότητα Εδάφους 10cm (DND 30)';
const LANG_dnd_30_sdi12_sal_20  = 'Αλατότητα Εδάφους 20cm (DND 30)';
const LANG_dnd_30_sdi12_sal_30  = 'Αλατότητα Εδάφους 30cm (DND 30)';

const LANG_dnd_30_sdi12_sm_10   = 'Υγρασία Εδάφους 10cm (%) (DND 30)';
const LANG_dnd_30_sdi12_sm_20   = 'Υγρασία Εδάφους 20cm (%) (DND 30)';
const LANG_dnd_30_sdi12_sm_30   = 'Υγρασία Εδάφους 30cm (%) (DND 30)';

/* DND 60 */
const LANG_dnd_60_sdi12_temp_10 = 'Θερμ/σία Εδάφους 10cm (°C) (DND 60)';
const LANG_dnd_60_sdi12_temp_20 = 'Θερμ/σία Εδάφους 20cm (°C) (DND 60)';
const LANG_dnd_60_sdi12_temp_30 = 'Θερμ/σία Εδάφους 30cm (°C) (DND 60)';
const LANG_dnd_60_sdi12_temp_40 = 'Θερμ/σία Εδάφους 40cm (°C) (DND 60)';
const LANG_dnd_60_sdi12_temp_50 = 'Θερμ/σία Εδάφους 50cm (°C) (DND 60)';
const LANG_dnd_60_sdi12_temp_60 = 'Θερμ/σία Εδάφους 60cm (°C) (DND 60)';

const LANG_dnd_60_sdi12_sal_10  = 'Αλατότητα Εδάφους 10cm (DND 60)';
const LANG_dnd_60_sdi12_sal_20  = 'Αλατότητα Εδάφους 20cm (DND 60)';
const LANG_dnd_60_sdi12_sal_30  = 'Αλατότητα Εδάφους 30cm (DND 60)';
const LANG_dnd_60_sdi12_sal_40  = 'Αλατότητα Εδάφους 40cm (DND 60)';
const LANG_dnd_60_sdi12_sal_50  = 'Αλατότητα Εδάφους 50cm (DND 60)';
const LANG_dnd_60_sdi12_sal_60  = 'Αλατότητα Εδάφους 60cm (DND 60)';

const LANG_dnd_60_sdi12_sm_10   = 'Υγρασία Εδάφους 10cm (%) (DND 60)';
const LANG_dnd_60_sdi12_sm_20   = 'Υγρασία Εδάφους 20cm (%) (DND 60)';
const LANG_dnd_60_sdi12_sm_30   = 'Υγρασία Εδάφους 30cm (%) (DND 60)';
const LANG_dnd_60_sdi12_sm_40   = 'Υγρασία Εδάφους 40cm (%) (DND 60)';
const LANG_dnd_60_sdi12_sm_50   = 'Υγρασία Εδάφους 50cm (%) (DND 60)';
const LANG_dnd_60_sdi12_sm_60   = 'Υγρασία Εδάφους 60cm (%) (DND 60)';

/* DND 90 */
const LANG_dnd_90_sdi12_temp_10 = 'Θερμ/σία Εδάφους 10cm (°C) (DND 90)';
const LANG_dnd_90_sdi12_temp_20 = 'Θερμ/σία Εδάφους 20cm (°C) (DND 90)';
const LANG_dnd_90_sdi12_temp_30 = 'Θερμ/σία Εδάφους 30cm (°C) (DND 90)';
const LANG_dnd_90_sdi12_temp_40 = 'Θερμ/σία Εδάφους 40cm (°C) (DND 90)';
const LANG_dnd_90_sdi12_temp_50 = 'Θερμ/σία Εδάφους 50cm (°C) (DND 90)';
const LANG_dnd_90_sdi12_temp_60 = 'Θερμ/σία Εδάφους 60cm (°C) (DND 90)';
const LANG_dnd_90_sdi12_temp_70 = 'Θερμ/σία Εδάφους 70cm (°C) (DND 90)';
const LANG_dnd_90_sdi12_temp_80 = 'Θερμ/σία Εδάφους 80cm (°C) (DND 90)';
const LANG_dnd_90_sdi12_temp_90 = 'Θερμ/σία Εδάφους 90cm (°C) (DND 90)';

const LANG_dnd_90_sdi12_sal_10  = 'Αλατότητα Εδάφους 10cm (DND 90)';
const LANG_dnd_90_sdi12_sal_20  = 'Αλατότητα Εδάφους 20cm (DND 90)';
const LANG_dnd_90_sdi12_sal_30  = 'Αλατότητα Εδάφους 30cm (DND 90)';
const LANG_dnd_90_sdi12_sal_40  = 'Αλατότητα Εδάφους 40cm (DND 90)';
const LANG_dnd_90_sdi12_sal_50  = 'Αλατότητα Εδάφους 50cm (DND 90)';
const LANG_dnd_90_sdi12_sal_60  = 'Αλατότητα Εδάφους 60cm (DND 90)';
const LANG_dnd_90_sdi12_sal_70  = 'Αλατότητα Εδάφους 70cm (DND 90)';
const LANG_dnd_90_sdi12_sal_80  = 'Αλατότητα Εδάφους 80cm (DND 90)';
const LANG_dnd_90_sdi12_sal_90  = 'Αλατότητα Εδάφους 90cm (DND 90)';

const LANG_dnd_90_sdi12_sm_10   = 'Υγρασία Εδάφους 10cm (%) (DND 90)';
const LANG_dnd_90_sdi12_sm_20   = 'Υγρασία Εδάφους 20cm (%) (DND 90)';
const LANG_dnd_90_sdi12_sm_30   = 'Υγρασία Εδάφους 30cm (%) (DND 90)';
const LANG_dnd_90_sdi12_sm_40   = 'Υγρασία Εδάφους 40cm (%) (DND 90)';
const LANG_dnd_90_sdi12_sm_50   = 'Υγρασία Εδάφους 50cm (%) (DND 90)';
const LANG_dnd_90_sdi12_sm_60   = 'Υγρασία Εδάφους 60cm (%) (DND 90)';
const LANG_dnd_90_sdi12_sm_70   = 'Υγρασία Εδάφους 70cm (%) (DND 90)';
const LANG_dnd_90_sdi12_sm_80   = 'Υγρασία Εδάφους 80cm (%) (DND 90)';
const LANG_dnd_90_sdi12_sm_90   = 'Υγρασία Εδάφους 90cm (%) (DND 90)';

/* EnviroScan */

const LANG_enviro_sdi12_temp_10 = 'Θερμ/σία Εδάφους 10cm (°C) (DND 90)';
const LANG_enviro_sdi12_temp_20 = 'Θερμ/σία Εδάφους 20cm (°C) (DND 90)';
const LANG_enviro_sdi12_temp_30 = 'Θερμ/σία Εδάφους 30cm (°C) (DND 90)';
const LANG_enviro_sdi12_temp_40 = 'Θερμ/σία Εδάφους 40cm (°C) (DND 90)';
const LANG_enviro_sdi12_temp_50 = 'Θερμ/σία Εδάφους 50cm (°C) (DND 90)';
const LANG_enviro_sdi12_temp_60 = 'Θερμ/σία Εδάφους 60cm (°C) (DND 90)';
const LANG_enviro_sdi12_temp_70 = 'Θερμ/σία Εδάφους 70cm (°C) (DND 90)';
const LANG_enviro_sdi12_temp_80 = 'Θερμ/σία Εδάφους 80cm (°C) (DND 90)';
const LANG_enviro_sdi12_temp_90 = 'Θερμ/σία Εδάφους 90cm (°C) (DND 90)';

const LANG_enviro_sdi12_sal_10  = 'Αλατότητα Εδάφους 10cm (DND 90)';
const LANG_enviro_sdi12_sal_20  = 'Αλατότητα Εδάφους 20cm (DND 90)';
const LANG_enviro_sdi12_sal_30  = 'Αλατότητα Εδάφους 30cm (DND 90)';
const LANG_enviro_sdi12_sal_40  = 'Αλατότητα Εδάφους 40cm (DND 90)';
const LANG_enviro_sdi12_sal_50  = 'Αλατότητα Εδάφους 50cm (DND 90)';
const LANG_enviro_sdi12_sal_60  = 'Αλατότητα Εδάφους 60cm (DND 90)';
const LANG_enviro_sdi12_sal_70  = 'Αλατότητα Εδάφους 70cm (DND 90)';
const LANG_enviro_sdi12_sal_80  = 'Αλατότητα Εδάφους 80cm (DND 90)';
const LANG_enviro_sdi12_sal_90  = 'Αλατότητα Εδάφους 90cm (DND 90)';

const LANG_enviro_sdi12_sm_10   = 'Υγρασία Εδάφους 10cm (%) (DND 90)';
const LANG_enviro_sdi12_sm_20   = 'Υγρασία Εδάφους 20cm (%) (DND 90)';
const LANG_enviro_sdi12_sm_30   = 'Υγρασία Εδάφους 30cm (%) (DND 90)';
const LANG_enviro_sdi12_sm_40   = 'Υγρασία Εδάφους 40cm (%) (DND 90)';
const LANG_enviro_sdi12_sm_50   = 'Υγρασία Εδάφους 50cm (%) (DND 90)';
const LANG_enviro_sdi12_sm_60   = 'Υγρασία Εδάφους 60cm (%) (DND 90)';
const LANG_enviro_sdi12_sm_70   = 'Υγρασία Εδάφους 70cm (%) (DND 90)';
const LANG_enviro_sdi12_sm_80   = 'Υγρασία Εδάφους 80cm (%) (DND 90)';
const LANG_enviro_sdi12_sm_90   = 'Υγρασία Εδάφους 90cm (%) (DND 90)';

/* ICM Data */
const LANG_parcelid             = "Κωδικός Αγροτ/χίου";
const LANG_date                 = "Ημερομηνία";
const LANG_startdatetime        = "Ημερομηνία΄Έναρξης";
const LANG_enddatetime          = "Ημερομηνία Λήξης";
const LANG_osdecode             = "Κωδικός ΟΣΔΕ";
const LANG_waterquantity        = "Ποσότητα Νερού";
const LANG_irrigationsystem     = "Σύστημα Άρδευσης";
const LANG_remarks              = "Παρατηρήσεις";
const LANG_irighour             = "Ώρα Άρδευσης";
const LANG_drugname             = "Φάρμακο";
const LANG_dose                 = "Δόση";
const LANG_unit                 = "Μονάδα";
const LANG_activesubstance      = "Ενεργή Ουσία";
const LANG_target               = "Εχθρός";
const LANG_sprays               = "Ψεκασμοί";
const LANG_irrigations          = "Αρδεύσεις";
const LANG_harvest              = "Συγκομιδή";
const LANG_phaenological_stages = "Φαινολογικά";
const LANG_land_management      = "Εδαφολογικές Εργασίες";
const LANG_fertilization        = "Λίπανση";
const LANG_fertilizations       = "Λίπανση";

/* Grid Headers */
const LANG_longitude               = "Γεωγρ. Μήκος";
const LANG_latitude                = "Γεωγρ. Πλάτος";
const LANG_altitude                = "Γεωγρ. Ύψος";
const LANG_name                    = "Όνομα";
const LANG_active                  = "Ενεργός";
const LANG_id                      = "Μοναδικός Αριθμός";
const LANG_on                      = "Εμφάνιση";
const LANG_off                     = "Απόκρυψη";
const LANG_salinity                = "Αλατότητα";
const LANG_rh_soil                 = "Υγρασία";
const LANG_radius                  = "Απόσταση Σταθμού (μ)";
const LANG_location_name           = "Τοποθεσία";
const LANG_local_name              = "Τοπονύμιο";
const LANG_previous_chilling_hours = "Προηγούμενο Ψύχος";
const LANG_current_chilling_hours  = "Τωρινό Ψύχος";
const LANG_previous_harvest        = "Προηγούμενη Συγκομιδή";
const LANG_current_harvest         = "Τελευταία Συγκομιδή";
const LANG_required_hours          = "Ώρες Ψύχους";

/* Models */
const LANG_leaf_curl_generic                          = "Leaf Curl (Generic)";
const LANG_leaf_curl_regression                       = "Leaf Curl (Regression)";
const LANG_monilia_fructicola                         = "Monilia Fructicola";
const LANG_pstages                                    = "Phaenologic Stages";
const LANG_downy_mildew                               = "Περονόσπορος Αμπελιού (γενικευμένο)";
const LANG_powedery_mildew                            = "Ωίδιο Αμπελιού (γενικευμένο)";
const LANG_powedery_mildew_grapes_regression          = "Ωίδιο Αμπελιού (εξειδικευμένο)";
const LANG_powedery_mildew_grapes_regression_conidial = "Ωίδιο Αμπελιού (Κονιδιακή φάση)";
const LANG_erysiphe_negator_regression_2              = "Ωίδιο Αμπέλου 2 (εξειδικευμένο)";
const LANG_erysiphe_negator_regression_3              = "Ωίδιο Αμπέλου 3 (εξειδικευμένο)";

/* Calendar */
const LANG_no_calendar_data = "Δεν υπάρχουν καταχωρήσεις για ";
const LANG_get_aggregations = "Σύνολα";
const LANG_compare_panel    = "Πρότυπα";
const LANG_control_panel    = "Ομάδες";
const LANG_aggregation      = "Σύνολα";
const LANG_select_group     = "Επιλογή Ομάδας";
const LANG_select_period    = "Επιλογή Περιόδου";
const LANG_created_on       = "Ημ. Δημιουργίας";
const LANG_is_active        = "Ενεργό";
const LANG_is_default       = "Default";
const LANG_actions          = "Ενέργειες";
const LANG_location_id      = "ID Τοποθεσίας";
const LANG_station_id       = "ID Σταθμού";
const LANG_parcel_id        = "ID Αγροτεμαχίου";
const LANG_icon             = "Ενεργό";
const LANG_create_group     = "Δημιουργία Ομάδας";
const LANG_clear_selections = "Καθαρισμός Επιλογών";
const LANG_group_name       = "Όνομα Ομάδας";
const LANG_parcel           = "Αγροτεμάχιο";

const LANG_totalwaterperstremma   = "Συνολικό νερό (μ3/Στρέμμα)";
const LANG_totalwaterperparcel    = "Συνολικό νερό (μ3)";
const LANG_period                 = "Περίοδος";
const LANG_total_parcels          = "Σύνολο Αγροτεμαχίων";
const LANG_average_parcel_area    = "Μέσος Όρος Έκτασης (Στρέμμα)";
const LANG_total_events           = "Σύνολο Καταχωρήσεων";
const LANG_top_three              = "Επικρατέστερα 3";
const LANG_no_aggregation_data    = "Δεν υπάρχουν διαθέσιμα δεδομένα για την επιλογή.";
const LANG_graphs                 = "Γραφικές Παραστάσεις";
const LANG_details                = "Πληροφορίες";
const LANG_aggregations           = "Σύνολα";
const LANG_group                  = "Ομάδα";
const LANG_own_parcel             = "Το Αγροτεμάχιο μου";
const LANG_own_parcel_against     = "Το αγροτεμάχιο σύγκρισης";
const LANG_agg_analysis_graph     = "Σύνολα";
const LANG_details_are_in_stremma = "Η ανάλυση είναι ανα στρέμμα";
const LANG_total                  = "Σύνολο";
const LANG_stremma                = "Στρέμμα";
const LANG_avg                    = "Μέσος Όρος";
const LANG_min                    = "Ελάχιστο";
const LANG_max                    = "Μέγιστο";
const LANG_water                  = "Νερό";
const LANG_hour                   = "Ώρα";
const LANG_hours                  = "Ώρες";
const LANG_search                 = "Αναζήτηση";

const LANG_adminlastname          = "Όνομα";
const LANG_adminfrstname          = "Επώνυμο";
const LANG_adminphone             = "Τηλέφωνο";
const LANG_adminsubscriber        = "Subscriber";
const LANG_rocrdescription        = "Καλλιέργεια";
const LANG_rocvdescription        = "Ποικιλία";
const LANG_parcelPrefecture       = "Περιοχή";
const LANG_parcelToponym          = "Τοπονύμιο";