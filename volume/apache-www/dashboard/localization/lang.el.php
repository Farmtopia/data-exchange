<?php
/* Grid Headers */
define("LANG_longitude" , "Γεωγρ. Μήκος");
define("LANG_latitude" , "Γεωγρ. Πλάτος");
define("LANG_altitude" , "Γεωγρ. Ύψος");
define("LANG_toponym" , "Τοπωνύμιο");
define("LANG_name" , "Όνομα");
define("LANG_active" , "Ενεργός");
define("LANG_id" , "Μοναδικός Αριθμός");
define("LANG_parcel_id" , "ID Αγροτεμαχίου" );
define("LANG_location_id" , "ID Τοποθεσίας" );
define("LANG_station_id" , "ID Σταθμού" );
define("LANG_created_on" , "Ημ. Δημιουργίας" );
define("LANG_description" , "Περιγραφή" );
define("LANG_state" , "Κατάσταση" );

/* Generic */
define("LANG_total" , "Σύνολο");

/* Models */ 
define("LANG_leaf_curl_generic" , "Εξώασκος Ροδακινιάς (γενικευμένο)" );
define("LANG_leaf_curl_regression" , "Εξώασκος Ροδακινιάς (εξειδικευμένο)" );
define("LANG_monilia_fructicola" , "Φαιά Σήψη (εξειδικευμένο)" );

/* Sensors */
define("LANG_m_date" , "Ημερομηνία");
define("LANG_wsht30_temp" , "Θερμοκρασία");
define("LANG_wsht30_rh" , "Υγρασία");
define("LANG_nplwlw1" , "Διύγρανση Φύλλου");
define("LANG_rain" , "Βροχόπτωση");

/* DND 30 */
define("LANG_dnd_30_sdi12_temp_10" , "Θερμοκρασία Εδάφους 10cm (°C) (DND 30)");
define("LANG_dnd_30_sdi12_temp_20" , "Θερμοκρασία Εδάφους 20cm (°C) (DND 30)");
define("LANG_dnd_30_sdi12_temp_30" , "Θερμοκρασία Εδάφους 30cm (°C) (DND 30)");
define("LANG_dnd_30_sdi12_sal_10" , "Αλατότητα Εδάφους 10cm (DND 30)");
define("LANG_dnd_30_sdi12_sal_20" , "Αλατότητα Εδάφους 20cm (DND 30)");
define("LANG_dnd_30_sdi12_sal_30" , "Αλατότητα Εδάφους 30cm (DND 30)");
define("LANG_dnd_30_sdi12_sm_10" , "Υγρασία Εδάφους 10cm (%) (DND 30)");
define("LANG_dnd_30_sdi12_sm_20" , "Υγρασία Εδάφους 20cm (%) (DND 30)");
define("LANG_dnd_30_sdi12_sm_30" , "Υγρασία Εδάφους 30cm (%) (DND 30)");

/* DND 60 */
define("LANG_dnd_60_sdi12_temp_10" , "Θερμοκρασία Εδάφους 10cm (°C) (DND 60)");
define("LANG_dnd_60_sdi12_temp_20" , "Θερμοκρασία Εδάφους 20cm (°C) (DND 60)");
define("LANG_dnd_60_sdi12_temp_30" , "Θερμοκρασία Εδάφους 30cm (°C) (DND 60)");
define("LANG_dnd_60_sdi12_temp_40" , "Θερμοκρασία Εδάφους 40cm (°C) (DND 60)");
define("LANG_dnd_60_sdi12_temp_50" , "Θερμοκρασία Εδάφους 50cm (°C) (DND 60)");
define("LANG_dnd_60_sdi12_temp_60" , "Θερμοκρασία Εδάφους 60cm (°C) (DND 60)");

define("LANG_dnd_60_sdi12_sal_10" , "Αλατότητα Εδάφους 10cm (DND 60)");
define("LANG_dnd_60_sdi12_sal_20" , "Αλατότητα Εδάφους 20cm (DND 60)");
define("LANG_dnd_60_sdi12_sal_30" , "Αλατότητα Εδάφους 30cm (DND 60)");
define("LANG_dnd_60_sdi12_sal_40" , "Αλατότητα Εδάφους 40cm (DND 60)");
define("LANG_dnd_60_sdi12_sal_50" , "Αλατότητα Εδάφους 50cm (DND 60)");
define("LANG_dnd_60_sdi12_sal_60" , "Αλατότητα Εδάφους 60cm (DND 60)");

define("LANG_dnd_60_sdi12_sm_10" , "Υγρασία Εδάφους 10cm (%) (DND 60)");
define("LANG_dnd_60_sdi12_sm_20" , "Υγρασία Εδάφους 20cm (%) (DND 60)");
define("LANG_dnd_60_sdi12_sm_30" , "Υγρασία Εδάφους 30cm (%) (DND 60)");
define("LANG_dnd_60_sdi12_sm_40" , "Υγρασία Εδάφους 40cm (%) (DND 60)");
define("LANG_dnd_60_sdi12_sm_50" , "Υγρασία Εδάφους 50cm (%) (DND 60)");
define("LANG_dnd_60_sdi12_sm_60" , "Υγρασία Εδάφους 60cm (%) (DND 60)");

/* DND 90 */
define("LANG_dnd_90_sdi12_temp_10" , "Θερμοκρασία Εδάφους 10cm (°C) (DND 90)");
define("LANG_dnd_90_sdi12_temp_20" , "Θερμοκρασία Εδάφους 20cm (°C) (DND 90)");
define("LANG_dnd_90_sdi12_temp_30" , "Θερμοκρασία Εδάφους 30cm (°C) (DND 90)");
define("LANG_dnd_90_sdi12_temp_40" , "Θερμοκρασία Εδάφους 40cm (°C) (DND 90)");
define("LANG_dnd_90_sdi12_temp_50" , "Θερμοκρασία Εδάφους 50cm (°C) (DND 90)");
define("LANG_dnd_90_sdi12_temp_60" , "Θερμοκρασία Εδάφους 60cm (°C) (DND 90)");
define("LANG_dnd_90_sdi12_temp_70" , "Θερμοκρασία Εδάφους 70cm (°C) (DND 90)");
define("LANG_dnd_90_sdi12_temp_80" , "Θερμοκρασία Εδάφους 80cm (°C) (DND 90)");
define("LANG_dnd_90_sdi12_temp_90" , "Θερμοκρασία Εδάφους 90cm (°C) (DND 90)");

define("LANG_dnd_90_sdi12_sal_10" , "Αλατότητα Εδάφους 10cm (DND 90)");
define("LANG_dnd_90_sdi12_sal_20" , "Αλατότητα Εδάφους 20cm (DND 90)");
define("LANG_dnd_90_sdi12_sal_30" , "Αλατότητα Εδάφους 30cm (DND 90)");
define("LANG_dnd_90_sdi12_sal_40" , "Αλατότητα Εδάφους 40cm (DND 90)");
define("LANG_dnd_90_sdi12_sal_50" , "Αλατότητα Εδάφους 50cm (DND 90)");
define("LANG_dnd_90_sdi12_sal_60" , "Αλατότητα Εδάφους 60cm (DND 90)");
define("LANG_dnd_90_sdi12_sal_70" , "Αλατότητα Εδάφους 70cm (DND 90)");
define("LANG_dnd_90_sdi12_sal_80" , "Αλατότητα Εδάφους 80cm (DND 90)");
define("LANG_dnd_90_sdi12_sal_90" , "Αλατότητα Εδάφους 90cm (DND 90)");

define("LANG_dnd_90_sdi12_sm_10" , "Υγρασία Εδάφους 10cm (%) (DND 90)");
define("LANG_dnd_90_sdi12_sm_20" , "Υγρασία Εδάφους 20cm (%) (DND 90)");
define("LANG_dnd_90_sdi12_sm_30" , "Υγρασία Εδάφους 30cm (%) (DND 90)");
define("LANG_dnd_90_sdi12_sm_40" , "Υγρασία Εδάφους 40cm (%) (DND 90)");
define("LANG_dnd_90_sdi12_sm_50" , "Υγρασία Εδάφους 50cm (%) (DND 90)");
define("LANG_dnd_90_sdi12_sm_60" , "Υγρασία Εδάφους 60cm (%) (DND 90)");
define("LANG_dnd_90_sdi12_sm_70" , "Υγρασία Εδάφους 70cm (%) (DND 90)");
define("LANG_dnd_90_sdi12_sm_80" , "Υγρασία Εδάφους 80cm (%) (DND 90)");
define("LANG_dnd_90_sdi12_sm_90" , "Υγρασία Εδάφους 90cm (%) (DND 90)");

/* EnviroScan */

define("LANG_enviro_sdi12_temp_10" , "Θερμοκρασία Εδάφους 10cm (°C) (DND 90)");
define("LANG_enviro_sdi12_temp_20" , "Θερμοκρασία Εδάφους 20cm (°C) (DND 90)");
define("LANG_enviro_sdi12_temp_30" , "Θερμοκρασία Εδάφους 30cm (°C) (DND 90)");
define("LANG_enviro_sdi12_temp_40" , "Θερμοκρασία Εδάφους 40cm (°C) (DND 90)");
define("LANG_enviro_sdi12_temp_50" , "Θερμοκρασία Εδάφους 50cm (°C) (DND 90)");
define("LANG_enviro_sdi12_temp_60" , "Θερμοκρασία Εδάφους 60cm (°C) (DND 90)");
define("LANG_enviro_sdi12_temp_70" , "Θερμοκρασία Εδάφους 70cm (°C) (DND 90)");
define("LANG_enviro_sdi12_temp_80" , "Θερμοκρασία Εδάφους 80cm (°C) (DND 90)");
define("LANG_enviro_sdi12_temp_90" , "Θερμοκρασία Εδάφους 90cm (°C) (DND 90)");

define("LANG_enviro_sdi12_sal_10" , "Αλατότητα Εδάφους 10cm (DND 90)");
define("LANG_enviro_sdi12_sal_20" , "Αλατότητα Εδάφους 20cm (DND 90)");
define("LANG_enviro_sdi12_sal_30" , "Αλατότητα Εδάφους 30cm (DND 90)");
define("LANG_enviro_sdi12_sal_40" , "Αλατότητα Εδάφους 40cm (DND 90)");
define("LANG_enviro_sdi12_sal_50" , "Αλατότητα Εδάφους 50cm (DND 90)");
define("LANG_enviro_sdi12_sal_60" , "Αλατότητα Εδάφους 60cm (DND 90)");
define("LANG_enviro_sdi12_sal_70" , "Αλατότητα Εδάφους 70cm (DND 90)");
define("LANG_enviro_sdi12_sal_80" , "Αλατότητα Εδάφους 80cm (DND 90)");
define("LANG_enviro_sdi12_sal_90" , "Αλατότητα Εδάφους 90cm (DND 90)");

define("LANG_enviro_sdi12_sm_10" , "Υγρασία Εδάφους 10cm (%) (DND 90)");
define("LANG_enviro_sdi12_sm_20" , "Υγρασία Εδάφους 20cm (%) (DND 90)");
define("LANG_enviro_sdi12_sm_30" , "Υγρασία Εδάφους 30cm (%) (DND 90)");
define("LANG_enviro_sdi12_sm_40" , "Υγρασία Εδάφους 40cm (%) (DND 90)");
define("LANG_enviro_sdi12_sm_50" , "Υγρασία Εδάφους 50cm (%) (DND 90)");
define("LANG_enviro_sdi12_sm_60" , "Υγρασία Εδάφους 60cm (%) (DND 90)");
define("LANG_enviro_sdi12_sm_70" , "Υγρασία Εδάφους 70cm (%) (DND 90)");
define("LANG_enviro_sdi12_sm_80" , "Υγρασία Εδάφους 80cm (%) (DND 90)");
define("LANG_enviro_sdi12_sm_90" , "Υγρασία Εδάφους 90cm (%) (DND 90)");
