<?php
/* Grid Headers */
define("LANG_longitude" , "Longitude");
define("LANG_latitude" , "Latitude");
define("LANG_altitude" , "Altitude");
define("LANG_toponym" , "Toponym");
define("LANG_name" , "Name");
define("LANG_active" , "Active");
define("LANG_id" , "Id");
define("LANG_parcel_id" , "Parcel Id" );
define("LANG_location_id" , "Location Id" );
define("LANG_station_id" , "Station Id" );
define("LANG_created_on" , "Created On" );
define("LANG_description" , "Description" );
define("LANG_state" , "State" );

/* Generic */
define("LANG_total" , "Total");

/* Models */ 
define("LANG_leaf_curl_generic" , "Leaf Curl (Generic)" );
define("LANG_leaf_curl_regression" , "Leaf Curl (Regression)" );
define("LANG_monilia_fructicola" , "Monilia Fructicola" );

/* Sensors */
define("LANG_m_date" , "Date");
define("LANG_wsht30_temp" , "Temperature");
define("LANG_wsht30_rh" , "Humidity");
define("LANG_nplwlw1" , "Leaf Wetness");
define("LANG_rain" , "Rain");

/* DND 30 */
define("LANG_dnd_30_sdi12_temp_10" , "Soil Temperature 10cm (°C) (DND 30)");
define("LANG_dnd_30_sdi12_temp_20" , "Soil Temperature 20cm (°C) (DND 30)");
define("LANG_dnd_30_sdi12_temp_30" , "Soil Temperature 30cm (°C) (DND 30)");
define("LANG_dnd_30_sdi12_sal_10" , "Soil Salinity 10cm (DND 30)");
define("LANG_dnd_30_sdi12_sal_20" , "Soil Salinity 20cm (DND 30)");
define("LANG_dnd_30_sdi12_sal_30" , "Soil Salinity 30cm (DND 30)");
define("LANG_dnd_30_sdi12_sm_10" , "Soil Humidity 10cm (%) (DND 30)");
define("LANG_dnd_30_sdi12_sm_20" , "Soil Humidity 20cm (%) (DND 30)");
define("LANG_dnd_30_sdi12_sm_30" , "Soil Humidity 30cm (%) (DND 30)");

/* DND 60 */
define("LANG_dnd_60_sdi12_temp_10" , "Soil Temperature 10cm (°C) (DND 60)");
define("LANG_dnd_60_sdi12_temp_20" , "Soil Temperature 20cm (°C) (DND 60)");
define("LANG_dnd_60_sdi12_temp_30" , "Soil Temperature 30cm (°C) (DND 60)");
define("LANG_dnd_60_sdi12_temp_40" , "Soil Temperature 40cm (°C) (DND 60)");
define("LANG_dnd_60_sdi12_temp_50" , "Soil Temperature 50cm (°C) (DND 60)");
define("LANG_dnd_60_sdi12_temp_60" , "Soil Temperature 60cm (°C) (DND 60)");

define("LANG_dnd_60_sdi12_sal_10" , "Soil Salinity 10cm (DND 60)");
define("LANG_dnd_60_sdi12_sal_20" , "Soil Salinity 20cm (DND 60)");
define("LANG_dnd_60_sdi12_sal_30" , "Soil Salinity 30cm (DND 60)");
define("LANG_dnd_60_sdi12_sal_40" , "Soil Salinity 40cm (DND 60)");
define("LANG_dnd_60_sdi12_sal_50" , "Soil Salinity 50cm (DND 60)");
define("LANG_dnd_60_sdi12_sal_60" , "Soil Salinity 60cm (DND 60)");

define("LANG_dnd_60_sdi12_sm_10" , "Soil Humidity 10cm (%) (DND 60)");
define("LANG_dnd_60_sdi12_sm_20" , "Soil Humidity 20cm (%) (DND 60)");
define("LANG_dnd_60_sdi12_sm_30" , "Soil Humidity 30cm (%) (DND 60)");
define("LANG_dnd_60_sdi12_sm_40" , "Soil Humidity 40cm (%) (DND 60)");
define("LANG_dnd_60_sdi12_sm_50" , "Soil Humidity 50cm (%) (DND 60)");
define("LANG_dnd_60_sdi12_sm_60" , "Soil Humidity 60cm (%) (DND 60)");

/* DND 90 */
define("LANG_dnd_90_sdi12_temp_10" , "Soil Temperature 10cm (°C) (DND 90)");
define("LANG_dnd_90_sdi12_temp_20" , "Soil Temperature 20cm (°C) (DND 90)");
define("LANG_dnd_90_sdi12_temp_30" , "Soil Temperature 30cm (°C) (DND 90)");
define("LANG_dnd_90_sdi12_temp_40" , "Soil Temperature 40cm (°C) (DND 90)");
define("LANG_dnd_90_sdi12_temp_50" , "Soil Temperature 50cm (°C) (DND 90)");
define("LANG_dnd_90_sdi12_temp_60" , "Soil Temperature 60cm (°C) (DND 90)");
define("LANG_dnd_90_sdi12_temp_70" , "Soil Temperature 70cm (°C) (DND 90)");
define("LANG_dnd_90_sdi12_temp_80" , "Soil Temperature 80cm (°C) (DND 90)");
define("LANG_dnd_90_sdi12_temp_90" , "Soil Temperature 90cm (°C) (DND 90)");

define("LANG_dnd_90_sdi12_sal_10" , "Soil Salinity 10cm (DND 90)");
define("LANG_dnd_90_sdi12_sal_20" , "Soil Salinity 20cm (DND 90)");
define("LANG_dnd_90_sdi12_sal_30" , "Soil Salinity 30cm (DND 90)");
define("LANG_dnd_90_sdi12_sal_40" , "Soil Salinity 40cm (DND 90)");
define("LANG_dnd_90_sdi12_sal_50" , "Soil Salinity 50cm (DND 90)");
define("LANG_dnd_90_sdi12_sal_60" , "Soil Salinity 60cm (DND 90)");
define("LANG_dnd_90_sdi12_sal_70" , "Soil Salinity 70cm (DND 90)");
define("LANG_dnd_90_sdi12_sal_80" , "Soil Salinity 80cm (DND 90)");
define("LANG_dnd_90_sdi12_sal_90" , "Soil Salinity 90cm (DND 90)");

define("LANG_dnd_90_sdi12_sm_10" , "Soil Humidity 10cm (%) (DND 90)");
define("LANG_dnd_90_sdi12_sm_20" , "Soil Humidity 20cm (%) (DND 90)");
define("LANG_dnd_90_sdi12_sm_30" , "Soil Humidity 30cm (%) (DND 90)");
define("LANG_dnd_90_sdi12_sm_40" , "Soil Humidity 40cm (%) (DND 90)");
define("LANG_dnd_90_sdi12_sm_50" , "Soil Humidity 50cm (%) (DND 90)");
define("LANG_dnd_90_sdi12_sm_60" , "Soil Humidity 60cm (%) (DND 90)");
define("LANG_dnd_90_sdi12_sm_70" , "Soil Humidity 70cm (%) (DND 90)");
define("LANG_dnd_90_sdi12_sm_80" , "Soil Humidity 80cm (%) (DND 90)");
define("LANG_dnd_90_sdi12_sm_90" , "Soil Humidity 90cm (%) (DND 90)");

/* EnviroScan */

define("LANG_enviro_sdi12_temp_10" , "Soil Temperature 10cm (°C) (DND 90)");
define("LANG_enviro_sdi12_temp_20" , "Soil Temperature 20cm (°C) (DND 90)");
define("LANG_enviro_sdi12_temp_30" , "Soil Temperature 30cm (°C) (DND 90)");
define("LANG_enviro_sdi12_temp_40" , "Soil Temperature 40cm (°C) (DND 90)");
define("LANG_enviro_sdi12_temp_50" , "Soil Temperature 50cm (°C) (DND 90)");
define("LANG_enviro_sdi12_temp_60" , "Soil Temperature 60cm (°C) (DND 90)");
define("LANG_enviro_sdi12_temp_70" , "Soil Temperature 70cm (°C) (DND 90)");
define("LANG_enviro_sdi12_temp_80" , "Soil Temperature 80cm (°C) (DND 90)");
define("LANG_enviro_sdi12_temp_90" , "Soil Temperature 90cm (°C) (DND 90)");

define("LANG_enviro_sdi12_sal_10" , "Soil Salinity 10cm (DND 90)");
define("LANG_enviro_sdi12_sal_20" , "Soil Salinity 20cm (DND 90)");
define("LANG_enviro_sdi12_sal_30" , "Soil Salinity 30cm (DND 90)");
define("LANG_enviro_sdi12_sal_40" , "Soil Salinity 40cm (DND 90)");
define("LANG_enviro_sdi12_sal_50" , "Soil Salinity 50cm (DND 90)");
define("LANG_enviro_sdi12_sal_60" , "Soil Salinity 60cm (DND 90)");
define("LANG_enviro_sdi12_sal_70" , "Soil Salinity 70cm (DND 90)");
define("LANG_enviro_sdi12_sal_80" , "Soil Salinity 80cm (DND 90)");
define("LANG_enviro_sdi12_sal_90" , "Soil Salinity 90cm (DND 90)");

define("LANG_enviro_sdi12_sm_10" , "Soil Humidity 10cm (%) (DND 90)");
define("LANG_enviro_sdi12_sm_20" , "Soil Humidity 20cm (%) (DND 90)");
define("LANG_enviro_sdi12_sm_30" , "Soil Humidity 30cm (%) (DND 90)");
define("LANG_enviro_sdi12_sm_40" , "Soil Humidity 40cm (%) (DND 90)");
define("LANG_enviro_sdi12_sm_50" , "Soil Humidity 50cm (%) (DND 90)");
define("LANG_enviro_sdi12_sm_60" , "Soil Humidity 60cm (%) (DND 90)");
define("LANG_enviro_sdi12_sm_70" , "Soil Humidity 70cm (%) (DND 90)");
define("LANG_enviro_sdi12_sm_80" , "Soil Humidity 80cm (%) (DND 90)");
define("LANG_enviro_sdi12_sm_90" , "Soil Humidity 90cm (%) (DND 90)");
