<?php
  include( "../header.php" );

  $Settings->amLog( "Login : " . $_SERVER['REQUEST_URI'] , 'uri' , 'a');
  $myLogo = array( 
    "img"        => "../template/site_items/app_icon_full.png" , 
    "link"       => "./index.php" , 
    "site_name"  => $Settings->SITE_TITLE , 
    "site_title" => $Settings->SITE_TITLE ,
    "img_width"  => "140",
    "img_height" => "50"
  );

?>
<title><?php echo $myLogo[ "site_title" ]; ?></title>
 <!-- Stuff for PWA -->
 <link rel="manifest" href="../manifest_v<?php echo $Settings->cacheVersion; ?>.json">
 <meta name="Description" content="<?php echo $Settings->META_DESCRIPTION; ?>">
 <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
 <meta name="theme-color" content="#FF9800"/> 
 <meta name="mobile-web-app-capable" content="yes"/>
 <meta name="apple-mobile-web-app-capable" content="yes"/>
 <meta name="application-name" content="<?php echo $Settings->SITE_NAME; ?>"/>
 <meta name="apple-mobile-web-app-title" content="<?php echo $Settings->SITE_NAME; ?>"/>
 <meta name="msapplication-navbutton-color" content="#FF9800"/>
 <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
 <meta name="msapplication-starturl" content="/index.php"/>
 <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $Settings->ICON_152x152; ?>">
 <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $Settings->ICON_167x167; ?>">
 <link rel="apple-touch-icon" sizes="167x167" href="<?php echo $Settings->ICON_180x180; ?>">
 <meta name="apple-mobile-web-app-title" content="<?php echo $Settings->MANIFEST_CONTENT; ?>">
 <meta name="apple-mobile-web-app-title" content="<?php echo $Settings->SITE_NAME; ?>"/>
 
 <!-- Google Maps -->
 <!-- <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $Settings->GoogleMapsApiKey; ?>"></script> -->
 <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $Settings->GoogleMapsApiKey; ?>&libraries=drawing"></script>

 <!-- Favicon -->
 <link href="<?php echo $Settings->FAVICON; ?>" rel="icon" type="image/vnd.microsoft.icon"></link>
 <link href="<?php echo $Settings->FAVICON; ?>" rel="shortcut icon" type="image/png"></link>
 
 <!-- Popper ( needed by nootstrap for dropdowns ) -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

 <!-- Main jQuery Lib -->
 <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

 <!-- Main Bootstrap Library -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.min.css"></link>

 <!-- Font-Awesome CSS -->
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"></link>

 <!-- Main Material Design Library -->
 <link rel='stylesheet' href='<?php echo $Settings->SERVER_URL; ?>template/<?php echo $Settings->TEMPLATE; ?>/ext_css/mdb.css'></link>
 <script src="../ext_js/mdb.js"></script>

 <!-- DataTables -->
 <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
 <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
 <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
 <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css"></link>

 <!-- CSS for Grid Export Buttons -->
 <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
 <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.bootstrap4.min.js"></script>
 <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>    
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>    
 <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>    
 <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>    
 <script src="https://cdn.datatables.net/buttons/2.3.5/js/buttons.print.min.js"></script>    

 <!-- Grid Export Dependancy -->    
 <!--
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
 -->

 <!-- For Exporting PDF from Grid  -->
 <!--
 <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
 -->

 <!-- For Print Support on Grid -->
 <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
 
 <!-- For Column Visibility on Grid -->
 <!--
 <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"></script>
 -->
 
 <!-- For Column Visibility on Grid -->
 <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
 <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css"></link>

 <!--
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>    
 -->

 <script src="../js_libs/moment.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.1.2/js/tempusdominus-bootstrap-4.min.js" integrity="sha512-2JBCbWoMJPH+Uj7Wq5OLub8E5edWHlTM4ar/YJkZh3plwB2INhhOC3eDoqHm1Za/ZOSksrLlURLoyXVdfQXqwg==" crossorigin="anonymous"></script>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.1.2/css/tempusdominus-bootstrap-4.min.css" integrity="sha512-PMjWzHVtwxdq7m7GIxBot5vdxUY+5aKP9wpKtvnNBZrVv1srI8tU6xvFMzG8crLNcMj/8Xl/WWmo/oAP/40p1g==" crossorigin="anonymous" />

 <!-- iPromptUI -->
 <script src="../js_libs/ipromptui/ipromptui.js"></script>
 <link rel="stylesheet" href="../js_libs/ipromptui/ipromptui.css"></link>
 
 <!-- Custom Scrollbar -->
 <script src="//malihu.github.io/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
 <link rel="stylesheet" href="//malihu.github.io/custom-scrollbar/jquery.mCustomScrollbar.min.css" />

 <!-- BootBox -->
 <!-- <script src="../js_libs/bootbox/bootbox.all.min.js"></script> -->

 <!-- Highcharts -->  
 <script type="text/javascript" src="../ext_js/highjs/highstock.js"></script>
 <script type="text/javascript" src="../ext_js/highjs/exporting.js"></script>
 <script type="text/javascript" src="../ext_js/highjs/highcharts-3d.js"></script>
 <script type="text/javascript" src="../ext_js/highjs/drilldown.js"></script>
 <script type="text/javascript" src="../ext_js/highjs/highcharts-more.js"></script>
 <script type="text/javascript" src="../ext_js/highjs/highcharts-gauge.js"></script>
 
 <!-- Custom CSS -->
 <link rel='stylesheet' href='<?php echo $Settings->SERVER_URL; ?>template/<?php echo $Settings->TEMPLATE; ?>/css/main_v<?php echo $Settings->cacheVersion; ?>.css'></link>
 <link rel='stylesheet' href='<?php echo $Settings->SERVER_URL; ?>template/<?php echo $Settings->TEMPLATE; ?>/css/widget_v<?php echo $Settings->cacheVersion; ?>.css'></link>
 <link rel='stylesheet' href='<?php echo $Settings->SERVER_URL; ?>template/<?php echo $Settings->TEMPLATE; ?>/css/override_v<?php echo $Settings->cacheVersion; ?>.css'></link>
 <link rel='stylesheet' href='<?php echo $Settings->SERVER_URL; ?>template/<?php echo $Settings->TEMPLATE; ?>/css/form_v<?php echo $Settings->cacheVersion; ?>.css'></link>
 <link rel='stylesheet' href='<?php echo $Settings->SERVER_URL; ?>template/<?php echo $Settings->TEMPLATE; ?>/css/timeline_v<?php echo $Settings->cacheVersion; ?>.css'></link>

 <!-- Card Widget -->
 <link rel='stylesheet' href='<?php echo $Settings->SERVER_URL; ?>template/<?php echo $Settings->TEMPLATE; ?>/css/card_widget_v<?php echo $Settings->cacheVersion; ?>.css'></link>

 <!-- Dashboard Style -->
 <link rel='stylesheet' href='<?php echo $Settings->SERVER_URL; ?>template/<?php echo $Settings->TEMPLATE; ?>/css/dashboard_v<?php echo $Settings->cacheVersion; ?>.css'></link>

 <!-- Set Global Language -->
 <script>var myLanguage = '<?php echo $amUser->getLocale(); ?>';</script>

 <!-- Custom JS Libs-->
 <script src='../localization/lang.<?php echo $amUser->getLocale(); ?>_v<?php echo $Settings->cacheVersion; ?>.js'></script>
 <script src='../js_libs/amAutocomplete_v<?php echo $Settings->cacheVersion; ?>.js'></script>
 <script src="../js_libs/tools_v<?php echo $Settings->cacheVersion; ?>.js"></script>
 <script src="../js_libs/amForm_v<?php echo $Settings->cacheVersion; ?>.js"></script>
 <script src="../js_libs/amGrid_v<?php echo $Settings->cacheVersion; ?>.js"></script>
 <script src="../js_libs/amList_v<?php echo $Settings->cacheVersion; ?>.js"></script>
 <script src="../js_libs/amLayout_v<?php echo $Settings->cacheVersion; ?>.js"></script>
 <script src="../js_libs/class.wsdata_v<?php echo $Settings->cacheVersion; ?>.js"></script>
 <script src="../js_libs/class_combo_grid_v<?php echo $Settings->cacheVersion; ?>.js"></script>
 <script src="../js_libs/class_combo_form_grid_v<?php echo $Settings->cacheVersion; ?>.js"></script>
 <script src='../js_libs/dom_components/amDom_CardWithIcon_v<?php echo $Settings->cacheVersion; ?>.js'></script>
 <script src="../js_libs/form.helper_v<?php echo $Settings->cacheVersion; ?>.js"></script>
 <script src='../js_libs/httprequest_v<?php echo $Settings->cacheVersion; ?>.js'></script>
 <script src='../js_libs/amUpload_v<?php echo $Settings->cacheVersion; ?>.js'></script>
 <script src='../js_libs/widget_v<?php echo $Settings->cacheVersion; ?>.js'></script>
 <script src='../js_libs/main_v<?php echo $Settings->cacheVersion; ?>.js'></script>
 <script src='../js_libs/amAjaxEndpoints_v<?php echo $Settings->cacheVersion; ?>.js'></script>
 <script src='../js_libs/amGraphView_v<?php echo $Settings->cacheVersion; ?>.js'></script>
 
 <!-- Custom JS Views -->
 <script src='../js_libs/views/Datasources_v<?php echo $Settings->cacheVersion; ?>.js'></script>
 
 <link rel="manifest" href="../manifest_v<?php echo $Settings->cacheVersion; ?>.json">

 <style>
 div.jqibox{ z-index: 1000000000 !important; }
 </style>

<body>

 <nav class="navbar navbar-expand-lg navbar-dark primary-color am-nav-bar">
   <div style='color:white;display: inline-block;padding-top: .3125rem;padding-bottom: .3125rem;margin-right: 1rem;font-size: 1.25rem;line-height: inherit;white-space: nowrap;'>
    <a href="<?php echo $myLogo[ "link" ]; ?>" style="color: black; font-weight: 400; padding: 4px;"><img src="<?php echo $myLogo[ "img" ]; ?>" width="<?php echo $myLogo[ "img_width" ]; ?>" height="<?php echo $myLogo[ "img_height" ]; ?>" class="d-inline-block align-middle" alt=""></a>
   </div>
  <button class="navbar-toggler" 
          type="button" 
          data-toggle="collapse" 
          data-target="#basicExampleNav"
          aria-controls="basicExampleNav" 
          aria-expanded="false" 
          aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="basicExampleNav">

   <ul class="navbar-nav mr-auto" id="nav-tab"></ul>

   <ul class="navbar-nav ml-auto nav-flex-icons">
     <li class="nav-item">
       <a class="nav-link waves-effect waves-light" style="padding: 12px;" navname="messages" navaction="messages">
         <i class="fas fa-envelope"></i>
       </a>
     </li>
     <li class="nav-item avatar dropdown">
       <a class="nav-link dropdown-toggle" 
          id="navbarDropdownMenuLink-55" 
          data-toggle="dropdown"
          aria-haspopup="true" 
          aria-expanded="false">
           <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-person-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
             <path d="M13.468 12.37C12.758 11.226 11.195 10 8 10s-4.757 1.225-5.468 2.37A6.987 6.987 0 0 0 8 15a6.987 6.987 0 0 0 5.468-2.63z"/>
             <path fill-rule="evenodd" d="M8 9a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
             <path fill-rule="evenodd" d="M8 1a7 7 0 1 0 0 14A7 7 0 0 0 8 1zM0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8z"/>
           </svg>
       </a>
       <div class="dropdown-menu dropdown-menu-lg-right dropdown-secondary"
         aria-labelledby="navbarDropdownMenuLink-55">
         <a class="dropdown-item" navname="settings" navaction="settings" href="#">Settings</a>
         <a class="dropdown-item" navname="logout" navaction="logout" href="#">Logout</a>
       </div>
     </li>
   </ul>

  </div>
  <div class="language-selection" id="language-selection">
   <div><img id="lang-flag-en" onClick="setLanguage( 'en' );" src="../template/site_items/flags/en_flag.png"></img></div>
   <div><img id="lang-flag-el" onClick="setLanguage( 'el' );" src="../template/site_items/flags/el_flag.png"></img></div>
  </div>
 </nav>

 <div class="nav-path" id="nav-path"></div>
 <div class="tab-content am-tab-content" id="nav-tabContent"></div>
 <div class="loader-container" id="loader-container" style="display: none; position: absolute; top: 0%; width: 100%; height: 100%; z-index: 15;     background-color: rgb(52,58,64,0.25);">
  <div class="d-flex justify-content-center" style="background-color: var(--info); color: white; padding: 10px; width: 150px; margin: auto; border-radius: 4px; top: 30%; position: relative;">
    <span style="line-height: 30px; margin-right: 10px;">Loading...</span>
    <div class="spinner-border" role="status">
    </div>
  </div>
 </div>

<div class="position-fixed bottom-0 right-0 p-3" style="z-index: 5; right: 0; bottom: 0;">
  <div id="liveToast" class="toast hide" role="alert" aria-live="assertive" aria-atomic="true" data-delay="2000">
    <div class="toast-header" id="toast-header">
      <i class="fas fa-exclamation-triangle"></i> &nbsp;&nbsp;
      <strong class="mr-auto" id="toast-title"></strong>
      <small id="toast-small-message"></small>
      <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="toast-body" id="toast-message"></div>
  </div>
</div>

</body>

 <!-- Custom Late JS Libs-->
 <script> addTabs(); </script>
 <script src='../js_libs/map_helper_v<?php echo $Settings->cacheVersion; ?>.js'></script> 
 <script src='../js_libs/nav_v<?php echo $Settings->cacheVersion; ?>.js'></script>

<!--
 <script src="https://code.highcharts.com/modules/no-data-to-display.js"></script>
 <script type="text/javascript" src="../ext_js/highjs/exporting.js"></script>
 <script type="text/javascript" src="../ext_js/highjs/highcharts-3d.js"></script>
 <script type="text/javascript" src="../ext_js/highjs/drilldown.js"></script>
-->

 <!-- Mui Library -->
 <!--
 <script src="https://unpkg.com/@material-ui/core@latest/umd/material-ui.development.js"></script>
 -->

 <script>
  setLanguageStyle( myLanguage );
  setTimeout( () => {
    document.getElementById( 'nav-datasources-tab' ).click();
  } , 500 );

$('.navbar-collapse a:not(.dropdown-toggle)').click(function(){
    $(".navbar-collapse").collapse('hide');
});
 </script>