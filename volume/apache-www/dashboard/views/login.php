<?php
  require_once('../php_libs/lib.main.php');     // main();
   $Settings = new main();

  $cTime = new Datetime();
  $dd    = $cTime->format( "HmdHis" );

  function generateUniqueToken(){
    global $Settings;
    global $dd;

    return md5( $dd."|".$Settings->APP_ID."|".$Settings->SITE_NAME );
  }

  require_once('../localization/lang.'.$Settings->LOCALE.'.php');
?>
<head>
 <title><?php echo $Settings->SITE_NAME; ?></title>
 <!-- Stuff for PWA -->
 <link rel="manifest" href="../manifest_v<?php echo $Settings->cacheVersion; ?>.json">
 <meta name="Description" content="<?php echo $Settings->META_DESCRIPTION; ?>">
 <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
 <meta name="theme-color" content="#FF9800"/> 
 <meta name="mobile-web-app-capable" content="yes"/>
 <meta name="apple-mobile-web-app-capable" content="yes"/>
 <meta name="application-name" content="<?php echo $Settings->SITE_NAME; ?>"/>
 <meta name="apple-mobile-web-app-title" content="<?php echo $Settings->SITE_NAME; ?>"/>
 <meta name="msapplication-navbutton-color" content="#FF9800"/>
 <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
 <meta name="msapplication-starturl" content="/index.php"/>
 <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $Settings->ICON_152x152; ?>">
 <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $Settings->ICON_167x167; ?>">
 <link rel="apple-touch-icon" sizes="167x167" href="<?php echo $Settings->ICON_180x180; ?>">
 <meta name="apple-mobile-web-app-title" content="<?php echo $Settings->MANIFEST_CONTENT; ?>">
 <meta name="apple-mobile-web-app-title" content="<?php echo $Settings->SITE_NAME; ?>"/>

 <!-- Main jQuery Lib -->
 <script src="../ext_js/jquery.min.js"></script>

 <!-- Font-Awesome CSS -->
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"></link>

 <!-- Main Bootstrap Library -->
 <script src="../ext_js/bootstrap.min.js"></script>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/css/bootstrap.min.css"></link>

 <!-- Favicon -->
 <link href="<?php echo $Settings->FAVICON; ?>" rel="icon" type="image/vnd.microsoft.icon"></link>
 <link href="<?php echo $Settings->FAVICON; ?>" rel="shortcut icon" type="image/png"></link>

 <!-- Custom CSS -->
 <link rel='stylesheet' href='<?php echo $Settings->SERVER_URL; ?>template/<?php echo $Settings->TEMPLATE; ?>/css/main_v<?php echo $Settings->cacheVersion; ?>.css'></link>
 <link rel='stylesheet' href='<?php echo $Settings->SERVER_URL; ?>template/<?php echo $Settings->TEMPLATE; ?>/css/login_v<?php echo $Settings->cacheVersion; ?>.css'></link>

 <!-- Custom JS Libs-->
 <script src='../js_libs/httprequest_v<?php echo $Settings->cacheVersion; ?>.js'></script>
 <script src="../js_libs/tools_v<?php echo $Settings->cacheVersion; ?>.js"></script> 
 <script src='../js_libs/main_v<?php echo $Settings->cacheVersion; ?>.js'></script>
 <script src='../js_libs/rstpw.js'></script>
</head>
<body>
 <div class='bs-alert-amanos-cnt' id='bs-alert-amanos-cnt'></div>
 <div class="container">
  <div class="d-flex justify-content-center h-100">
    <div style='position: absolute; top: 15px; left: 15px; background-color: rgb(247 247 247); border-radius: 100px; padding: 5px 10px 5px 10px; border: 1px solid #0000000f;'>
    <img src="../template/site_items/np_logo.png" alt="<?php echo $Settings->SITE_NAME; ?>-logo" style='width:130px;'></img>
   </div>
   <div class="card" style='background-color: #f8f9fac2;'>
    <div class="card-header">
     <h3>Sign In</h3>
    </div>
    <div class="card-body">
     <form>
      <div class="input-group form-group">
       <div class="input-group-prepend">
        <span class="input-group-text">
         <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-person-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
           <path d="M13.468 12.37C12.758 11.226 11.195 10 8 10s-4.757 1.225-5.468 2.37A6.987 6.987 0 0 0 8 15a6.987 6.987 0 0 0 5.468-2.63z"/>
           <path fill-rule="evenodd" d="M8 9a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
           <path fill-rule="evenodd" d="M8 1a7 7 0 1 0 0 14A7 7 0 0 0 8 1zM0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8z"/>
         </svg>
        </span>
       </div>
       <input type="text" class="form-control" placeholder="username" name="usr" id="usr" />
      </div>
      <div class="input-group form-group">
       <div class="input-group-prepend">
        <span class="input-group-text">
         <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-key" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
           <path fill-rule="evenodd" d="M0 8a4 4 0 0 1 7.465-2H14a.5.5 0 0 1 .354.146l1.5 1.5a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0L13 9.207l-.646.647a.5.5 0 0 1-.708 0L11 9.207l-.646.647a.5.5 0 0 1-.708 0L9 9.207l-.646.647A.5.5 0 0 1 8 10h-.535A4 4 0 0 1 0 8zm4-3a3 3 0 1 0 2.712 4.285A.5.5 0 0 1 7.163 9h.63l.853-.854a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.793-.793-1-1h-6.63a.5.5 0 0 1-.451-.285A3 3 0 0 0 4 5z"/>
           <path d="M4 8a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/>
         </svg>
        </span>
       </div>
       <input type="password" class="form-control" placeholder="password" name="pw" id="pw" />
       <div class="input-group-prepend" style='cursor: pointer;' onClick='togglePasswordVisibility( event );'>
        <span class="input-group-text" id="pw-toggler">
         <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-slash-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
           <path d="M10.79 12.912l-1.614-1.615a3.5 3.5 0 0 1-4.474-4.474l-2.06-2.06C.938 6.278 0 8 0 8s3 5.5 8 5.5a7.029 7.029 0 0 0 2.79-.588zM5.21 3.088A7.028 7.028 0 0 1 8 2.5c5 0 8 5.5 8 5.5s-.939 1.721-2.641 3.238l-2.062-2.062a3.5 3.5 0 0 0-4.474-4.474L5.21 3.089z"/>
           <path d="M5.525 7.646a2.5 2.5 0 0 0 2.829 2.829l-2.83-2.829zm4.95.708l-2.829-2.83a2.5 2.5 0 0 1 2.829 2.829z"/>
           <path fill-rule="evenodd" d="M13.646 14.354l-12-12 .708-.708 12 12-.708.708z"/>
         </svg>
        </span>
       </div>

      </div>
      <div class="form-group">
       <button type="button" aria-label='Login' class="btn btn-primary btn-sm" onclick='login()'>Login</button>
       <button type="button" aria-label='ResetPassword' class="btn btn-secondary btn-sm" onclick='requestReset();'>Reset Password</button>
      </div>
     </form>
    </div>
   </div>
  </div>
 </div>
</body>