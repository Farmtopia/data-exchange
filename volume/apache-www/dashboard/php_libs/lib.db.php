<?php

class getDBO extends PDO{
  public $link;
  public $query;
  public $error;
  public $params;
  public $pdo;

  public function __construct($options = NULL){
   $Settings  = new main();

   if(!$options){
     $this->db         = $Settings->DB_NAME;
     $this->host       = $Settings->DB_HOST;
     $this->dbusername = $Settings->DB_USERNAME;
     $this->dbpassword = $Settings->DB_PASSWORD;
     $this->dbport     = isset($Settings->DB_PORT) ? $Settings->DB_PORT : '';
     $this->dbtype     = isset($Settings->DB_TYPE) ? $Settings->DB_TYPE : "mysql";
   }else{
     $options          = (object) $options;
     $this->db         = ($options->DB_NAME)     ? $options->DB_NAME     : $this->DB_NAME;
     $this->host       = ($options->DB_HOST)     ? $options->DB_HOST     : $this->DB_HOST;
     $this->dbusername = ($options->DB_USERNAME) ? $options->DB_USERNAME : $this->DB_USERNAME;
     $this->dbpassword = ($options->DB_PASSWORD) ? $options->DB_PASSWORD : $this->DB_PASSWORD;
     $this->dbport     = ($options->DB_PORT)     ? $options->DB_PORT     : $this->DB_PORT;
     $this->dbtype     = ($options->DB_TYPE)     ? $options->DB_TYPE     : $this->DB_TYPE;
   }

   if( $this->dbtype === "mysql" )
    $dsn = $this->dbtype.":host={$this->host};dbname={$this->db};charset=utf8";
   else if( $this->dbtype === "pgsql" )
    $dsn = $this->dbtype.":host={$this->host};port={$this->dbport};dbname={$this->db};user={$this->dbusername};password={$this->dbpassword};";

   $options = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
   ];

   try {
    $pdo = new PDO($dsn, $this->dbusername, $this->dbpassword, $options);
    $this->pdo = $pdo;
    return $pdo;
   } catch (\PDOException $e) {
    // throw new \PDOException($e->getMessage(), (int)$e->getCode());
    $this->error = $e->getMessage();
    return [ "error" => $this->error ];
   }

  }

  public function setQuery($query){
   $this->query = $query;
  }

  public function lastInsertedID($seqname = NULL){
   return $this->pdo->lastInsertId($seqname);
  }

  public function prepare($statement , $statementValues = NULL){
   $stmt = $this->pdo->prepare($statement);
   $stmt->execute($statementValues);
   return $stmt->fetch();
  }

  public function loadAssocList($statement , $statementValues){
   $this->query  = $statement;
   $this->params = $statementValues;

   $myParams = array();
   $query    = $this->pdo->prepare($this->query);

   foreach($this->params as $idx => &$value){
    if( is_int($value) ) {
     // echo "[is_int] Bidning on [ ".($idx + 1)." ] Value [ {$value} ] <br>";
     $query->bindParam( ( $idx + 1 ) , $value, PDO::PARAM_INT);
    }else if( is_float($value) ){
     // echo "[is_float] Bidning on [ ".($idx + 1)." ] Value [ {$value} ] <br>";
     $query->bindParam( ( $idx + 1 ) , $value, PDO::PARAM_INT);
    }else{
     // echo "[string] Bidning on [ ".($idx + 1)." ] Value [ {$value} ] <br>";
     $query->bindParam( ( $idx + 1 ) , $value, PDO::PARAM_STR);
    }
    $myParams[] = $value;
   }

   $this->prep_query = sprintf( str_replace("?" , "'%s'" , $statement) , ...$myParams);
   $this->query = $this->prep_query;

   try{
    $query->execute();

    $myRes = $query->fetchAll();

    $query->closeCursor();

    $tError = $query->errorInfo();
    if( $tError[0] != "00000" ){
     var_dump( $tError );
     $this->error = array(
      "sqlerror" => $tError,
      "query"    => $this->query
     );
     return false;
    }
    else{
     if($myRes && sizeOf($myRes) > 0){
      return $myRes;
     }
     else{
      return [];
     }
    }
   }catch( Exception $error ){
    // $tError = $query->errorInfo();
    var_dump( $error->getMessage() );
    $this->error = array(
     // "sqlerror"  => $tError,
     "exception" => $error->getMessage(),
     "query"     => $this->query
    );
    return false;
   }
  }

  public function execute($statement , $statementValues){
   $this->query  = $statement;
   $this->params = $statementValues;

   $myParams = array();
   $query    = $this->pdo->prepare($this->query);

   foreach($this->params as $idx => &$value){
    if( is_int($value) ) {
     // echo "[is_int] Bidning on [ ".($idx + 1)." ] Value [ {$value} ] <br>";
     $query->bindParam( ( $idx + 1 ) , $value, PDO::PARAM_INT);
    }else if( is_float($value) ){
     // echo "[is_float] Bidning on [ ".($idx + 1)." ] Value [ {$value} ] <br>";
     $query->bindParam( ( $idx + 1 ) , $value, PDO::PARAM_INT);
    }else{
     // echo "[string] Bidning on [ ".($idx + 1)." ] Value [ {$value} ] <br>";
     $query->bindParam( ( $idx + 1 ) , $value, PDO::PARAM_STR);
    }
    $myParams[] = $value;
   }

   $this->prep_query = sprintf( str_replace("?" , "'%s'" , $statement) , ...$myParams);
   $this->query = $this->prep_query;

   $myRes = $query->execute();
   
   if( !$myRes ){
    $tError = $query->errorInfo();
    if( $tError[0] != "00000" ){
     $this->error = $tError;
    }

     return false;
   }
   else{
    return [ "success" ];
    $query->closeCursor(); 
   }
  }

}

?>