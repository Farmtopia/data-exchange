<?php

class amWS{
 
  public $endPoint;
  public $endPointPath;
  public $auth;
  public $method = "GET";
  public $parameters;

  public function __construct( ){
   $this->main    = new main();
   $this->timeout = 60;
   $this->debug   = true;
   $this->headers = [
       'Content-Type: application/json',
       'Accept: application/json'
   ];
  }

  public function setEndPoint( $endpoint ){
   $this->endPoint = $endpoint;
  }

  public function setTimeout( $timeout ){
   $this->timeout = (int)$timeout;
  }

  public function setEndPointPath( $endPointPath ){
   $this->endPointPath = $endPointPath;
  }

  public function setAuth( $auth ){
   $this->auth = $auth;
  }

  public function setMethod( $method ){
   $this->method = $method;
  }

  public function setParameters( $parameters ){
   if( $this->debug ) $this->main->amLog( json_encode( $parameters ) , "log" );
   if( $this->method == "GET" ){
    $this->parameters = "?";
    $myParametersArray = [];
    foreach( $parameters as $key => $value ){
      $myParametersArray[] = $key . "=" . $value;
    }

    $this->parameters .= implode( "&" , $myParametersArray );
    if( $this->debug ) $this->main->amLog( $this->parameters , "log" );
   }
   else{
    // $this->parameters = json_encode( $parameters );
    $this->parameters = $parameters;
   }
  }

  public function setHeaders( $headers ){
    $this->headers = $headers;
  }

  public function cUrl( ){
   date_default_timezone_set("Europe/Athens");
   $ch  = curl_init();
   if( $this->endPointPath == "" ){
     $url = $this->endPoint;
   }
   else{
     $url = $this->endPoint."/".$this->endPointPath;
   }

    if( $this->method == "PUT" ){
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    }
    else if( $this->method == "DELETE" ){
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    }

   curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
   curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, true );
   curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT , $this->timeout );
   curl_setopt( $ch, CURLOPT_TIMEOUT  , $this->timeout ); 
   curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
   curl_setopt( $ch, CURLOPT_USERPWD, $this->auth );

   if( $this->method == "GET" ){
    curl_setopt( $ch, CURLOPT_URL, $url . $this->parameters );
    curl_setopt( $ch, CURLOPT_HTTPHEADER, $this->headers );
   }
   else{
    curl_setopt( $ch, CURLOPT_URL, $url );
    curl_setopt( $ch, CURLOPT_POST, 1 );
    curl_setopt( $ch, CURLOPT_POSTFIELDS  , $this->parameters );
    // $this->headers[] = 'Content-Length: ' . strlen( $this->parameters );
    curl_setopt( $ch, CURLOPT_HTTPHEADER, $this->headers );
/*
    curl_setopt( $ch, CURLOPT_HTTPHEADER, array( 
        // 'Content-Type: application/json' , 
        'Content-Type: application/x-www-form-urlencoded',
        'Content-Length: ' . strlen( $this->parameters ) , 
        'Accept: application/json' 
      )
    );
*/
   }

   if( $this->debug ){
     $myCurlForLogging = "";
     $myCurlForLogging .= "CURL -X" . $this->method . " \"" . $url . "\" ";
     $myCurlForLogging .= " -H \"" . implode( "\" -H \"" , $this->headers ) . "\"";
     $this->main->amLog( $myCurlForLogging );
   }

   $output = curl_exec( $ch );
   $info   = curl_getinfo( $ch );
   
   // print( "<pre>" );print_r( $info );print( "</pre>" );
   // VAR_DUMP( $output );
   
   try{
    $myRes = json_decode( $output );
    // print( "<pre>" );print_r( $myRes );print( "</pre>" );
    if( isset( $myRes->detail ) && is_array( $myRes->detail ) ){
     if( isset( $myRes->detail[ 0 ] ) ){
      if( isset( $myRes->detail[ 0 ]->msg ) ){
       $myResponse = [
        "data"        => [] , 
        "error"       => $myRes , 
        "status_code" => $info[ "http_code" ]
       ];
       curl_close( $ch );
       return $myResponse;
      }
     }
     else{
      if( isset( $myRes->detail->auth ) ){
       return $myRes->detail;
      }
     }
    }
   }catch( Exception $e ){
    // Do nothing
   }

   if( $output === false )
   {
       $myResponse = [ 
        "data"  => [] , 
        "error" => curl_error( $ch ) ,
        "status_code" => $info[ "http_code" ]
       ];
   }
   else{
       $myResponse = [
        "data"  => json_decode( $output ) ,
        "error" => false , 
        "status_code" => $info[ "http_code" ]
       ];
   }
   
   curl_close( $ch );
   return $myResponse;

  }

}

?>