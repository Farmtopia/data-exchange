<?php

class amCache{

  public function __construct(){
   // GLOBAL VARIABLES
      date_default_timezone_set('Europe/Athens');
      $this->hasCache   = true;
      $this->Settings   = new main();
      $this->cache_file = "dataset.json";
      $this->data       = $this->read_file();
  }

  public function read_file(){
    if( !$this->hasCache ) return false;
    $file     = fopen( "{$this->Settings->CACHE_PATH}{$this->cache_file}" , "r" );
    $fileSize = filesize( "{$this->Settings->CACHE_PATH}{$this->cache_file}" );
    $fileData = "{}";
    if( $fileSize > 0 ){
      $fileData = fread( $file , filesize( "{$this->Settings->CACHE_PATH}{$this->cache_file}" ) );
    }
    fclose( $file );
    return json_decode( $fileData );
  }

  public function strip_space($myString){
   return preg_replace('/\s+/', '', $myString);
  }

  public function write( $fileString , $fileType = "w" ){
    if( !$this->hasCache ) return false;
    $file = fopen( "{$this->Settings->CACHE_PATH}{$this->cache_file}" , "{$fileType}" );
    fwrite( $file , $fileString );
    fclose( $file );
  }

  public function write_to( $path , $data ){
    if( !$this->hasCache ) return false;
    if( !isset( $this->data->{"tplid_".$path} ) ){
      $this->data->{"tplid_".$path} = [];
    }
    // print( "<pre>" ); print_r( $data ); print( "</pre>" );
    $this->data->{"tplid_".$path} = $data;

    // print( "<pre>" ); print_r( $this->data ); print( "</pre>" );

    $this->write( json_encode( $this->data ) );
  }

  public function add_aggregation( $template_id , $agg_type , $data ){
    if( !$this->hasCache ) return false;
    if( !isset( $this->data->{"tplid_".$template_id} ) ){
      $this->data->{"tplid_".$template_id} = (object)[  $agg_type => [] ];
    }
    // print( "<pre>" ); print_r( $data ); print( "</pre>" );
    $this->data->{"tplid_".$template_id}->{$agg_type} = $data;

    // print( "<pre>" ); print_r( $this->data ); print( "</pre>" );

    $this->write( json_encode( $this->data ) );
  }

  public function read_from( $path ){
    if( !$this->hasCache ) return false;
    if( !isset( $this->data->{$path} ) ){
      return $this->data->{$path};
    }
    else{
      return false;
    }
  }

  public function exists( $key , $agg_type = false ){
    if( !$this->hasCache ) return false;
    // echo $key . " | " . $agg_type;
    if( isset( $this->data->{$key} ) ){
      // echo "has template";
      if( $agg_type ){
        // echo "has agg_trype";
        if( isset( $this->data->{$key}->{$agg_type} ) ){
          return true;
        }
        else{
          return false;
        }
      }
      else{
        return true;
      }
    }
    else{
        return false;
    }
  }

  // public function getParcels( $cdate ){
    // $myParcels = [];
    // foreach( $this->data->{$cdate}->data->parcel as $parcel ){
      // if( !in_array( $parcel->parcel_info->parcel_id , $myParcels ) ){
        // $myParcels[] = $parcel->parcel_info->parcel_id;
      // }
    // }
    // return $myParcels;
  // }

  // public function hasParcel( $cdate , $parcel_id ){
    // $myParcels = $this->getParcels( $cdate );

    // if( in_array( $parcel_id , $myParcels ) )
      // return true;
    // else
      // return false;
  // }

}
?>