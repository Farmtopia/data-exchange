<?php

class main{

  public function __construct(){
   // GLOBAL VARIABLES
      date_default_timezone_set('Europe/Athens');
      $this->ENVIRONMENT           = 'prod';
      $this->SITE_NAME             = "dashboard";
      $this->SITE_TITLE            = "Famrtopia Dashboard";
      $this->APP_ID                = 1;
      $this->SERVER_PROTOCOL       = 'http';
      $this->HTDOC_NAME            = 'dashboard';
      $this->MY_LOCAL_IP           = getenv( "localip" );
      $this->RELATIVE_PATH         = "/var/www/html/";
      $this->LOG_PATH              = $this->RELATIVE_PATH.$this->HTDOC_NAME."/z_logs/";
      $this->CACHE_PATH            = $this->RELATIVE_PATH.$this->HTDOC_NAME."/z_cache/";
      $this->SERVER_PATH           = $this->HTDOC_NAME."/";
      $this->SERVER_URL            = $this->SERVER_PROTOCOL."://".$this->MY_LOCAL_IP.":8880/".$this->HTDOC_NAME."/";
      $this->TEMPLATE              = "default";
      $this->MAXIMUM_IDLE_TIME     = 5000; //Seconds
      $this->LOCALE                = 'el';
      $this->DEBUG                 = false;
      $this->FAVICON               = '../template/site_items/favicon.png';
      $this->GoogleMapsApiKey      = "AIzaSyBMt8-9oN0lBMMBRATJB33ByEumI25qOe8";
      $this->hashKey               = "Q!bApWg5%24zNG&*u7@fJUxZ";

   // WebService EndPoint
      $this->ws_main = [
       "endpoint" => "http://" . $this->MY_LOCAL_IP . ":8888",
       "auth"     => "admin:123456"
      ];

      $this->ws_directory = [
       "endpoint" => "http://" . $this->MY_LOCAL_IP . ":8885"
      ];

   // PWA
      $this->META_DESCRIPTION = "Developer: Thanasis Manos.";
      $this->MANIFEST_CONTENT = "FarmTopia Dashboard";

   // ICONS
      $this->ICON_DEFAULT     = "./template/default/site_items/iphone_180.png";
      $this->ICON_152x152     = "./template/default/site_items/iphone_152.png";
      $this->ICON_167x167     = "./template/default/site_items/iphone_167.png";
      $this->ICON_180x180     = "./template/default/site_items/iphone_180.png";

   // Versions
      $this->previousCacheVersion  = 2;
      $this->cacheVersion          = 3;
      $this->myVersionDetails      = array(
        "../",
        "../js_libs/",
        "../js_libs/dom_components/",
        "../localization/",
        "../template/default/css/",
      );

   // DB PROPERTIES 
      $this->DB_TYPE     = 'pgsql';
      $this->DB_NAME     = 'postgres';
      $this->DB_SCHEMA   = 'farmtopia_dashboard';
      $this->DB_HOST     = $this->MY_LOCAL_IP;   // This is the Container Name of Postgresql , you can also use the IP
      $this->DB_PORT     = 5556;                 // This is the port on Postgresql. The exposed one goes here.
      $this->DB_USERNAME = 'postgres';
      $this->DB_PASSWORD = 'mysecretpassword';
      $this->AUTH_SCHEMA = 'farmtopia_dashboard';

   // Image and File Properties
      $this->IMAGES_SAVE_FOLDER_PATH   = $this->RELATIVE_PATH.$this->HTDOC_NAME.'/media/photos';
      $this->IMAGES_ALLOWED_EXTENSIONS = array('png','jpg','bmp','jpeg','JPG','PNG','BMP','JPEG','TIFF','tiff','TIF','tif');
      $this->FILES_ALLOWED_EXTENSIONS  = array('zip');
      $this->IMAGES_COMPRESSION        = false;
      $this->FILES_MAXIMUM_SIZE        = 500000;
      $this->IMAGES_MAXIMUM_SIZE       = 500000;
  }

  public function lang($myString){
    if($myString != ''){
     $myTranslation = @constant('LANG_'.strtolower($myString)) ? constant('LANG_'.strtolower($myString)) : "_{$myString}";
    }else
     $myTranslation = "";

    return $myTranslation;
  }

  public function strip_space($myString){
   return preg_replace('/\s+/', '', $myString);
  }

  public function amLog( $fileString , $type='' , $fileType = 'a' ){
    $fileName = 'log.log';
    $Prefix   = "[ ".date("Y-m-d H:i:s")." ] -- ";
    $file     = fopen("{$this->LOG_PATH}{$fileName}","{$fileType}");
    fwrite($file,$Prefix.$fileString."\r\n");
    fclose($file);
  }

  public function getVar($myVar , $type = 'str' , $default="5617e75240481b4d3a083353ce1bbd8d"){
     $rawRequest = file_get_contents("php://input");
     if($rawRequest !== "")
      $myRequest  = json_decode($rawRequest);
     else{
      $myRequest = (object) $_REQUEST;
     }
     $validatedString = isset($myRequest->$myVar) ? $myRequest->$myVar : false;
     if($default !== "5617e75240481b4d3a083353ce1bbd8d" && !$validatedString)
      return $default;

     if($validatedString){
      if(gettype($validatedString) == "string"){
       if($type == 'int')
        $validatedString = (int)$validatedString;
       else if($type == 'float')
        $validatedString = (float)$validatedString;
       else if($type == 'str'){
         $myString = strip_tags($validatedString);
         $myString = html_entity_decode($myString);
         $myString = preg_replace('/ +/', ' ', $myString);
         $myString = trim($myString);
         $validatedString = $myString;
       }
      }

      return $validatedString;
     }
     else
      return false;
  }

  public function getRequest(){
     $rawRequest = file_get_contents("php://input");
     if($rawRequest !== "")
      $myRequest  = json_decode($rawRequest);
     else{
      $myRequest = (object) $_REQUEST;
     }

     return $myRequest;
  }

  public function getUserIp(){
      if(!empty($_SERVER['HTTP_CLIENT_IP'])){
          $ip = $_SERVER['HTTP_CLIENT_IP'];
      }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
          $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
      }else{
          $ip = $_SERVER['REMOTE_ADDR'];
      }
      return $ip;
  }

  public function sendEmail($options){
    require_once('C:/Apache24/htdocs/PHPMailer/PHPMailerAutoload.php');
    date_default_timezone_set('Etc/UTC');

    $from       = isset($options["from"])    ? $options["from"]    : [ "noreply@neuropublic.gr" , "FarmTopia Dashboard" ];
    $AltBody    = isset($options["alt"])     ? $options["alt"]     : "";
    $Recipients = isset($options["to"])      ? $options["to"]      : array("th_manos@neuropublic.gr");
    $Body       = isset($options["body"])    ? $options["body"]    : "";
    $Subject    = isset($options["subject"]) ? $options["subject"] : "";

    $mail = new PHPMailer;
    $mail->isSMTP();
    $mail->Encoding = 'base64';
    $mail->SMTPDebug = 0;
    $mail->CharSet = "UTF-8";
    $mail->Debugoutput = 'html';
    $mail->Host = "email.neuropublic.com";
    $mail->Port = 25;
    $mail->SMTPAuth = false;
    $mail->Username = "station_monitor@neuropublic.com";
    $mail->Password = "#poi098@";
    $mail->setFrom( $from[ 0 ] , $from[ 1 ] );

    foreach($Recipients as $key => $Recipient){
      $mail->addAddress($Recipient);
    }   

    $mail->Subject = $Subject;
    $mail->Body = $Body;
    $mail->AltBody = $AltBody;
     
    if (!$mail->send()) 
     echo $mail->ErrorInfo;
    // else 
     // echo "Email succesfully sent<br>";
  }

}
?>