<?php
// namespace CFG\local;

class amUser{

  public $username;
  public $error;
  public $userLocationAccess;
  protected $un;
  protected $pw;
  protected $SSID;

  public function __construct( $session ){
   $this->main               = new main();
   $this->db                 = new getDBO();
   $this->SSID               = isset( $session[ "isLogged" ] ) ? $session[ "isLogged" ] : null;
   $this->id                 = $this->getUserBySessionId();
  }

  public function getUserBySessionId(){
   if( $this->SSID )
    $myRes = $this->db->loadAssocList(
     "SELECT id from ".$this->main->AUTH_SCHEMA.".user where session_id = ?" , 
     [ $this->SSID ]
    );
   else
    return false;

   if( $this->db->error ){
    return false;
   }
   else{
    if( sizeOf( $myRes ) <= 0 )
     return false;
    else
     return $myRes[ 0 ][ "id" ];
   }

  }

  public function getUsrObj(){
    $myRes = $this->db->loadAssocList( 
      "SELECT * from {$this->main->DB_SCHEMA}.user where session_id = ?" , 
      [ $this->SSID ]
    );

    if( $this->db->error ){
     return [ "error" => $this->db->error ];
    }
    else{
      return $myRes[ 0 ];
    }
  }

  public function isLogged(){
   if( $this->SSID ) 
    return true;
   else
    return false;
  }

  public function logout(){
   $myUserID = $this->getUserBySessionId();
   if( !$myUserID ) {
    $this->error = "Action: logout , getUserBySessionId Failed [logout]";
    return;
   }

   $updateUserRec = $this->db->execute(
    'update '.$this->main->AUTH_SCHEMA.'.user set session_id = null where id = ?' , 
    [ $myUserID ]
   );

   if( !$updateUserRec ) {
    $this->error = "Action: logout , query Failed [logout]";
    return;
   }
   else{
    $_SESSION["isLogged"] = null;
    return;
   }

  }

  public function login( $password , $username ){
   if( !$password || !$username ) {
    $this->error = "Missing Credentials";
    return;
   }

   $this->un   = $password;
   $this->pw   = $username;
   $hasAccess  = $this->hasAccess( $username , $password );
   if( !$hasAccess ) {
    $this->error = "Invalid User";
    return;
   }

   return $hasAccess;
  }

  private function getEncyrptedPassword( $username , $password ){

    $myUser = $this->db->loadAssocList(
      "SELECT id , password , salt from ".$this->main->AUTH_SCHEMA.".user where email = ?" , 
      [ $username ]
    );

    if( $this->db->error ){
      $this->main->amLog( json_encode( $this->db->error ) , "access" );
      return "";
    }
    else{
      if( $myUser[ 0 ][ "salt" ] == "" ){
        $this->main->amLog( "Old password" , "access" );
        $this->main->amLog( "pw : " . $password . " | Encrypted: " . md5( $password ), "access" );
        return md5( $password );
      }
      else{
        $myEncodedUserPassword = hash( "sha256" , $myUser[ 0 ][ "salt" ] . $password . $this->main->hashKey );
        if( $myEncodedUserPassword !== $myUser[ 0 ][ "password" ] ){
          $this->main->amLog( "Incompatible passwords" , "access" );
          $this->main->amLog( "salt : " . $myUser[ 0 ][ "salt" ] , "access" );
          $this->main->amLog( "pw : "   . $password , "access" );
          $this->main->amLog( "hash : " . $this->main->hashKey , "access" );
          $this->main->amLog( "User Encrypted : " . $myEncodedUserPassword , "access" );
          $this->main->amLog( "Server Encrypted : " . $myUser[ 0 ][ "password" ] , "access" );

          return "";
        }
        else{
          return $myEncodedUserPassword;
        }
      }
    }
  }

  public function hasAccess( $username , $password ){

    $encryptedPassword = $this->getEncyrptedPassword( $username , $password );

 // Check if the user Exists in the Database and has ACTIVE Status.
    $res = $this->db->loadAssocList(
      "SELECT id , status from ".$this->main->AUTH_SCHEMA.".user where email = ? and password = ? " , 
      [ $username , $encryptedPassword ]
    );

    if( $this->db->error ){
     $this->main->amLog( json_encode( $this->db->error ) , "access" );
     return false;
    }

    if( sizeOf( $res ) <= 0 ){
     $this->main->amLog( "User not Activated : {$username}" , "access" );
     $this->main->amLog( "Query : " . $this->db->query , "access" );
     return false;
    }

    if( $res[ 0 ][ "id" ] == '' || $res[ 0 ][ "status" ] < 0 ) {
     $this->main->amLog( "User not Activated : {$username}" , "access" );
     return false;
    }

    $this->main->amLog( json_encode( $res ) , "access");

    $uid        = $res[ 0 ][ "id" ];
    $aDate      = new Datetime();
    $myNewToken = md5( session_id() . "|" . $aDate->format( "m-d-y H:i:s" ) );
    $cookieEnd  = ( time() + 60*60*24*30 );

    $_SESSION[ "isLogged" ] = $myNewToken;

 // Check if the User has Access to the Specific Application 
    // $query = "SELECT T3.app_name , T3.app_id " . 
             // "from overseer.user_profiles T1 " . 
             // "inner join overseer.user T2 on T2.access_id = T1.access_id " . 
             // "inner join overseer.applications T3 on T3.app_id = T2.app_id " . 
             // "where T1.id = ? and T3.app_id = ? ";
    // $res = $this->db->loadAssocList( $query , [ $uid , $this->main->APP_ID ] );
    // $this->main->amLog( $this->db->query , "access" );

    // if( $this->db->error ){
     // $this->main->amLog( json_encode( $this->db->error ) , "access" );
     // return false;
    // }

    // if( $res[ 0 ][ "app_id" ] == '' ) return false;

  // Update Last Login and Session ID in our Database for the Specific User
     $this->db->execute( 
      "UPDATE ".$this->main->AUTH_SCHEMA.".user set last_login = now() , session_id = ? where id = ? " , 
      [ $myNewToken , $uid ] 
     );
     $this->main->amLog( $this->db->query , "access" );

     if( $this->db->error ){
      $this->main->amLog( json_encode( $this->db->error ) , "access" ); 
      return false;
     }
     else{
      return $myNewToken;
     }

  }

  public function isAdmin(){
    $myRes = $this->db->loadAssocList( 
      "SELECT is_admin from {$this->main->DB_SCHEMA}.user where id = ? " , 
      [ $this->id ] 
    );

    if( $this->db->error ){
     print_r( $this->db->error );
     return false;
    }
    else{
     if( sizeOf( $myRes ) > 0 ){
      if( $myRes[ 0 ][ "is_admin" ] == 1 )
       return true;
      else
       return false;
     }
     else{
       return false;
     }
    }

  }

  public function getLocale(){
    $myRes = $this->db->loadAssocList( 
      "SELECT locale from ".$this->main->DB_SCHEMA.".user where id = ? " , 
      [ $this->id ]
    );
    
    // $this->main->amLog( $this->db->query );

    if( $this->db->error ){
     return false;
    }
    else{
     return $myRes[ 0 ][ "locale" ];
    }
  }

  public function setLocale( $lang ){
    $this->db->execute( 
      "UPDATE ".$this->main->DB_SCHEMA.".user set locale = ? where id = ? " , 
      [ $lang , $this->id ] 
    );

    if( $this->db->error ){
     return array( "error" => $this->db->error );
    }
    else{
     return true;
    }
  }

  public function getProfileId(){
    $myRes = $this->db->loadAssocList( 
      "SELECT profile_id from ".$this->main->DB_SCHEMA.".user where id = ? " , 
      [ $this->id ]
    );

    if( $this->db->error ){
     return false;
    }
    else{
     return $myRes[ 0 ][ "profile_id" ];
    }
  }

  public function getUIAccessId(){
    $myRes = $this->db->loadAssocList( 
      "SELECT ui_access from ".$this->main->DB_SCHEMA.".user where id = ? " , 
      [ $this->id ]
    );

    if( $this->db->error ){
     return false;
    }
    else{
     return $myRes[ 0 ][ "ui_access" ];
    }
  }

  public function getSetting(){
    $myRes = $this->db->loadAssocList( 
      "SELECT setting from ".$this->main->DB_SCHEMA.".user_setting where id = ? " , 
      [ $this->id ]
    );

    if( $this->db->error ){
     return false;
    }
    else{
     if( sizeOf( $myRes ) > 0 )
       return json_decode( $myRes[ 0 ][ "setting" ] );
     else
       return json_encode([]);
    }
  }

  public function getLocationAccess(){
   if( $this->isAdmin() ){
    $myQuery = "select " . 
               "T1.parcel_id , T1.location_id " . 
               "from {$this->main->DB_SCHEMA}.parcels T1 ";
    $myRes = $this->db->loadAssocList( $myQuery , [ ] );
   }
   else{
    $myQuery = "select " . 
               "T1.parcel_id , T1.location_id " . 
               "from {$this->main->DB_SCHEMA}.parcels T1 " . 
               "inner join {$this->main->DB_SCHEMA}.user_to_parcel_mapping T2 on T2.parcel_id = T1.parcel_id " . 
               "inner join {$this->main->DB_SCHEMA}.user T3 on T3.profile_id = T2.profile_id " . 
               "where T3.id = ? ";
    $myRes = $this->db->loadAssocList( $myQuery , [ $this->id ] );
   }

   if( $this->db->error ){
    $this->main->amLog( json_encode( $this->db->error ) );
    return [];
   }
   else{
    $myResponse = [];
    foreach( $myRes as $aRow => $aValue ){
     $myResponse[ $aValue[ "parcel_id" ] ] = $aValue[ "location_id" ];
    }
    $this->userLocationAccess = $myResponse;
    return $myResponse;
   }
  }

  public function hasAccessToLocationId( $location_id ){
   $hasAccess = false;
   if( !$this->userLocationAccess )
    $this->getLocationAccess();

   foreach( $this->userLocationAccess as $parcelIdentifier => $locationId ){
    if( (int)$locationId === (int)$location_id ) $hasAccess = true;
   }

   return $hasAccess;
  }

  public function hasAccessToParcelId( $parcelId ){
   $hasAccess = false;
   if( !$this->userLocationAccess )
    $this->getLocationAccess();
   
   // print( "<pre>" ); print_r( $this->userLocationAccess ); print( "</pre>" );
   // echo "Parcel I D: " . $parcelId . "<BR>";

   foreach( $this->userLocationAccess as $parcelIdentifier => $locationId ){
    if( (int)$parcelIdentifier === (int)$parcelId ) $hasAccess = true;
   }

   return $hasAccess;
  }

  public function getDirectoryJWT(){
    $myRes = $this->db->loadAssocList( 
      "SELECT * from {$this->main->DB_SCHEMA}.api_connection where id = 1" , 
      []
    );

    if( $this->db->error ){
     return [ "error" => $this->db->error ];
    }
    else{
      return $myRes[ 0 ];
    }
  }
}

?>