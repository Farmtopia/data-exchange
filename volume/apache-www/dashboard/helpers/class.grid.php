<?php

class amGrid{

  function __construct($options){
   $this->error             = "";
   $this->db                = $options["db"];
   $this->filter            = $options["filter"];
   $this->previousfilter    = "";
   $this->orderby           = "";
   $this->processtype       = "client";  // client , server
   $this->hasWhereClause    = false;
   $this->columnSearch      = array();
   $this->searchableColumns = array();
   $this->headers           = array();
   $this->request           = $_REQUEST;
   $this->rangeClause       = "";
   $this->columns           = isset($_REQUEST["columns"]) ? $_REQUEST["columns"] : "";
   $this->Response          = array( "data" => array() );
   $this->query             = "";
  }

  public function setHeaders($headers){
   $this->headers    = array();
   $this->headerlist = array();
   foreach($headers as $header => $headerDetails){
    $this->headers[] = array(
       "locale"    => $headerDetails["locale"],
       "type"      => $headerDetails["type"],
       "column"    => $header
    );
    $this->headerlist[] = $header;
   }
  }

  public function setQuery($query){
   $this->dataQuery = $query;
   $this->query     = $query;
  }

  public function setOrderBy($orderBy=false , $orderDir="desc"){
   if(!$orderBy){
    $headerToBeOrdered = $this->headers[ $this->filter["order"][0]["column"] ]["column"];
    $orderBy           = $headerToBeOrdered;
   }

   // The very first draw has draw = 0. So we need to order by our settings.
   // If draw > 0 then that means that the user is requesting an order by.
   if( $this->filter["draw"] > 0 )
    $this->orderby   = " order by ".$orderBy." ".$this->filter["order"][0]["dir"];
   else
    if($orderBy)
     $this->orderby = " order by ".$orderBy." ".$orderDir;
    else
     $this->orderby = "";
  }

  public function setSearchableColumns($mySearchClumns){
   $this->searchableColumns = $mySearchClumns;
  }

  public function getDataClient(){
   $aRes = $this->db->loadAssocList( $this->dataQuery , []);
   if($this->db->error){
    echo json_encode( [ "error" => $this->error ] );
   }
   else{
    foreach($aRes as $row => $rowData){

     $this->Response["data"][$row] = array();
     foreach($rowData as $value){
      $this->Response["data"][$row][] = $value;
     }
    }

    return $this->Response;
   }
  }

  public function getHeaderFromIndex($index){
   // Headers are created in the same order as the columns of the Grid 
   // in order to have consistency between the indexing of the Grid Columns 
   // and the backend columns
   $myResponse = false;
   foreach($this->headers as $idx => $headerDetails){
    if($idx === $index){
     $myResponse = array( "header" => $headerDetails["column"] , "locale" => $headerDetails["locale"]);
    }
   }
   
   // echo $myResponse;
   return $myResponse;
  }
   
  public function getData(){

   if($this->error){
    echo json_encode(["error" => $this->error]);
   }
   else{

 // Set our ColumnSearch Array for later use
 // When a user is using the "Global Search" function , it will be performed on ALL fields defined through the Config as searchable.
 // So i create an array with all the select QueryStrings for each of the searchable fields in the Config.
    $myQueryValues = array();
    foreach($this->request["columns"] as $idx => $details){
     if($details["search"]["value"] !== ''){
      $this->columnSearch[] = $this->getHeaderFromIndex($idx)["header"] . " like ? ";
      $myQueryValues[] = "%%%".$details["search"]["value"]."%%%";
     }
    }

 // Set our RangeSearch for Later use (Get data for a Period of Time).
 // Make SURE that you only set it when a full FROM-TO has been provided.
 // $aVal["column"] is the Name of the Column that the "BETWEEN - AND" will be applied
 // $aVal["range"] is an Array passed from DataTables that has on first index the From and second the To
    foreach($this->request as $property => $aVal){
     if($property === 'period'){
      if(isset($aVal["column"]) && isset($aVal["range"])){
       if(
        isset($aVal["range"][0]) && ($aVal["range"][0] !== "") 
        &&
        isset($aVal["range"][1]) && ($aVal["range"][1] !== "")
       ){
        $this->rangeClause = $aVal["column"] . " between '".$aVal["range"][0]."' and '".$aVal["range"][1]."'";
       }
      }
     }
    }

 // In order to make queries a lot less complex in code. Avoiding having some with a where clause and some without is crucial.
 // So i add a "where 1=1" in all the queries that do not have a where clause so that i deal with all of them the same way.
    if(!$this->hasWhereClause){
     $this->dataQuery .= " where 1 = 1 ";
    }

 // If we have a Global SearchFilter then proceed in building our query accordingly
    if($this->filter["searchVal"]["value"] != ""){

  // A Global SearchFilter will search in every single Searchable Field 
  // So we need to prepare our Query to have all searchable fields in it.
     $mySearchArray = array();
     $this->dataQuery .= " and ( ";
     foreach($this->searchableColumns as $column){
      $mySearchArray[] = " {$column}::text like ? ";
      $myQueryValues[] = "%%%".$this->filter["searchVal"]["value"]."%%%";
     }
     $this->dataQuery .= implode(" OR " , $mySearchArray)." ) ";

  // Adding our Range Clause if there is one.
     if($this->rangeClause != ""){
      $this->dataQuery .= " and " . $this->rangeClause . " ";
     }

  // If apart from the Global Search the user has also Column Searches active we add them too.
     if(sizeOf($this->columnSearch) > 0){
      $this->dataQuery .= " and " . implode(" and " , $this->columnSearch) . " ";
     }

  // We add our Limits
     // $this->dataQuery .= $this->orderby . " limit ".(int)$this->filter["offset_start"]." , ".(int)$this->filter["offset_length"].";";
     $this->dataQuery .= $this->orderby . " limit 10 offset ".(int)$this->filter["offset_start"].";";

  // We execute our Prepared Query with the Parameters
     $aRes = $this->db->loadAssocList($this->dataQuery , $myQueryValues);
    }
    else{

  // Adding our Range Clause if there is one.
     if($this->rangeClause != ""){
      $this->dataQuery .= " and " . $this->rangeClause . " ";
     }

  // If apart from the Global Search the user has also Column Searches active we add them too.
     if( sizeOf( $this->columnSearch ) > 0 ){
      $this->dataQuery .= " and " . implode(" and " , $this->columnSearch) . " ";
     }

  // We add our Limits
     $this->dataQuery .= $this->orderby . " limit 10 offset ".(int)$this->filter["offset_start"].";";

  // We execute our Prepared Query with the Parameters
     $aRes = $this->db->loadAssocList( $this->dataQuery , $myQueryValues );
    }

    if($this->db->error){
     echo json_encode(["error" => $this->db->error]);
    }
    else{
     foreach($aRes as $row => $rowData){
      $this->Response["data"][$row] = array();
      foreach($rowData as $value){
       $this->Response["data"][$row][] = $value; 
      }
     }

     $this->Response["recordsTotal"]    = isset($this->recordsTotal)    ? $this->recordsTotal    : 0;
     $this->Response["recordsFiltered"] = isset($this->recordsFiltered) ? $this->recordsFiltered : 0;

     return $this->Response;
    }
   }

  }

}
?>