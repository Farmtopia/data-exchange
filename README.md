# Farmtopia's agri-data sharing service

## Generic Description
<p align="center">The “Farmtopia Data Exchange Service” provides the core mechanisms for facilitating the sharing of agricultural data among stakeholders. The “Farmtopia Data Exchange Service” provides a “Data Sources Directory service” which should be accessible as a cloud based service where data providers can register data sources and data consumers can search and select for use the data sources.</p>
<p align="center"> Current implementation supports the registration of data sources that provide datasets through REST API web services. For a data provision REST API to be able to use the “Directory service”, it should comply with a set of key requirements: a) to provide documentation using an OpenAPI Specification, b) to provide an OAuth based authorisation/authentication mechanism [RFC6749] , and c) optionally to provide datasets that are modelled using RDFS semantics. More information about the Farmtopia Semantic Model are available here:  https://gitlab.com/Farmtopia/farmtopia-semantic-model).</p>
<p align="center"> The objective of this service is to provide a practical and reusable data sharing mechanism that can easily be installed and operated for the needs of small scale sharing of datasets. For example, an ICT provider may reuse “Farmtopia Data Exchange Service” as the core mechanism to facilitate data sharing among the members of a farmers’ association or a “producers organisation”. We consider that small-scale, (or regional based) data exchange can be considered as a form of “agricultural knowledge sharing”, something that promotes collaboration and empowers small farmers.</p>



<p align="center">
  This is a container for Data Sharing. It includes a : 
  - Provider 
  - The Directory Listing 
  - The Authentication server
</p>

<p align="center">
  You will need to have Docker installed on your system
  <br>
  Windows : <a href="https://docs.docker.com/desktop/install/windows-install/"><strong>Installation Guide</strong></a>
  <br>
  Ubuntu : <a href="https://docs.docker.com/engine/install/ubuntu/"><strong>Installation Guide</strong></a>
  <br>
  Linux : <a href="https://docs.docker.com/desktop/install/mac-install/"><strong>Installation Guide</strong></a>
</p>

<p align="center">
  The following Technolgies / APIs / Libraries are utilised isnide the Containers:
  <br>
  Python 3 : <a href="https://docs.python.org/3/"><strong>Explore Python 3.6+ docs »</strong></a>
  <br>
  FastAPI : <a href="https://fastapi.tiangolo.com/"><strong>FastAPI »</strong></a>
  <br>
  PHP : <a href="https://www.php.net/"><strong>PHP »</strong></a>
  <br>
  PostgreSQL : <a href="https://www.postgresql.org/"><strong>PostgreSQL »</strong></a>
</p>

## Installation

- Create a Folder in your System
- Clone / Download the Git Code into the folder
- Update the property "localip" in "docker-compose.yml" to match your deployed environment local ip. Set the same exact localip for all containers.
- Do not change the Ports of the containers , this is a DEV container , and some properties of the configuration need to stay as is.
- Execute the following command 
```console
$ docker compose build
$ docker compose up
```
- Done


## Initialize the Containers
There is a manual way to do it and that is navigating to the swagger of Directory and Provider and executing the following in the same order that they appear here: 
- "/administration/create_db_tables"
- "/administration/create_input_models"

There is also a script that will take of that , you can read on how to execute the scripts in the "Integration Testing" a bit below.


## Container Code Paths 

There is a "volume" folder inside the repository. 
Inside this folder there are 2 subfolders :
- fastapi-app : This is for the Provider and Directory Server APIs.
- python-scripts : Scripts for unit / integration testing


## Integration Testing

When all containers are up and running you need to :
```console
$ docker ps
```

Find the Container that has is running the Directory and 
```console
$ docker exec -it CONTAINER_ID bash
```

Then navigate to the Scripts Folder of the container 
```console
$ cd /home/python-scripts
```

Execute the Startup script to initialize the database system : 
```console
$ python startup.py
```

Execute the Main script to execute an integration test : 
```console
$ python main.py
```

Check the integration test for any errors. 
If you reached step 32/32 without receiving any ERROR (Errors will stop the inttegration test) you are fine.
You can execute the Integration as many times as you like , keep in mind that everytime it will clear everything in the Database and start fresh.
If you want to clear the Database after completing the Integration test you can manually do it by executing the following cUrl on your instance : 

NOTE : The Token for the following request is Hard Coded and can be found inside "./volume/fastapi-app/directory/source" at line 88 with proeprty name "serverSecretKey".

```console
curl -X "GET" "https://farmtopia_directory.neuropublic.gr/administration/clear_everything?token=TOKEN_FROM_ABOVE_NOTE"
```


## Farmtopia Data exchange service API specification
The API specification of the service is available here:
https://farmtopia_directory.neuropublic.gr/documentation