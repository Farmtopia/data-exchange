#!/bin/bash
set -o errexit
set -o pipefail
set -o nounset

echo "Creating API Database"
PGPASSWORD=mysecretpassword PGUSER=postgres psql --dbname=postgres <<-'EOSQL'
   CREATE SCHEMA farmtopia_directory;
   CREATE SCHEMA farmtopia_provider;
   CREATE SCHEMA farmtopia_provider_2;
   CREATE SCHEMA farmtopia_dashboard;

   DROP TABLE IF EXISTS farmtopia_directory.user_access;

   CREATE TABLE farmtopia_directory.user_access (
    user_id serial NOT NULL,
    created_on timestamp NOT NULL default now(),
    email varchar(150) NOT NULL, 
    password varchar(150) NOT NULL,
    profile_id int4 NOT NULL DEFAULT 1::numeric,
    is_admin int4 NOT NULL DEFAULT (- 1::numeric),
    locale varchar(2) NOT NULL DEFAULT 'el'::character varying,
    last_login timestamp NULL,
    session_id varchar(150) NULL, 
    ui_access int4 NULL DEFAULT 1,
    status int4 NULL,
    reset_token varchar(150) NULL,
    salt varchar(150) NULL,
    apikey varchar(150) NULL
   );
   CREATE UNIQUE INDEX user_access_id_idx ON farmtopia_directory.user_access USING btree (user_id);
   CREATE UNIQUE INDEX user_access_email_idx ON farmtopia_directory.user_access (email);

   DROP TABLE IF EXISTS farmtopia_dashboard.user;

   CREATE TABLE farmtopia_dashboard.user (
    id serial NOT NULL,
    created_on timestamp NOT NULL default now(),
    email varchar(150) NOT NULL, 
    password varchar(150) NOT NULL,
    profile_id int4 NOT NULL DEFAULT 1::numeric,
    is_admin int4 NOT NULL DEFAULT (- 1::numeric),
    locale varchar(2) NOT NULL DEFAULT 'el'::character varying,
    last_login timestamp NULL,
    session_id varchar(150) NULL, 
    ui_access int4 NULL DEFAULT 1,
    status int4 NULL,
    reset_token varchar(150) NULL,
    salt varchar(150) NULL,
    apikey varchar(150) NULL
   );
   CREATE UNIQUE INDEX user_access_id_idx ON farmtopia_dashboard.user USING btree (id);
   CREATE UNIQUE INDEX user_access_email_idx ON farmtopia_dashboard.user (email);

   INSERT INTO farmtopia_dashboard.user (email,password,profile_id,is_admin,locale,ui_access) VALUES ('admin','e10adc3949ba59abbe56e057f20f883e',1,1,'el',1);

   DROP TABLE if exists farmtopia_dashboard."api_connection";

   create table farmtopia_dashboard."api_connection" (
    id serial4 not null,
    user_id int4 not null,
    created_on timestamp not null default now(),
    api_username varchar(150) null,
    api_password varchar(150) null,
    api_url varchar(150) null,
    api_name varchar(150) null,
    apikey varchar(150) null,
    constraint user_id_api_connection foreign key (user_id) references farmtopia_dashboard."user"(id)
   );
   CREATE UNIQUE INDEX api_connection_idx ON farmtopia_dashboard.api_connection USING btree (id);
   CREATE INDEX api_connection_user_id_idx ON farmtopia_dashboard.api_connection USING btree (id);
EOSQL